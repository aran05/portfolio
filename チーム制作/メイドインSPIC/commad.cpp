#include "iextreme.h"
#include "commad.h"
#include "..\sound.h"


game_commad::game_commad()
{
}


game_commad::~game_commad()
{
	SOUNDMANAGER->~Sound();
}

void game_commad::Init()
{
	commad_image = new iex2DObj("DATA/button.png");
	Gradius[0] = new iex2DObj("DATA/Gradius.png");
	Gradius[1] = new iex2DObj("DATA/option.png");
	Gradius[2] = new iex2DObj("DATA/shield.png");
	
	SOUNDMANAGER->Se_Initialize();

	x = 128;
	y = 120;
	gx = 128;
    gy= 400;
	angle = 0;
	clear = false;
	state = 0;
	sizex =sizey= 0;
	timer = 0;
	flag_size = false;
	clear_count = 0;
}

void game_commad::Update()
{
	angle += 0.1f;
	gx += 2.0f;

	timer++;
	if(timer>30)
	{
		flag_size = !flag_size;
		timer = 0;
	}
	if(flag_size)
	{
		sizex = 0;
		sizey = 0;
	}
	else
	{
		sizex = 6;
		sizey = 6;
	}

	switch (state)
	{
	case 0:
		if(KEY_Get(KEY_UP)==3)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Push, FALSE);
			state++;
		}
		break;
	case 1:
		if (KEY_Get(KEY_UP) == 3)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Push, FALSE);
			state++;
		}
		break;
	case 2:
		if (KEY_Get(KEY_DOWN) == 3)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Push, FALSE);
			state++;
		}
		break;
	case 3:
		if (KEY_Get(KEY_DOWN) == 3)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Push, FALSE);
			state++;
		}
		break;
	case 4:
		if (KEY_Get(KEY_LEFT) == 3)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Push, FALSE);
			state++;
		}
		break;
	case 5:
		if (KEY_Get(KEY_RIGHT) == 3)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Push, FALSE);
			state++;
		}
		break;
	case 6:
		if (KEY_Get(KEY_LEFT) == 3)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Push, FALSE);
			state++;
		}
		break;
	case 7:
		if (KEY_Get(KEY_RIGHT) == 3)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Push, FALSE);
			state++;
		}
		break;
	case 8:
		if (KEY_Get(KEY_A) == 3)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Push, FALSE);
			state++;
		}
		break;
	case 9:
		if (KEY_Get(KEY_B) == 3)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Push, FALSE);
			state++;
		}
		break;
	case 10:
		if (clear_count == 0)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Wario_1, FALSE);
			clear_count = 1;
		}
		clear = true;
		break;
	default:
		break;
	}
}

void game_commad::Render()
{
	Gradius[0]->Render(gx, gy, 128, 128, 0, 0, 256, 256);
	
	if(clear==true)
	{
		Gradius[1]->Render(gx - 32, gy + 32, 32 + sizex, 32 + sizey, 0, 0, 256, 256);
		Gradius[1]->Render(gx - 64, gy + 64, 32 + sizex, 32 + sizey, 0, 0, 256, 256);
		Gradius[2]->ROT_Render(gx + 128, gy + 32, 32 , 32 , 0, 0, 256, 256, 0, ARGB(255, 255, 255, 255), angle);
		Gradius[2]->ROT_Render(gx + 128, gy + 64, 32 , 32 , 0, 0, 256, 256, 0, ARGB(255, 255, 255, 255), angle);
	}

	
	switch (state)
	{
	case 0:
		//��
		commad_image->Render(x,y,100,100,100,0,100,100);
		commad_image->Render(x +(128), y, 100, 100, 100, 0, 100, 100);
		commad_image->Render(x +(128*2), y, 100, 100, 300, 0, 100, 100);
		commad_image->Render(x +(128*3), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x + (128 * 4), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x + (128 * 5), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x + (128 * 6), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x + (128 * 7), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x + (128*3), y+(128), 100, 100, 100, 120, 100, 100);
		commad_image->Render(x +(128*4), y + (128), 100, 100, 0, 120, 100, 100);
		break;
	case 1:
		//��
		commad_image->Render(x, y, 100, 100, 100, 0, 100, 100);
		commad_image->Render(x + (128), y, 100, 100, 300, 0, 100, 100);
		commad_image->Render(x + (128 * 2), y, 100, 100, 300, 0, 100, 100);
		commad_image->Render(x + (128 * 3), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x + (128 * 4), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x + (128 * 5), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x + (128 * 6), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x +(128*7), y, 100, 100, 100, 120, 100, 100);
		commad_image->Render(x +(128*3), y + 128, 100, 0, 0, 120, 100, 100);
		break;
	case 2:
		//��
		commad_image->Render(x, y, 100, 100, 300, 0, 100, 100);
		commad_image->Render(x + (128), y, 100, 100, 300, 0, 100, 100);
		commad_image->Render(x + (128 * 2), y, 100, 100, 200, 0, 100, 100);
		commad_image->Render(x + (128 * 3), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x + (128 * 4), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x + (128 * 5), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x + (128 * 6), y, 100, 100, 100, 120, 100, 100);
		commad_image->Render(x +(128 * 7), y, 100, 100, 0, 120, 100, 100);
		break;
	case 3:
		//��
		//commad_image->Render(x, y, 100, 100, 300, 0, 100, 100);
		commad_image->Render(x, y, 100, 100, 300, 0, 100, 100);
		commad_image->Render(x + (128), y, 100, 100, 400, 0, 100, 100);
		commad_image->Render(x + (128 * 2), y, 100, 100, 200, 0, 100, 100);
		commad_image->Render(x + (128 * 3), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x + (128 * 4), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x + (128 * 5), y, 100, 100, 100, 120, 100, 100);
		commad_image->Render(x +(128 * 6), y, 100, 100, 0, 120, 100, 100);
		break;
	case 4:
		//��
		//commad_image->Render(x, y, 100, 100, 300, 0, 100, 100);
		commad_image->Render(x, y, 100, 100, 400, 0, 100, 100);
		commad_image->Render(x + 128, y, 100, 100, 200, 0, 100, 100);
		commad_image->Render(x + (128 * 2), y, 100, 100, 400, 0, 100, 100);
		commad_image->Render(x + (128 * 3), y, 100, 100, 0, 0, 100, 100);
		commad_image->Render(x + (128 * 4), y, 100, 100, 100, 120, 100, 100);
		commad_image->Render(x +(128 * 5), y, 100, 100, 0, 120, 100, 100);
		break;
	case 5:
		//��
		//commad_image->Render(x + (128), y, 100, 100, 200, 0, 100, 100);
		commad_image->Render(x , y, 100, 100, 200, 0, 100, 100);
		commad_image->Render(x + 128, y, 100, 100, 400, 0, 100, 100);
		commad_image->Render(x + (128 * 2), y, 100, 100, 200, 0, 100, 100);
		commad_image->Render(x + (128 * 3), y, 100, 100, 100, 120, 100, 100);
		commad_image->Render(x+(128 * 4), y, 100, 100, 0, 120, 100, 100);
		break;
	case 6:
		//��
		//commad_image->Render(x + (128 * 2), y, 100, 100, 200, 0, 100, 100);
		commad_image->Render(x , y, 100, 100, 400, 0, 100, 100);
		commad_image->Render(x + 128, y, 100, 100, 200,  0, 100, 100);
		commad_image->Render(x + (128 * 2), y, 100, 100, 300, 120, 100, 100);
		commad_image->Render(x+(128 * 3), y, 100, 100,   0, 120, 100, 100);
		break;
	case 7:
		//��
		//commad_image->Render(x + (128 * 3), y, 100, 100, 400, 0, 100, 100);
		commad_image->Render(x, y, 100, 100, 200, 0, 100, 100);
		commad_image->Render(x + (128), y, 100, 100, 300, 120, 100, 100);
		commad_image->Render(x+(128 * 2), y, 100, 100,200, 120, 100, 100);
		break;
	case 8:
		commad_image->Render(x, y, 100, 100, 300, 120, 100, 100);
		commad_image->Render(x + 128, y, 100, 100, 200, 120, 100, 100);
		break;
	case 9:
		//commad_image->Render(x, y, 100, 100, 0, 120, 100, 100);
		commad_image->Render(x, y, 100, 100, 200, 120, 100, 100);
		break;
	case 10:
		break;
	default:
		break;
	}
}
