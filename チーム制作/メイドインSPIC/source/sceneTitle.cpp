#include	"iextreme.h"
#include	"system\Scene.h"
#include	"sceneTitle.h"
#include	"system\Framework.h"
#include	"sceneMain.h"
#include    "../sound.h"
#include "..\mode.h"



bool sceneTitle::Initialize()
{
	iexLight::SetFog(800, 1000, 0);
	view = new iexView();
	title[0] = new iex2DObj("DATA\\haikei01.png");
	title[1] = new iex2DObj("DATA\\haikei01_L.png");
	title[2] = new iex2DObj("DATA\\haikei01_R.png");
	title[3] = new iex2DObj("DATA\\haikei02_L.png");
	title[4] = new iex2DObj("DATA\\haikei02_R.png");

	img = new iex2DObj("DATA\\taitoru.png");
	push_image = new iex2DObj("DATA\\push01.png");
	push_image2 = new iex2DObj("DATA\\moji2.png");
	k[0] = new iex2DObj("DATA\\k.png");
	k[1] = new iex2DObj("DATA\\k1.png");
	kakumori = new iex2DObj("DATA\\kakumori01.png");
	Effect = new iex2DObj("DATA\\bomb2.png");
	Effect_flg = false;
	kakumori_pos = Vector2(-300.0f, 150.0f);
	kakumori_scale = Vector2(0.0f, 0.0f);
	angle = 0.0f;
	kakumori_flg = false;
	kakumori_state = 5;
	push_flg2 = false;
	kakumori_timer = 25;

	timer2 = 0;
	flg = false;
	color = 0xFFFFFFFF;
	scroll_x = 0;
	scroll_x2 = 0;
	elapsedTime = 0;
	alpha = t = 0.0f;
	timer = 120;
	push_flg = false;
	push_flg2 = false;
	mode = 0;
	state = 0;
	SOUNDMANAGER->Sound_Title();
	return true;
}

sceneTitle::~sceneTitle()
{
	for (int i = 0; i < 5; i++)
	{
		if (title)
		{
			delete title[i];
			title[i] = NULL;
		}
	}


	if (img)
	{
		delete img;
		img = NULL;
	}
	if (title_effect)
	{
		delete title_effect;
		title_effect = NULL;
	}

	if (title_effect2)
	{
		delete title_effect2;
		title_effect2 = NULL;
	}
	if (push_image)
	{
		delete push_image;
		push_image = NULL;
	}
	if (push_image2)
	{
		delete push_image;
		push_image = NULL;
	}
	for (int i = 0; i < 2; i++)
	{
		if (k)
		{
			delete k[i];
			k[i] = NULL;
		}
	}

	if (kakumori)
	{
		delete kakumori;
		kakumori = NULL;
	}

	if (Effect)
	{
		delete Effect;
		Effect = NULL;
	}



	SOUNDMANAGER->Sound_Title_Release();
}

void sceneTitle::Update()
{
	switch (state)
	{
	case 0:
		if (KEY_Get(KEY_B) == 3&& push_flg2==false)
		{
			mode = 0;
			kakumori_state = 0;
			push_flg2 = true;
			//MainFrame->ChangeScene(new sceneMain());
			return;
		}
		if (KEY_Get(KEY_RIGHT) == 3 && push_flg2 == false)
		{
			state++;
			break;
		}
		break;
	case 1:
		if (KEY_Get(KEY_B) == 3 && push_flg2 == false)
		{
			mode = 1;
			kakumori_state = 0;
			push_flg2 = true;
			//MainFrame->ChangeScene(new sceneMain());
			return;
		}
		if (KEY_Get(KEY_LEFT) == 3 && push_flg2 == false)
		{
			state--;
			break;
		}
		break;
	}

	switch (kakumori_state)
	{
	case 0:
		kakumori_pos.x += 5;
		angle += 0.20f;
		if (kakumori_pos.x >= 560)
		{
			kakumori_pos.x = 560;
			angle = 0.0f;
			kakumori_timer--;
			Effect_flg = true;
			if (kakumori_timer <= 0)
			{
				Effect_flg = false;
				kakumori_state = 1;
			}
		}
		break;
	case 1:
		kakumori_pos.x -= 5;
		angle -= 0.20f;
		if (kakumori_pos.x <= 90)
		{
			kakumori_pos.x = 90;
			angle = 0.0f;
			kakumori_state = 2;
		}
		break;
	case 2:
		MainFrame->ChangeScene(new sceneMain());
		break;

	}

}

void sceneTitle::Render()
{
	view->Activate();
	view->Clear();

	title[0]->Render(0, 0, 1280, 720, 0, 0, 2048, 1024);
	//title_effect->Render(0 + scroll_x, 0, 1280, 720, 0, 0, 1, 1024);
	//title_effect2->Render(-1280 + scroll_x2, 0, 1280, 720, 0, 0, 1024, 1024);


	//push_image2->Render((1280 / 2) - 160, 548, 1280, 720, 0, 0, 2048, 1024);
	img->Render((iexSystem::ScreenWidth / 2) - (400), 16, 1280, 720, 0, 0, 2048, 1024, RS_COPY, ARGB((int)200, 255, 255, 255));
	if (Effect_flg == true)
	{
		Effect->Render(kakumori_pos.x + 100, kakumori_pos.y, 128, 128, 0, 0, 256, 256);
	}
	kakumori->ROT_Render(kakumori_pos.x, kakumori_pos.y, 256 + kakumori_scale.x, 256 + kakumori_scale.y, 0, 0, 128, 128, 0, ARGB(255, 255, 255, 255), angle);

	switch (state)
	{
	case 0:
		push_image2->Render((1280 / 2) - 450, 400, 1024, 128, 0, 0, 1024, 128);
		title[2]->Render(0, 0, 1280, 720, 0, 0, 2048, 1024);
		title[3]->Render(0, 0, 1280, 720, 0, 0, 2048, 1024);
		push_image->Render((1280 / 2) - 450, 548, 1280, 720, 0, 0, 2048, 1024);
		k[0]->Render((1280 / 2) - 100, 500, 256, 256, 0, 0, 512, 512);
		break;
	case 1:
		push_image2->Render((1280 / 2) + 150, 400, 1024, 128, 0, 128, 1024, 128);
		title[1]->Render(0, 0, 1280, 720, 0, 0, 2048, 1024);
		title[4]->Render(0, 0, 1280, 720, 0, 0, 2048, 1024);
		push_image->Render((1280 / 2) + 140, 548, 1280, 720, 0, 0, 2048, 1024);
		k[1]->Render((1280 / 2) - 150, 500, 256, 256, 0, 0, 512, 512);
		break;
	}








	//img->Render((iexSystem::ScreenWidth / 2) - (540), 580, 540 * 2, 32 * 2, 0, 31 * 3, 540, 32, RS_ADD, ARGB((int)alpha, 255, 255, 255));
}