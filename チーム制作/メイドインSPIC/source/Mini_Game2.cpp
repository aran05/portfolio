#include "iextreme.h"
#include "system/system.h"
#include "Mini_Game2.h"
#include "collision.h"
#include "..\sound.h"
#include "..\Particle.h"
#include "..\mode.h"
bool a;
void Mini_Game2::Mini_Game2_Init()
{
	player = new iex2DObj("DATA\\sentouki1.png");
	player_pos = Vector2(640, 550);
	player_move = Vector2(0.0f, 0.0f);
	shot = new iex2DObj("DATA\\tama1.png");
	enemy = new iex2DObj("DATA\\sentouki1.png");
	back = new iex2DObj("DATA\\back_haikei2.png");
	SOUNDMANAGER->Se_Initialize();

	particle = new Particle();
	particle->Initialize();
	enemy_dead_count = 3;
	shot_count = 3;

	clear_flg = false;
	over_flg = false;

	x = 0;
	x2 = 0;
	y = 0;
	y2 = 720;
	a = false;

	timer = 120;
	clear_count = 0;

	for (int i = 0; i < MAX; i++)
	{
		S[i].pos = Vector2(220.0f - (60 * i), 570.0f);
		S[i].move = Vector2(0.0f, 0.0f);
		S[i].flg = false;

		S[i].img_flg = false;
		S[i].count = 0;
	}

	for (int j = 0; j < MAX; j++)
	{
		E[j].pos = Vector2(0.0f, 0.0f);
		E[j].move = Vector2(0.0f, 0.0f);
		E[j].flg = false;
		E[j].move_flg = false;
		Enemy_Set(Vector2(640, 32), 3.0f);
	}

}

Mini_Game2::~Mini_Game2()
{
	if (player)
	{
		delete  player;
		player = NULL;
	}

	if (shot)
	{
		delete shot;
		shot = NULL;
	}

	if (enemy)
	{
		delete enemy;
		enemy = NULL;
	}

	if (particle)
	{
		delete particle;
		particle = NULL;
	}

	SOUNDMANAGER->~Sound();
}

void Mini_Game2::Mini_Game2_Update()
{
	player_pos += player_move;

	particle->Update();
	for (int i = 0; i < MAX; i++)
	{
		if (S[i].flg == false)continue;
		S[i].pos.y -= S[i].move.y;

	}

	for (int j = 0; j < MAX; j++)
	{
		if (E[j].flg == false)continue;

		if (E[j].pos.x >= 1050)
		{
			E[j].move_flg = true;
		}
		if (E[j].pos.x <= 128)
		{
			E[j].move_flg = false;
		}
		if (E[j].move_flg == false)
		{
			E[j].pos.x += E[j].move.x;
		}
		else
		{
			E[j].pos.x -= E[j].move.x;
		}
	}

	if (mode == 0)
	{
		if (KEY_Get(KEY_RIGHT) == 1)
		{
			player_move.x = 4.5f;
		}
		else if (KEY_Get(KEY_RIGHT) == 2)
		{
			player_move = Vector2(0.0f, 0.0f);
		}
		if (KEY_Get(KEY_LEFT) == 1)
		{
			player_move.x = -4.5f;
		}
		else if (KEY_Get(KEY_LEFT) == 2)
		{
			player_move = Vector2(0.0f, 0.0f);
		}

		if (KEY_Get(KEY_B) == 3)
		{
			Shot_Set(player_pos, 10.0f);


		}

	}

	if (mode == 1)
	{
		if (KEY_Get(KEY_RIGHT) == 1)
		{
			player_move.x = 7.5f;
		}
		else if (KEY_Get(KEY_RIGHT) == 2)
		{
			player_move = Vector2(0.0f, 0.0f);
		}
		if (KEY_Get(KEY_LEFT) == 1)
		{
			player_move.x = -7.5f;
		}
		else if (KEY_Get(KEY_LEFT) == 2)
		{
			player_move = Vector2(0.0f, 0.0f);
		}

		if (KEY_Get(KEY_B) == 3)
		{
			Shot_Set(player_pos, 20.0f);


		}

	}

	if(y<=-720)
	{
		y = 720;
	}
	else
	{
		y--;
	}
	if(y2<=-720)
	{
		y2 = 720;
	}
	else
	{
		y2--;
	}


	if (player_pos.x <= 128)
	{
		player_pos.x = 128;
	}
	if (player_pos.x >= 1050)
	{
		player_pos.x = 1050;
	}


	if (a == true && shot_count == 0|| a == true && shot_count >= 2 && shot_count<6)
	{
		SOUNDMANAGER->Se_Play(SeNum::SE_Shot, FALSE);
		a = false;
	}

	for (int i = 0; i < MAX; i++)
	{
		if (S[i].count > 3)
		{
			S[i].img_flg = true;
		}

	}




	for (int i = 0; i < MAX; i++)
	{
		for (int j = 0; j < MAX; j++)
		{
			if (CheckHitRect2(S[i].pos.x, S[i].pos.y, 96, 96, E[j].pos.x, E[j].pos.y, 128, 96))
			{
				if (S[i].flg == false)continue;
				if (E[j].flg == false)continue;
				if (S[i].img_flg == true)continue;
				S[i].flg = false;
				E[j].flg = false;
				S[i].img_flg = true;
				enemy_dead_count -= 1;
				particle->Effect1(E[j].pos);

			}
		}

	}

	if (enemy_dead_count == 0)
	{
		clear_flg = true;
		a = false;
	}

	if (clear_flg == true)
	{
		if (clear_count == 0)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Wario_1, FALSE);
			clear_count = 1;
		}
	}


	if (shot_count == 0 && enemy_dead_count != 0)
	{
		timer--;
		if (timer <= 0)
		{
			over_flg = true;
		}
	}

	//for (int i = 0; i < MAX; i++)
	//{
	//	if (S[i].img_flg == false)
	//	{
	//		SOUNDMANAGER->Se_Play(SeNum::SE_Shot, FALSE);

	//	}
	//}


}

void Mini_Game2::Mini_Game2_Render()
{
	back->Render(x2, y2, 1280, 720, 0, 0, 2024, 1024);
	back->Render(x, y, 1280, 720, 0, 0, 2024, 1024);
	
	//back->Render(0,0,)
	for (int i = 0; i < MAX; i++)
	{
		if (S[i].img_flg == false)
		{
			shot->Render(S[i].pos.x, S[i].pos.y, 96, 96, 0, 0, 128, 128,0,ARGB(255,239,117,188));
		}

	}

	player->Render(player_pos.x, player_pos.y, 96, 96, 0, 64, 64, 64);



	for (int j = 0; j < MAX; j++)
	{
		if (E[j].flg == true)
		{
			enemy->Render(E[j].pos.x, E[j].pos.y, 128, 96, 0, 0, 128, 64);
		}
	}
	particle->Render();

}

void Mini_Game2::Shot_Set(Vector2 p, float speed)
{
	for (int i = 0; i < MAX; i++)
	{
		if (S[i].flg == true)continue;
		S[i].pos = p;
		S[i].move.y = speed;
		S[i].flg = true;
		S[i].count += 1;
		shot_count -= 1;
		break;
	}
	a = true;
}

void Mini_Game2::Enemy_Set(Vector2 p, float speed)
{
	if (mode == 0)
	{
		for (int j = 0; j < MAX; j++)
		{
			if (E[j].flg == true)continue;
			E[j].pos.x = p.x;
			E[j].pos.y = p.y + (96 * j);
			E[0].move.x = speed;
			E[1].move.x = speed * 2;
			E[2].move.x = speed * 3;
			E[j].flg = true;
			break;
		}

	}

	if (mode == 1)
	{
		for (int j = 0; j < MAX; j++)
		{
			if (E[j].flg == true)continue;
			E[j].pos.x = p.x;
			E[j].pos.y = p.y + (96 * j);
			E[0].move.x = speed * 2;
			E[1].move.x = speed * 3;
			E[2].move.x = speed * 4;
			E[j].flg = true;
			break;
		}

	}
}