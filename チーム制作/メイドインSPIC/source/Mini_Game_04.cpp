#include "iextreme.h"
#include "system\Framework.h"
#include "system\Scene.h"
#include "system\System.h"
#include "Mini_Game_04.h"
#include "..\Game.h"

Mini_Game_04 enemy[ENEMY_MAX];
Mini_Game_04 enemy2[ENEMY_MAX];

int time;
bool Mini_Game_04::Init()
{
	iexLight::SetFog(800, 1000, 0);

	BG = new iex2DObj("DATA\\sky.png");
	obj = new iex2DObj("DATA\\playerRot.png");
	propeller = new iex2DObj("DATA\\propeller.png");

	for (int i = 0; i < ENEMY_MAX; i++)
	{
		enemy[i].EnemyXY = D3DXVECTOR2(enemy[i].EnemyX, enemy[i].EnemyY);
		enemy[i].Enemy = new iex2DObj("DATA\\enemy.png");
		vec = PlayerXY - enemy[i].EnemyXY;
		len = (vec.x + vec.x) + (vec.y + vec.y);
		FLG[i] = 0;
		enemy[i].clear();
		time = 0;
		enemy[i].EnemyY = rand() % iexSystem::ScreenHeight;
		enemy[i].EnemyX = rand() % iexSystem::ScreenWidth;


		enemy2[i].EnemyXY = D3DXVECTOR2(enemy2[i].EnemyX, enemy2[i].EnemyY);
		enemy2[i].Enemy = new iex2DObj("DATA\\enemy.png");
		vec = PlayerXY - enemy2[i].EnemyXY;
		enemy2[i].clear();
		enemy2[i].EnemyY = rand() % iexSystem::ScreenHeight;
		enemy2[i].EnemyX = rand() % iexSystem::ScreenWidth;
	}

	count = 0;
	Mini3 = new iex2DObj("DATA\\angry.jpg");
	clear_flg = true;
	return true;
}


Mini_Game_04::Mini_Game_04()
{
}


Mini_Game_04::~Mini_Game_04()
{
	if (BG)
	{
		delete BG;
		BG = NULL;
	}

	if (obj)
	{
		delete obj;
		obj = NULL;
	}

	if (propeller)
	{
		delete propeller;
		propeller = NULL;
	}

	for (int i = 0; i < ENEMY_MAX; i++)
	{

		delete enemy[i].Enemy;
		enemy[i].Enemy = NULL;
	}
}

void Mini_Game_04::PlayerAnimation(void)
{
	//	アニメーション更新						
	if (KEY_Get(KEY_A) == 1)
	{
		propellerRotation = 1;
		if (MoveX > 0) playerAnime = 0;
		if (MoveX < 0) playerAnime = 1;
	}
	else
	{
		propellerRotation = 8;
		if (MoveX > 0) playerAnime = 2;
		if (MoveX < 0) playerAnime = 3;

	}

	//プレイヤーコマ送り
	playerFrame++;
	if (playerAnime == 0) {
		if (playerFrame >= 24) playerFrame = 0;
	}
	if (playerAnime == 1) {
		if (playerFrame >= 24) playerFrame = 0;
	}
	if (playerAnime == 2) {
		if (playerFrame >= 24) playerFrame = 0;
	}
	if (playerAnime == 3) {
		if (playerFrame >= 24) playerFrame = 0;

	}

	//プロペラコマ送り
	propellerFrame++;
	if (propellerAnime == 0)
	{
		if (propellerFrame >= 24)propellerFrame = 0;
	}
}


void Mini_Game_04::MovePlayer()
{
	if (KEY_Get(KEY_LEFT) == 1)
	{
		MoveX = -MoveSpeed;
		playerX += MoveX;
		if (playerX <= 100)
		{
			playerX = 100;
		}
	}
	if (KEY_Get(KEY_RIGHT) == 1)
	{
		MoveX = MoveSpeed;
		playerX += MoveX;
		if (playerX >= WSizeMax)
		{
			playerX = WSizeMax;
		}
	}

	if (KEY_Get(KEY_B) == 1) {
		MoveY = UpSpeed;
		playerY -= MoveY;
		if (playerY <= 55)
		{
			playerY = 55;
		}
	}
	else							
	{
		MoveY = FallSpeed;
		playerY += MoveY;
		if (playerY >= HSizeMax)
		{
			playerY = HSizeMax;
		}
	}
}


void Mini_Game_04::EnemyAnimation(void)
{
	//	アニメーション更新						
	if (MoveX < 0) EnemyAnime = 1;
	if (MoveX > 0) EnemyAnime = 0;
	if (MoveY < 0) EnemyAnime = 3;
	if (MoveY > 0) EnemyAnime = 2;

	EnemyFrame++;
	if (EnemyAnime == 0) {
		if (EnemyFrame >= 24) EnemyFrame = 0;
	}
	if (EnemyAnime == 1) {
		if (EnemyFrame >= 24) EnemyFrame = 0;
	}
	if (EnemyAnime == 2) {
		if (EnemyFrame >= 32) EnemyFrame = 0;
	}
	if (EnemyAnime == 3) {
		if (EnemyFrame >= 32) EnemyFrame = 0;
	}
}


bool  Mini_Game_04::CheckHitRect(int ax, int ay, int aw, int ah, int bx, int by, int bw, int bh)
{
	int al = ax - aw / 2;               
	int ar = ax + aw / 2;                
	int au = ay - ah;                    
	int ad = ay;                         


	int bl = bx - bw / 2;                
	int br = bx + bw / 2;                
	int bu = by - bh;                    
	int bd = by;                         


										 
	if (ar < bl) return FALSE;           
	if (br < al) return FALSE;           
	if (bd < au) return FALSE;           
	if (ad < bu) return FALSE;           

	return true;
}

void Mini_Game_04::MoveEnemy()
{
	for (int i = 0; i < ENEMY_MAX; i++)
	{
		switch (enemy[i].state)
		{
		case 0:
			enemy[i].MoveX = ENEMY_SPEED;
			enemy[i].MoveY = 0;
			enemy[i].state++;
			enemy[i].count = 0;
		case 1:
			enemy[i].EnemyX += enemy[i].MoveX;
			enemy[i].EnemyY += enemy[i].MoveY;
			enemy[i].count++;
			if (enemy[i].count == 1) enemy[i].state = 0;
			break;
		}

		switch (enemy2[i].state)
		{
		case 0:
			enemy2[i].MoveX = -ENEMY_SPEED;
			enemy2[i].MoveY = 0;
			enemy2[i].state++;
			enemy2[i].count = 0;
		case 1:
			enemy2[i].EnemyX += enemy2[i].MoveX;
			enemy2[i].EnemyY += enemy2[i].MoveY;
			enemy2[i].count++;
			if (enemy2[i].count == 1) enemy2[i].state = 0;
			break;
		}
	}
}

void Mini_Game_04::Update()
{
	time++;

	//移動
	MovePlayer();
	MoveEnemy();

	//アニメーション
	PlayerAnimation();
	EnemyAnimation();

	for (int i = 0; i < ENEMY_MAX; i++)
	{
		if (time >= 60)
		{
			if (CheckHitRect(playerX, playerY, 30, 58, enemy[i].EnemyX, enemy[i].EnemyY, 32, 32))
			{
				count++;
				FLG[i] = 1;
			}
		}
		if (enemy[i].EnemyX > 1090)
		{
			enemy[i].EnemyY = rand() % 610;
			enemy[i].EnemyX = 110;
		}

		if (time >= 60)
		{
			if (CheckHitRect(playerX, playerY, 30, 58, enemy2[i].EnemyX, enemy2[i].EnemyY, 32, 32))
			{
				FLG[i] = 1;
			}
		}
		if (enemy2[i].EnemyX < 110)
		{
			enemy2[i].EnemyY = rand() % 500;
			enemy2[i].EnemyX = 1090;
		}
	}
}

void Mini_Game_04::Render()
{
	BG->Render(110, 35, 1060, 635, 0, 0, 1024, 512);

	propeller->Render(playerX, playerY - 40, 64, 64, (propellerFrame / propellerRotation) * 64, propellerAnime * 64, 64, 64);
	obj->Render(playerX, playerY, 64, 64, (playerFrame / 8) * 64, playerAnime * 64, 64, 64);
	if (count >= 1)
	{
		clear_flg = false;
		Mini3->Render();
		for (int i = 0; i < ENEMY_MAX; i++)
		{
			enemy[i].render_clear();
		}
	}

	for (int i = 0; i < ENEMY_MAX; i++)
		if (FLG[i] == 0)
		{
			{
				enemy[i].Enemy->Render(enemy[i].EnemyX, enemy[i].EnemyY, 64 * 1.5f, 64 * 1.5f, (EnemyFrame / 8) * 64, 128 + 64, 64, 64);
				//enemy2[i].Enemy->Render(enemy2[i].EnemyX, enemy2[i].EnemyY, 64 * 1.5f, 64 * 1.5f, (EnemyFrame / 8) * 64, 128, 64, 64);
			}
		}
		else if (FLG[i] == 1)
		{
			enemy[i].clear();
			enemy2[i].clear();
		}
}