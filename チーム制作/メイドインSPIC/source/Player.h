#pragma once

#define	GRAVITY	   (0.4f)			//	プレイヤー重力
#define MOVE_SPEED (4.0f)
class c_Player
{
public:
	iex2DObj* Player;
	Vector2 Pos;
	Vector2 Move;
	float Angle;
	bool jump_flag;


	void Initialize();
	void Release();
	void Update();
	void Render();

	void Set_Pos(Vector2 pos) 
	{
		Pos = pos;
	};


	static c_Player *getInstance() {
		static c_Player instance;
		return &instance;
	}
};
#define PLAYERMANAGER (c_Player::getInstance())