#include "iextreme.h"
#include "system/system.h"
#include "Boss.h"
#include "collision.h"
#include "..\sound.h"
#include "..\Particle.h"
#include "..\mode.h"
#include "sceneMain.h"


void Boss::Boss_Game_Init()
{
	player = new iex2DObj("DATA\\sentouki1.png");
	boss = new iex2DObj("DATA\\kakumori3.png");

	player_pos = Vector2(640.0f, 550.0f);
	player_move = Vector2(0.0f, 0.0f);

	shot = new iex2DObj("DATA\\tama1.png");

	boss_pos = Vector2(640.0f, 120.0f);
	boss_move = Vector2(0.0f, 0.0f);
	back = new iex2DObj("DATA\\back_haikei2.png");
	
	yure = new sceneMain();

	x = 0;
	x2 = 0;
	y = 0;
	y2 = 720;

	if (mode == 0)
	{
		boss_hp = 30;
	}

	if (mode == 1)
	{
		boss_hp = 60;
	}
	boss_move_flg = false;
	over_count = 0;
	boss_atk_timer = 120;
	boss_atk_flg = false;
	boss_anime_flg = false;

	over_flg = false;

	clear_flg = false;
	timer = 0;
	se_timer = 180;
	particle = new Particle();
	particle->Initialize();

	SOUNDMANAGER->Se_Initialize();

	for (int i = 0; i < SHOT_MAX; i++)
	{
		S[i].pos = Vector2(0.0f, 0.0f);
		S[i].move = Vector2(0.0f, 0.0f);
		S[i].timer = 0;
		S[i].flg = false;
	}

	for (int i = 0; i < SHOT_MAX; i++)
	{
		BS[i].pos = Vector2(0.0f, 0.0f);
		BS[i].move = Vector2(0.0f, 0.0f);
		BS[i].timer = 0;
		BS[i].flg = false;
	}

}

Boss::~Boss()
{
	if (player)
	{
		delete player;
		player = NULL;
	}

	if (boss)
	{
		delete boss;
		boss = NULL;
	}

	if (shot)
	{
		delete shot;
		shot = NULL;
	}

	if (particle)
	{
		delete particle;
		particle = NULL;
	}


	if (yure)
	{
		delete yure;
		yure = NULL;
	}


	SOUNDMANAGER->~Sound();
}

void Boss::Boss_Game_Update()
{
	yure->ShakeUpdate();

	if (y <= -720)
	{
		y = 719;
	}
	else
	{
		y--;
	}
	if (y2 <= -720)
	{
		y2 = 719;
	}
	else
	{
		y2--;
	}

	boss_atk_timer--;

	if (over_count != 1 && over_count != 2)
	{
		Player_Move();

		Shot_Move();

		Boss_Shot_Move();


		if (boss_atk_timer <= 120 && boss_atk_timer >= 30)
		{
			boss_anime_flg = false;
			Boss_Move();
		}
		else
		{
			boss_anime_flg = true;
		}

		if (mode == 0)
		{
			if (boss_hp <= 30 && boss_hp >= 20)
			{
				Boss_Atk1();
			}

			if (boss_hp <= 19 && boss_hp >= 10)
			{
				Boss_Atk2();
			}

			if (boss_hp <= 9 && boss_hp >= 0)
			{
				Boss_Atk3();
			}

		}

		if (mode == 1)
		{
			if (boss_hp <= 60 && boss_hp >= 40)
			{
				Boss_Atk1();
			}

			if (boss_hp <= 40 && boss_hp >= 20)
			{
				Boss_Atk2();
			}

			if (boss_hp <= 20 && boss_hp >= 0)
			{
				Boss_Atk3();
			}

		}

	}

	if (boss_hp <= 0)
	{
		over_count = 1;
	}

	if(over_count==1)
	{
		timer++;
		yure->Set_Shake(250, 100);
		for (int i = 0; i < 10; i++)
		{
			particle->Effect3(Vector2(128.0f*i, 100.0f));
		}
		if(timer>60*5)
		{
			clear_flg = true;
		}
	}
	else if(over_count==2)
	{
		timer++;
		if (timer>60 * 4)
		{
			over_flg = true;
		}
	}
	//Boss_Atk2();
	//Boss_Atk3();
	//Boss_Atk4();
	//Boss_Atk5();

	particle->Update();

	for (int i = 0; i < SHOT_MAX; i++)
	{
		if (CheckHitRect2(S[i].pos.x, S[i].pos.y, 96, 96, boss_pos.x, boss_pos.y, 96, 96))
		{
			if (S[i].flg == false)continue;
			S[i].flg = false;
			boss_hp -= 1;
			SOUNDMANAGER->Se_Play(SeNum::SE_Explosion, FALSE);
			particle->Effect2(boss_pos);
		}
		
	}

	

	for (int i = 0; i < SHOT_MAX; i++)
	{
		if (CheckHitRect2(BS[i].pos.x, BS[i].pos.y, 64, 64, player_pos.x, player_pos.y, 64, 64))
		{
			if (BS[i].flg == false)continue;
			BS[i].flg = false;
			over_count = 2;

		}

	}

	if (CheckHitRect2(boss_pos.x, boss_pos.y, 64, 64, player_pos.x, player_pos.y, 64, 64))
	{
		over_count = 2;
	}


}

void Boss::Boss_Game_Render()
{
	back->Render(x2, y2+ yure->ShakeY, 1280, 720, 0, 0, 2024, 1024);
	back->Render(x, y + yure->ShakeY, 1280, 720, 0, 0, 2024, 1024);

	particle->Render();

	if (over_count != 1)
	{


		for (int i = 0; i < SHOT_MAX; i++)
		{
			if (S[i].flg == true)
			{
				shot->Render(S[i].pos.x, S[i].pos.y, 96, 96,0, 0, 128, 128, 0, ARGB(255, 239, 117, 188));
			}

		}

		for (int i = 0; i < SHOT_MAX; i++)
		{
			if (BS[i].flg == true)
			{
				shot->Render(BS[i].pos.x, BS[i].pos.y, 96, 96, 0, 0, 128, 128, 0, ARGB(255, 142, 0, 204));
			}

		}
	}

	if(over_count!=2)
	{
		player->Render(player_pos.x, player_pos.y, 96, 96, 0, 64, 64, 64);
	}
	


	if (over_count != 1)
	{
		if (boss_anime_flg == false)
		{
			if (mode == 0)
			{
				if (boss_hp > 10)
				{
					boss->Render(boss_pos.x, boss_pos.y, 160, 160, 0, 0, 256, 256);
				}
				else
				{
					boss->Render(boss_pos.x, boss_pos.y, 160, 160, 0, 0, 256, 256, 0, ARGB(255, 255, 0, 0));
				}

			}

			if (mode == 1)
			{
				if (boss_hp > 20)
				{
					boss->Render(boss_pos.x, boss_pos.y, 160, 160, 0, 0, 256, 256);
				}
				else
				{
					boss->Render(boss_pos.x, boss_pos.y, 160, 160, 0, 0, 256, 256, 0, ARGB(255, 255, 0, 0));
				}

			}

		}
		else
		{

			boss->Render(boss_pos.x, boss_pos.y, 160, 160, 256, 0, 256, 256, 0, ARGB(255, 255, 241, 15));
		}
	}

	//char str[64];
	//wsprintf(str, "%d", boss_hp);
	//IEX_DrawText(str, 640, 360, 200, 20, 0xFFFFFF00);
}

void Boss::Shot_Set(Vector2 p, float speed, int timer)
{
	for (int i = 0; i < SHOT_MAX; i++)
	{
		if (S[i].flg == true)continue;
		S[i].pos = p;
		S[i].move.y = speed;
		S[i].flg = true;
		S[i].timer = timer;
		break;
	}

}

void Boss::Boss_Shot_Set(Vector2 p, float speed, int timer)
{
	for (int i = 0; i < SHOT_MAX; i++)
	{
		if (BS[i].flg == true)continue;
		BS[i].pos = p;
		BS[i].move.y = speed;
		BS[i].flg = true;
		BS[i].timer = timer;
		break;
	}

}

void Boss::Boss_Shot_Set2(Vector2 p, Vector2 m, int timer)
{
	for (int i = 0; i < SHOT_MAX; i++)
	{
		if (BS[i].flg == true)continue;
		BS[i].pos = p;
		BS[i].move = m;
		BS[i].flg = true;
		BS[i].timer = timer;
		break;
	}


}



void Boss::Player_Move()
{

	if(player_pos.x<128)
	{
		player_pos.x = 128;
	}
	else if(player_pos.x>1050)
	{
		player_pos.x = 1050;
	}
	else if(player_pos.y>550)
	{
		player_pos.y = 550;
	}
	else if (player_pos.y<64)
	{
		player_pos.y = 64;
	}

	player_pos += player_move;

	if (KEY_Get(KEY_LEFT) == 1)
	{
		player_move.x = -10.0f;
	}
	else if (KEY_Get(KEY_LEFT) == 2)
	{
		player_move.x = 0.0f;
	}

	if (KEY_Get(KEY_RIGHT) == 1)
	{
		player_move.x = 10.0f;
	}
	else if (KEY_Get(KEY_RIGHT) == 2)
	{
		player_move.x = 0.0f;
	}

	if (KEY_Get(KEY_UP) == 1)
	{
		player_move.y = -10.0f;
	}
	else if (KEY_Get(KEY_UP) == 2)
	{
		player_move.y = 0.0f;
	}

	if (KEY_Get(KEY_DOWN) == 1)
	{
		player_move.y = 10.0f;
	}
	else if (KEY_Get(KEY_DOWN) == 2)
	{
		player_move.y = 0.0f;
	}

	if (KEY_Get(KEY_B) == 3)
	{
		Shot_Set(player_pos, 10.0f, 80);
	}

}

void Boss::Shot_Move()
{
	for (int i = 0; i < SHOT_MAX; i++)
	{
		S[i].pos -= S[i].move;
		S[i].timer--;
		if (S[i].timer <= 0)
		{
			S[i].flg = false;
			S[i].timer = 80;
		}
	}

}

void Boss::Boss_Shot_Move()
{
	for (int i = 0; i < SHOT_MAX; i++)
	{
		BS[i].pos -= BS[i].move;
		BS[i].timer--;
		if (BS[i].timer <= 0)
		{
			BS[i].flg = false;
			BS[i].timer = 80;
		}
	}

}

void Boss::Boss_Move()
{
	boss_pos += boss_move;
	if (boss_pos.x >= 1050.0f)
	{
		boss_move_flg = true;
	}
	else if (boss_pos.x <= 128)
	{
		
		boss_move_flg = false;
	}

	if (boss_move_flg == false)
	{
		boss_move.x = 10.0f;
	}
	else
	{
		boss_move.x = -10.0f;
	}

}

void Boss::Boss_Atk1()
{
	//boss_atk_timer--;
	if (boss_atk_timer <= 0)
	{
		boss_atk_flg = true;
		boss_atk_timer = 120;
	}
	if (boss_atk_flg == true)
	{
		Boss_Shot_Set(boss_pos, -10.0f, 80);
		boss_atk_flg = false;
		
	}
}

void Boss::Boss_Atk2()
{
	//boss_atk_timer--;

	if (boss_atk_timer >= 0)
	{
		//particle->Effect5(boss_pos);
	}

	if (boss_atk_timer <= 0)
	{
		boss_atk_flg = true;
	}

	if (boss_atk_flg == true)
	{
		for (int i = 0; i < 5; i++)
		{
			Boss_Shot_Set(boss_pos + Vector2(64.0f*i, 0.0f), -8.0f, 80);
		}
		boss_atk_flg = false;
		
		boss_atk_timer = 120;

	}

}

void Boss::Boss_Atk3()
{
	//boss_atk_timer--;

	if (boss_atk_timer <= 0)
	{
		boss_atk_flg = true;
	}

	if (boss_atk_flg == true)
	{
		Boss_Shot_Set2(boss_pos, Vector2(2.0f, -3.0f), 130);
		Boss_Shot_Set2(boss_pos, Vector2(5.0f, -5.0f), 130);
		Boss_Shot_Set2(boss_pos, Vector2(0.0f, -5.0f), 130);
		Boss_Shot_Set2(boss_pos, Vector2(-2.0f, -3.0f), 130);
		Boss_Shot_Set2(boss_pos, Vector2(-5.0f, -5.0f), 130);

		boss_atk_flg = false;
		boss_atk_timer = 130;

	}

}

void Boss::Boss_Atk4()
{
	//boss_atk_timer--;

	if (boss_atk_timer <= 0)
	{
		boss_atk_flg = true;
	}

	if (boss_atk_flg == true)
	{
		Boss_Shot_Set2(Vector2(100.0f, 100.0f), Vector2(-10.0f, 0.0f), 120);
		Boss_Shot_Set2(Vector2(100.0f, 200.0f), Vector2(-10.0f, 0.0f), 120);
		Boss_Shot_Set2(Vector2(100.0f, 300.0f), Vector2(-10.0f, 0.0f), 120);
		Boss_Shot_Set2(Vector2(100.0f, 400.0f), Vector2(-10.0f, 0.0f), 120);
		Boss_Shot_Set2(Vector2(100.0f, 500.0f), Vector2(-10.0f, 0.0f), 120);

		boss_atk_flg = false;
		boss_atk_timer = 120;

	}

}

void Boss::Boss_Atk5()
{

	//boss_atk_timer--;

	if (boss_atk_timer <= 0)
	{
		boss_atk_flg = true;
	}

	if (boss_atk_flg == true)
	{
		Boss_Shot_Set2(Vector2(1100.0f, 100.0f), Vector2(10.0f, 0.0f), 120);
		Boss_Shot_Set2(Vector2(1100.0f, 200.0f), Vector2(10.0f, 0.0f), 120);
		Boss_Shot_Set2(Vector2(1100.0f, 300.0f), Vector2(10.0f, 0.0f), 120);
		Boss_Shot_Set2(Vector2(1100.0f, 400.0f), Vector2(10.0f, 0.0f), 120);
		Boss_Shot_Set2(Vector2(1100.0f, 500.0f), Vector2(10.0f, 0.0f), 120);

		boss_atk_flg = false;
		boss_atk_timer = 120;

	}



}