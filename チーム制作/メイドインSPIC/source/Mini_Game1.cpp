#include "iextreme.h"
#include "system/system.h"
#include "Mini_Game1.h"
#include "..\\sound.h"

void Mini_Game1::Mini_Game_Init()
{
	img[0] = new iex2DObj("DATA\\minigame1.png");
	img[1] = new iex2DObj("DATA\\minigame2.png");
	img[2] = new iex2DObj("DATA\\minigame3.png");
	img[3] = new iex2DObj("DATA\\minigame4.png");

	cursol = new iex2DObj("DATA\\huti.png");

	maru = new iex2DObj("DATA\\maru.png");
	batu = new iex2DObj("DATA\\batu.png");

	clear_flg = false;
	over_flg = false;
	confirm_flg = false;
	clear_count = 0;
	over_count = 0;
	x = 256;
	y = 120;

	img_rand = 0;
	img_rand = rand() % 4;

	timer = 60;
	over_timer = 60;

}

Mini_Game1::~Mini_Game1()
{
	for (int i = 0; i < 4; i++)
	{
		if (img[i])
		{
			delete img[i];
		}

	}

	if (cursol)
	{
		delete cursol;
		cursol = NULL;
	}
	if (maru)
	{
		delete maru;
		maru = NULL;
	}
	if (batu)
	{
		delete batu;
		batu = NULL;
	}
}
void Mini_Game1::Mini_Game_Update()
{
	
	if (KEY_Get(KEY_B) == 3 && state == img_rand)
	{
		clear_flg = true;
		confirm_flg = true;

	}
	if (KEY_Get(KEY_B) == 3 && state != img_rand)
	{

		over_flg = true;
		confirm_flg = true;
	}

	if (confirm_flg == false)
	{
		switch (state)
		{
		case 0: //左上

				//右上へ
			if (KEY_Get(KEY_RIGHT) == 3)
			{
				x = 640;
				y = 120;
				state = 1;
			}
			//左下へ
			if (KEY_Get(KEY_DOWN) == 3)
			{
				x = 256;
				y = 365;
				state = 3;
			}
			break;
		case 1: //右上

				//左上へ
			if (KEY_Get(KEY_LEFT) == 3)
			{
				x = 256;
				y = 120;
				state = 0;
			}
			//右下へ
			if (KEY_Get(KEY_DOWN) == 3)
			{
				x = 640;
				y = 365;
				state = 2;
			}

			break;
		case 2: //右下

				//左下へ
			if (KEY_Get(KEY_LEFT) == 3)
			{
				x = 256;
				y = 365;
				state = 3;
			}

			//右上
			if (KEY_Get(KEY_UP) == 3)
			{
				x = 640;
				y = 120;
				state = 1;
			}
			break;
		case 3: //左下

			if (KEY_Get(KEY_UP) == 3)
			{
				x = 256;
				y = 120;
				state = 0;
			}
			if (KEY_Get(KEY_RIGHT) == 3)
			{
				x = 640;
				y = 365;
				state = 2;
			}

			break;
		}

	}

	else
	{
		if (clear_flg == true && clear_count == 0)
		{

			SOUNDMANAGER->Se_Play(SeNum::SE_Correct, FALSE);
			clear_count = 1;



		}
		if (clear_count == 1)
		{
			timer--;
			if (timer == 0)
			{
				SOUNDMANAGER->Se_Play(SeNum::SE_Wario_1, FALSE);
				clear_count = 2;

			}
		}

	}

	if (over_flg == true && over_count == 0)
	{
		SOUNDMANAGER->Se_Play(SeNum::SE_Incorrect, FALSE);
		over_count = 1;
	}

	if (over_count == 1)
	{
		over_timer--;
		if (over_timer == 0)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Wario_3, FALSE);
			over_count = 2;
		}
	}


}

void Mini_Game1::Mini_Game_Render()
{
	if (img_rand == 0)
	{
		img[0]->Render(266, 128, 760, 500, 0, 0, 1024, 1024);
	}
	else if (img_rand == 1)
	{
		img[1]->Render(266, 128, 760, 500, 0, 0, 1024, 1024);
	}
	else if (img_rand == 2)
	{
		img[2]->Render(266, 128, 760, 500, 0, 0, 1024, 1024);
	}
	else if (img_rand == 3)
	{
		img[3]->Render(266, 128, 760, 500, 0, 0, 1024, 1024);
	}

	if (clear_flg == true)
	{
		//maru->Render(350, 120, 500, 500, 0, 0, 1025, 1025);
	}
	if (over_flg == true)
	{
		//batu->Render(360, 120, 500, 500, 0, 0, 1000, 1000);
	}
	cursol->Render(x, y, 400, 360, 0, 0, 300, 300);

}