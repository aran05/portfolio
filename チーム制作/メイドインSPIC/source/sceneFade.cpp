#include	"iextreme.h"
#include	"system\Scene.h"
#include	"system\Framework.h"
#include    "sceneFade.h"

sceneFade::sceneFade(int type, int timer, DWORD color, Scene* new_scene) :
	type(type), timer(timer), max_timer(timer), next_scene(new_scene)
{
	this->color = color & 0x00FFFFFF;
}

bool sceneFade::Initialize()
{
	switch (type)
	{
	case Fade_In:
		this->Stuck_In(next_scene);
		next_scene->Initialize();
		next_scene = NULL;
		break;
	}

	return true;
};

void sceneFade::Update()
{
	stuck->Update();

	timer--;
	if (timer < 0)
	{
		Scene* work;

		switch (type)
		{
		case Fade_In:
			MainFrame->Pop_Scene();
			break;

		case Fade_Out:
			work = next_scene;
			next_scene = NULL;
			MainFrame->ChangeScene(work);
			break;
		}

		return;
	}

	BYTE alpha = 255;
	switch (type)
	{
	case Fade_In:
		alpha = 255 * timer / max_timer;
		break;

	case Fade_Out:
		alpha = 255 * (max_timer - timer) / max_timer;
		break;
	}

	color = (alpha << 24) | (color & 0x00FFFFFF);

}

void sceneFade::Render()
{
	stuck->Render();

	iexPolygon::Rect(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, RS_COPY, color);
}