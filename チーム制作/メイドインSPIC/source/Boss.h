#pragma once

#define SHOT_MAX 30
class Particle;
class sceneMain;

class Boss
{
public:

	iex2DObj* player;
	iex2DObj* boss;
	iex2DObj* shot;
	iex2DObj* back;
	Particle* particle;
	sceneMain* yure;


	Vector2 player_pos;
	Vector2 player_move;

	Vector2 boss_pos;
	Vector2 boss_move;
	int boss_hp;
	bool boss_move_flg;
	int boss_atk_timer;
	bool boss_atk_flg;
	bool boss_anime_flg;
	
	int over_count;
	int se_count;
	int timer;
	int se_timer;
	/*bool hit_flg;*/

	int x, y, x2, y2;
	bool over_flg;
	bool clear_flg;

	~Boss();

	struct SHOT
	{
		Vector2 pos;
		Vector2 move;
		bool flg;
		int timer;
	}S[SHOT_MAX];

	struct BOSS_SHOT
	{
		Vector2 pos;
		Vector2 move;
		bool flg;
		int timer;
	}BS[SHOT_MAX];

	void Shot_Set(Vector2 p, float speed, int timer);


	void Boss_Shot_Set(Vector2 p, float speed, int timer);

	void Boss_Shot_Set2(Vector2 p,Vector2 m, int timer);



	void Player_Move();
	void Shot_Move();
	void Boss_Shot_Move();


	void Boss_Move();
	void Boss_Atk1();
	void Boss_Atk2();
	void Boss_Atk3();
	void Boss_Atk4();
	void Boss_Atk5();

	void Boss_Game_Init();
	void Boss_Game_Update();
	void Boss_Game_Render();

};