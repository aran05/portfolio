#include "iextreme.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include "MAP.h"
#include<time.h>
#include<vector>
#include "Player.h"


////マップ読み込み
void Map::loadmap(int mapnum)
{

	if (mapnum == ST1)
	{
		loadmap2("DATA\\BG\\stage1.csv", lmap, &MapX, &MapY);
		return;
	}
	MapX = 0;
	MapY = 0;
}

void Map::loadmap2(char filename[1024], int mapchip[512][512], int *mapx, int *mapy)
{
	std::ifstream map(filename);

	int xlen = 0;
	int ylen = 0;

	bool lenpassx = FALSE;
	bool lenpassy = FALSE;

	std::string str;
	while (getline(map, str)) {

		xlen = 0;
		std::string token;
		std::istringstream stream(str);

		while (getline(stream, token, ',')) {

			mapchip[ylen][xlen] = stoi(token);

			xlen++;
		}
		if (!lenpassx)
		{
			*mapx = xlen;
			lenpassx = TRUE;
		}
		ylen++;


		if (!lenpassy)
		{
			*mapy = ylen - 1;
			lenpassy = TRUE;
		}
	}
}

void Map::Initialize()
{

	lpMap = new iex2DObj("DATA\\BG\\Mapchip.png");
	Map *map = new Map();
	st = false;
	s_flag = false;
	next = false;
	next = 0;
	MapX = 0;
	loadmap(0);

}

void Map::Render()
{

	int x, y, block;
	int dx = MapX / CHIP_SIZE;
	int mx = MapX%CHIP_SIZE;

	for (y = 0; y < MAP_HEIGHT; y++) {
		for (x = 0; x < MAP_WIDTH + 10; x++) {
			block = lmap[y][x + dx];
			lpMap->Render(x*CHIP_SIZE - mx, y*CHIP_SIZE, CHIP_SIZE, CHIP_SIZE, (block%CHIP_1LINE_NUM)*CHIP_SIZE, (block / CHIP_1LINE_NUM)*CHIP_SIZE, CHIP_SIZE, CHIP_SIZE);
		}
	}
}