#include "collision.h"
#include "MAP.h"
#include "system\Scene.h"
#include "sceneMain.h"
#include "system\Framework.h"


//プレイヤー足元とブロックの左上
bool CheckHitRect(int x, int y, int aw, int ah, int tx, int ty, int bw, int bh) 
{
	int al = x - aw / 2;			//物体Aの左端(Left)
	int ar = x + aw / 2;			//物体Aの右端(Right)
	int au = y - ah;				//物体Aの上端(Up)
	int ad = y;					//物体Aの下端(Down)

	int bl = tx;					//物体Bの左端(Left)
	int br = tx + bw;			//物体Bの右端(Right)
	int bu = ty;					//物体Bの上端(Up)
	int bd = ty + bh;			//物体Bの下端(Down)

										//当たっていない条件
	if (ar < bl) return FALSE;		//Aの右端<Bの左端
	if (br < al) return FALSE;		//Bの右端<Aの左端
	if (bd < au) return FALSE;		//Bの下端<Aの左端
	if (ad < bu) return FALSE;		//Aの下端<Bの左端

									//当たっている
	return TRUE;
}

bool CheckHitRect2(int x, int y, int aw, int ah, int tx, int ty, int bw, int bh)
{
	int al = x;					//物体Aの左端(Left)
	int ar = x + aw;			//物体Aの右端(Right)
	int au = y;					//物体Aの上端(Up)
	int ad = y + ah;			//物体Aの下端(Down)

	int bl = tx;				//物体Bの左端(Left)
	int br = tx + bw;			//物体Bの右端(Right)
	int bu = ty;				//物体Bの上端(Up)
	int bd = ty + bh;			//物体Bの下端(Down)

								//当たっていない条件
	if (ar < bl) return FALSE;		//Aの右端<Bの左端
	if (br < al) return FALSE;		//Bの右端<Aの左端
	if (bd < au) return FALSE;		//Bの下端<Aの左端
	if (ad < bu) return FALSE;		//Aの下端<Bの左端

									//当たっている
	return TRUE;
}

/***************************************************************/
