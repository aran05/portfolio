#include "iextreme.h"
#include "system/system.h"
#include "Mini_Game5.h"
#include "..\\sound.h"
#include "collision.h"
#include "..\mode.h"


void Mini_Game5::Mini_Game5_Init()
{
	player = new iex2DObj("DATA\\chokobo.png");
	player_pos = Vector2(150.0f, 425.0f);
	player_move = Vector2(0.0f, 0.0f);

	enemy = new iex2DObj("DATA\\car.png");
	enemy_pos = Vector2(1380.0f, 455.0f);
	enemy_move = Vector2(0.0f, 0.0f);

	jump_power = -30.0f;
	gravity = 1.0f;

	ground = new iex2DObj("DATA\\jimen.png");

	timer = 50;
	enemy_type = 0;
	enemy_type = rand()%3;
	anime_timer = 0;
	anime_frame = 0;

	over_flg = false;
	push_flg = false;

	SOUNDMANAGER->Se_Initialize();
}

Mini_Game5::~Mini_Game5()
{
	if (player)
	{
		delete player;
		player = NULL;
	}
	if (enemy)
	{
		delete enemy;
		enemy = NULL;
	}
	if (ground)
	{
		delete ground;
		ground = NULL;
	}

	SOUNDMANAGER->~Sound();
}

void Mini_Game5::Mini_Game5_Update()
{
	enemy_pos += enemy_move;
	enemy_move_type(enemy_type);
	anime_timer++;

	if(anime_timer>10)
	{
		if(anime_frame!=3)
		{
			anime_frame += 1;
		}
		else
		{
			anime_frame = 0;
		}
		
		anime_timer = 0;
	}
	if (KEY_Get(KEY_B) == 3&& push_flg==false)
	{
		SOUNDMANAGER->Se_Play(SeNum::SE_Jump, FALSE);
		player_move.y = jump_power;
		push_flg = true;
	}
	player_move.y += gravity;
	player_pos += player_move;

	if (player_pos.y > 425.0f)
	{
		player_pos.y = 425.0f;
		player_move.y = 0.0f;
	}
	if (CheckHitRect2(player_pos.x, player_pos.y, 64, 64, enemy_pos.x, enemy_pos.y, 64, 64))
	{
		over_flg = true;
		SOUNDMANAGER->Se_Play(SeNum::SE_Wario_3, FALSE);
	}
	
}

void Mini_Game5::Mini_Game5_Render()
{
	ground->Render(0, 550, 1280, 720, 0, 0, 1024, 1024);
	player->Render(player_pos.x, player_pos.y, 160, 160, 0, 0, 512, 512);

	if(enemy_type==0)
	{
		enemy->Render(enemy_pos.x, enemy_pos.y, 150, 128, (128*2)*anime_frame, 0, 128*2, 128);
	}
	else if (enemy_type == 1)
	{
		enemy->Render(enemy_pos.x, enemy_pos.y, 128, 128, 128 * anime_frame, 128, 128, 128);
	}
	else if (enemy_type == 2)
	{
		enemy->Render(enemy_pos.x, enemy_pos.y, 128, 128, 128 * anime_frame, 128*2, 128, 128);
	}
}

void Mini_Game5::enemy_move_type(int type)
{
	if (mode == 0)
	{
		switch (type)
		{
		case 0:
			enemy_move.x = -10.0f;
			break;
		case 1:
			enemy_move.x = -10.0f;
			if (enemy_pos.x <= 330.0f)
			{
				enemy_move.y = -30.0f;
			}
			break;
		case 2:
			enemy_move.x = -10.0f;
			if (enemy_pos.x == 380.0f)
			{
				enemy_move.x = 0.0f;
				timer--;
			}
			if (timer <= 0)
			{
				enemy_move.x = -10.0f;
				timer = 50;
			}

			break;
		}

	}

	if (mode == 1)
	{
		switch (type)
		{
		case 0:
			enemy_move.x = -20.0f;
			break;
		case 1:
			enemy_move.x = -20.0f;
			if (enemy_pos.x <= 370.0f)
			{
				enemy_move.y = -30.0f;
			}
			break;
		case 2:
			enemy_move.x = -20.0f;
			if (enemy_pos.x == 380.0f)
			{
				enemy_move.x = 0.0f;
				timer--;
			}
			if (timer <= 0)
			{
				enemy_move.x = -20.0f;
				timer = 50;
			}

			break;
		}
	}
}
