#pragma once
#define ENEMY_MAX 10
#define ENEMY_SPEED 3

#define MoveSpeed 5
//#define HSizeMax 750
//#define WSizeMax 1240
#define HSizeMax 610
#define WSizeMax 1110
#define FallSpeed 3
#define UpSpeed 4

class Mini_Game_04
{
private:
	iex2DObj* BG;
	iex2DObj* Enemy = NULL;
	iex2DObj* Mini3;
	iex2DObj* obj;
	iex2DObj* propeller;

public:
	D3DXVECTOR2 PlayerXY;

	int EnemyX = 0;
	int EnemyY = 0;
	int MoveX = 0;
	int MoveY = 0;
	int FLG[ENEMY_MAX];

	int state = 0;
	int count = 0;
	int EnemyFrame = 64, EnemyAnime = 0;

	int P_MoveX = 0, P_MoveY = 0;
	int playerX = 620, playerY = 420;
	int playerFrame = 0, playerAnime = 0;
	int propellerFrame = 0, propellerAnime = 0;
	int	propellerRotation = 1;


	bool clear_flg;

	Mini_Game_04();
	~Mini_Game_04();
	bool Init();
	void Update();
	void Render();
	void MovePlayer();
	void Animation();
	void PlayerAnimation();
	void MoveEnemy();
	void EnemyAnimation();
	void clear()
	{
		EnemyX = 0;
		EnemyY = 0;
		MoveX = 0;
		MoveY = 0;
	};

	void render_clear()
	{
		EnemyX = -100;
	};
	bool  CheckHitRect(int ax, int ay, int aw, int ah, int bx, int by, int bw, int bh);

	D3DXVECTOR2 EnemyXY;
	D3DXVECTOR2 vec;
	float len;
};
extern Mini_Game_04 enemy[ENEMY_MAX];