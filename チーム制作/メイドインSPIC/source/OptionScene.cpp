#include "iextreme.h"
#include "system\Scene.h"
#include "system\Framework.h"
#include "OptionScene.h"
#include "sceneTitle.h"
#include "Score.h"
#include "../sound.h"


bool OptionScene::Initialize()
{
	pause = new iex2DObj("DATA\\result.png");
	SOUNDMANAGER->Sound_Result();
	
	GetScore->HiScore_Load();
	return true;
}

OptionScene::~OptionScene()
{
	//if (pause)
	//{
	//	delete pause;
	//	pause = NULL;
	//}

	SOUNDMANAGER->Sound_Result_Release();

}

void OptionScene::Update()
{
	GetScore->sort();

	GetScore->Resolve();
	GetScore->Resolve2();
	GetScore->Resolve3();


	if (KEY_Get(KEY_A) == 3)
	{
		MainFrame->Pop_Scene();
	}

	if (KEY_Get(KEY_B) == 3)
	{
		MainFrame->ChangeScene(new sceneTitle());
	}

}

void OptionScene::Render()
{
	stuck->Render();

	iexPolygon::Rect(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, RS_COPY, 0x80000000);

	GetScore->RenderScore(MAKI1_POS_X - 400, MAKI1_POS_Y - 220, 38, 38);

	GetScore->RenderScore2(MAKI1_POS_X - 400, MAKI1_POS_Y - 120, 38, 38);

	GetScore->RenderScore3(MAKI1_POS_X - 400, MAKI1_POS_Y - 20, 38, 38);

	//�x�X�g�R
	pause->Render(280,96,1024, 96, 0,0,1024,128);
	
	if(GetScore->newnum==GetScore->saveData[0])
	{
		pause->Render(280 + 64, MAKI1_POS_Y - 270, 1024, 96, 0, 128, 1024, 128);
		pause->Render(750, MAKI1_POS_Y - 270, 1024, 96, 0, 128 * 4, 1024, 128);
	}
	else
	{
		pause->Render(280 + 64, MAKI1_POS_Y - 270, 1024, 96, 0, 128, 1024, 128,0, ARGB(255, 255, 241, 15));
		pause->Render(750, MAKI1_POS_Y - 270, 1024, 96, 0, 128 * 4, 1024, 128, 0, ARGB(255, 255, 241, 15));
	}
	
	if (GetScore->newnum == GetScore->saveData[1])
	{
		pause->Render(280 + 64, MAKI1_POS_Y - 170, 1024, 96, 0, 128 * 2, 1024, 128);
		pause->Render(750, MAKI1_POS_Y - 170, 1024, 96, 0, 128 * 4, 1024, 128);
	}
	else
	{
		pause->Render(280 + 64, MAKI1_POS_Y - 170, 1024, 96, 0, 128 * 2, 1024, 128, 0, ARGB(255, 255, 241, 15));
		pause->Render(750, MAKI1_POS_Y - 170, 1024, 96, 0, 128 * 5, 1024, 128, 0, ARGB(255, 255, 241, 15));
	}
	if (GetScore->newnum == GetScore->saveData[2])
	{
		pause->Render(280 + 64, MAKI1_POS_Y - 70, 1024, 96, 0, 128 * 3, 1024, 128);
		pause->Render(750, MAKI1_POS_Y - 70, 1024, 96, 0, 128 * 4, 1024, 128);
	}
	else
	{
		pause->Render(280 + 64, MAKI1_POS_Y - 70, 1024, 96, 0, 128 * 3, 1024, 128, 0, ARGB(255, 255, 241, 15));
		pause->Render(750, MAKI1_POS_Y - 70, 1024, 96, 0, 128 * 5, 1024, 128, 0, ARGB(255, 255, 241, 15));
	}

	pause->Render(100+64, 600, 1024, 96, 0, 128 * 6, 1024, 128);
	pause->Render(900, 600, 1024, 96, 0, 128 * 7, 1024, 128);
}
