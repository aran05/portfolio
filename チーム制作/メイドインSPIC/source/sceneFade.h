#pragma once

class sceneFade : public Scene
{
public:
	enum
	{
		Fade_In,
		Fade_Out
	};

protected:
	int type;
	int timer, max_timer;
	DWORD color;
	Scene* next_scene;

public:
	sceneFade(int type, int timer, DWORD color, Scene* new_scene);
	~sceneFade() { if (next_scene) delete next_scene; }
	bool Initialize();
	void Update();
	void Render();

};