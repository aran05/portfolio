#include "iextreme.h"
#include "system\Framework.h"
#include "clear.h"
#include "system\Scene.h"
#include "sceneTitle.h"

#include "sceneMain.h"


sceneClear::~sceneClear()
{
	if (title)
	{
		delete title;
		title = NULL;
	}

	if (img)
	{
		delete img;
		img = NULL;
	}

}

bool sceneClear::Initialize()
{
	
	iexLight::SetFog(800, 1000, 0);
	title = new iex2DObj("DATA\\BG\\gameclear.png");

	img = new iex2DObj("DATA\\BG\\PUSH_OPTIONS.png");
	elapsedTime = 0;
	alpha = t = 0.0f;
	push_flg = false;

	return true;
}

void sceneClear::Update()
{
	if(KEY_Get(KEY_A)==3)
	{

	}
}

void sceneClear::Render()
{
	title->Render(0, 0, 1280, 720, 0, 0, 1024, 1024);
	img->Render((iexSystem::ScreenWidth / 2) - (250), 580, 540 * 2, 32 * 2, 0, 31 * 3, 540, 32, RS_COPY, ARGB((int)200, 255, 255, 255));
	img->Render((iexSystem::ScreenWidth / 2) - (250), 580, 540 * 2, 32 * 2, 0, 31 * 3, 540, 32, RS_ADD, ARGB((int)alpha, 255, 255, 255));
}

sceneClear::sceneClear()
{
}
