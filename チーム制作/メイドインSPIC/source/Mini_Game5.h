#pragma once

class Mini_Game5
{
public:
	iex2DObj* player;
	iex2DObj* enemy;
	iex2DObj* ground;

	Vector2 player_pos;
	Vector2 player_move;
	float jump_power;
	float gravity;

	int timer;
	bool push_flg;

	bool over_flg;

	Vector2 enemy_pos;
	Vector2 enemy_move;
	void enemy_move_type(int type);
	int enemy_type;
	int anime_timer;
	int anime_frame;

	~Mini_Game5();
	void Mini_Game5_Init();
	void Mini_Game5_Update();
	void Mini_Game5_Render();

};