#pragma once
#include	"iextreme.h"			//	IEXライブラリ(全般)
#include	"system/system.h"		//	IEXライブラリ(システム)



#define GetScore (Score::getInstance())
static const int KETA = 2;
extern int score;
class Score {
private:
	iex2DObj* number;

public:
	int saveData[5];
	int saveData2[5];
	int runkScore;
	int num[KETA];//
	int num2[KETA];//
	int num3[KETA];//
	int newnum;
	Score();
	~Score();

	void Initialize();

	//桁分解
	void Resolve();
	void Resolve2();
	void Resolve3();

	//int getScore() { return saveData; }
	void setScore(int n);
	void setScore2(int n);
	void RenderScore(int x, int y, int w, int h);
	void RenderScore2(int x, int y, int w, int h);
	void RenderScore3(int x, int y, int w, int h);
	void Release();
	void SaveResult();
	void LoadResult();

	void swap(int *a, int *b);
	

	void sort();
	

	int HiScore_Load(void);
	void HiScore_Save(int NewScore);
	void main();
	static Score * getInstance() {
		static Score instance;
		return &instance;
	}
};



