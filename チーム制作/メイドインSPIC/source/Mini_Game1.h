#pragma once

class Mini_Game1
{
public:
	iex2DObj* img[4];		//画像
	iex2DObj* cursol;	//カーソル
	iex2DObj* maru;
	iex2DObj* batu;

	bool clear_flg;		//成功したかのフラグ
	int clear_count;
	int over_count;

	bool over_flg;		//失敗したかのフラグ

	bool  confirm_flg;	//決定キーを管理するフラグ

	int x, y;		//座標位置
	int state;
	int timer;
	int over_timer;

	int img_rand;
	//Mini_Game1();
	~Mini_Game1();

	void Mini_Game_Init();
	void Mini_Game_Update();
	void Mini_Game_Render();
};