#include "iextreme.h"
#include "system\Framework.h"
#include "system\Scene.h"
#include "Mini_Game_01.h"
#include "..\Game.h"
#include "../sound.h"
#include "sceneMain.h"
#include "..\mode.h"

bool Mini_Game_01::Init()
{
	iexLight::SetFog(800, 1000, 0);

	test = new iex2DObj("DATA\\ice2.png");
	test2 = new iex2DObj("DATA\\ice.png");
	test3 = new iex2DObj("DATA\\kmori.png");
	test4 = new iex2DObj("DATA\\hikouki.png");
	yure = new sceneMain();
	count = 0;
	clear_flg = false;
	flg = false;
	clear_count = 0;
	x = 1050;
	SOUNDMANAGER->Se_Initialize();

	return true;
}

Mini_Game_01::~Mini_Game_01()
{
	if (test)
	{
		delete test;
		test = NULL;
	}

	if (test2)
	{
		delete test2;
		test2 = NULL;
	}

	if (test3)
	{
		delete test3;
		test3 = NULL;
	}
	if(test4)
	{
		delete test4;
		test4 = NULL;
	}

	if (yure)
	{
		delete yure;
		yure = NULL;
	}

	SOUNDMANAGER->~Sound();
}

void Mini_Game_01::Update()
{
	yure->ShakeUpdate();
	if (KEY_Get(KEY_B) == 3 && count < 10)
	{
		
		flg = true;
		count++;
		yure->Set_Shake(250,100);
	}
	if (flg == true)
	{
		SOUNDMANAGER->Se_Play(SeNum::SE_Stone, FALSE);
		flg = false;
	}

	if(clear_flg==true)
	{
		if(clear_count==0)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Wario_1, FALSE);
			clear_count = 1;
		}
		
	}
	
	if(mode==0)
	{
		if (count >= 10)
		{
			x -= 10;
		}
	}
	else if(mode==1)
	{
		if (count >= 10)
		{
			x -= 20;
		}
	}
	
}

void Mini_Game_01::Render()
{

	test3->Render(185, 50, 1478, 1108, 0, 0, 3210, 3210);

	if (count <= 4)
	{
		test->Render(0,0 + yure->ShakeY,1280,720,0,0,1024,1024);
	}


	if (count >= 5 && count < 10)
	{
		test2->Render(0, 0 + yure->ShakeY, 1280, 720, 0, 0, 1024, 1024);
	}
	if (count >= 10)
	{
		test4->Render(x, 0, 256*2, 256*2, 0, 0, 256, 256);
	}

	if (count >= 10)
	{
		
		clear_flg = true;
		flg = false;
	}
}