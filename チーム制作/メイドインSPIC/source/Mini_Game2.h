#pragma once
#define MAX 3
class Particle;

class Mini_Game2
{
public:
	iex2DObj* player;
	Vector2 player_pos;
	Vector2 player_move;

	iex2DObj* back;
	iex2DObj* shot;

	iex2DObj* enemy;

	Particle* particle;

	bool  clear_flg;
	int	  enemy_dead_count;
	int clear_count;
	bool  over_flg;
	int	  shot_count;
	int x, y,x2,y2;
	int timer;

	struct ENEMY
	{
		Vector2 pos;
		Vector2 move;
		bool flg;
		bool move_flg;
	}E[MAX];

	struct SHOT
	{
		Vector2 pos;
		Vector2 move;
		bool flg;

		bool img_flg;
		int count;
	}S[MAX];

	~Mini_Game2();

	void Mini_Game2_Init();
	void Mini_Game2_Update();
	void Mini_Game2_Render();

	void Shot_Set(Vector2 p, float speed);
	void Enemy_Set(Vector2 p, float speed);
};