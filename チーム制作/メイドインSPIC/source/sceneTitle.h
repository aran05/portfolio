#pragma once

class sceneTitle : public Scene
{
private:
	iexView * view;
	iex2DObj* title[5]; //タイトル画像変数
	iex2DObj* title_effect;
	iex2DObj* title_effect2;
	int scroll_x;
	int scroll_x2;
	iex2DObj* img;
	iex2DObj* push_image;
	iex2DObj* push_image2;
	iex2DObj* k[2];
	int state;
	int timer; 

	int timer2;
	bool flg;

	DWORD color;

	bool push_flg;
	bool push_flg2;

	int elapsedTime;
	float t;
	float alpha;

	iex2DObj* kakumori;
	Vector2 kakumori_pos;
	Vector2 kakumori_scale;
	bool kakumori_flg;
	float angle;
	int kakumori_state;
	int kakumori_timer;

	iex2DObj* Effect;
	bool Effect_flg;

public:
	~sceneTitle();	//解放
	bool Initialize();	//初期化
						//更新・描画
	void Update();
	void Render();
};
