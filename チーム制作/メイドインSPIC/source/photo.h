#pragma once

class photo
{
public:
	photo();
	~photo();

	iex2DObj *photo_image[4];
	iex2DObj *photo_back;
	iex2DObj *musasabi;
	iex2DObj *kaku;

	int x, y;
	int seveX, seveY;
	int moveX, moveY;
	int state;
	int type;

	int shutter_timer;

	int kx, ky;

	int clear_count;
	int timer;
	int timer2;
	int angle;

	bool push_flg;
	bool clear_flg;
	bool hit_flg;

	void Init();
	void Update();
	void Render();
};

