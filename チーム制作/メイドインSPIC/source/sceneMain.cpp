#include	"iextreme.h"
#include	"system/system.h"
#include	"system\Framework.h"
#include	"clear.h"
#include	"sceneTitle.h"
#include	"sceneMain.h"
#include	"Player.h"
#include	"MAP.h"
#include    "..\Game.h"
#include    "../sound.h"
#include	"..\Particle.h"
#include	"OptionScene.h"
#include	"Score.h"
#include	"..\mode.h"

//*****************************************************************************************************************************
//
//	グローバル変数
//
//*****************************************************************************************************************************
//	カメラ用
//	ステージ用


//*****************************************************************************************************************************
//
//	初期化
//
//*****************************************************************************************************************************


bool sceneMain::Initialize()
{
	//	環境設定
	iexLight::SetFog( 800, 1000, 0 );

	//	カメラ設定
	view = new iexView();

	game_main = new Game();
	
	game_main->Game_Init();
	

	hp_gage = 2;
	hp_gage2 = 2;
	hp = new iex2DObj("DATA/heart.png");
	main_image[0] = new iex2DObj("DATA/back_hikei.png");
	main_image[1] = new iex2DObj("DATA/speaker1.png");
	main_image[2] = new iex2DObj("DATA/speaker2.png");
	main_image[3] = new iex2DObj("DATA/radio.png");
	main_image[4] = new iex2DObj("DATA/tape1.png");
	main_image[5] = new iex2DObj("DATA/tape1.png");
	main_image[6] = new iex2DObj("DATA/monitor.png");
	main_image[7] = new iex2DObj("DATA/number.png");
	main_image[8] = new iex2DObj("DATA/marubatu.png");
	boss_image = new iex2DObj("DATA/BossStage.png");

	font = new iex2DObj("DATA/moji.png");
	game_type = 0;
	state = 0;
	next_timer = 0;
	Scaling_timer = 0;
	ScalingX2=ScalingY2=0;
	Scaling_count = -1;
	angle = 0;
	Shake = false;
	marubatu_flg = false;
	ScalingX = 0;
	ScalingY = 0;
	Scaling_flg = false;
	Scaling_flg2 = false;
	keta=keta2=keta3=0;
	ketat = ketat2 = ketat3 = 0;
	gema_count = 0;
	c_flg=false;
	s_flg = 0;
	c_count = 0;
	se_clear_count = 0;
	se_clear_timer = 180;
	se_over_count = 0;
	se_over_timer = 120;

	SOUNDMANAGER->Se_Initialize();
	GetScore->Initialize();
	return true;
}

sceneMain::~sceneMain()
{
	if(game_main)
	{
		delete game_main;
		game_main = NULL;
	}
	for (int i = 0; i < 10;i++)
	{
		if (main_image)
		{
			delete main_image[i];
			main_image[i] = NULL;
		}
	}

	if(bgm_main)
	{
		IEX_StopStreamSound(bgm_main);
		bgm_main = NULL;
	}
	//SOUNDMANAGER->Sound_Main_Release();
	SOUNDMANAGER->~Sound();
	
}

//*****************************************************************************************************************************
//
//		更新
//
//*****************************************************************************************************************************


void	sceneMain::Update()
{
	//ノーマルモード
	if(mode==0)
	{
		switch (state)
		{
		case MAINSCREEN:

			if(no_flag==true)
			{
				bgm_main = IEX_PlayStreamSound("DATA\\Sound\\over2.ogg");
				s_flg = 1;
				no_flag = false;
			}
			if (s_flg == 0)
			{
				bgm_main = IEX_PlayStreamSound("DATA\\Sound\\b.ogg");
				//SOUNDMANAGER->Sound_Main();
				s_flg = 1;
			}
			if (hp_gage2 == 0)
			{
				if (bgm_main)
				{
					IEX_StopStreamSound(bgm_main);
					bgm_main = NULL;
				}

				GetScore->newnum = gema_count - 1;
				GetScore->HiScore_Save(gema_count - 1);
				Initialize();
				MainFrame->Push_Scene(new OptionScene());
				//MainFrame->ChangeScene(new sceneTitle());
				return;
			}

			if (gema_count == 10)
			{
				game_type = 7;
			}

			if(gema_count == 11)
			{
				GetScore->newnum = gema_count - 1;
				GetScore->HiScore_Save(gema_count - 1);
				if (bgm_main)
				{
					IEX_StopStreamSound(bgm_main);
					bgm_main = NULL;
				}

				MainFrame->Push_Scene(new OptionScene());
				Initialize();
				return;
			}
		/*	if (KEY_Get(KEY_ENTER) == 3)
			{
				if (bgm_main)
				{
					IEX_StopStreamSound(bgm_main);
					bgm_main = NULL;
				}
				GetScore->newnum = gema_count - 1;
				GetScore->HiScore_Save(gema_count - 1);
				Initialize();
				MainFrame->Push_Scene(new OptionScene());
			}*/

			//クリアしたか判定
			if (flag == false)
			{
				if (hp_gage != 0)
				{
					hp_gage -= 1;
				}
				else
				{
					hp_gage2 -= 1;
				}
				flag = true;
			}

			angle += 0.15f;
			next_timer++;
			Scaling_timer++;



			if (next_timer>(60 * 3)-15 && Scaling_count >= 6)
			{
				next_timer = 0;
				//メイン背景からゲームへ
				state++;
				break;
			}
			else if (Scaling_count >= 7 && Scaling_count == 10 && next_timer>(60 * 3) - 15)
			{
				next_timer = 0;
				state++;
			}



			if (Scaling_timer>15 && Scaling_count <= 6)
			{
				Scaling_flg = !Scaling_flg;
				Scaling_timer = 0;
				if (Scaling_count<9)
				{
					Scaling_count += 1;
				}
			}


			if (Scaling_timer>15)
			{
				Scaling_flg2 = !Scaling_flg2;
				Scaling_timer = 0;
				
			}

			if (Scaling_flg2 == true)
			{
				ScalingX2 = 10;
				ScalingY2 = 10;
			}
			else if (Scaling_flg2 == false)
			{
				ScalingX2 = 0;
				ScalingY2 = 0;
			}

			if (Scaling_count <= 6)
			{
				if (Scaling_flg == true)
				{
					ScalingX = 64;
					ScalingY = 32;
				}
				else if (Scaling_flg == false)
				{
					ScalingX = 0;
					ScalingY = 0;
				}
			}
			else
			{
				ShakeUpdate();
				Set_Shake(200, 60);

				//数字
				if (gema_count != 10)
				{
					if (gema_count > 0 && next_timer == (60 * 2) + 30)
					{
						if (gema_count > 0 && next_timer == (60 * 2) + 30)
						{
							if (keta == 7)
							{
								keta = 0;
								ketat = 1;
							}
							else if (keta >= 0 && keta < 7)
							{
								keta += 1;
							}
							if (keta == 2 && ketat == 1)
							{
								if (keta2 >= 0 && keta2 < 7)
								{
									keta = 0;
									ketat = 0;
									keta2 += 1;
								}
								else if (keta2 == 7)
								{
									keta2 = 0;
									ketat2 = 1;
								}
							}
							if (keta2 == 2 && ketat2 == 1)
							{
								if (keta3 >= 0 && keta3 < 7)
								{
									keta = 0;
									ketat = 0;
									keta2 = 0;
									ketat2 = 0;
									keta3 += 1;
								}
								else if (keta3 == 7)
								{
									keta3 = 0;
									ketat3 = 1;
								}
							}

						}
					}
				}
			}


			break;
		case GAME_INIT:
			if (next_timer == 0)
			{
				game_main->Game_Set_Timer(0);
				game_main->Game_Serct(game_type);
			}
			next_timer++;

			if (gema_count <= 1)
			{
				if (next_timer>10)
				{
					next_timer = 0;
					state++;
				}
			}
			else
			{
				if (next_timer>30)
				{
					if (gema_count == 10)
					{
						s_flg = 1;
						game_count = 0;
						c_count = 0;
						se_clear_count = 0;
						se_over_count = 0;
						se_over_timer = 120;
						se_clear_timer = 180;
					}
					next_timer = 0;
					state++;
				}
			}



			break;
		case GAME_MAIN:

			if (game_count == 1 || game_count == 2)
			{
				if (bgm_main)
				{
					IEX_StopStreamSound(bgm_main);
					bgm_main = NULL;
				}
			}
			if (game_count == 1&& c_count==0 && se_clear_count == 0)
			{
				SOUNDMANAGER->Se_Play(SeNum::SE_Boss_Clear, FALSE);
				c_count = 1;
				se_clear_count = 1;
			}
			else if (game_count == 2 && c_count == 0 && se_over_count == 0)
			{
				SOUNDMANAGER->Se_Play(SeNum::SE_Boss_Lose, FALSE);
				c_count = 1;
				se_over_count = 1;
			}

			if (se_clear_count == 1)
			{
				se_clear_timer--;
				if (se_clear_timer == 0)
				{
					SOUNDMANAGER->Se_Play(SeNum::SE_Wario_2, FALSE);
					se_clear_count = 2;
				}
			}

			if (se_over_count == 1)
			{
				se_over_timer--;
				if (se_over_timer == 0)
				{
					SOUNDMANAGER->Se_Play(SeNum::SE_Wario_3, FALSE);
					se_over_count = 2;
				}
			}

			if (s_flg == 1)
			{
				if (bgm_main)
				{
					IEX_StopStreamSound(bgm_main);
					bgm_main = NULL;
					bgm_main = IEX_PlayStreamSound("DATA\\Sound\\kari.ogg");
					s_flg = 2;
				}

			}

			game_main->Game_Main();

			if(gema_count!=10)
			{
				next_timer++;
			}
			else
			{
				if (no_flag == true)
				{
					state = 0;
					gema_count = 10;
					next_timer = 0;
					marubatu_flg = false;
					if (bgm_main)
					{
						IEX_StopStreamSound(bgm_main);
						bgm_main = NULL;
						bgm_main = IEX_PlayStreamSound("DATA\\Sound\\over2.ogg");
						s_flg = 1;

						SOUNDMANAGER->Se_Play(SeNum::SE_Wario_2, FALSE);
					}
				}
				if (flag == true)
				{
					state = 0;
					gema_count = 11;
					next_timer = 0;
					marubatu_flg = true;
					if (bgm_main)
					{
						IEX_StopStreamSound(bgm_main);
						bgm_main = NULL;
						bgm_main = IEX_PlayStreamSound("DATA\\Sound\\clear2.ogg");
						s_flg = 1;

						SOUNDMANAGER->Se_Play(SeNum::SE_Wario_2, FALSE);
					}
				}
				
			}

			
			game_main->Game_Set_Timer(next_timer);
			if (next_timer>(60 * 6) + 20)
			{
				next_timer = 0;

				if (game_type<6)
				{
					game_type += 1;
					gema_count += 1;

				}
				else
				{
					gema_count += 1;
					
					if (gema_count>=0&& gema_count<9)
					{
						game_type = rand() % 6;
					}
					
				}

			
				Scaling_count = 10;
				state = 0;
				game_main->main ->~Game_IN();

			

				if (flag == false)
				{
					marubatu_flg = flag;
					if (bgm_main)
					{
						IEX_StopStreamSound(bgm_main);
						bgm_main = NULL;
						bgm_main = IEX_PlayStreamSound("DATA\\Sound\\over.ogg");

						s_flg = 1;
					}
				}
				else
				{
					marubatu_flg = flag;
					if (bgm_main)
					{
						IEX_StopStreamSound(bgm_main);
						bgm_main = NULL;
						bgm_main = IEX_PlayStreamSound("DATA\\Sound\\clear.ogg");
						s_flg = 1;

						SOUNDMANAGER->Se_Play(SeNum::SE_Wario_2, FALSE);
					}
				}

			}

			break;
		}
	}
//ハードモード
else if(mode==1)
{

	switch (state)
	{
	case MAINSCREEN:

		if (no_flag == true)
		{
			bgm_main = IEX_PlayStreamSound("DATA\\Sound\\over2.ogg");
			marubatu_flg != no_flag;
			gema_count = 10;
			s_flg = 1;
			no_flag = false;
		}

		if (s_flg == 0)
		{
			bgm_main = IEX_PlayStreamSound("DATA\\Sound\\b2.ogg");
			//SOUNDMANAGER->Sound_Main();
			s_flg = 1;
		}
		if (hp_gage2 == 0)
		{
			if (bgm_main)
			{
				IEX_StopStreamSound(bgm_main);
				bgm_main = NULL;
			}
			GetScore->newnum = gema_count - 1;
			GetScore->HiScore_Save(gema_count - 1);
			Initialize();
			MainFrame->Push_Scene(new OptionScene());
			//MainFrame->ChangeScene(new sceneTitle());
			return;
		}

		if (gema_count == 10)
		{
			game_type = 7;
		}

		if (gema_count == 11)
		{
			if(no_flag==false)
			{
				GetScore->newnum = gema_count - 1;
				GetScore->HiScore_Save(gema_count - 1);
			}
			else
			{
				GetScore->newnum = gema_count - 2;
				GetScore->HiScore_Save(gema_count - 2);
			}
			
			if (bgm_main)
			{
				IEX_StopStreamSound(bgm_main);
				bgm_main = NULL;
			}

			MainFrame->Push_Scene(new OptionScene());
			Initialize();
			return;
		}

		if (KEY_Get(KEY_ENTER) == 3)
		{
			if (bgm_main)
			{
				IEX_StopStreamSound(bgm_main);
				bgm_main = NULL;
			}
			GetScore->newnum = gema_count - 1;
			GetScore->HiScore_Save(gema_count - 1);
			Initialize();
			MainFrame->Push_Scene(new OptionScene());
		}

		//クリアしたか判定
		if (flag == false)
		{
			if (hp_gage != 0)
			{
				hp_gage -= 1;
			}
			else
			{
				hp_gage2 -= 1;
			}
			flag = true;
		}

		angle += 0.3f;
		next_timer++;
		Scaling_timer++;

		

		if (next_timer>(60 * 2)-20 && Scaling_count >= 6)
		{
			next_timer = 0;
			//メイン背景からゲームへ
			state++;
			break;
		}
		else if (Scaling_count >= 7 && Scaling_count == 10 && next_timer>(60 * 2) - 15)
		{
			next_timer = 0;
			state++;
		}



		if (Scaling_timer>8 && Scaling_count <= 6)
		{
			Scaling_flg = !Scaling_flg;
			Scaling_timer = 0;
			if (Scaling_count<9)
			{
				Scaling_count += 1;
			}
		}

		if (Scaling_timer>8)
		{
			Scaling_flg2 = !Scaling_flg2;
			Scaling_timer = 0;

		}

		if (Scaling_flg2 == true)
		{
			ScalingX2 = 10;
			ScalingY2 = 10;
		}
		else if (Scaling_flg2 == false)
		{
			ScalingX2 = 0;
			ScalingY2 = 0;
		}

		if (Scaling_count <= 4)
		{
			if (Scaling_flg == true)
			{
				ScalingX = 64;
				ScalingY = 32;
			}
			else if (Scaling_flg == false)
			{
				ScalingX = 0;
				ScalingY = 0;
			}
		}
		else
		{
			ShakeUpdate();
			Set_Shake(200, 60);
			if (gema_count != 10)
			{
				//数字
				if (gema_count > 0 && next_timer == (60 * 1))
				{
					if (gema_count > 0 && next_timer == (60 * 1))
					{
						if (keta == 7)
						{
							keta = 0;
							ketat = 1;
						}
						else if (keta >= 0 && keta < 7)
						{
							keta += 1;
						}
						if (keta == 2 && ketat == 1)
						{
							if (keta2 >= 0 && keta2 < 7)
							{
								keta = 0;
								ketat = 0;
								keta2 += 1;
							}
							else if (keta2 == 7)
							{
								keta2 = 0;
								ketat2 = 1;
							}
						}
						if (keta2 == 2 && ketat2 == 1)
						{
							if (keta3 >= 0 && keta3 < 7)
							{
								keta = 0;
								ketat = 0;
								keta2 = 0;
								ketat2 = 0;
								keta3 += 1;
							}
							else if (keta3 == 7)
							{
								keta3 = 0;
								ketat3 = 1;
							}
						}

					}
				}
			}
		}


		break;
	case GAME_INIT:
		if (next_timer == 0)
		{
			game_main->Game_Set_Timer(0);
			game_main->Game_Serct(game_type);
		}
		
		next_timer++;

		if (gema_count <= 1)
		{
			if (next_timer>20)
			{
				next_timer = 0;
				state++;
			}
		}
		else
		{
			if (next_timer>30)
			{
				if (gema_count == 10)
				{
					s_flg = 1;
					game_count = 0;
					c_count = 0;
					se_clear_count = 0;
					se_over_count = 0;
					se_over_timer = 120;
					se_clear_timer = 180;
				}
				next_timer = 0;
				state++;
			}
		}

		

		break;
	case GAME_MAIN:

		if (game_count == 1 || game_count == 2)
		{
			if (bgm_main)
			{
				IEX_StopStreamSound(bgm_main);
				bgm_main = NULL;
			}
		}
		

		if (s_flg == 1)
		{
			if (bgm_main)
			{
				IEX_StopStreamSound(bgm_main);
				bgm_main = NULL;
				bgm_main = IEX_PlayStreamSound("DATA\\Sound\\kari.ogg");
				s_flg = 2;
			}

		}

		if (game_count == 1 && c_count == 0 && se_clear_count == 0)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Boss_Clear, FALSE);
			c_count = 1;
			se_clear_count = 1;
		}
		else if (game_count == 2 && c_count == 0 && se_over_count == 0)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Boss_Lose, FALSE);
			c_count = 1;
			se_over_count = 1;
		}

		if (se_clear_count == 1)
		{
			se_clear_timer--;
			if (se_clear_timer == 0)
			{
				SOUNDMANAGER->Se_Play(SeNum::SE_Wario_2, FALSE);
				se_clear_count = 2;
			}
		}

		if (se_over_count == 1)
		{
			se_over_timer--;
			if (se_over_timer == 0)
			{
				SOUNDMANAGER->Se_Play(SeNum::SE_Wario_3, FALSE);
				se_over_count = 2;
			}
		}
		

		game_main->Game_Main();

		if (gema_count != 10)
		{



			next_timer++;

			game_main->Game_Set_Timer(next_timer);

			if (next_timer>(60 * 4))
			{
				next_timer = 0;

				if (game_type<6)
				{
					game_type += 1;
					gema_count += 1;

				}
				else
				{
					gema_count += 1;

					if (gema_count >= 0 && gema_count<9)
					{
						game_type = rand() % 6;
					}
				}
				Scaling_count = 10;
				state = 0;
				game_main->main ->~Game_IN();

				if (flag == false)
				{
					marubatu_flg = flag;
					if (bgm_main)
					{
						IEX_StopStreamSound(bgm_main);
						bgm_main = NULL;
						bgm_main = IEX_PlayStreamSound("DATA\\Sound\\over2.ogg");

						s_flg = 1;
					}
				}
				else
				{
					marubatu_flg = flag;
					if (bgm_main)
					{
						IEX_StopStreamSound(bgm_main);
						bgm_main = NULL;
						bgm_main = IEX_PlayStreamSound("DATA\\Sound\\clear2.ogg");
						s_flg = 1;

						SOUNDMANAGER->Se_Play(SeNum::SE_Wario_2, FALSE);
					}
				}

			}
		}
		else
		{
			if (s_flg == 1)
			{
				if (bgm_main)
				{
					IEX_StopStreamSound(bgm_main);
					bgm_main = NULL;
					
				}
				bgm_main = IEX_PlayStreamSound("DATA\\Sound\\kari.ogg");
				s_flg = 2;
			}

			if (no_flag == true)
			{
				state = 0;
				gema_count = 10;
				next_timer = 0;
				marubatu_flg = false;
				if (bgm_main)
				{
					IEX_StopStreamSound(bgm_main);
					bgm_main = NULL;
					bgm_main = IEX_PlayStreamSound("DATA\\Sound\\over2.ogg");
					s_flg = 1;

					SOUNDMANAGER->Se_Play(SeNum::SE_Wario_2, FALSE);
				}
			}
			if (flag == true)
			{
				state = 0;
				gema_count = 11;
				next_timer = 0;
				marubatu_flg = true;
				if (bgm_main)
				{
					IEX_StopStreamSound(bgm_main);
					bgm_main = NULL;
					bgm_main = IEX_PlayStreamSound("DATA\\Sound\\clear2.ogg");
					s_flg = 1;

					SOUNDMANAGER->Se_Play(SeNum::SE_Wario_2, FALSE);
				}
			}

		}
		
		break;
	}
}
	
}

//*****************************************************************************************************************************
//
//		描画関連
//
//*****************************************************************************************************************************

void	sceneMain::Render()
{
	//	画面クリア
	view->Activate();
	view->Clear();
	switch (state)
	{
	case 0:
		//back背景
		main_image[0]->Render(0, 0, 1280, 720, 0, 0, 1280, 720);
		
		
		if (gema_count != 0)
		{
			if (marubatu_flg == false)
			{
				main_image[8]->Render(560, 80, 128 + ScalingX2, 128 + ScalingY2, 0, 0, 256, 256);
			}
			else if (marubatu_flg == true)
			{
				main_image[8]->Render(560, 80, 128 + ScalingX2, 128 + ScalingY2, 256, 0, 256, 256);
			}
		}
		if (gema_count == 10)
		{
			boss_image->Render(495, 10, 280 + ScalingX2, 256 + ScalingY2, 0, 0, 512, 512);
		}

		//ラジカセ
		main_image[3]->Render(0, 0, 1280, 720, 0, 0, 1280, 720);
		//テープ１
		main_image[4]->ROT_Render(632, 480, 128, 128, 0, 0, 128, 128, 0,ARGB(255,255,255,255),angle);
		//テープ２
		main_image[4]->ROT_Render(500, 480, 128, 128, 0, 0, 128, 128, 0, ARGB(255, 255, 255, 255),angle);
		//モニター
		main_image[6]->Render(0, 0, 1280, 720, 0, 0, 1280, 720);

		if (Scaling_flg == false)
		{
			//スピーカ１
			main_image[1]->Render(0+ShakeY, 0, 1280, 720, 0, 0, 1280, 720);
			//スピーカ２
			main_image[2]->Render(0+ShakeY, 0, 1280, 720, 0, 0, 1280, 720);
		}
		else
		{
			//スピーカ１
			main_image[1]->Render(0, -32, 1280 + Shake + ScalingX, 720 + Shake + ScalingY, 0, 0, 1280, 720);
			//スピーカ２
			main_image[2]->Render(-64, -32, 1280 + Shake + ScalingX, 720 + Shake + ScalingY, 0, 0, 1280, 720);
		}

		
		

		//数字
		for (int i = 0; i < 3; i++)
		{
			if(i==0)
			{
				main_image[7]->Render(532 + 64 * i, 332, 64, 64, (64*keta3), (64*ketat3), 64, 64);
			}
			if(i==1)
			{
				main_image[7]->Render(532 + 64 * i, 332, 64, 64, (64 * keta2), (64 * ketat2), 64, 64);
			}
			if(i==2)
			{
				main_image[7]->Render(532 + 64 * i, 332, 64, 64, (64 * keta), (64 * ketat), 64, 64);
			}
			
		}
		
		for (int i = 0; i < hp_gage; i++)
		{
			
			hp->Render(880 + 170 * i, 64, 160, 160, 0, 0, 128, 128);
		}

		for (int i = 0; i < hp_gage2; i++)
		{

			hp->Render(96 + 170 * i, 64, 160, 160, 0, 0, 128, 128);
		}

		/*if(next_timer>((60 * 5)+40))
		{

			if (game_type == 0)
			{
				font->Render((1280 / 2) - 256, 720 / 2, 1024, 128, 0, 128, 1024, 128);
			}

			if(game_type==1)
			{
				font->Render((1280 / 2) - 256, 720 / 2, 1024, 128, 0, 0, 1024, 128);
			}
			
		}*/
		
		break;
	case 1:
		if (game_type == 0)
		{
			font->Render((1280 / 2) - 400, 720 / 2 - 64, 1024, 128, 0, 128, 1024, 128);
		}

		if (game_type == 1)
		{
			font->Render((1280 / 2) - 200, 720 / 2 - 64, 1024, 128, 0, 0, 1024, 128);
		}
		if (game_type == 2)
		{
			font->Render((1280 / 2) - 400, 720 / 2 - 64, 1024, 128, 0, 128*2, 1024, 128);
		}

		if (game_type == 3)
		{
			font->Render((1280 / 2) - 400, 720 / 2 - 64, 1024, 128, 0, 128 * 3, 1024, 128);
		}


		if (game_type == 4)
		{
			font->Render((1280 / 2) - 400, 720 / 2-64, 1024, 128, 0, 128 * 4, 1024, 128);
		}

		if (game_type == 5)
		{
			font->Render((1280 / 2) - 200, 720 / 2 - 64, 1024, 128, 0, 128 * 5, 1024, 128);
		}

		if (game_type == 6)
		{
			font->Render((1280 / 2) - 200, 720 / 2 - 64, 1024, 128, 0, 128 * 5, 1024, 128);
		}

		if (game_type == 7)
		{
			font->Render((1280 / 2) - 200, 720 / 2 - 64, 1024, 128, 0, 128*6, 1024, 128);
		}

		break;
	case 2:
		game_main->Game_Render();
		break;
	}
#ifdef _DEBUG
	char str[64];
	wsprintf(str, "%d %d", gema_count, game_type);
	IEX_DrawText(str, 300, 20, 200, 20, 0xFFFFFF00);
#endif // DEBUG


}

void sceneMain::ShakeUpdate()
{

	if (0 >= ShakeTimer) {
		ShakeX = ShakeY = 0;
		ShakeTimer = 0;
		return;
	}

	static float angle = 0;
	angle += 0.1f;
	//ShakeY += ((sinf(angle)*ShakeWidth));
	ShakeY = (int)(((rand() % 201) - 100)*0.00001f*ShakeWidth*ShakeTimer);

	ShakeTimer--;
}

void sceneMain::Set_Shake(float width, int timer)
{
	ShakeWidth = width;   //-width〜+widthの幅で揺れる
	ShakeTimer = timer;

}