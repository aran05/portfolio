#include "iextreme.h"
#include "photo.h"
#include "collision.h"
#include "..\sound.h"
#include "..\mode.h"
photo::photo()
{
	
}


photo::~photo()
{
	for (int i = 0; i < 3; i++)
	{
		if (photo_image)
		{
			delete photo_image[i];
			photo_image[i] = NULL;
		}
	}
	if (musasabi)
	{
		delete musasabi;
		musasabi = NULL;
	}

	if (photo_back)
	{
		delete photo_back;
		photo_back = NULL;
	}

	if (kaku)
	{
		delete kaku;
		kaku = NULL;
	}

	SOUNDMANAGER->~Sound();
}

void photo::Init()
{
	photo_image[0] = new iex2DObj("DATA/ki_haikei.png");
	photo_image[1] = new iex2DObj("DATA/shutter.png");
	photo_image[2] = new iex2DObj("DATA/shutter2.png");
	photo_image[3] = new iex2DObj("DATA/shutter3.png");
	photo_back = new iex2DObj("DATA/haikei6.png");
	musasabi = new iex2DObj("DATA/musasabi.png");
	kaku = new iex2DObj("DATA/kakumori2.png");
	push_flg = false;
	hit_flg = false;
	clear_flg = false;
	state = 0;
	type = rand()%3;
	timer2 = 0;
	kx = ky = 0;
	angle = 0;
	if (type == 0)
	{
		x = 0;
		y = 180;
	}
	else if (type == 1)
	{
		x = 0;
		y = 0;
	}
	else if (type == 2)
	{
		x = 0;
		y = 500;
	}
	clear_count = 0;
	timer = 0;
	shutter_timer = 60;
}

void photo::Update()
{

	timer++;

	if (timer<10)
	{
		kx -= 12;
	}

	if (push_flg == false)
	{

		if(mode==0)
		{
			if (timer > 80)
			{
				if (type == 0)
				{
					x += 14;
					//y += 7;
				}
			}

			if (timer > 60)
			{
				if (type == 1)
				{
					x += 14;
					y += 7;
				}
			}

			if (type == 2)
			{
				x += 14;
				y -= 7;
			}
		}
		else if(mode==1)
		{
			if (timer > 80)
			{
				if (type == 0)
				{
					x += 26;
					//y += 7;
				}
			}

			if (timer > 60)
			{
				if (type == 1)
				{
					x += 24;
					y += 12;
				}
			}

			if (type == 2)
			{
				x += 24;
				y -= 12;
			}
		}
		


	}


	
	
	if(KEY_Get(KEY_B)==3&& push_flg ==false)
	{
		SOUNDMANAGER->Se_Play(SeNum::SE_Shutter, FALSE);
		seveX = x, seveY = y;
		
		if (seveY>324 && seveY<700 && seveX>660 && seveX<1100)
		{
			hit_flg = false;
		}

		if (seveY>160 && seveY<324 && seveX>200 && seveX<660)
		{
			hit_flg = true;
		}

		if (seveY>0 && seveY<160 && seveX>0 && seveX>100)
		{
			hit_flg = false;
		}
		
		push_flg = true;
		state++;
	}


	if (x>1000&& push_flg==false)
	{
		SOUNDMANAGER->Se_Play(SeNum::SE_Wario_3, FALSE);
		seveX = -1000, seveY = -1000;
		push_flg = true;
		state++;
	}

	if (state == 1)
	{
		timer2++;
		if (timer2>15)
		{
			state++;
		}
	}

	if (state == 2)
	{
		if (hit_flg == true)
		{
			shutter_timer--;
			if (clear_count == 0 && shutter_timer == 0)
			{
				SOUNDMANAGER->Se_Play(SeNum::SE_Wario_1, FALSE);
				clear_count = 1;
			}
		}
	}
}

void photo::Render()
{
	switch (state)
	{
	case 0:
		photo_image[0]->Render(115, 38, 1054, 632, 0, 0, 512, 512);
		if (x>0 && x<1000)
		{
			musasabi->Render(x, y, 288, 256, 0, 0, 128, 128);
		}
		photo_image[1]->Render(115, 38, 1054, 632, 0, 0, 512, 512);
		if (type == 0)
		{
			kaku->ROT_Render(1100 + kx, 500, 288, 256, 0, 0, 128, 128, 0, ARGB(255, 255, 255, 255), angle);
		}
		break;
	case 1:
		photo_back->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		//photo_image[1]->Render(115, 38, 1054, 632, 0, 0, 512, 512);
		break;
	case 2:
		photo_image[2]->Render(115, 38, 1054, 632, 0, 0, 512, 512);
		musasabi->Render(seveX, seveY, 288, 256, 0, 0, 128, 128);
		photo_image[3]->Render(115, 38, 1054, 632, 0, 0, 512, 512);
		//photo_image[1]->Render(115, 38, 1054, 632, 0, 0, 512, 512);
		break;
	default:
		break;
	}
	
}
