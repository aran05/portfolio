#include "Score.h"

#include <fstream>
static char FileName[] = "DATA/result.txt";	// ハイスコアを格納するファイル名

Score::Score() {
	number = NULL;
}

Score::~Score()
{
	
}

void Score::Initialize()
{
	//number = new iex2DObj("DATA\\スコア仮.png");

	number = new iex2DObj("DATA\\suji2.png");
	runkScore = 0;

	/*for (int i = 0; i < 3; i++)
	{
		saveData[i] = 0;
	}*/
	
}
void Score::Release()
{
	/*FILE *fp;
	char mes[80];

	if ((fp = fopen(FileName, "w")) == NULL)
	{
		wsprintf(mes, "データファイルのオープンに失敗しました。保存を中止しました。\n");
		OutputDebugString(mes);
		return;
	}
	for (int i = 0; i < 4;i++)
	{
		fprintf(fp, "%d\n", saveData[i]);
	}
	fclose(fp);*/
}
void Score::RenderScore(int x, int y, int w, int h)
{
	if(newnum== saveData[0])
	{
		for (int i = 0; i < KETA; i++)
		{
			number->Render(x + i*(w + 24) - 80, y - 30, w * 2, h * 2, (num[i] % 8) * 64, (num[i] / 8) * 64, 64, 64);
		}
	}
	else
	{
		for (int i = 0; i < KETA; i++)
		{
			number->Render(x + i*(w + 24) - 80, y - 30, w * 2, h * 2, (num[i] % 8) * 64, (num[i] / 8) * 64, 64, 64,0,ARGB(255,255,241,15));
		}
	}
	
}

void Score::RenderScore2(int x, int y, int w, int h)
{

	if (newnum == saveData[1])
	{
		for (int i = 0; i < KETA; i++)
		{
			number->Render(x + i*(w + 24) - 80, y - 30, w * 2, h * 2, (num2[i] % 8) * 64, (num2[i] / 8) * 64, 64, 64);
		}
	}
	else
	{
		for (int i = 0; i < KETA; i++)
		{
			number->Render(x + i*(w + 24) - 80, y - 30, w * 2, h * 2, (num2[i] % 8) * 64, (num2[i] / 8) * 64, 64, 64, 0, ARGB(255, 255, 241, 15));
		}
	}
}


void Score::RenderScore3(int x, int y, int w, int h)
{
	if (newnum == saveData[2])
	{
		for (int i = 0; i < KETA; i++)
		{
			number->Render(x + i*(w + 24) - 80, y - 30, w * 2, h * 2, (num3[i] % 8) * 64, (num3[i] / 8) * 64, 64, 64);
		}
	}
	else
	{
		for (int i = 0; i < KETA; i++)
		{
			number->Render(x + i*(w + 24) - 80, y - 30, w * 2, h * 2, (num3[i] % 8) * 64, (num3[i] / 8) * 64, 64, 64, 0, ARGB(255, 255, 241, 15));
		}
	}
}


void Score::Resolve()
{
	int temp;

	/*saveData2[0] = saveData[0];

	if(saveData[0]<saveData[1])
	{
		saveData[0] = saveData[1];

	}*/

	temp= saveData[0];


	for (int i = KETA - 1; i >= 0; i--)
	{
		if (temp > 0) {
			num[i] = temp % 10;
			temp /= 10;
		}
		
		else {
			num[i] = 0;
		}

	}
	
	
}

void Score::Resolve2()
{

	//saveData2[1] = saveData[1];
	//
	//if(saveData2[0]<saveData[1])
	//{
	//	saveData[1] = saveData2[0];
	//}

	//if(saveData[1]<saveData[2])
	//{
	//	saveData[1] = saveData[2];
	//}
	int temp = saveData[1];
	
	for (int i = KETA - 1; i >= 0; i--)
	{
		if (temp> 0)
		{
			num2[i] = temp % 10;
			temp /= 10;
		}
		else {
			num2[i] = 0;
		}
	}
}

void Score::Resolve3()
{
	/*saveData2[2] = saveData[2];
	if (saveData[2]>saveData2[1])
	{
		saveData[2] = saveData2[1];
	}*/



	int temp = saveData[2];

	for (int i = KETA - 1; i >= 0; i--)
	{
		if (temp > 0)
		{
			num3[i] = temp % 10;
			temp /= 10;
		}
		else {
			num3[i] = 0;
		}
	}
}


void Score::setScore(int n) {// 加算
	runkScore += n;
}

void Score::setScore2(int n) {// 代入
	runkScore = n;
	
}


void Score::sort()
{
	int i, j, temp;

	for (i = 0; i < 6 - 1; i++) {
		for (j = 6 - 1; j > i; j--) {
 			if (saveData[j -1] < saveData[j]) {  /* 前の要素の方が大きかったら */
				temp = saveData[j];        /* 交換する */
				saveData2[j] = temp;
				saveData[j] = saveData[j - 1];
				saveData[j-1] = temp;
				
			}
		}
	}
	
}

//-----------------------------------------------------------------------------
										// 関数名　：　HiScore_Load()　
										// 機能概要：　ハイスコアを読み込む
										//-----------------------------------------------------------------------------
int Score::HiScore_Load(void)
{
	FILE *fp;
	//int HiScore;

	if ((fp = fopen(FileName, "r")) == NULL)
		for (int i = 0; i < 3; i++)
		{
			saveData[i] = 0;
		}
	else
	{
		fscanf(fp, "%d", &saveData[0]);

		fscanf(fp, "%d\n", &saveData[1]);

		fscanf(fp, "%d\n\n", &saveData[2]);

		fscanf(fp, "%d\n\n\n", &saveData[3]);

		fscanf(fp, "%d\n\n\n", &saveData[4]);

		sort();

		/*fscanf(fp, "%d", &saveData[0]);

		fscanf(fp, "%d\n", &saveData[1]);

		fscanf(fp, "%d\n\n", &saveData[2]);

		fscanf(fp, "%d\n\n\n", &saveData[3]);

		fscanf(fp, "%d\n\n\n", &saveData[4]);
*/
		char mes[80];

		if ((fp = fopen(FileName, "w")) == NULL)
		{
		wsprintf(mes, "データファイルのオープンに失敗しました。保存を中止しました。\n");
		OutputDebugString(mes);
		//return;
		}
		for (int i = 0; i < 4;i++)
		{
		fprintf(fp, "%d\n", saveData[i]);
		}
		fclose(fp);
	}

	return saveData[0], saveData[1], saveData[2], saveData[3];

}

//-----------------------------------------------------------------------------
// 関数名　：　HiScore_Save()　
// 機能概要：　ハイスコアを保存する
//-----------------------------------------------------------------------------
void Score::HiScore_Save(int NewScore)
{
	FILE *fp;
	char mes[80];

	if ((fp = fopen(FileName, "a")) == NULL)
	{
		wsprintf(mes, "データファイルのオープンに失敗しました。保存を中止しました。\n");
		OutputDebugString(mes);
		return;
	}
	
	if(saveData[3]<NewScore)
	{
		fprintf(fp, "%d", NewScore);
	}
	
	fclose(fp);

}
void Score::SaveResult()
{
	// これをファイルに出し入れしたい
	int n = 9999;

	// バイナリ出力モードで開く
	std::fstream file("DATA\\result.dat", std::ios::binary | std::ios::out);
	// こっちでも良し
	//file.open("./filename.dat", ios::binary | ios::out);
	// 読み込むならこう
	file.open("DATA\\result.dat", std::ios::binary | std::ios::in);
	//sort();
	// 書き込む
	//file.write((char*)&n, sizeof(n));


	// 閉じる
	file.close();
}

void Score::LoadResult()
{
	
}

void Score::swap(int * a, int * b)
{
	{
		int t;

		t = *a;
		*a = *b;
		*b = t;
	}
}

void Score::main()
{
	/*int i, j;
	int data[100];
	float dev[100];
	float sum, sum2, ave, sigma;
	FILE *fp;

	fp = fopen("testdata.txt", "r");
	if (fp == NULL) return;

	sum = 0;
	sum2 = 0;

	for (i = 0; i<100; i++) {
		fscanf(fp, "%d", &(data[i]));
		sum += data[i];
		sum2 += data[i] * data[i];
	}

	fclose(fp);

	ave = sum / 100.0;
	sigma = sqrt((100 * sum2 - (sum*sum)) / 100 / (100 - 1));

	printf("平均値＝%f\n", ave);
	printf("標準偏差＝%f\n", sigma);

	for (i = 0; i<100; i++) {
		dev[i] = (data[i] - ave) / sigma * 10 + 50;
	}

	sort(100, dev);

	for (i = 0; i<100; i++) {
		printf("%d, %f\n", i, dev[i]);
	}
}*/
}
