#pragma once

//ここでマップを増やすことが可能
//ステージ
#define		WIDTH		(1024)				 //画面サイズ幅
#define		HEIGHT		(720)				 //画面サイズ高さ
#define		CHIP_SIZE			 (32)				 //1チップのサイズ
#define		CHIP_1LINE_NUM		 (16)				 //画像内の1行のチップ
#define		MAP_WIDTH		(WIDTH /CHIP_SIZE)//
#define		MAP_HEIGHT		(HEIGHT/CHIP_SIZE)//
#define		MAP_LIMIT_W		(WIDTH*2)
#define		MAP_LIMIT_H		(HEIGHT)
#define		MAP_SIZE_W		(MAP_LIMIT_W/CHIP_SIZE)				 //1マップのサイズ(幅)
#define		MAP_SIZE_H		(MAP_LIMIT_H/CHIP_SIZE)				 //1マップのサイズ(高さ)
#define		SCROLL_ACT_DW		(200)			//スクロール連動制御幅

#define		MAP_WITH_CHECK		(97)	//当たり判定が発生するチップ番号

#define		P_CHECK_BLOCK_W11	(-32)			//プレイヤー 縦当たり判定用修正量X1
#define		P_CHECK_BLOCK_W12	(32)			//プレイヤー 縦当たり判定用修正量X2
#define		P_CHECK_BLOCK_W2	(-20)			//プレイヤー 左当たり判定用修正量X
#define		P_CHECK_BLOCK_W3	(+10)			//プレイヤー 右当たり判定用修正量X
#define		P_CHECK_BLOCK_H11	(-32)			//プレイヤー 横当たり判定用修正量Y
#define		P_CHECK_BLOCK_H12	(-32)			//プレイヤー 横当たり判定用修正量Y2
#define		P_CHECK_BLOCK_H2	(-16)			//プレイヤー 上当たり判定用修正量Y

//

#define		CHIP_WITH_CHECK		 (64)			//これ以上は判定が発生するチップ番号
enum
{
	ST1,
	ST2,
	ST3,
};

struct SCROLLX
{
	int state;
};

class Map
{
private:
	iex2DObj *lpMap;
public:
	int MapX, MapY;
	int lmap[512][512];
	bool st;
	bool s_flag;
	bool next;
	void Initialize();
	void Render();

	static Map * getInstance() {
		static Map instance;
		return &instance;
	}

	void loadmap(int mapnum);
	//テキストを読み込む
	void loadmap2(char filename[1024], int mapchip[512][512], int *mapx, int *mapy);
	Map() {};
	~Map() {};


};

#define MAPMANAGER (Map::getInstance())
