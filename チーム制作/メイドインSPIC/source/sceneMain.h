//*****************************************************************************************************************************
//
//		メインシーン
//
//*****************************************************************************************************************************
class Game;

enum 
{
	MAINSCREEN,
	GAME_INIT,
	GAME_MAIN
};


class	sceneMain : public Scene
{
private:
	iexView*	view;
	iex2DObj* hp;

	iex2DObj *main_image[10];
	iex2DObj *boss_image;
	
	iex2DObj *font;
	LPDSSTREAM bgm_main;
	int ScalingX, ScalingY; //拡大縮小
	int ScalingX2, ScalingY2; //拡大縮小
	int Scaling_timer;
	int Scaling_count;
	bool Scaling_flg;
	bool Scaling_flg2;
	bool marubatu_flg;
	bool c_flg;
	int Shake;  //揺れ
	float angle;
	int c_count;
	int se_clear_count;
	int se_clear_timer;
	int se_over_count;
	int se_over_timer;


	int s_flg;

	//ゲームをカウントする変数
	int keta, keta2, keta3;
	int ketat, ketat2, ketat3;
	int gema_count;

	

	int hp_gage;
	int hp_gage2;


	int game_type;     //ゲームの切り替え
	int state;		  //cese切り替え
	int next_timer;	 //背景を切り替えるタイマー

	Game *game_main;

public:
	~sceneMain();
	//	初期化
	bool Initialize();
	//	更新・描画
	void Update();	//	更新
	void Render();	//	描画

	int ShakeTimer;					//振動時間と開始してからの経過時間
	float ShakeWidth;               //振動の際の揺れ幅
	int ShakeX, ShakeY;

	void Set_Shake(float width, int timer);
	void ShakeUpdate();
	
};


