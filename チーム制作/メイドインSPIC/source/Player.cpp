#include "iextreme.h"
#include "system\System.h"
#include "Player.h"
#include "collision.h"
#include "MAP.h"

void c_Player::Initialize()
{
	Player = new iex2DObj("DATA\\CHR\\Player\\Player.png");
	Pos = Vector2 (0.0f, 0.0f);
	Move = Vector2(0.0f, 0.0f);
	Angle = 0.0f;
	jump_flag = false;
}

void c_Player::Release()
{
	if (Player)
	{
		delete Player;
		Player = NULL;
	}
}

void c_Player::Update()
{


	Move.x = 0.0f;
	
	if (KEY_Get(KEY_LEFT) == 1)
	{

		Move.x = -MOVE_SPEED;
	}

	if (KEY_Get(KEY_RIGHT) == 1)
	{
		Move.x = MOVE_SPEED;
	}

	/*if (KEY_Get(KEY_UP) == 1)
	{
		Move.y = -MOVE_SPEED;
	}

	if (KEY_Get(KEY_DOWN) == 1)
	{
		Move.y = MOVE_SPEED;
	}*/

	//ジャンプは1回だけにする//collision.cpp内の下あたり判定でフラグをfalseにしている
	if(KEY_Get(KEY_SPACE)==3&&jump_flag==false)
	{
		Move.y = -8.0f;
		jump_flag = true;
	}
	Move.y += GRAVITY;
	Pos += Move;
	
	/*プレイヤーとブロックの当たり判定*/
	if(Move.x<0)
	{
		COLLISIONMANAGER->CheckWallLeft();
	}
	
	if (Move.x>0)
	{
		COLLISIONMANAGER->CheckWallRight();
	}

	if (Move.y<0)
	{
		COLLISIONMANAGER->CheckWallUP();
	}

	if (Move.y>0)
	{
		COLLISIONMANAGER->CheckWallDown();
	}



	//スクロール処理
	if (Pos.x - MAPMANAGER->MapX < (0 + SCROLL_ACT_DW))
		MAPMANAGER->MapX = Pos.x - SCROLL_ACT_DW;
	if (MAPMANAGER->MapX < 0)
		MAPMANAGER->MapX = 0;

	if (Pos.x - MAPMANAGER->MapX >(WIDTH - SCROLL_ACT_DW)) //右側
		MAPMANAGER->MapX = Pos.x - (WIDTH - SCROLL_ACT_DW);//
	if (MAPMANAGER->MapX >(MAP_LIMIT_W))			//右側
		MAPMANAGER->MapX = MAP_LIMIT_W;			//
	if (Pos.x >= MAP_LIMIT_W)
	{
		Pos.x = MAP_LIMIT_W;
	}
	/**/
}

void c_Player::Render()
{
	Player->Render(Pos.x - (128 / 2) - MAPMANAGER->MapX, Pos.y-128, 128, 128, 0, 0, 128, 128);
}