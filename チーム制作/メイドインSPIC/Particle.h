#pragma once
#define PARTICLE_MAX 100

class Particle
{
public:
	//iexParticle* particle = NULL;
	iex2DObj* particle;

	struct PARTICLE
	{
		int img_x;
		int img_y;
		int scale_x;
		int scale_y;
		Vector2 pos;
		Vector2 move;
		Vector2 power;
		DWORD blend;
		DWORD color;
		bool state;
		int timer;
	}P[PARTICLE_MAX];

	void Initialize();
	void Release();
	void Update();
	void Render();

	void Set(int x, int y, int scale_x, int scale_y, Vector2 pos, Vector2 move, Vector2 power, DWORD blend, DWORD color, int timer);
	void Effect1(Vector2 pos);

	void Effect2(Vector2 pos);
	void Effect3(Vector2 pos);

	void Effect4(Vector2 pos);

	void Effect5(Vector2 pos);
};
