#include "iextreme.h"
#include "system\System.h"
#include "Particle.h"

void Particle::Initialize()
{
	particle = new iex2DObj("DATA\\particle.png");
	for (int i = 0; i < PARTICLE_MAX; i++)
	{
		P[i].img_x = 0;
		P[i].img_y = 0;
		P[i].pos = Vector2(0.0f, 0.0f);
		P[i].move = Vector2(0.0f, 0.0f);
		P[i].power = Vector2(0.0f, 0.0f);
		P[i].state = false;
		P[i].timer = 0;
	}
	//particle = new iexParticle();
	//particle->Initialize("DATA\\particle.png", 3000);
}

void Particle::Release()
{
	if (particle)
	{
		delete particle;
		particle = NULL;
	}
	//if (particle)
	//{
	//	particle->Release();
	//	delete particle;
	//	particle = NULL;
	//}
}

void Particle::Update()
{
	for (int i = 0; i < PARTICLE_MAX; i++)
	{
		if (P[i].state == false)
		{
			continue;
		}
		P[i].move += P[i].power;
		P[i].pos += P[i].move;
		P[i].timer--;
		if (P[i].timer <= 0)
		{
			P[i].state = false;
		}

	}

}

void Particle::Render()
{
	for (int i = 0; i < PARTICLE_MAX; i++)
	{
		if (P[i].state == true)
		{
			particle->Render(P[i].pos.x, P[i].pos.y, P[i].scale_x * 32, P[i].scale_y * 32, P[i].img_x * 128, P[i].img_y * 128, 128, 128, P[i].blend, P[i].color);
		}
	}
}

void Particle::Set(int x, int y, int scale_x, int scale_y, Vector2 pos, Vector2 move, Vector2 power, DWORD blend, DWORD color, int timer)
{
	for (int i = 0; i < PARTICLE_MAX; i++)
	{
		if (P[i].state == true)
		{
			continue;
		}
		P[i].img_x = x;
		P[i].img_y = y;
		P[i].pos = pos;
		P[i].move = move;
		P[i].power = power;
		P[i].blend = blend;
		P[i].color = color;
		P[i].scale_x = scale_x;
		P[i].scale_y = scale_y;
		P[i].state = true;
		P[i].timer = timer;

		break;
	}
}
void Particle::Effect1(Vector2 pos)
{
	Vector2 Pos, Move, Power;
	DWORD Blend, Color;
	int Timer;
	for (int j = 0; j < 1; j++) {
		Pos.x = pos.x;
		Pos.y = pos.y;
		Move.x = 0.0f;
		Move.y = 0.0f;
		Power.x = 0.0f;
		Power.y = 0.0f;
		Blend = RS_ADD;
		Color = 0xFFFFFFFF;
		Timer = 30;
		Set(2, 2, 3, 3, Pos, Move, Power, Blend, Color, Timer);
	}

}

void Particle::Effect2(Vector2 pos)
{
	Vector2 Pos, Move, Power;
	DWORD Blend, Color;
	int Timer;

	for (int i = 0; i < 2; i++)
	{
		Pos.x = pos.x;
		Pos.y = pos.y;
		Move.x = (rand() % 20 - 30) * 0.2f;
		Move.y = (rand() % 20 - 30) * 0.2f;
		Power.x = 0.1f;
		Power.y = 0.2f;
		Blend = RS_COPY;
		Color = 0xFF00FFFF;
		Timer = 30;
		Set(3, 2, 2, 2, Pos, Move, Power, Blend, Color, Timer);

	}


}

void Particle::Effect3(Vector2 pos)
{
	Vector2 Pos, Move, Power;
	DWORD Blend, Color;
	int Timer;
	for (int i = 0; i < 5; i++)
	{
		Pos.x = pos.x;
		Pos.y = pos.y;
		Move.x = (rand() % 20 - 30) * 0.2f;
		Move.y = (rand() % 20 - 30) * 0.2f;
		Power.x = 0.0f;
		Power.y = 0.4f;
		Blend = RS_COPY;
		Color = 0xFF00FFFF;
		Timer = 60;
		Set(3, 2, 2, 2, Pos, Move, Power, Blend, Color, Timer);

	}

}

void Particle::Effect4(Vector2 pos)
{
	Vector2 Pos, Move, Power;
	DWORD Blend, Color;
	int Timer;

	Pos.x = pos.x;
	Pos.y = pos.y;
	Move.x = 0.0f;
	Move.y = 0.0f;
	Power.x = 0.0f;
	Power.y = 0.0f;
	Blend = RS_ADD;
	Color = 0xFFFFFFFF;
	Timer = 10;
	Set(0, 0, 3, 1, Pos, Move, Power, Blend, Color, Timer);

}

void Particle::Effect5(Vector2 pos)
{
	Vector2 Pos, Move, Power;
	DWORD Blend, Color;
	int Timer;

	Pos.x = pos.x;
	Pos.y = pos.y;
	Move.x = 0.0f;
	Move.y = 0.0f;
	Power.x = 0.0f;
	Power.y = 0.0f;
	Blend = RS_ADD;
	Color = 0xFFFFFFFF;
	Timer = 10;
	Set(3, 3, 3, 1, Pos, Move, Power, Blend, Color, Timer);

}