#include "iextreme.h"
#include "Game.h"
#include "commad.h"
#include "photo.h"
#include "Mini_Game1.h"
#include "Mini_Game2.h"
#include "Mini_Game_01.h"
#include "Mini_Game_04.h"
#include "Mini_Game5.h"
#include "mode.h"
#include "Boss.h"
bool flag;
bool no_flag;
int  game_count;


void set_falg(bool flag_t)
{
	flag = flag_t;
}

void set_count(int c)
{
	game_count = c;
}

void no_set_falg(bool flag_t)
{
	no_flag = flag_t;
}


//ファイルパス
char* time_limit_image[] =
{
	"DATA/bomb.png",
	"DATA/himo.png",
	"DATA/num.png",
	"DATA/bomb2.png"
};


char* image_name[] =
{
	"DATA/haikei.png",
	"DATA/haikei2.png",
	"DATA/haikei3.png",
	"DATA/haikei4.png",
	"DATA/haikei5.png",
};


Game::Game()
{

}


Game::~Game()
{
	if(main)
	{
		delete main;
		main = NULL;
	}
}


//初期化
void Game::Game_Init()
{
	main = new Game_IN();
	flag = true;
	no_flag = false;
	game_count = 0;
	state = 0;
}


//ゲーム切り替え(1:ゲームの切り替え番号)
void Game::Game_Serct(int type)
{
	state = type;

	switch (type)
	{
	case 0:
		main->Game_In_Init(type, geme_timer);
		break;
	case 1:
		main->Game_In_Init(type, geme_timer);
		break;
	case 2:
		main->Game_In_Init(type, geme_timer);
		break;
	case 3:
		main->Game_In_Init(type, geme_timer);
		break;
	case 4:
		main->Game_In_Init(type, geme_timer);
		break;
	case 5:
		main->Game_In_Init(type,geme_timer);
		break;
	case 6:
		main->Game_In_Init(type, geme_timer);
		break;
	case 7:
		main->Game_In_Init(type, geme_timer);
		break;
	default:
		break;
	}
}


void Game::Game_Main()
{

	switch (state)
	{
	case 0:
		main->Game_In_Main(state,geme_timer);
		break;
	case 1:
		main->Game_In_Main(state, geme_timer);
		break;
	case 2:
		main->Game_In_Main(state, geme_timer);
		break;
	case 3:
		main->Game_In_Main(state, geme_timer);
		break;
	case 4:
		main->Game_In_Main(state, geme_timer);
		break;
	case 5:
		main->Game_In_Main(state, geme_timer);
		break;
	case 6:
		main->Game_In_Main(state, geme_timer);
		break;
	case 7:
		main->Game_In_Main(state, geme_timer);
		break;
	default:
		break;
	}
}


//描画
void Game::Game_Render()
{
	switch (state)
	{
	case 0:
		main->Game_In_Render(state);
		break;
	case 1:
		main->Game_In_Render(state);
		break;
	case 2:
		main->Game_In_Render(state);
		break;
	case 3:
		main->Game_In_Render(state);
		break;
	case 4:
		main->Game_In_Render(state);
		break;
	case 5:
		main->Game_In_Render(state);
		break;
	case 6:
		main->Game_In_Render(state);
		break;
	case 7:
		main->Game_In_Render(state);
		break;
	default:
		break;
	}
}


//画像の読み取りとGame_INの初期化(1:ゲーム番号)
void Game_IN::Game_In_Init(int type, int timer)
{

	pGame_in[type] = new Game_IN();
	fireX = 1190;
	himo_size = 1160;

	image_timer[0]= new iex2DObj(time_limit_image[0]);
	image_timer[1] = new iex2DObj(time_limit_image[1]);
	image_timer[2] = new iex2DObj(time_limit_image[2]);
	image_timer[3] = new iex2DObj(time_limit_image[3]);
	
	switch (type)
	{
	case 0:
		pGame_in[type]->image[0]=new iex2DObj(image_name[0]);
		pGame_in[type]->image[1] = new iex2DObj(image_name[2]);
		pGame_in[type]->image[2] = new iex2DObj(image_name[3]);
		pGame_in[type]->image[3] = new iex2DObj(image_name[4]);

		mini1 = new Mini_Game1();
		mini1->Mini_Game_Init();
		
		break;
	case 1:
		pGame_in[type]->image[0] = new iex2DObj(image_name[0]);
		pGame_in[type]->image[1] = new iex2DObj(image_name[1]);
		pGame_in[type]->image[2] = new iex2DObj(image_name[3]);
		pGame_in[type]->image[3] = new iex2DObj(image_name[4]);
		mini_game_01 = new Mini_Game_01();
		mini_game_01->Init();
		break;
	case 2:
		pGame_in[type]->image[0] = new iex2DObj(image_name[0]);
		pGame_in[type]->image[1] = new iex2DObj(image_name[1]);
		pGame_in[type]->image[2] = new iex2DObj(image_name[3]);
		pGame_in[type]->image[3] = new iex2DObj(image_name[4]);
		mini2 = new Mini_Game2();
		mini2->Mini_Game2_Init();
		break;
	case 3:
		pGame_in[type]->image[0] = new iex2DObj(image_name[0]);
		pGame_in[type]->image[1] = new iex2DObj(image_name[2]);
		pGame_in[type]->image[2] = new iex2DObj(image_name[3]);
		pGame_in[type]->image[3] = new iex2DObj(image_name[4]);
		cmomad = new game_commad();
		cmomad->Init();
		break;
	case 4:
		pGame_in[type]->image[0] = new iex2DObj(image_name[0]);
		pGame_in[type]->image[1] = new iex2DObj(image_name[1]);
		pGame_in[type]->image[2] = new iex2DObj(image_name[3]);
		pGame_in[type]->image[3] = new iex2DObj(image_name[4]);
		Pphoto = new photo();
		Pphoto->Init();
		break;
	case 5:
		
		pGame_in[type]->image[0] = new iex2DObj(image_name[0]);
		pGame_in[type]->image[1] = new iex2DObj(image_name[1]);
		pGame_in[type]->image[2] = new iex2DObj(image_name[3]);
		pGame_in[type]->image[3] = new iex2DObj(image_name[4]);

		mini4 = new Mini_Game_04();
		mini4->Init();
		break;
	case 6:

		pGame_in[type]->image[0] = new iex2DObj(image_name[0]);
		pGame_in[type]->image[1] = new iex2DObj(image_name[1]);
		pGame_in[type]->image[2] = new iex2DObj(image_name[3]);
		pGame_in[type]->image[3] = new iex2DObj(image_name[4]);

		mini5 = new Mini_Game5();
		mini5->Mini_Game5_Init();
		break;
	case 7:

		pGame_in[type]->image[0] = new iex2DObj(image_name[0]);
		pGame_in[type]->image[1] = new iex2DObj(image_name[1]);
		pGame_in[type]->image[2] = new iex2DObj(image_name[3]);
		pGame_in[type]->image[3] = new iex2DObj(image_name[4]);

		boss = new Boss();
		boss->Boss_Game_Init();
		break;
	default:
		break;
	}


}


//ゲーム本編(1:ゲーム番号)
void Game_IN::Game_In_Main(int type,int timer)
{

	game_timer = timer;
	switch (type)
	{
	case 0:
		mini1->Mini_Game_Update();
		set_falg(mini1->clear_flg);
		
		break;
	case 1:
		mini_game_01->Update();
		set_falg(mini_game_01->clear_flg);
		break;
	case 2:
		mini2->Mini_Game2_Update();
		set_falg(mini2->clear_flg);
		break;
	case 3:
		cmomad->Update();
		set_falg(cmomad->clear);
		break;
	case 4:
		Pphoto->Update();
		set_falg(Pphoto->hit_flg);
		break;
	case 5:
		mini4->Update();
		set_falg(mini4->clear_flg);
		break;
	case 6:
		mini5->Mini_Game5_Update();
		set_falg(!mini5->over_flg);
		break;
	case 7:
		boss->Boss_Game_Update();
		set_falg(boss->clear_flg);
		no_set_falg(boss->over_flg);
		set_count(boss->over_count);
		break;
	default:
		break;
	}
}


//ゲームの時間制限
void Game_IN::Game_Time_Limit()
{

	if(mode==0)
	{
		if (game_timer > 0 && game_timer < 60 * 6)
		{
			image_timer[0]->Render(40, 640, 64, 64, 0, 0, 32, 32);
		}
		if (game_timer>0 && game_timer<60)
		{
			image_timer[1]->Render(55, 570, 1280, 128, 0, 0, 1280, 64);
		}
		if (game_timer >= 60 && game_timer<60 * 2)
		{
			image_timer[1]->Render(55, 570, 1280, 128, 0, 64, 1280, 64);
		}
		if (game_timer >= 60 * 2 && game_timer<60 * 3)
		{
			image_timer[1]->Render(55, 570, 1280, 128, 0, 64 * 2, 1280, 64);
		}
		if (game_timer >= 60 * 3 && game_timer<60 * 4)
		{
			image_timer[1]->Render(55, 570, 1280, 128, 0, 64 * 3, 1280, 64);
			image_timer[2]->Render(40, 590, 60, 60, 0, 128, 128, 128);
		}
		if (game_timer >= 60 * 4 && game_timer<60 * 5)
		{
			image_timer[1]->Render(55, 570, 1280, 128, 0, 64 * 4, 1280, 64);
			image_timer[2]->Render(40, 590, 60, 60, 128, 0, 128, 128);
		}
		if (game_timer >= 60 * 5 && game_timer<60 * 6)
		{

			image_timer[1]->Render(55, 570, 1280, 128, 0, 64 * 5, 1280, 64);
			image_timer[2]->Render(40, 590, 60, 60, 0, 0, 128, 128);
		}
		if (game_timer >= 60 * 6 && game_timer < 60 * 6 + 20)
		{
			image_timer[3]->Render(-50, 520, 288, 288, 0, 0, 256, 256);
		}
	}else if (mode == 1)
	{
		if (game_timer > 0 && game_timer < 60 * 4)
		{
			image_timer[0]->Render(40, 640, 64, 64, 0, 0, 32, 32);
		}
		if (game_timer>0 && game_timer<30)
		{
			image_timer[1]->Render(55, 570, 1280, 128, 0, 0, 1280, 64);
		}
		if (game_timer >= 30 && game_timer<60)
		{
			image_timer[1]->Render(55, 570, 1280, 128, 0, 64, 1280, 64);
		}
		if (game_timer >= 60 && game_timer<90)
		{
			image_timer[1]->Render(55, 570, 1280, 128, 0, 64 * 2, 1280, 64);
		}
		if (game_timer >= 90 && game_timer<120)
		{
			image_timer[1]->Render(55, 570, 1280, 128, 0, 64 * 3, 1280, 64);
			image_timer[2]->Render(40, 590, 60, 60, 0, 128, 128, 128);
		}
		if (game_timer >= 120 && game_timer<150)
		{
			image_timer[1]->Render(55, 570, 1280, 128, 0, 64 * 4, 1280, 64);
			image_timer[2]->Render(40, 590, 60, 60, 128, 0, 128, 128);
		}
		if (game_timer >= 150 && game_timer<190)
		{

			image_timer[1]->Render(55, 570, 1280, 128, 0, 64 * 5, 1280, 64);
			image_timer[2]->Render(40, 590, 60, 60, 0, 0, 128, 128);
		}
		if (game_timer >= 190 && game_timer < 60 * 4)
		{
			image_timer[3]->Render(-50, 520, 288, 288, 0, 0, 256, 256);
		}
	}
	

}


//描画(1:ゲーム番号)
void Game_IN::Game_In_Render(int type)
{
	switch (type)
	{
	case 0:
		pGame_in[type]->image[0]->Render(0, 0, 1280, 720, 0, 0, 1024, 1024);
		pGame_in[type]->image[1]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		mini1->Mini_Game_Render();
		pGame_in[type]->image[3]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		pGame_in[type]->image[2]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		break;
	case 1:
		pGame_in[type]->image[0]->Render(0, 0, 1280, 720, 0, 0, 1024, 1024);
		pGame_in[type]->image[1]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		mini_game_01->Render();
		pGame_in[type]->image[3]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		pGame_in[type]->image[2]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		break;
	case 2:
		pGame_in[type]->image[0]->Render(0, 0, 1280, 720, 0, 0, 1024, 1024);
		pGame_in[type]->image[1]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		mini2->Mini_Game2_Render();
		pGame_in[type]->image[3]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		pGame_in[type]->image[2]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		
		break;
	case 3:
		pGame_in[type]->image[0]->Render(0, 0, 1280, 720, 0, 0, 1024, 1024);
		pGame_in[type]->image[1]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		cmomad->Render();
		pGame_in[type]->image[3]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		pGame_in[type]->image[2]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		
		break;
	case 4:
		pGame_in[type]->image[0]->Render(0, 0, 1280, 720, 0, 0, 1024, 1024);
		pGame_in[type]->image[1]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		Pphoto->Render();
		pGame_in[type]->image[3]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		pGame_in[type]->image[2]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		
		break;
	case 5:
		pGame_in[type]->image[0]->Render(0, 0, 1280, 720, 0, 0, 1024, 1024);
		pGame_in[type]->image[1]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		mini4->Render();
		if (mini4->count != 1)
		{
			pGame_in[type]->image[3]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
			pGame_in[type]->image[2]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);	
		}
		break;
	case 6:
		pGame_in[type]->image[0]->Render(0, 0, 1280, 720, 0, 0, 1024, 1024);
		pGame_in[type]->image[1]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		mini5->Mini_Game5_Render();
		pGame_in[type]->image[3]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		pGame_in[type]->image[2]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		break;
	case 7:
		pGame_in[type]->image[0]->Render(0, 0, 1280, 720, 0, 0, 1024, 1024);
		pGame_in[type]->image[1]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		boss->Boss_Game_Render();
		pGame_in[type]->image[3]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		pGame_in[type]->image[2]->Render(0, 0, 1280, 1024, 0, 0, 1024, 1024);
		break;
	default:
		break;
	}
	Game_Time_Limit();
	
}



void Game_IN::Game1()
{
		
}


Game_IN::~Game_IN()
{
	for (int i = 0; i < 10; i++)
	{
		if (pGame_in[i])
		{
			delete pGame_in[i];
			pGame_in[i] = NULL;
		}

		if(image[i])
		{
			delete image[i];
			image[i] = NULL;
		}
			
	}
	for (int t = 0; t < 3;t++)
	{
		if(image_timer[t])
		{
			delete image_timer[t];
			image_timer[t] = NULL;
		}
	}

	if(mini1)
	{
		delete mini1;
		mini1 = NULL;
	}

	if (mini2)
	{
		delete mini2;
		mini2 = NULL;
	}

	if (mini4)
	{
		delete mini4;
		mini4 = NULL;
	}

	if (mini5)
	{
		delete mini5;
		mini5 = NULL;
	}


	if (mini_game_01)
	{
		delete mini_game_01;
		mini_game_01 = NULL;
	}

	if(cmomad)
	{
		delete cmomad;
		cmomad = NULL;
	}

	if(Pphoto)
	{
		delete Pphoto;
		Pphoto = NULL;
	}

	if(boss)
	{
		delete boss;
		boss = NULL;
	}

	

}