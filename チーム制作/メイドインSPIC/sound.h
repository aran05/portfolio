#pragma once

class Sound
{
public:
	~Sound();
	bool Sound_Title();
	bool Sound_Main();
	bool Sound_Result();

	void Sound_Title_Release();
	void Sound_Main_Release();
	void Sound_Result_Release();

	void Se_Initialize();
	void Se_Play(int num, bool loopFlg);
	void Se_Stop(int num);

	static Sound *getInstance() {
		static Sound instance;
		return &instance;
	}
};

enum SeNum
{
	SE_Wario_1,
	SE_Wario_2,
	SE_Wario_3,
	SE_Correct,
	SE_Incorrect,
	SE_Shutter,
	SE_Stone,
	SE_Shot,
	SE_Push,
	SE_Jump,
	SE_Boss_Clear,
	SE_Boss_Lose,
	SE_Explosion,

	SE_End,
};
#define SOUNDMANAGER (Sound::getInstance())
