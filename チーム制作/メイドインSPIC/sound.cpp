#include "iextreme.h"
#include "sound.h"

LPDSSTREAM bgm_title;
LPDSSTREAM bgm_main;
LPDSSTREAM bgm_result;

Sound::~Sound()
{
	for (int i = 0; i < SeNum::SE_End; i++) Se_Stop(i);
}

void Sound::Se_Initialize()
{
	IEX_SetWAV(SeNum::SE_Wario_1, "DATA\\Sound\\wario_ok.wav");
	IEX_SetWAV(SeNum::SE_Wario_2, "DATA\\Sound\\wario_yaho.wav");
	IEX_SetWAV(SeNum::SE_Wario_3, "DATA\\Sound\\wario_no.wav");
	IEX_SetWAV(SeNum::SE_Correct, "DATA\\Sound\\correct.wav");
	IEX_SetWAV(SeNum::SE_Incorrect, "DATA\\Sound\\incorrect.wav");
	IEX_SetWAV(SeNum::SE_Shutter, "DATA\\Sound\\camera-shutter.wav");
	IEX_SetWAV(SeNum::SE_Shot, "DATA\\Sound\\shot.wav");
	IEX_SetWAV(SeNum::SE_Stone, "DATA\\Sound\\stone.wav");
	IEX_SetWAV(SeNum::SE_Push, "DATA\\Sound\\push.wav");
	IEX_SetWAV(SeNum::SE_Jump, "DATA\\Sound\\jump.wav");
	IEX_SetWAV(SeNum::SE_Boss_Clear, "DATA\\Sound\\boss_clear.wav");
	IEX_SetWAV(SeNum::SE_Boss_Lose, "DATA\\Sound\\boss_lose.wav");
	IEX_SetWAV(SeNum::SE_Explosion, "DATA\\Sound\\explosion.wav");
}


bool Sound::Sound_Title()
{
	bgm_title = IEX_PlayStreamSound("DATA\\Sound\\title1.ogg");

	//	BGM ->����
	bgm_title->SetVolume(200);
	return true;
}


bool Sound::Sound_Main()
{
	bgm_main = IEX_PlayStreamSound("DATA\\Sound\\b.ogg");

	//	BGM ->����
	bgm_main->SetVolume(200);
	return true;
}

bool Sound::Sound_Result()
{
	bgm_result = IEX_PlayStreamSound("DATA\\Sound\\result.ogg");

	//	BGM ->����
	bgm_result->SetVolume(200);
	return true;
}


void Sound::Sound_Title_Release()
{
	if (bgm_title)
	{
		IEX_StopStreamSound(bgm_title);
		bgm_title = NULL;
	}

}

void Sound::Sound_Main_Release()
{
	if (bgm_main)
	{
		IEX_StopStreamSound(bgm_main);
		bgm_main = NULL;
	}

}

void Sound::Sound_Result_Release()
{
	if (bgm_result)
	{
		IEX_StopStreamSound(bgm_result);
		bgm_result = NULL;
	}

}

void Sound::Se_Play(int num, bool loopFlg)
{
	IEX_PlaySound(num, loopFlg);
}

void Sound::Se_Stop(int num)
{
	IEX_StopSound(num);
}