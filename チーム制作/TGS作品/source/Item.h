#pragma once

#define ITEM_MAX 36
#define ITEM_STAGE_MAX 6
#define ITEM_OBJ_MAX 2
#define ITEM_OBJ_SCALE 2.0f
#define ITEM_MAX_MOVE 60
enum
{
	ITEM_1,//アイテム
	OBJ_NONE_1,//モデルなし
};

class Item
{
public:
	iexMesh* item_obj;
	Vector3 item_pos;
	Vector3 item_scale;
	Vector3 item_angle;
	bool item_display_flg;
	int item_type;

	int angle_timer;
	bool mirror_check_flg; //鏡を消すフラグ　//falseなら表示 true
	bool left;	  //左方向に移動するフラグ/falseなら動かない　trueなら動く
	bool right;   //右方向に移動するフラグ/falseなら動かない　trueなら動く

	Vector3 move;
	float move_x;
	int mirror_count;
	bool delete_flg;
	bool set_pos_flg;
	~Item();
	void Initialize(int type);
	void Update();
	void Render(iexShader * shader);
	char* file_name[ITEM_OBJ_MAX]
	{
		"DATA/StageDATA/mirror7.IMO",
		"",
	};
	void Set_Item(Vector3 pos, Vector3 scale, Vector3 angle, bool display_flg);
	void Check_Item_Stage(Vector3 pos, int count); //鏡の位置と現実側ポジション比較して描画するか判断する
	void Check_Item_Mirror(Vector3 pos, int count);//鏡の位置とミラーポジション比較して描画するか判断する
	void Move(float max);
	//鏡の進む方向を決める

};

class Item_Manager
{
public:
	Item* ITEM_OBJ[ITEM_MAX];
	Item* MIRROR_ITEM_OBJ[ITEM_MAX];

	iexShader* stage_shader;
	int stage_state;

	Vector3 mirror_pos;
	int mirror_count;

	struct ITEM
	{
		Vector3 pos;		//アイテムの位置
		Vector3 scale;		//アイテムのサイズ
		Vector3 angle;		//アイテムの向き
		bool display_flg;	//アイテムが表示しているかを判断するフラグ
		int type;			//アイテムの種類
	}item[ITEM_MAX];

	struct MIRROR_ITEM
	{
		Vector3 pos;		//アイテムの位置
		Vector3 scale;		//アイテムのサイズ
		Vector3 angle;		//アイテムの向き
		bool display_flg;	//アイテムが表示しているかを判断するフラグ
		int type;			//アイテムの種類
	}mirror_item[ITEM_MAX];

	void Initialize(int stage_num, Vector3 pos, int count);
	~Item_Manager();
	void Update(int camer_type);
	void Render();


	void Item_Map(int state);
	void Item_Set(int map[ITEM_STAGE_MAX][ITEM_STAGE_MAX], int mirror_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX]);
	void Set_Mirrror_Count(int count)
	{
		mirror_count = count;
	}

	//鏡の位置を取得
	void Set_Mirrror_Pos(Vector3 pos)
	{
		mirror_pos = pos;
	}
};
