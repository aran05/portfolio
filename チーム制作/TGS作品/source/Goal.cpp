#include "iextreme.h"
#include "Goal.h"
#include "Player.h"


void GOAL::Initialize(int type)
{
	//ステージモデルを読み込み
	stage_obj = new iexMesh(file_name[type]);
	//goal_pos = Vector3(0.0f, 0.0f, 0.0f);
	goal_scale = Vector3(GOAL_SCALE, GOAL_SCALE, GOAL_SCALE);
	stage_type = type;

	if (stage_type == 0)
	{
		goal = new iexMesh("DATA/StageDATA/obj.IMO");
	}

	stage_display_flg = false;
}

void GOAL::Set_Goal(Vector3 pos, Vector3 scale, Vector3 angle, bool display_flg)
{
	pos.y *= -1;
	stage_display_flg = display_flg;
	//引数でもらった値をモデルに代入
	stage_obj->SetPos(pos.x, pos.y, pos.z);
	stage_obj->SetScale(scale);
	stage_obj->SetAngle(angle);
	if (stage_type == 0)
	{
		goal->SetPos(pos.x, pos.y, pos.z);
		goal->SetScale(scale);
		goal->SetAngle(Vector3(angle.x, angle.y, angle.z));
	}

	goal_pos = pos;
	goal_scale = scale;
	move = Vector3(0, 0, 0);
	mirror_check_flg = false;
	left = right = inverted = false;
	move_x = 0;
	goal_flg = false;

}


GOAL::~GOAL()
{
	//解放
	if (stage_obj)
	{
		delete stage_obj;
		stage_obj = NULL;
	}

	if (goal)
	{
		delete goal;
		goal = NULL;
	}
}


void GOAL::Update()
{
	stage_obj->Update();
	if (stage_type == 0)
	{
		goal->Update();
	}
}

void GOAL::Render(iexShader* shader)
{
	stage_obj->Render();
	//stage_obj->Render(shader, "copy3");
	//goal->Render();
}

void GOAL::Goal_Render(iexShader * shader)
{
	if (stage_type == 0)
	{
		goal->Render(shader, "copy_fx2");
	}
}



void Goal_Manager::Initialize(int stage_num)
{
	stage_shader = new iexShader("DATA/SHADER/mirror_shader.fx");

	mirror_count = 0;
	mirror_pos = Vector3(0, 0, 0);
	camer_type = 0;
	stage_state = stage_num;

	for (int i = 0; i < GOAL_HALF_MAX; i++)
	{
		//構造体データの初期化(現実世界)
		stage[i].pos = Vector3(-165.5f, -75.0f, -5.0f);
		stage[i].angle = Vector3(0.0f, 0.0f, 0.0f);
		stage[i].scale = Vector3(GOAL_SCALE, GOAL_SCALE, GOAL_SCALE);
		stage[i].type = 0;
		stage[i].display_flg = false;
		STAGE_OBJ[i] = NULL;
		goal_pos[i] = Vector3(-165.5f, -75.0f, -5.0f);
	}


	//足場ブロックの配置(引数のステイトはステイトの値でステージの種類が変わる)
	Stage_Map(stage_state);

}

Goal_Manager::~Goal_Manager()
{
	for (int i = 0; i < GOAL_HALF_MAX; i++)
	{
		delete STAGE_OBJ[i];
		STAGE_OBJ[i] = NULL;
	}

	if (stage_shader)
	{
		delete stage_shader;
		stage_shader = NULL;
	}

	if (mirror_point)
	{
		delete mirror_point;
		mirror_point = NULL;
	}

}

void Goal_Manager::Update()
{

	for (int i = 0; i < GOAL_HALF_MAX; i++)
	{
		if (STAGE_OBJ[i]->stage_obj == NULL) continue;
		STAGE_OBJ[i]->Update();
	}



}


void Goal_Manager::Render()
{
	for (int i = 0; i < GOAL_HALF_MAX; i++)
	{
		if (STAGE_OBJ[i]->stage_obj == NULL) continue;
		if (stage[i].display_flg == false && STAGE_OBJ[i]->Get_Mirror_Flg() == false && STAGE_OBJ[i]->Get_Virtual_Flg() == false && player->Item_count <= 0)
		{
			STAGE_OBJ[i]->Render(stage_shader);
			STAGE_OBJ[i]->Goal_Render(stage_shader);
		}

	}


}

//bool Goal_Manager::Hit_block(Vector3 pos, Vector3 & local)
//{
//	for (int i = 0; i < GOAL_HALF_MAX; i++)
//	{
//		if (STAGE_OBJ[i] == NULL) continue;
//
//		if (player->Item_count == 0&&stage[i].display_flg == false || STAGE_OBJ[i]->Get_Mirror_Flg() == true && STAGE_OBJ[i]->Get_Virtual_Flg() == false)
//		{
//			STAGE_OBJ[i]->Hit_block(pos, local);
//		}
//	}
//
//	return true;
//}
//
//bool Goal_Manager::R_Hit_block(const Vector3 & pos, Vector3 & local)
//{
//	for (int i = 0; i < GOAL_HALF_MAX; i++)
//	{
//		if (STAGE_OBJ[i] == NULL) continue;
//		if (player->Item_count == 0 && stage[i].display_flg == false)
//		{
//			STAGE_OBJ[i]->R_Hit_block(pos, local);
//		}
//	}
//
//
//	return true;
//}
//
//bool Goal_Manager::L_Hit_block(const Vector3 & pos, Vector3 & local)
//{
//	for (int i = 0; i < GOAL_HALF_MAX; i++)
//	{
//		if (STAGE_OBJ[i] == NULL) continue;
//		if (player->Item_count == 0 && stage[i].display_flg == false)
//		{
//			STAGE_OBJ[i]->L_Hit_block(pos, local);
//		}
//	}
//
//	return true;
//}

void Goal_Manager::Stage_Map(int state)
{
	//ステージ切り替え
	switch (state)
	{
		//ステージ１
		//ステージ１
	case 0:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ Goal, NONE,NONE ,NONE,NONE ,NONE }
		};


		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map);
	}
	break;
	case 1:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ Goal, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}
	case 2:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ Goal, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}

	case 3:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ Goal, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}
	case 4:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, Goal,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}
	case 5:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,Goal ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}
	case 6:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ Goal, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}
	case 7:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,Goal ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}
	case 8:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,Goal,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}
	
	case 9:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, Goal,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}
	case 10:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,Goal,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}
	
	case 11:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,Goal ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}
	case 12:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ Goal, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}
	case 13:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ Goal, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}
	
	case 14:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ Goal, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}
	case 15:
	{
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ Goal, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE },
			{ NONE, NONE,NONE ,NONE,NONE ,NONE }
		};

		Stage_Set(stage_map);
		break;
	}
	
	}
}

void Goal_Manager::Stage_Set(int map[STAGE_MAX][STAGE_MAX])
{

	for (int y = 0; y < STAGE_MAX; y++)
	{
		for (int x = 0; x < STAGE_MAX; x++)
		{

			if (map[y][x] == Goal)
			{
				STAGE_OBJ[y * STAGE_MAX + x] = new GOAL();
				//二次元配列を一次元配列にする
				//stage[y * STAGE_MAX + x].pos = map[y][x];
				stage[y * STAGE_MAX + x].type = Goal;
				STAGE_OBJ[y * STAGE_MAX + x]->Initialize(stage[y * STAGE_MAX + x].type);
				STAGE_OBJ[y * STAGE_MAX + x]->Set_Goal(Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z), stage[y * STAGE_MAX + x].scale, stage[y * STAGE_MAX + x].angle, stage[y * STAGE_MAX + x].display_flg);
				//goal_pos[y * STAGE_MAX + x] = Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z);
				//STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z), stage[y * STAGE_MAX + x].scale, stage[y*STAGE_MAX + x].angle, stage[y * STAGE_MAX + x].display_flg, stage[y * STAGE_MAX + x].pasting_flg);
			}
			if (map[y][x] == NONE)
			{
				STAGE_OBJ[y * STAGE_MAX + x] = new GOAL();
				//二次元配列を一次元配列にする
				stage[y * STAGE_MAX + x].type = NONE;
				STAGE_OBJ[y * STAGE_MAX + x]->stage_obj = NULL;
				STAGE_OBJ[y * STAGE_MAX + x]->Initialize(stage[y * STAGE_MAX + x].type);
				STAGE_OBJ[y * STAGE_MAX + x]->Set_Goal(Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z), stage[y * STAGE_MAX + x].scale, stage[y * STAGE_MAX + x].angle, stage[y * STAGE_MAX + x].display_flg);
			}

		}
	}

}