#pragma once
#define PLAYER_MOVE_X 0.5f
#define PLAYER_SCALE 1.0f
#define PLAYER_JUMP_POWER 2.0f
#define PLAYER_MOVE_SPPED 0.8f
#define PLAYER_ANIME_TIEMR 38

enum
{
	STAND,
	MOVE,
	JUMP,
	TAKE = 4,
	GOAL_ANIME,
	GOAL_ANIME2,
	SLOPE,
	VIRTUAL_STAND,
	VIRTUAL_MOVE,
};

class Player
{
public:
	//*************************************************************
	//アニメーションが出来たらコメントをはずす
	iex3DObj* player_obj;	//アニメーション有りモデ
	//*************************************************************
	iexMesh* virtual_obj;	//アニメーションなしモデル
	Vector3 player_pos, player_move, player_scale, player_angle;
	bool floor_flg;			//足元の当たり判定用フラグ　:falseなら足場がなし　:trueなら足場がある
	int move_flg;			//
	bool goal_flg;			//ゴールしたかしてないかのフラグ　:falseならゴールしていない　:trueならゴールしている
	bool wall_hit_flg;		//壁に当たっときに押し返す処理　:falseなら当たっていない　:trueなら当たっている
	bool virtual_flg;		//虚像を持っているかを判断する変数　:falseなら持っていない　:trueなら持っている
	bool jack_flg;
	int  player_jump_timer;	//プレイヤーの滞空時間を管理する変数
	int  player_take_timer; //プレイヤーが物を取るアニメーションの時間を設定する変数

	int	 player_anime;		//プレイヤーのアニメーション管理変数
	int anime_timer;
	bool anime_flg;
	bool player_direction;	//プレイヤーの向いている方向:　falseなら右向き　:trueなら左向き
	bool stand_flg;
	bool reset_flg;
	float angle;		//	方向
	bool push_jump_key;
	//float axisX;
	float axisY;

	int Item_count;

	~Player();
	void Initialize();
	void Update();
	void Render();
	void Reset();

	void Move(float max);
	void Anime();
	void Goal_Anime();
	void Get_Virtual_Obj();
	Vector3 Get_Pos() { return player_pos; }
	void Set_Pos(Vector3 pos) { player_pos = pos; }
};

extern Player*	player;