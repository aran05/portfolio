#include "Sound.h"

LPDSSTREAM bgm_title;
LPDSSTREAM bgm_main;

Sound::~Sound()
{
	for (int i = 0; i < SeNum::SE_End; i++) Se_Stop(i);
}

void Sound::Se_Initialize()
{
	IEX_SetWAV(SeNum::SE_Kyozo, "DATA\\Sound\\se_kyozo.wav");
	IEX_SetWAV(SeNum::SE_Kyozo_Take, "DATA\\Sound\\se_kyozo_take.wav");
	IEX_SetWAV(SeNum::SE_Mirror, "DATA\\Sound\\se_mirror.wav");
	IEX_SetWAV(SeNum::SE_Book, "DATA\\Sound\\se_book.wav");
	IEX_SetWAV(SeNum::SE_Get, "DATA\\Sound\\se_get.wav");
	IEX_SetWAV(SeNum::SE_Enter, "DATA\\Sound\\se_enter.wav");
}


void Sound::Se_Play(int num, bool loopFlg)
{
	IEX_PlaySound(num, loopFlg);
}

void Sound::Se_Stop(int num)
{
	IEX_StopSound(num);
}

bool Sound::Sound_Title()
{
	bgm_title = IEX_PlayStreamSound("DATA\\Sound\\Title.ogg");

	bgm_title->SetVolume(200);
	return true;
}

bool Sound::Sound_Main()
{
	bgm_main = IEX_PlayStreamSound("DATA\\Main.ogg");

	return true;
}

// ポーズ用
bool Sound::Sound_Main_Volume()
{
	bgm_main->SetVolume(200);
	return true;
}

// ポーズ用
bool Sound::Sound_Main_Volume2()
{
	bgm_main->SetVolume(100);
	return true;
}

void Sound::Sound_Title_Release()
{
	if (bgm_title)
	{
		IEX_StopStreamSound(bgm_title);
		bgm_title = NULL;
	}
}

void Sound::Sound_Main_Release()
{
	if (bgm_main)
	{
		IEX_StopStreamSound(bgm_main);
		bgm_main = NULL;
	}
}