#pragma once
#define MAX_PARTICLE 3000

class Particle
{
private:
	iexParticle* particle;

public:
	void Effect_Init(void);
	void Effect_Release(void);
	void Effect_Update(void);
	void Effect_Render(void);
	void Effect(float x, float y, float z);
	void Effect_2(float x, float y, float z);
	void Effect_3(float x, float y, float z);

	static Particle *getInstance() {
		static Particle instance;
		return &instance;
	}
};

#define PARTICLEMANAGER (Particle::getInstance())