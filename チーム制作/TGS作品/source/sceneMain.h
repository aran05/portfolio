//*****************************************************************************************************************************
//
//		メインシーン
//
//*****************************************************************************************************************************
class Player;
class Camera;
class Stage_Manager;
class MIRROR;
class Mirror_Point;
class Goal_Manager;
class Tutorial;
class Item_Manager;
#define STAGE_NUM 0

class	sceneMain : public Scene
{
private:
	iex2DObj* jack;
	iexMesh* x;
	iex2DObj* stage_num;
	//iexView*	view;
	Camera* camera;
	iex2DObj* pause;

	//Player*	player;
	Vector3 local;
	Stage_Manager* stage_manager;
	Goal_Manager* goal_manager;
	Item_Manager* item_manager;
	
	Mirror_Point *mirror_point;
	iex2DObj * fe_image[4];
	int y;
	bool fe;
	bool fe_2;
	int fe_2_timer;
	bool volume_flg;
	int aplha;
	bool aplha_flg;

	//Tutorial *tuto;
	int anime_count;
	int goal_timer;
	//int Item_count;
	bool eff_flg;
	bool eff_flg2;
	bool eff_flg3;
	bool eff_flg4;


	void Reset();
	int story_count;
	bool tuto_flg; //チュートリアルの切り替え
	bool story_flg; //チュートリアルの切り替え
public:

	~sceneMain();
	//	初期化
	bool Initialize();
	//	更新・描画
	void Update();	//	更新
	void Render();	//	描画
	void Story_Set();
	static bool	ColAABB(Vector3 c1, Vector3 s1, Vector3 c2, Vector3 s2);					//	AABB同士の当たり判定

};

