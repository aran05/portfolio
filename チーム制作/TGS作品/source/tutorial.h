#pragma once
#define WINDOW_POS_X 500					//ウィンドウ画像位置(X軸)
#define WINDOW_POS_Y 0						//ウィンドウ画像位置(Y軸) 360
#define WINDOW_RENDER_X_SIZE 1280			//ウィンドウ画像描画サイズ(X軸)
#define WINDOW_RENDER_Y_SIZE 730			//ウィンドウ画像描画サイズ(Y軸) 450
#define	WINDOW_IMAGE_X 0					//ウィンドウ画像切り取り位置(X軸)
#define	WINDOW_IMAGE_Y 0					//ウィンドウ画像切り取り位置(Y軸)
#define WINDOW_WIDTH_SIZE 512				//ウィンドウ画像の幅
#define WINDOW_HEIGHT_SIZE 512				//ウィンドウ画像の高さ


#define BOOK_SCALE 1.0f


class Tutorial
{
public:
	iex2DObj* Window;			//ウィンドウ画像の表示
	iex2DObj* Font1;			//文字画像の表示
	iex2DObj* Font2;			//文字画像の表示
	iex2DObj* Font3;			//文字画像の表示
	iex2DObj* Font4;			//文字画像の表示
	iex2DObj* Font5;			//文字画像の表示

	iex2DObj* Jack;				//ジャック画像

	iexMesh* Book;				//本、これを取るとチュートリアルが見れる
	iexMesh* x;					//アイコン表示

	Vector3 Jack_Pos;			//文字の位置
	Vector3 Jack_Render_Size;	//文字の描画サイズ
	Vector3 Jack_Image_Pos;		//文字の画像切り取りサイズ
	Vector3 Jack_Width_Height;	//文字の幅、高さ

	Vector3 Font1_Pos;			//文字の位置
	Vector3 Font1_Render_Size;	//文字の描画サイズ
	Vector3 Font1_Image_Pos;		//文字の画像切り取りサイズ
	Vector3 Font1_Width_Height;	//文字の幅、高さ

	Vector3 Font2_Pos;			//文字の位置
	Vector3 Font2_Render_Size;	//文字の描画サイズ
	Vector3 Font2_Image_Pos;		//文字の画像切り取りサイズ
	Vector3 Font2_Width_Height;	//文字の幅、高さ

	Vector3 Font3_Pos;			//文字の位置
	Vector3 Font3_Render_Size;	//文字の描画サイズ
	Vector3 Font3_Image_Pos;		//文字の画像切り取りサイズ
	Vector3 Font3_Width_Height;	//文字の幅、高さ

	Vector3 Font4_Pos;			//文字の位置
	Vector3 Font4_Render_Size;	//文字の描画サイズ
	Vector3 Font4_Image_Pos;		//文字の画像切り取りサイズ
	Vector3 Font4_Width_Height;	//文字の幅、高さ

	Vector3 Book_Pos;			//本の位置
	Vector3 Book_Scale;			//本のサイズ
	Vector3 Book_Angle;			//本の向き

	bool Book_Flg;				//本を取ったか、取っていないかのフラグ:false取っていない:true取った
	int Book_State;				//本の種類

	void Set_Book_Pos(Vector3 pos) { Book_Pos = pos; }
	void Set_Book_State(int state) { Book_State = state; }
	void Set_Book_Flg(bool flg) { Book_Flg = flg; }
	//Tutorial();
	~Tutorial();

	//int Get_State() { return state; }
	//void Set_State(int s) { state = s; }
	void Initialize();
	void Update();	//	更新
	void Render();	//	描画
	void Book_Render();
	void x_Render();
	void Book_Move();
private:
	//説明切り替え
	//int state;
	//int push_count;
};