﻿#include "iextreme.h"
#include "Item.h"
#include "Player.h"
#include "mirror.h"
#include "Global.h"

void Item::Initialize(int type)
{
	item_obj = new iexMesh(file_name[type]);
	item_pos = Vector3(0.0f, 0.0f, 0.0f);
	item_angle = Vector3(0.0f, 0.0f, 0.0f);
	item_scale = Vector3(ITEM_OBJ_SCALE, ITEM_OBJ_SCALE, ITEM_OBJ_SCALE);
	item_display_flg = false;
	item_type = type;
}

void Item::Set_Item(Vector3 pos, Vector3 scale, Vector3 angle, bool display_flg)
{
	pos.y *= -1;
	item_pos = pos;
	item_scale = scale;
	item_obj->SetPos(pos);
	item_obj->SetScale(scale);
	item_obj->SetAngle(angle);
	item_display_flg = display_flg;
	left = right = false;
	move = Vector3(0, 0, 0);
	set_pos_flg = false;
	mirror_check_flg = false;
	move_x = 0;
	mirror_check_flg = false;
	mirror_count = 0;
	delete_flg = false;
	angle_timer = 60;
	if (item_type == ITEM_1)
	{
		player->Item_count += 1;
	}

}

void Item::Check_Item_Stage(Vector3 pos, int count)
{
	mirror_count = count;

	if (item_pos.x < pos.x)
	{
		item_display_flg = false;
	}
	else
	{

		item_display_flg = true;
	}


	item_obj->SetPos(item_pos);
	item_obj->Update();
	//item_obj->SetAngle(item_angle);
}

void Item::Check_Item_Mirror(Vector3 pos, int count)
{
	mirror_count = count;

	if (count == count&& set_pos_flg == false)
	{
		item_pos.x = item_pos.x + (-60 * count);
		set_pos_flg = true;
	}

	Move(0.4f);

	move.x = 2.0f;
	//鏡の動かせるカウントが4以下なら足場を動かす
	if (count <= 5)
	{

		if (right == true)
		{
			//position.xを進める
			item_pos.x += move.x;
			move_x += move.x;
			if (move_x == ITEM_MAX_MOVE)
			{

				//position.x += 0.6f + (count*0.05f);
				right = false;
				move_x = 0;
			}
		}
		if (left == true)
		{
			//position.xを戻す
			item_pos.x -= move.x;
			move_x -= move.x;
			if (move_x == -ITEM_MAX_MOVE)
			{
				//position.x -= 0.6f+(count*0.05f);
				left = false;
				move_x = 0;
			}
		}
	}
	else
	{
		if (right == true)
		{
			item_pos.x += move.x;
			move_x += move.x;
			if (move_x == ITEM_MAX_MOVE)
			{
				right = false;
				move_x = 0;
			}
		}
	}


	//足場のポジションと鏡のポジションの比較　
	if (item_pos.x < pos.x)
	{
		mirror_check_flg = true; //足場を消す

	}
	else
	{
		mirror_check_flg = false;//足場を表示する
	}


	item_obj->SetPos(item_pos);
	--angle_timer;
	if (angle_timer>0)
	{
		item_angle.y += 0.1f;
		if (angle_timer <= 0)
		{
			angle_timer = 60;
		}

	}
	item_obj->Update();
}

void Item::Move(float max)
{
	static float speed = 0;
	//	軸の傾きの取得
	float axisX = KEY_GetAxisX()*0.001f;


	//	加速
	speed += 0.2f;
	if (speed > max) speed = max;

	//　軸の傾きと長さ
	float d = sqrtf(axisX*axisX);


	//	軸の傾きの遊び(適当)
	if (d < 0.4f) {
		speed = 0;
		//return false;
	}

	//	移動
	if (mirror_count != 5 && axisX<-0.040f&& left == false && right == false && left_1 == true)
	{
		left = true;
	}
	else if (mirror_count != 0 && axisX>0.040f&&left == false && right == false && right_1 == true)
	{
		right = true;
	}
}

Item::~Item()
{
	if (item_obj)
	{
		delete item_obj;
		item_obj = NULL;
	}
}

void Item::Update()
{
	item_obj->Update();
}

void Item::Render(iexShader * shader)
{
	item_obj->Render(shader, "Stealth");
}


void Item_Manager::Initialize(int stage_num, Vector3 pos, int count)
{
	stage_shader = new iexShader("DATA/SHADER/PhoneShader.fx");
	stage_state = stage_num;

	mirror_pos = pos;
	mirror_count = count;

	for (int i = 0; i < ITEM_MAX; i++)
	{
		item[i].pos = Vector3(-20.0f, -75.0f, -5.0f);
		item[i].scale = Vector3(ITEM_OBJ_SCALE, ITEM_OBJ_SCALE, ITEM_OBJ_SCALE);
		item[i].angle = Vector3(0.0f, 0.0f, 0.0f);
		item[i].display_flg = true;

		mirror_item[i].pos = Vector3(20.0f, -75.0f, -5.0f);
		mirror_item[i].scale = Vector3(ITEM_OBJ_SCALE, ITEM_OBJ_SCALE, ITEM_OBJ_SCALE);
		mirror_item[i].angle = Vector3(0.0f, 0.0f, 0.0f);
		mirror_item[i].display_flg = true;

		ITEM_OBJ[i] = NULL;
		MIRROR_ITEM_OBJ[i] = NULL;

	}
	Item_Map(stage_state);
}

Item_Manager::~Item_Manager()
{
	if (stage_shader)
	{
		delete stage_shader;
		stage_shader = NULL;
	}
	for (int i = 0; i < ITEM_MAX; i++)
	{
		delete ITEM_OBJ[i];
		ITEM_OBJ[i] = NULL;

		delete MIRROR_ITEM_OBJ[i];
		MIRROR_ITEM_OBJ[i] = NULL;
	}
}

void Item_Manager::Update(int camer_type)
{
	for (int i = 0; i < ITEM_MAX; i++)
	{
		if (ITEM_OBJ[i]->item_obj == NULL) continue;
		ITEM_OBJ[i]->Update();

		if (camer_type == 1 || camer_type == 4)
		{
			ITEM_OBJ[i]->Check_Item_Stage(mirror_pos, mirror_count);

		}

	}
	for (int i = 0; i < ITEM_MAX; i++)
	{
		if (MIRROR_ITEM_OBJ[i]->item_obj == NULL) continue;
		MIRROR_ITEM_OBJ[i]->Update();
		if (camer_type == 1 || camer_type == 4)
		{
			MIRROR_ITEM_OBJ[i]->Check_Item_Mirror(mirror_pos, mirror_count);
		}
	}

}

void Item_Manager::Render()
{
	for (int i = 0; i < ITEM_MAX; i++)
	{
		if (ITEM_OBJ[i]->item_obj == NULL) continue;
		if (ITEM_OBJ[i]->item_display_flg == false)
		{
			ITEM_OBJ[i]->Render(stage_shader);
		}

	}
	for (int i = 0; i < ITEM_MAX; i++)
	{
		if (MIRROR_ITEM_OBJ[i]->item_obj == NULL) continue;
		if (MIRROR_ITEM_OBJ[i]->mirror_check_flg == false)
		{
			MIRROR_ITEM_OBJ[i]->Render(stage_shader);
		}


	}
}

void Item_Manager::Item_Map(int state)
{
	switch (state)
	{
	case 0:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{

			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,ITEM_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,ITEM_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};
		Item_Set(item_map, mirror_item_map);
	}
	break;
	case 1:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{

			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
		};
		Item_Set(item_map, mirror_item_map);

	}
	break;
	case 2:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,ITEM_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,ITEM_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};
		Item_Set(item_map, mirror_item_map);

	}
	break;
	case 3:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{

			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,ITEM_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },
		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,ITEM_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};
		Item_Set(item_map, mirror_item_map);

	}
	break;
	case 4:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1 ,ITEM_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1 ,ITEM_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};
		Item_Set(item_map, mirror_item_map);

	}
	break;
	case 5:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{

			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },


		};
		Item_Set(item_map, mirror_item_map);

	}
	break;
	case 6:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{

			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, ITEM_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, ITEM_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};
		Item_Set(item_map, mirror_item_map);

	}
	break;
	case 7:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{

			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,ITEM_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,ITEM_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};
		Item_Set(item_map, mirror_item_map);

	}
	break;
	case 8:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{

			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};
		Item_Set(item_map, mirror_item_map);
		mirror_count = mirror->get_count();
		mirror_pos = mirror->get_pos();
	}
	break;

	case 9:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{

			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,ITEM_1 ,ITEM_1 ,ITEM_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, ITEM_1 ,ITEM_1 ,ITEM_1 ,ITEM_1, OBJ_NONE_1 },

		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,ITEM_1 ,ITEM_1 ,ITEM_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, ITEM_1 ,ITEM_1 ,ITEM_1 ,ITEM_1, OBJ_NONE_1 },

		};
		Item_Set(item_map, mirror_item_map);

	}
	break;
	case 10:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{

			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },
		};
		Item_Set(item_map, mirror_item_map);

	}
	break;

	case 11:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{

			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, ITEM_1 ,ITEM_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },

		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, ITEM_1 ,ITEM_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },
		};
		Item_Set(item_map, mirror_item_map);

	}
	break;
	case 12:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{

			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },
		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, ITEM_1 ,OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1, OBJ_NONE_1 },
		};
		Item_Set(item_map, mirror_item_map);
		mirror_count = mirror->get_count();
		mirror_pos = mirror->get_pos();
	}
	break;
	case 13:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{

			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1,     OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1 ,ITEM_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1, OBJ_NONE_1 },

		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1,     OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1 ,ITEM_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,ITEM_1, OBJ_NONE_1 },

		};
		Item_Set(item_map, mirror_item_map);

	}
	break;

	case 14:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{

			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1 ,ITEM_1, ITEM_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1 ,ITEM_1, ITEM_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ ITEM_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
		};
		Item_Set(item_map, mirror_item_map);
		mirror_count = mirror->get_count();
		mirror_pos = mirror->get_pos();
	}
	break;
	case 15:
	{
		//現実世界の足場の配置
		int item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{

			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
		};

		int mirror_item_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX] =
		{
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,ITEM_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
			{ OBJ_NONE_1, OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1 ,OBJ_NONE_1, OBJ_NONE_1 },
		};
		Item_Set(item_map, mirror_item_map);
		mirror_count = mirror->get_count();
		mirror_pos = mirror->get_pos();
	}
	break;
	}
}

void Item_Manager::Item_Set(int map[ITEM_STAGE_MAX][ITEM_STAGE_MAX], int mirror_map[ITEM_STAGE_MAX][ITEM_STAGE_MAX])
{
	for (int y = 0; y < ITEM_STAGE_MAX; y++)
	{
		for (int x = 0; x < ITEM_STAGE_MAX; x++)
		{
			if (map[y][x] == ITEM_1)
			{
				//二次元配列を一次元配列にする
				ITEM_OBJ[y * ITEM_STAGE_MAX + x] = new Item();
				item[y * ITEM_STAGE_MAX + x].type = ITEM_1;
				item[y * ITEM_STAGE_MAX + x].display_flg = false;
				ITEM_OBJ[y * ITEM_STAGE_MAX + x]->Initialize(item[y * ITEM_STAGE_MAX + x].type);
				ITEM_OBJ[y * ITEM_STAGE_MAX + x]->Set_Item(Vector3(item[y * ITEM_STAGE_MAX + x].pos.x + (-30.0f * x), item[y * ITEM_STAGE_MAX + x].pos.y + (30.0f * y), item[y * ITEM_STAGE_MAX + x].pos.z), item[y * ITEM_STAGE_MAX + x].scale, item[y*ITEM_STAGE_MAX + x].angle, item[y * ITEM_STAGE_MAX + x].display_flg);
			}

			if (mirror_map[y][x] == ITEM_1)
			{
				MIRROR_ITEM_OBJ[y * ITEM_STAGE_MAX + x] = new Item();
				mirror_item[y * ITEM_STAGE_MAX + x].type = ITEM_1;
				mirror_item[y * ITEM_STAGE_MAX + x].display_flg = false;
				MIRROR_ITEM_OBJ[y * ITEM_STAGE_MAX + x]->Initialize(mirror_item[y * ITEM_STAGE_MAX + x].type);
				MIRROR_ITEM_OBJ[y * ITEM_STAGE_MAX + x]->Set_Item(Vector3(mirror_item[y * ITEM_STAGE_MAX + x].pos.x + (30.0f * x), mirror_item[y * ITEM_STAGE_MAX + x].pos.y + (30.0f * y), mirror_item[y * ITEM_STAGE_MAX + x].pos.z), mirror_item[y * ITEM_STAGE_MAX + x].scale, mirror_item[y*ITEM_STAGE_MAX + x].angle, mirror_item[y * ITEM_STAGE_MAX + x].display_flg);

			}
			if (map[y][x] == OBJ_NONE_1)
			{
				ITEM_OBJ[y * ITEM_STAGE_MAX + x] = new Item();
				//二次元配列を一次元配列にする
				ITEM_OBJ[y * ITEM_STAGE_MAX + x]->item_obj = NULL;
			}

			if (mirror_map[y][x] == OBJ_NONE_1)
			{
				MIRROR_ITEM_OBJ[y * ITEM_STAGE_MAX + x] = new Item();

				MIRROR_ITEM_OBJ[y * ITEM_STAGE_MAX + x]->item_obj = NULL;

			}

		}

	}
}