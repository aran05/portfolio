#include "iextreme.h"
#include "tutorial.h"
#include "Player.h"


void Tutorial::Initialize()
{
	Window = new iex2DObj("DATA/BG/book.png");				//ウィンドウ画像の読み込み
	Font1 = new iex2DObj("DATA/BG/text1.png");				//文字画像の読み込み
	Font2 = new iex2DObj("DATA/BG/text2.png");				//文字画像の読み込み
	Font3 = new iex2DObj("DATA/BG/text4.png");				//文字画像の読み込み
	Font4 = new iex2DObj("DATA/BG/text5.png");				//文字画像の読み込み
	Font5 = new iex2DObj("DATA/BG/text6.png");				//文字画像の読み込み
	Jack = new iex2DObj("DATA/BG/a.png");
	Book = new iexMesh("DATA/StageDATA/book.IMO");			//本のモデル読み込み
	x = new iexMesh("DATA/StageDATA/x.IMO");				//アイコンモデル

	Font1_Pos = Vector3(540.0f, 70.0f, 0.0f);				//文字の位置
	Font1_Render_Size = Vector3(700.0f, 600.0f, 0.0f);		//文字の描画サイズ
	Font1_Image_Pos = Vector3(0.0f, 0.0f, 0.0f);			//文字の切り取り位置
	Font1_Width_Height = Vector3(1024.0f, 512.0f, 0.0f);	//文字の幅、高さ

	Font2_Pos = Vector3(600.0f, 240.0f, 0.0f);				//文字の位置
	Font2_Render_Size = Vector3(700.0f, 600.0f, 0.0f);		//文字の描画サイズ
	Font2_Image_Pos = Vector3(0.0f, 0.0f, 0.0f);				//文字の切り取り位置
	Font2_Width_Height = Vector3(1024.0f, 512.0f, 0.0f);	//文字の幅、高さ

	Font3_Pos = Vector3(570.0f, 130.0f, 0.0f);				//文字の位置
	Font3_Render_Size = Vector3(750.0f, 600.0f, 0.0f);		//文字の描画サイズ
	Font3_Image_Pos = Vector3(0.0f, 0.0f, 0.0f);				//文字の切り取り位置
	Font3_Width_Height = Vector3(1024.0f, 512.0f, 0.0f);	//文字の幅、高さ

	Font4_Pos = Vector3(520.0f, 400.0f, 0.0f);				//文字の位置
	Font4_Render_Size = Vector3(700.0f, 600.0f, 0.0f);		//文字の描画サイズ
	Font4_Image_Pos = Vector3(0.0f, 0.0f, 0.0f);				//文字の切り取り位置
	Font4_Width_Height = Vector3(1024.0f, 512.0f, 0.0f);	//文字の幅、高さ

	Jack_Pos = Vector3(-50.0f, 120.0f, 0.0f);
	Jack_Render_Size = Vector3(640.0f, 600.0f, 0.0f);
	Jack_Image_Pos = Vector3(0.0f, 0.0f, 0.0f);
	Jack_Width_Height = Vector3(1024.0f, 1024.0f, 0.0f);

	Book_Pos = Vector3(0.0f, 0.0f, 0.0f);				//本の位置初期化
	Book_Angle = Vector3(0.0f, 0.0f, 0.0f);				//本の向き初期化
	Book_Scale = Vector3(BOOK_SCALE, BOOK_SCALE, BOOK_SCALE);//本のサイズ初期化(仮で1)

	Book_Flg = false;									//本を取っていない
	Book_State = 0;										//最初はゼロで

	Book->SetPos(Book_Pos);								//本の位置をセット
	Book->SetAngle(Book_Angle);							//本の向きをセット
	Book->SetScale(Book_Scale);							//本のサイズをセット
}

Tutorial::~Tutorial()
{
	if (Book)
	{
		delete Book;
		Book = NULL;
	}

	if (Window)
	{
		delete Window;
		Window = NULL;
	}

	if (Font1)
	{
		delete Font1;
		Font1 = NULL;
	}

	if (Font2)
	{
		delete Font2;
		Font2 = NULL;
	}

	if (Font3)
	{
		delete Font3;
		Font3 = NULL;
	}

	if (Font4)
	{
		delete Font4;
		Font4 = NULL;
	}

	if (Font5)
	{
		delete Font5;
		Font5 = NULL;
	}


	if (Jack)
	{
		delete Jack;
		Jack = NULL;
	}

	if (x)
	{
		delete x;
		x = NULL;
	}

}


void Tutorial::Update()
{
	Book->Update();
	Book_Move();
	Book->SetPos(Book_Pos);								//本の位置をセット
	x->Update();
	x->SetPos(Book_Pos + Vector3(0.0f, 15.0f, 0.0f));
	x->SetScale(Vector3(8.0f, 8.0f, 8.0f));
	x->SetAngle(Vector3(0.0f, -1.56f, 0.0f));
}

void Tutorial::Render()
{
	if (Book_Flg == true && Book_State == 0)
	{
		Window->Render(WINDOW_POS_X, WINDOW_POS_Y, WINDOW_RENDER_X_SIZE, WINDOW_RENDER_Y_SIZE, WINDOW_IMAGE_X, WINDOW_IMAGE_Y, WINDOW_WIDTH_SIZE, WINDOW_HEIGHT_SIZE);
		Font1->Render((int)Font1_Pos.x, (int)Font1_Pos.y, (int)Font1_Render_Size.x, (int)Font1_Render_Size.y, (int)Font1_Image_Pos.x, (int)Font1_Image_Pos.y, (int)Font1_Width_Height.x, (int)Font1_Width_Height.y);
		Font4->Render((int)Font4_Pos.x, (int)Font4_Pos.y, (int)Font4_Render_Size.x, (int)Font4_Render_Size.y, (int)Font4_Image_Pos.x, (int)Font4_Image_Pos.y, (int)Font4_Width_Height.x, (int)Font4_Width_Height.y);
		Jack->Render((int)Jack_Pos.x, (int)Jack_Pos.y, (int)Jack_Render_Size.x, (int)Jack_Render_Size.y, (int)Jack_Image_Pos.x, (int)Jack_Image_Pos.y, (int)Jack_Width_Height.x, (int)Jack_Width_Height.y);
		Font5->Render(1000, 500, 256, 256, 0, 0, 256, 256);

	}

	if (Book_Flg == true && Book_State == 1)
	{
		Window->Render(WINDOW_POS_X, WINDOW_POS_Y, WINDOW_RENDER_X_SIZE, WINDOW_RENDER_Y_SIZE, WINDOW_IMAGE_X, WINDOW_IMAGE_Y, WINDOW_WIDTH_SIZE, WINDOW_HEIGHT_SIZE);
		Font2->Render((int)Font2_Pos.x, (int)Font2_Pos.y, (int)Font2_Render_Size.x, (int)Font2_Render_Size.y, (int)Font2_Image_Pos.x, (int)Font2_Image_Pos.y, (int)Font2_Width_Height.x, (int)Font2_Width_Height.y);
		Jack->Render((int)Jack_Pos.x, (int)Jack_Pos.y, (int)Jack_Render_Size.x, (int)Jack_Render_Size.y, (int)Jack_Image_Pos.x, (int)Jack_Image_Pos.y, (int)Jack_Width_Height.x, (int)Jack_Width_Height.y);
		Font5->Render(1000, 500, 256, 256, 0, 0, 256, 256);

	}

	if (Book_Flg == true && Book_State == 2)
	{
		Window->Render(WINDOW_POS_X, WINDOW_POS_Y, WINDOW_RENDER_X_SIZE, WINDOW_RENDER_Y_SIZE, WINDOW_IMAGE_X, WINDOW_IMAGE_Y, WINDOW_WIDTH_SIZE, WINDOW_HEIGHT_SIZE);
		Font3->Render((int)Font3_Pos.x, (int)Font3_Pos.y, (int)Font3_Render_Size.x, (int)Font3_Render_Size.y, (int)Font3_Image_Pos.x, (int)Font3_Image_Pos.y, (int)Font3_Width_Height.x, (int)Font3_Width_Height.y);
		Jack->Render((int)Jack_Pos.x, (int)Jack_Pos.y, (int)Jack_Render_Size.x, (int)Jack_Render_Size.y, (int)Jack_Image_Pos.x, (int)Jack_Image_Pos.y, (int)Jack_Width_Height.x, (int)Jack_Width_Height.y);
		Font5->Render(1000, 500, 256, 256, 0, 0, 256, 256);

	}

}

void Tutorial::Book_Render()
{
	x->Render();
	Book->Render();
}

void Tutorial::Book_Move()
{
	Book_Angle.y -= 0.05f;
	Book->SetAngle(Book_Angle);

	//Book_Pos.y += sinf(Book_Angle.y)* -0.2f;
}