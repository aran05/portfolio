#include "iextreme.h"
#include "system\Scene.h"
#include "system\Framework.h"

#include "source\Story_Ch2.h"
#include "sceneMain.h"

bool Story_Ch2::Initialize()
{
	s_05 = new iex2DObj("DATA\\Story\\05.png");
	s_06 = new iex2DObj("DATA\\Story\\06.png");
	s_07 = new iex2DObj("DATA\\Story\\07.png");

	skip = new iex2DObj("DATA/skip.png");
	alpha = 255;
	story_alpha5 = 255;
	story_alpha6 = 0;
	story_alpha7 = 0;
	story_timer = 0;

	//flg_3 = false;
	//flg_4 = false;
	//flg_7 = false;

	return true;
}

Story_Ch2::~Story_Ch2()
{
	if (s_05)
	{
		delete s_05;
		s_05 = NULL;
	}

	if (s_06)
	{
		delete s_06;
		s_06 = NULL;
	}

	if (s_07)
	{
		delete s_07;
		s_07 = NULL;
	}


	if (skip)
	{
		delete skip;
		skip = NULL;
	}

	//if (s_07)
	//{
	//	delete s_07;
	//	s_07 = NULL;
	//}
}

void Story_Ch2::Update()
{

	story_timer++;

	//flg_3 = true;

	if (KEY_Get(9) == 3)
	{
		//MainFrame->ChangeScene(new sceneMain());
		MainFrame->Pop_Scene();
	}

	if (story_timer >= 120 && story_timer <= 250)
	{
		story_alpha5-=2;
		story_alpha6+=2;
		if (story_alpha5 <= 0)
		{
			story_alpha5 = 0;
		}
		if (story_alpha6 >= 255)
		{
			story_alpha6 = 255;
		}
	}
	if (story_timer >= 300)
	{
		story_alpha6-=2;
		story_alpha7+=2;
		if (story_alpha6 <= 0)
		{
			story_alpha6 = 0;
		}

		if (story_alpha7 >= 255)
		{
			story_alpha7 = 255;
		}
	}



	//if (story_timer == 240)
	//{
	//	flg_6 = false;
	//	flg_7 = true;
	//}

	if (story_timer == 530)
	{
		MainFrame->Pop_Scene();
	}
	alpha -= 3;
}

void Story_Ch2::Render()
{
	s_07->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha7, 255, 255, 255));
	s_06->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha6, 255, 255, 255));
	s_05->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha5, 255, 255, 255));

	skip->Render(1000, 600, 300, 128, 0, 0, 512, 128, 0, ARGB(alpha, 255, 255, 255));
}