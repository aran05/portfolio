#include "iextreme.h"
#include "mirror_point.h"
#include "Player.h"

Mirror_Point::~Mirror_Point()
{

	if (map)
	{
		delete map;
		map = NULL;
	}
	
	if(haikei)
	{
		delete haikei;
		haikei = NULL;
	}

	if(mirror_shader)
	{
		delete mirror_shader;
		mirror_shader = NULL;
	}

	if (mirror_shader2)
	{
		delete mirror_shader2;
		mirror_shader2 = NULL;
	}
}

void Mirror_Point::Initialize(Vector3 pos)
{
	mirror_shader = new iexShader("DATA/SHADER/PhoneShader.fx");
	mirror_shader2 = new iexShader("DATA/SHADER/3DEx.fx");
	//鏡の世界用
	map = new iexMesh("DATA/StageDATA/mirror_obj.IMO");
	map->SetPos(pos.x,0,0);
	map->SetScale(1.0f, 10.0f, 10.0f);
	map->SetAngle(0, 0, 0);
	map->Update();

	inverted = false;
	right = false;
	left = false;
	check_wall_flg= false;
	

	haikei = new iexMesh("DATA/StageDATA/stage1.IMO");
	
	
	
	//動かない壁
	haikei->SetPos(0.0f, 0.0f,-400.0f);
	haikei->SetScale(1, 1, 1);
	haikei->SetAngle(0.0f,0.0f,0.0f);
	haikei->Update();


	

	move_x = 0.5f;
	set_move_x = 0;

	inverted = false;

}


//bool Mirror_Point::L_Hit_Block(const Vector3 & pos, Vector3 & local)
//{
//	Matrix mat = haikei[1]->TransMatrix;
//	D3DXMatrixInverse(&mat, NULL, &mat);
//
//	local.x = pos.x*mat._11 + pos.y*mat._21 + pos.z*mat._31 + mat._41;
//	local.y = pos.x*mat._12 + pos.y*mat._22 + pos.z*mat._32 + mat._42;
//	local.z = pos.x*mat._13 + pos.y*mat._23 + pos.z*mat._33 + mat._43;
//
//	Vector3 p = local + Vector3(-0.1f, -1.0f, 0.0f);
//	Vector3 v(-0.8f, 0, 0);
//	float d = 0.3f;
//
//	Vector3 out;
//
//	if (haikei[1]->RayPick(&out, &p, &v, &d) >= 0)
//	{
//		local = out;
//
//
//		player->player_pos.x += 0.5f;
//
//		return 0;
//	}
//
//
//	return 1;
//}
//
//bool Mirror_Point::R_Hit_Block(const Vector3 & pos, Vector3 & local)
//{
//
//	
//
//	Matrix mat = haikei[2]->TransMatrix;
//	D3DXMatrixInverse(&mat, NULL, &mat);
//
//	local.x = pos.x*mat._11 + pos.y*mat._21 + pos.z*mat._31 + mat._41;
//	local.y = pos.x*mat._12 + pos.y*mat._22 + pos.z*mat._32 + mat._42;
//	local.z = pos.x*mat._13 + pos.y*mat._23 + pos.z*mat._33 + mat._43;
//
//	Vector3 p = local + Vector3(0.1f, -1.0f, 0.0f);
//	Vector3 v(1.0f, 0, 0);
//	float d = 0.5f;
//
//	Vector3 out;
//
//	if (haikei[2]->RayPick(&out, &p, &v, &d) >= 0)
//	{
//		local = out;
//
//		
//		player->player_pos.x -= 0.5f;
//		
//		return 0;
//	}
//	
//
//	return 1;
//}

////プレイヤーが止まっている時だけしよう
//bool Mirror_Point::Hit_Wall(const Vector3 & pos, Vector3 & local)
//{
//	Matrix mat = haikei[2]->TransMatrix;
//	D3DXMatrixInverse(&mat, NULL, &mat);
//
//	local.x = pos.x*mat._11 + pos.y*mat._21 + pos.z*mat._31 + mat._41;
//	local.y = pos.x*mat._12 + pos.y*mat._22 + pos.z*mat._32 + mat._42;
//	local.z = pos.x*mat._13 + pos.y*mat._23 + pos.z*mat._33 + mat._43;
//
//	Vector3 p = local + Vector3(0.1f, -1.0f, 0.0f);
//	Vector3 v(1.0f, 0, 0);
//	float d = 0.3f;
//
//	Vector3 out;
//
//	Vector3 l;
//	Matrix ma = haikei[1]->TransMatrix;
//	D3DXMatrixInverse(&ma, NULL, &ma);
//
//	l.x = pos.x*ma._11 + pos.y*ma._21 + pos.z*ma._31 + ma._41;
//	l.y = pos.x*ma._12 + pos.y*ma._22 + pos.z*ma._32 + ma._42;
//	l.z = pos.x*ma._13 + pos.y*ma._23 + pos.z*ma._33 + ma._43;
//
//	Vector3 p2 = l + Vector3(-0.5f, -1.0f, 0.0f);
//	Vector3 v2(-1.0f, 0, 0);
//	float d2 = 1.0f;
//
//
//
//
//
//	if(inverted==false)
//	{
//		if (haikei[2]->RayPick(&out, &p, &v, &d) >= 0)
//		{
//			local = out;
//
//			player->wall_hit_flg = true;
//
//			return 0;
//		}
//	}
//	else
//	{
//		if (haikei[1]->RayPick(&out, &p2, &v2, &d2) >= 0)
//		{
//
//			
//			player->wall_hit_flg = true;
//			
//		
//
//			return 0;
//		}
//	}
//	
//	return 1;
//}

void Mirror_Point::Update(int type,int count)
{

	
	/*if(KEY_Get(KEY_C)==3)
	{
		inverted = !inverted;
	}*/
	haikei->SetPos(Vector3(0,0,-400.0f));
	haikei->Update();

	if(type==1)
	{
		map->SetPos(Vector3(0, 0,0));
	}
	else
	{
		map->SetPos(Vector3(0,0,0));
	}

	map->Update();

}

void Mirror_Point::Render()
{
	haikei->Render(mirror_shader2, "copy_fx2");
#ifdef _DEBUG
	char	str[64];
	wsprintf(str, "%d", (int)position2.x);
	IEX_DrawText(str, 10, 60, 200, 180, 0xFFFFFF00);
#endif
}
