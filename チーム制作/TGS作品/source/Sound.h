#pragma once
#include "iextreme.h"

class Sound
{
public:
	Sound() {};
	~Sound();

	bool Sound_Title();
	bool Sound_Main();
	bool Sound_Main_Volume();
	bool Sound_Main_Volume2();

	void Sound_Title_Release();
	void Sound_Main_Release();

	void Se_Initialize();
	void Se_Play(int num, bool loopFlg);
	void Se_Stop(int num);

	static Sound *getInstance() {
		static Sound instance;
		return &instance;
	}
};

enum SeNum
{
	SE_Kyozo,
	SE_Kyozo_Take,
	SE_Mirror,
	SE_Book,
	SE_Get,
	SE_Enter,
	SE_End,
};

#define SOUNDMANAGER (Sound::getInstance())