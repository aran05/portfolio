#include "iextreme.h"
#include "system\Scene.h"
#include "system\Framework.h"

#include "source\Story_Ch5.h"
#include "source\Sound.h"
#include "sceneMain.h"
#include "sceneTitle.h"


bool Story_Ch5::Initialize()
{
	s_14 = new iex2DObj("DATA\\Story\\14.png");
	s_15 = new iex2DObj("DATA\\Story\\15.png");
	s_16 = new iex2DObj("DATA\\Story\\16.png");
	s_01 = new iex2DObj("DATA\\Story\\01.png");
	s_16_2 = new iex2DObj("DATA\\Story\\16.png");

	//skip = new iex2DObj("DATA/skip.png");

	//flg_9 = false;
	//flg_11 = false;
	//flg_12 = false;
	//alpha = 255;
	story_alpha14 = 255;
	story_alpha15 = 0;
	story_alpha16 = 0;
	story_alpha16_2 = 0;
	story_alpha1 = 0;
	story_timer = 0;
	return true;
}

Story_Ch5::~Story_Ch5()
{
	if (s_14)
	{
		delete s_14;
		s_14 = NULL;
	}

	if (s_15)
	{
		delete s_15;
		s_15 = NULL;
	}

	if (s_16)
	{
		delete s_16;
		s_16 = NULL;
	}

	if (s_16_2)
	{
		delete s_16_2;
		s_16_2 = NULL;

	}

	if (s_01)
	{
		delete s_01;
		s_01 = NULL;
	}

	//if (skip)
	//{
	//	delete skip;
	//	skip = NULL;
	//}

	//if (s_04)
	//{
	//	delete s_04;
	//	s_04 = NULL;
	//}

}

void Story_Ch5::Update()
{
	//flg_9 = true;
	story_timer++;
	//if (KEY_Get(9) == 3)
	//{
	//	MainFrame->Pop_Scene();
	//}

	if (story_timer >= 120 && story_timer <= 150)
	{
		story_alpha14-=10;
		story_alpha15+= 10;
		//flg_11 = true;
		if (story_alpha14 <= 0)
		{
			story_alpha14 = 0;
		}
		if (story_alpha15 >= 255)
		{
			story_alpha15 = 255;
		}
		//flg_8 = false;
		//flg_9 = true;

		//count++;
	}

	if (story_timer >= 200 && story_timer <= 240)
	{
		story_alpha15 = 0;
		//story_alpha16 +=10;
		story_alpha16 = 255;
		//flg_11 = true;
		if (story_alpha15 <= 0)
		{
			story_alpha15 = 0;
		}
		if (story_alpha16 >= 255)
		{
			story_alpha16 = 255;
		}
		//flg_8 = false;
		//flg_9 = true;

		//count++;
	}
	if (story_timer >= 270 && story_timer <= 525)
	{
		story_alpha16--;
		story_alpha1++;
		//flg_11 = true;
		if (story_alpha16 <= 0)
		{
			story_alpha16 = 0;
		}
		if (story_alpha1 >= 255)
		{
			story_alpha1 = 255;
		}
		//flg_8 = false;
		//flg_9 = true;
		//count++;
	}
	if (story_timer >= 525)
	{
		story_alpha1-=4;
		story_alpha16_2+=4;
		//flg_11 = true;
		if (story_alpha1 <= 0)
		{
			story_alpha1 = 0;
		}
		if (story_alpha16_2 >= 255)
		{
			story_alpha16_2 = 255;
		}

	}

	//if (story_timer == 360) // ����180
	//{
	//	flg_3 = false;
	//	flg_4 = true;
	//}

	if (story_timer == 650) // ����240
	{
		MainFrame->ChangeScene(new sceneTitle());
	}
	//alpha -= 3;
}

void Story_Ch5::Render()
{
	s_16_2->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha16_2, 255, 255, 255));
	s_01->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha1, 255, 255, 255));
	s_16->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha16, 255, 255, 255));
	s_15->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha15, 255, 255, 255));
	s_14->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha14, 255, 255, 255));

	//if (flg_4 == true) s_04->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);

	//skip->Render(1000, 600, 300, 128, 0, 0, 512, 128, 0, ARGB(alpha, 255, 255, 255));
}