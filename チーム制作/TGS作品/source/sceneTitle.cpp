#include "iextreme.h"
#include "system\Framework.h"
#include "system\System.h"
#include "sceneTitle.h"
#include "source\Player.h"
#include "source\Camera.h"
#include "source\Stage.h"
#include "source\mirror.h"
#include "source\mirror_point.h"
#include "source\sceneMain.h"
#include "source\Goal.h"
#include "source\tutorial.h"
#include "source\Story.h"
#include "source\Story_Ch2.h"
#include "sceneFade.h"
#include "Sound.h"

bool sceneTitle::Initialize()
{
	//	環境設定
	iexLight::SetAmbient(0x808080);
	iexLight::SetFog(800, 1000, 0);

	Vector3 dir(1.0f, -1.0f, -0.5f);
	iexLight::DirLight(shader, 0, &dir, 0.8f, 0.8f, 0.8f);

	title = new iex2DObj("DATA\\t1.png");
	push = new iex2DObj("DATA\\BG\\PUSHA.png");
	//goal_count = 0;
	//tuto_flg = false;
	//	環境光


	//	平行光


	//プレイヤー初期化
	player = new Player();
	player->Initialize();
	//a = Vector3(player->player_angle.x, player->player_angle.y, player->player_angle.z);

	//鏡の初期化
	mirror = new MIRROR();
	mirror->Initialize();
	mirror_point = new Mirror_Point();
	mirror_point->Initialize(mirror->get_pos());

	//	カメラ設定
	camera = new Camera();
	camera->Initialize(2);


	//	ステージ読み込み
	stage_manager = new Stage_Manager();
	stage_manager->Initialize(0, mirror->get_pos(), mirror->Get_Mirror_Count());

	SOUNDMANAGER->Sound_Title();
	SOUNDMANAGER->Se_Initialize();
	//goal_manager = new Goal_Manager();
	//goal_manager->Initialize(goal_count);

	//tuto = new Tutorial();
	//tuto->Initialize();

	//color = 
	aplha = 255;
	aplha_flg = false;
	red = 255;
	green = 255;
	blue = 255;
	image_x = 1024;
	image_y = 512;
	pos_x = 50;
	pos_y = 0;
	se_count = 0;
	flg = false;
	flg2 = false;
	return true;
}


sceneTitle::~sceneTitle()
{
	if (player)
	{
		delete player;
		player = NULL;
	}

	if (camera)
	{
		delete camera;
		camera = NULL;
	}

	if (stage_manager)
	{
		delete stage_manager;
		stage_manager = NULL;
	}


	if (mirror)
	{
		delete mirror;
		mirror = NULL;;
	}

	if (mirror_point)
	{
		delete mirror_point;
		mirror_point = NULL;
	}

	if (title)
	{
		delete title;
		title = NULL;
	}
	if (push)
	{
		delete push;
		push = NULL;
	}
	SOUNDMANAGER->Sound_Title_Release();
	SOUNDMANAGER->~Sound();
}

void sceneTitle::Update()
{
	iexLight::PointLight(0, &Vector3(player->Get_Pos().x, player->Get_Pos().y, player->Get_Pos().z), 0.2f, 0.6f, 0.6f, 5);
	if (KEY_Get(KEY_X) == 3)
	{
		se_count++;
		flg = true;
		if (se_count == 1)
		{
			SOUNDMANAGER->Se_Play(SeNum::SE_Enter, FALSE);
		}
		//Scene* newScene = new sceneFade(sceneFade::fade_in, 60, 0x000000, new sceneMain); // 0x003D00
		//MainFrame->Push_Scene(new sceneFade(sceneFade::fade_out, 120, 0x000000, newScene));  // 0xD7DDFF
		//MainFrame->ChangeScene(new sceneMain());

	}
	if (aplha >= 0 && aplha_flg == false)
	{
		aplha -= 3;
	}
	if (aplha == 0)
	{
		aplha_flg = true;
	}
	if (aplha <= 255 && aplha_flg==true)
	{
		aplha += 3;
	}
	if (aplha == 255)
	{
		aplha_flg = false;
	}

	if (flg == true)
	{
		red-= 3;
		green -= 3;
		blue -= 3;
		if (red < 0 || green < 0 || blue < 0)
		{
			red = 0;
			green = 0;
			blue = 0;
		}
	}

	if (red == 0 && green == 0 && blue == 0)
	{
		image_x+= 28;
		image_y+= 28;
		pos_x -= 14;
		pos_y -= 14;
	}
	if (image_x > 4600 || image_y > 4600)
	{
		flg2 = true;
	}


	if (player->player_pos.y >= -150)
	{
		//	プレイヤー更新

		//player->Update();
		player->Anime();
		//	カメラ更新
		
		camera->Update(mirror->Get_Left(), mirror->Get_Right(), true);

		stage_manager->Update(1, mirror->Get_Left(), mirror->Get_Right());
		stage_manager->Hit_block(player->Get_Pos(), local);
		stage_manager->Up_Hit_block(player->Get_Pos(), local);

		//鏡側の区別モデル

		if (camera->Get_Camera_State() == 1)
		{
			mirror->Update(camera->Get_Camera_State());
			mirror_point->Update(camera->Get_Camera_State(), mirror->Get_Mirror_Count());
		}



		/*	if(camera->Get_Camera_State() == 0)
		{
		mirror_point->R_Hit_Block(player->Get_Pos(), local);
		mirror_point->L_Hit_Block(player->Get_Pos(), local);
		}
		else
		{
		mirror_point->Hit_Wall(player->Get_Pos(), local);
		}*/



		stage_manager->Set_Mirrror_Pos(mirror->get_pos());
		stage_manager->Set_Mirrror_Count(mirror->Get_Mirror_Count());
		stage_manager->Set_Camer_Type(camera->Get_Camera_State());
		

	}

	if (flg2 == true)
	{
		//Scene* newScene = new sceneFade(sceneFade::fade_in, 60, 0x000000, new sceneMain); // 0x003D00
		//MainFrame->Push_Scene(new sceneFade(sceneFade::fade_out, 120, 0x000000, newScene));  // 0xD7DDFF

		MainFrame->ChangeScene(new sceneMain());
		//MainFrame->ChangeScene(new Story());
	}
}

void sceneTitle::Render()
{
	camera->Activate();
	camera->Clear();

	//	ステージ描画
	stage_manager->Render();


	//	プレイヤー描画
	player->Render();

	mirror_point->Render();

	// 鏡描画
	mirror->Render();

	push->Render(200, 320, 512, 512, 0, 0, 512, 512, RS_COPY, ARGB(aplha, 255, 255, 255));
	title->Render(pos_x, pos_y, image_x, image_y, 0, 0, 1024, 512, RS_COPY,ARGB(255,red,green,blue));
}