#include "iextreme.h"
#include "system\Scene.h"
#include "system\Framework.h"

#include "source\Story_Ch4.h"
#include "sceneMain.h"

bool Story_Ch4::Initialize()
{
	s_11 = new iex2DObj("DATA\\Story\\11.png");
	s_12 = new iex2DObj("DATA\\Story\\12.png");
	s_13 = new iex2DObj("DATA\\Story\\13.png");
	s_16 = new iex2DObj("DATA\\Story\\16.png");
	s_17 = new iex2DObj("DATA\\Story\\17.png");
	//s_17 = new iex2DObj("DATA\\Story\\11.png");
	//s_18 = new iex2DObj("DATA\\Story\\12.png");
	//s_19 = new iex2DObj("DATA\\Story\\13.png");
	//s_20 = new iex2DObj("DATA\\Story\\02.png");
	//s_21 = new iex2DObj("DATA\\Story\\01.png");

	skip = new iex2DObj("DATA/skip.png");
	story_alpha11 = 255;
	story_alpha12 = 0;
	story_alpha13 = 0;
	story_alpha16 = 0;
	story_alpha17 = 0;

	alpha = 255;
	story_timer = 0;

	//flg_8 = false;
	//flg_9 = false;
	//flg_22 = false;
	//flg_23 = false;
	//flg_24 = false;
	//flg_25 = false;
	//flg_26 = false;
	//flg_27 = false;
	//flg_28 = false;
	//flg_29 = false;

	return true;
}

Story_Ch4::~Story_Ch4()
{
	if (s_11)
	{
		delete s_11;
		s_11 = NULL;
	}

	if (s_12)
	{
		delete s_12;
		s_12 = NULL;
	}

	if (s_13)
	{
		delete s_13;
		s_13 = NULL;
	}

	if (s_16)
	{
		delete s_16;
		s_16 = NULL;
	}

	if (s_17)
	{
		delete s_17;
		s_17 = NULL;
	}


	if (skip)
	{
		delete skip;
		skip = NULL;
	}

	//if (s_16)
	//{
	//	delete s_16;
	//	s_16 = NULL;
	//}

	//if (s_17)
	//{
	//	delete s_17;
	//	s_17 = NULL;
	//}

	//if (s_18)
	//{
	//	delete s_18;
	//	s_18 = NULL;
	//}

	//if (s_19)
	//{
	//	delete s_19;
	//	s_19 = NULL;
	//}

	//if (s_20)
	//{
	//	delete s_20;
	//	s_20 = NULL;
	//}

	//if (s_21)
	//{
	//	delete s_21;
	//	s_21 = NULL;
	//}
}

void Story_Ch4::Update()
{
	//flg_8 = true;
	story_timer++;

	if (KEY_Get(9) == 3)
	{
		//MainFrame->ChangeScene(new sceneMain());
		MainFrame->Pop_Scene();
	}

	if (story_timer >= 120 && story_timer <= 130)
	{
		story_alpha11-=30;
		story_alpha12+=30;
		//flg_9 = true;
		if (story_alpha11 <= 0)
		{
			story_alpha11 = 0;
		}
		if (story_alpha12 >= 255)
		{
			story_alpha12 = 255;
		}
		//count++;
	}

	if (story_timer >= 130 && story_timer <= 140)
	{
		story_alpha12-=30;
		story_alpha13+=30;
		//flg_9 = true;
		if (story_alpha12 <= 0)
		{
			story_alpha12 = 0;
		}
		if (story_alpha13 >= 255)
		{
			story_alpha13 = 255;
		}
		//count++;
	}

	if (story_timer >= 140 && story_timer <= 170)
	{
		story_alpha13 -= 30;
		story_alpha16 += 30;
		//flg_9 = true;
		if (story_alpha13 <= 0)
		{
			story_alpha13 = 0;
		}
		if (story_alpha16 >= 255)
		{
			story_alpha16 = 255;
		}
		//count++;
	}

	if (story_timer >= 180)
	{
		story_alpha16 -= 10;
		story_alpha17 += 10;
		//flg_9 = true;
		if (story_alpha16 <= 0)
		{
			story_alpha16 = 0;
		}
		if (story_alpha17 >= 255)
		{
			story_alpha17 = 255;
		}
		//count++;
	}


	//if (story_timer == 300)
	//{
	//	flg_21 = false;
	//	flg_22 = true;
	//}

	//if (story_timer == 340)
	//{
	//	flg_22 = false;
	//	flg_23 = true;
	//}

	//if (story_timer == 380)
	//{
	//	flg_23 = false;
	//	flg_24 = true;
	//}

	//if (story_timer == 420)
	//{
	//	flg_24 = false;
	//	flg_25 = true;
	//}

	//if (story_timer == 460)
	//{
	//	flg_25 = false;
	//	flg_26 = true;
	//}

	//if (story_timer == 580)
	//{
	//	flg_26 = false;
	//	flg_27 = true;
	//}

	//if (story_timer == 640)
	//{
	//	flg_27 = false;
	//	flg_28 = true;
	//}

	//if (story_timer == 820)
	//{
	//	flg_28 = false;
	//	flg_29 = true;
	//}

	if (story_timer == 300)
	{
		MainFrame->Pop_Scene();
	}
	alpha -= 3;
}

void Story_Ch4::Render()
{
	s_17->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha17, 255, 255, 255));
	s_16->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha16, 255, 255, 255));
	s_13->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha13, 255, 255, 255));
	s_12->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha12, 255, 255, 255));
	s_11->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha11, 255, 255, 255));
	//if (flg_22 == true) s_16->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_23 == true) s_17->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_24 == true) s_18->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_25 == true) s_19->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_26 == true) s_14->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_27 == true) s_20->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_28 == true) s_21->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_29 == true) s_19->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);

	skip->Render(1000, 600, 300, 128, 0, 0, 512, 128, 0, ARGB(alpha, 255, 255, 255));
}