#include "iextreme.h"
#include "system\Scene.h"
#include "system\Framework.h"
#include "sceneFade.h"

sceneFade::sceneFade(int type, int timer, DWORD color, Scene* new_scene) : type(type), timer(timer), timer_max(timer), next_scene(new_scene)
{
	this->color = color & 0x00FFFFFF;
}

bool sceneFade::Initialize()
{
	switch (type)
	{
	case fade_in:
		this->Stack_In(next_scene);
		next_scene->Initialize();
		next_scene = NULL;
		break;
	}

	return true;
};

void sceneFade::Update()
{
	stack->Update();

	timer--;
	if (timer < 0)
	{
		Scene* work;

		switch (type)
		{
		case fade_in:
			MainFrame->Pop_Scene();
			break;

		case fade_out:
			work = next_scene;
			next_scene = NULL;
			MainFrame->ChangeScene(work);
			break;
		}
		return;
	}

	BYTE alpha = 255;
	switch (type)
	{
	case fade_in:
		alpha = 255 * timer / timer_max;
		break;

	case fade_out:
		alpha = 255 * (timer_max - timer) / timer_max;
		break;
	}

	color = (alpha << 24) | (color & 0x00FFFFFF);
}

void sceneFade::Render()
{
	stack->Render();

	iexPolygon::Rect(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, RS_COPY, color);
}