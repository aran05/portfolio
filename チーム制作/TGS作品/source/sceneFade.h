#pragma once
class sceneFade : public Scene
{
public:
	enum
	{
		fade_in,
		fade_out
	};

protected:
	int type;
	int timer, timer_max;
	DWORD color;
	Scene* next_scene;

public:
	sceneFade(int type, int timer, DWORD color, Scene* new_scene);
	~sceneFade(){ if (next_scene) delete next_scene; };
	bool Initialize();
	void Update();
	void Render();
};