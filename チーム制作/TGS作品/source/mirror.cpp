#include "iextreme.h"
#include "mirror.h"
#include "Player.h"
#include "Global.h"
MIRROR *mirror = NULL;

void MIRROR::Initialize()
{
	//鏡にシェイダーを適用させる
	kagami_shader = new iexShader("DATA/SHADER/PhoneShader.fx");
	kagami_shader2 = new iexShader("DATA/SHADER/3DEx.fx");

	mirror_frame = new iexMesh(mirror_file[0]);
	mirror_obj = new iexMesh(mirror_file[1]);

	pos = Vector3(0, -MIRROR_SET_Y, -MIRROR_SET_Z);

	mirror_obj->SetPos(pos);
	mirror_obj->SetAngle(MIRROR_ANGLE2);
	mirror_obj->SetScale(MIRROR_SCALE2);
	mirror_obj->Update();

	//フレーム側にはシェイダーは使わない(いまのところ)
	mirror_frame->SetPos(pos + Vector3(-1, 0, -20));
	mirror_frame->SetAngle(MIRROR_ANGLE);
	mirror_frame->SetScale(MIRROR_SCALE);
	mirror_frame->Update();

	left = false;
	right = false;
	inverted = false;
	mirror_count = 0;
	SetPos = 0;
	//iexLight::DirectionalLight(0, &pos, 1.0f, 1.0f, 1.0f, 10);
	//kagami_shader->SetTexture2(mirror_obj->GetTexture(0));
}

void MIRROR::Update(int type)
{

	

	if (type == 1 || type == 4)
	{
		Move(0.4f);
		if (inverted == false)
		{


			if (left == true)
			{

				pos.x -= MIRROR_SPEED;
				SetPos -= MIRROR_SPEED;
				if (SetPos == -30)
				{
					left = false;
					SetPos = 0;
					//TODO 変更
					if (mirror_count < 5)
					{
						mirror_count += 1;

					}
				}

			}

			if (right == true)
			{

				pos.x += MIRROR_SPEED;
				SetPos += MIRROR_SPEED;
				if (SetPos == 30)
				{
					SetPos = 0;
					right = false;

					if (mirror_count > -5)
					{
						mirror_count -= 1;
					}
				}

			}
		}
		else
		{



			if (left == true)
			{

				pos.x -= MIRROR_SPEED;
				SetPos -= MIRROR_SPEED;
				if (SetPos == -30)
				{
					left = false;
					SetPos = 0;
					//TODO 変更

					if (mirror_count < 5)
					{
						mirror_count += 1;


					}
				}

			}

			if (right == true)
			{

				pos.x += MIRROR_SPEED;
				SetPos += MIRROR_SPEED;
				if (SetPos == 30)
				{
					SetPos = 0;
					right = false;


					if (mirror_count > -5)
					{
						mirror_count -= 1;
					}
				}

			}
		}

		if (pos.x >= MIRROR_MAX_POS)
		{
			pos.x = MIRROR_MAX_POS;
		}

		if (pos.x <= MIRROR_MIN_POS)
		{
			pos.x = MIRROR_MIN_POS;
		}

	}



	mirror_frame->SetPos(pos + Vector3(-1,0, -70));
	mirror_frame->Update();



	mirror_obj->SetPos(pos);
	mirror_obj->Update();
}

void MIRROR::Render()
{
	mirror_frame->Render(kagami_shader2, S_CFX2);
	mirror_obj->Render(kagami_shader, S_CFX);

	/*char	str[64];
	wsprintf(str, "%d%d", (int)left,(int)right);
	IEX_DrawText(str, 10,100, 300, 180, 0xFFFFFF00);*/
}

void MIRROR::Move(float max)
{
	static float speed = 0;
	//	軸の傾きの取得
	float axisX = KEY_GetAxisX()*0.001f;


	//	加速
	speed += 0.4f;
	if (speed > max) speed = max;

	//　軸の傾きと長さ
	float d = sqrtf(axisX*axisX);


	//	軸の傾きの遊び(適当)
	if (d < 0.4f) {
		speed = 0;
		//return false;
	}

	//	移動
	if (mirror_count != 5 && left_1 == true && axisX<=-0.1f&& left == false && right == false)
	{
		left = true;

	}
	else if (mirror_count != 0 && right_1 == true&&axisX>=0.1f&&left == false && right == false )
	{
		right = true;
	}

}




MIRROR::MIRROR()
{

}


MIRROR::~MIRROR()
{
	if (mirror_obj)
	{
		delete mirror_obj;
		mirror_obj = NULL;
	}

	if (mirror_frame)
	{
		delete mirror_frame;
		mirror_frame = NULL;
	}

	if (kagami_shader)
	{
		delete kagami_shader;
		kagami_shader = NULL;
	}

	if (kagami_shader2)
	{
		delete kagami_shader2;
		kagami_shader2 = NULL;
	}

}
