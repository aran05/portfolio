#include "iextreme.h"
#include "Effect.h"

iexParticle* particle = NULL;

void Particle::Effect_Init(void)
{
	particle = new iexParticle();
	particle->Initialize("DATA\\particle_2.png", MAX_PARTICLE);
}

void Particle::Effect_Release(void)
{
	if (particle)
	{
		particle->Release();

		delete particle;
		particle = NULL;
	}
}

void Particle::Effect_Update(void)
{
	particle->Update();
}

void Particle::Effect_Render(void)
{
	particle->Render();
}

void Particle::Effect(float x, float y, float z)
{
	Vector3 pos, move, power;

	for (int i = 0; i < 60; i++)
	{
		pos.x = x;
		pos.y = y;
		pos.z = z;

		move.x = (rand() % 200 - 50) * 0.006f;
		move.y = (rand() % 200 - 50) * 0.006f;
		move.z = (rand() % 200 - 50) * 0.006f;

		power.x = 0.0f;
		power.y = 0.009f;
		power.z = 0.0f;

		particle->Set(10, 0, 0.0f, 25, 0.0f, 10, 1.0f, &pos, &move, &power, 0.5f, 0.5f, 0.5f, 5.0f, RS_ADD);
	}
}

void Particle::Effect_2(float x, float y, float z)
{
	Vector3 pos, move, power;

	for (int i = 0; i < 30; i++)
	{
		//pos.x = x + (rand() % 200 - 100) * 0.2f;
		pos.x = x;
		pos.y = y + (rand() % 150-50) * 0.15f;
		pos.z = z + (rand() % 200 - 100) * 0.1f;
		//pos.z = z;

		//move.x = (rand() % 200 - 100) * 0.0001f;
		//move.y = (rand() % 200 - 100) * 0.0001f;
		//move.z = (rand() % 200 - 100) * 0.0001f;

		move.x = 0.0f;
		move.y = 0.0f;
		move.z = (rand() % 200 - 100) * 0.001f;

		//power.x = (rand() % 100) * -0.0002f;
		//power.y = (rand() % 100) * 0.0002f;
		power.x = 0.0f;
		power.y = 0.0f;
		power.z = 0.0f;

		//particle->Set(0, 0, 0.0f, 25, 0.0f, 10, 1.0f, &pos, &move, &power, 0.5f, 0.5f, 0.5f, 1.0f, RS_ADD);
		particle->Set(11, 0, 0.0f, 60, 0.0f, 40, 1.0f, &pos, &move, &power, 0.2f, 0.2f, 0.25f, 5.0f, RS_ADD);
	}
}

void Particle::Effect_3(float x, float y, float z)
{
	Vector3 pos, move, power;

	for (int i = 0; i < 10; i++)
	{
		pos.x = x + (rand() % 200 - 100) * 0.02f;
		pos.y = y + (rand() % 200 - 100) * 0.01f;
		pos.z = z + (rand() % 200 - 100) * 0.02f;

		move.x = (rand() % 200 - 100) * 0.0001f;
		move.y = (rand() % 200 - 100) * 0.0001f;
		move.z = (rand() % 200 - 100) * 0.0001f;

		power.x = (rand() % 100) * 0.0002f;
		power.y = (rand() % 100) * 0.0002f;
		power.z = 0.0f;

		particle->Set(0, 0, 0.0f, 60, 0.0f, 40, 1.0f, &pos, &move, &power, 0.5f, 0.5f, 0.5f, 5.0f, RS_ADD);
	}
}