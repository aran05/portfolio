#pragma once
class Mirror_Point
{
public:
	Mirror_Point() {};
	~Mirror_Point();

	void Initialize(Vector3 pos);

	/*bool L_Hit_Block(const Vector3 & pos, Vector3 & local);
	bool R_Hit_Block(const Vector3 & pos, Vector3 & local);
	bool Hit_Wall(const Vector3 & pos, Vector3 & local);*/

	void Update(int type, int count);	//	更新
	void Render();	//	描画

private:
	bool inverted; //反転フラグ falseの場合は左が現実,右が鏡,trueの場合は左が鏡,右が現実 
	bool check_wall_flg;
	bool right; //左方向に移動するフラグ/falseなら動かない　trueなら動く
	bool left;  //右方向に移動するフラグ/falseなら動かない　trueなら動く
	float move_x; 
	float set_move_x;
	iexMesh* map;
	iexShader *mirror_shader;
	iexShader *mirror_shader2;
	iexMesh *haikei;
	Vector3 position, position2;
};
