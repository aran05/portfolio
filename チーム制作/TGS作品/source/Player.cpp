#include	"iextreme.h"
#include	"system/system.h"
#include	"Player.h"

Player*	player = NULL;
extern Vector3		cpos, ctarget;

void Player::Initialize()
{
	//プレイヤーの初期化
	//player_obj = new iexMesh("DATA/CHR/sotai.IMO");						//モデルの読み込み
	player_obj = new iex3DObj("DATA/CHR/k6.IEM");
	virtual_obj = new iexMesh("DATA/StageDATA/floor.IMO");
	player_pos = Vector3(0.0f, 0.0f, 0.0f);
	player_move = Vector3(0.0f, 0.0f, 0.0f);
	player_scale = Vector3(PLAYER_SCALE, PLAYER_SCALE, PLAYER_SCALE);	//デファイン定義で調整可
	player_angle = Vector3(0.0f, 3.0f, 0.0f);
	player_direction = false;											//falseなら右向き:trueなら左向き
	player_jump_timer = 0;
	player_anime = 0;
	player_take_timer = 0;
	//セット関数
	player_obj->SetPos(player_pos);
	player_obj->SetAngle(player_angle);
	player_obj->SetScale(player_scale);
	player_obj->SetMotion(STAND);

	virtual_obj->SetPos(Vector3(0.0f, 0.0f, 0.0f));
	virtual_obj->SetAngle(Vector3(0.0f, 0.0f, 0.0f));
	virtual_obj->SetScale(Vector3(1.0f, 1.0f, 1.0f));
	//フラグの初期化
	floor_flg = false;
	goal_flg = false;
	reset_flg = false;
	wall_hit_flg = false;
	virtual_flg = false;
	angle = 0;
	//axisX = 0;
	Item_count = 0;
	move_flg = 0;
	anime_timer = 0;
	anime_flg = false;
	push_jump_key = false;
	
}

void Player::Reset()
{
	player_pos = Vector3(0.0f, 0.0f, 0.0f);
	player_move = Vector3(0.0f, 0.0f, 0.0f);
	player_scale = Vector3(PLAYER_SCALE, PLAYER_SCALE, PLAYER_SCALE);
	player_angle = Vector3(0.0f, 0.0f, 0.0f);

	player_obj->SetPos(player_pos);
	player_obj->SetAngle(player_angle);
	player_obj->SetScale(player_scale);
	player_obj->SetMotion(STAND);

	virtual_obj->SetPos(Vector3(0.0f, 0.0f, 0.0f));
	virtual_obj->SetAngle(Vector3(0.0f, 0.0f, 0.0f));
	virtual_obj->SetScale(Vector3(1.0f, 1.0f, 1.0f));
	//セット
	player_obj->SetPos(player_pos);
	player_direction = false;
	player_jump_timer = 0;
	player_anime = 0;
	player_take_timer = 0;


	//フラグの初期化
	floor_flg = false;
	goal_flg = false;
	reset_flg = false;
	wall_hit_flg = false;
	virtual_flg = false;
	angle = 0;
	//axisX = 0;
	Item_count = 0;
	move_flg = 0;
	anime_timer = 0;
	anime_flg = false;
	push_jump_key = false;

}

Player::~Player()
{
	if (player_obj)
	{
		delete player_obj;
		player_obj = NULL;
	}
	if (virtual_obj)
	{
		delete virtual_obj;
		virtual_obj = NULL;
	}
}

void Player::Update()
{
	if (goal_flg != true) {
		//足場が無いとき重力をかける
		if (floor_flg == false)
		{
			player_move.y = 0.5f;
			player_pos.y -= player_move.y;
		}
		if (player_anime != GOAL_ANIME || player_anime != GOAL_ANIME2)
		{
			if (virtual_flg == false)
			{
				Move(1.5f);
			}
			else
			{
				Move(1.0f);
			}
			
		}

		player_obj->Animation();

		//Player_Move();
		floor_flg = false;

		player_obj->SetAngle(player_angle);
		player_obj->SetPos(player_pos);
		player_obj->Update();

		virtual_obj->SetPos(player_pos);
		virtual_obj->SetAngle(player_angle);
		virtual_obj->SetScale(Vector3(1.0f, 1.0f, 1.0f));
		virtual_obj->Update();
	}
	//player_pos.x += player_move.x;
}

void Player::Render()
{
	player_obj->Render();

	if (virtual_flg == true)
	{
		Get_Virtual_Obj();
		virtual_obj->Render();
	}
}

void Player::Move(float max)
{
	if (player_anime != GOAL_ANIME2 || player_anime != GOAL_ANIME)
	{

		player_move.x = 0;
		//	軸の傾きの取得
		float axisX = KEY_GetAxisX()*0.001f;

		//	加速
		if (virtual_flg == false)
		{
			player_move.x += 1.5f;
		}
		else
		{
			player_move.x += 1.0f;
		}

		if (player_move.x > max) player_move.x = max;

		//　軸の傾きと長さ
		float d = sqrtf(axisX*axisX);

		//	軸の傾きの遊び(適当)
		if (d < 0.4f) {
			player_move.x = 0;
			//player_obj->SetMotion(STAND);
			//return false;
		}

		//	軸の傾き補正
		if (d > 1.0f) {
			axisX /= d;
		}

		////	カメラ前方方向取得と正規化
		//Vector3 f(matView._13, 0, matView._33);

		//d = sqrtf(f.x * f.x + f.z * f.z);
		//f /= d;

		////	カメラ左右方向算出
		//Vector3	r(f.x, 0, -f.z);
		////	移動方向算出
		//Vector3 m(0, 0, 0);
		//m = f*axisY + r*axisX;


		//	移動
		if (anime_timer == 0 && axisX >= 0.1f&&  floor_flg == true)
		{


			if (player_anime != JUMP && player_anime != TAKE&&player_anime != SLOPE)
			{
				player_angle = Vector3(0.0f, 1.56f, 0.0f);	//プレイヤーの向きを変える;
				player_direction = false;
				player_pos.x += player_move.x;
			}

		}
		else if (anime_timer == 0 && axisX <= -0.1f&&floor_flg == true)
		{

			if (player_anime != JUMP && player_anime != TAKE&&player_anime != SLOPE)
			{
				player_angle = Vector3(0.0f, -1.56f, 0.0f);	//プレイヤーの向きを変える;
				player_direction = true;
				player_pos.x -= player_move.x;
			}
		}
		//player_pos.z += m.z*speed;
		//	現在の向き
		//Vector3 v(0, 0, 0);
		//v.x = sinf(player_angle.x);
		//v.z = cosf(player_angle.z);

		////左右判定(外積)
		//float cross = m.x * v.z - m.z * v.x;

		////	角度補正計算(内積)
		//float dot = m.x * v.x + m.z * v.z;
		//float L1 = sqrtf(m.x * m.x + m.z * m.z);
		////float L2 = sqrtf( v.x*v.x + v.z*v.z );
		//dot /= L1;
		//dot = 1.0f - dot;
		//if (dot > 0.2f) dot = 0.2f;
		//if (floor_flg == true && player_anime != JUMP &&anime_timer == 0 && cross < -0.1 && player_anime != JUMP && player_anime != TAKE)
		//{
		//	player_direction = true;
		//}
		//else if (floor_flg == true && player_anime != JUMP &&anime_timer == 0 && cross > 0.1 && player_anime != JUMP&& player_anime != TAKE)
		//{
		//	player_direction = false;
		//	//player_angle = Vector3(0.0f, 1.56f, 0.0f);	//プレイヤーの向きを変える;
		//}


		if (player->move_flg != 0)
		{
			player_anime = SLOPE;
		}

		switch (player_anime)
		{
		case STAND:
			move_flg = 0;
			anime_timer = 0;
			if (player_move.x > 0 && floor_flg == true && virtual_flg == false)
			{
				player_anime = MOVE;
				player_obj->SetMotion(MOVE);
			}
			else if (player_move.x > 0 && floor_flg == true && virtual_flg == true)
			{
				player_anime = VIRTUAL_MOVE;
				player_obj->SetMotion(6);
			}

			if (KEY_Get(KEY_C) == 3 && floor_flg == true && virtual_flg == false)//TODO追加　virtual_flg=trueの時はジャンプしない
			{
				push_jump_key = true;
				player_obj->SetMotion(JUMP);
				player_anime = JUMP;						//プレイヤーの状態遷移を移動状態にする
				player_jump_timer = 25;						//滞空時間を設定
			}
			if (KEY_Get(KEY_B) == 3 && floor_flg == true)//TODO追加　virtual_flg=trueの時はジャンプしない
			{
				player_obj->SetMotion(TAKE);
				player_anime = TAKE;						//プレイヤーの状態遷移を移動状態にする
				player_take_timer = 65;
			}

			break;
		case MOVE:

			push_jump_key = false;
			if (player_move.x == 0 && virtual_flg == false)
			{
				player_anime = STAND;
				player_obj->SetMotion(STAND);
			}
			else if (player_move.x == 0 && virtual_flg == true)
			{
				player_anime = VIRTUAL_STAND;
				player_obj->SetMotion(5);
			}
			break;
		case JUMP:
			player_jump_timer--;				//滞空時間を減らす
			if (player_jump_timer > 0)
			{
				player_move.y = PLAYER_JUMP_POWER;	//プレイヤーにジャンプ力を加える
				player_pos.y += player_move.y;		//プレイヤーに移動量を加算
													//滞空時間が0になったら

			}
			if (player_jump_timer <= 0)
			{
				player_move.y = 0.2f;
				player_pos.y -= player_move.y;
				if (floor_flg == true)
				{
					player_obj->SetMotion(STAND);
					player_anime = STAND;			//プレイヤーの状態遷移を待機状態にする
				}

			}
			break;
		case TAKE:
			player_take_timer--;
			if (player_take_timer <= 0 && virtual_flg == false)
			{
				player_obj->SetMotion(STAND);
				player_anime = STAND;			//プレイヤーの状態遷移を待機状態にする

			}
			else if (player_take_timer <= 0 && virtual_flg == true)
			{
				player_obj->SetMotion(5);
				player_anime = VIRTUAL_STAND;			//プレイヤーの状態遷移を待機状態にする

			}

			break;
		case GOAL_ANIME:
			player_obj->SetMotion(MOVE);
			player_anime = GOAL_ANIME2;
			break;
		case SLOPE:
			reset_flg = true;


			if (move_flg == 1)
			{
				if (anime_flg == false)
				{
					if (virtual_flg == false)
					{
						player_obj->SetMotion(MOVE);
						//player_anime = STAND;			//プレイヤーの状態遷移を待機状態にする

					}
					else if (virtual_flg == true)
					{
						player_obj->SetMotion(6);
						//player_anime = VIRTUAL_STAND;			//プレイヤーの状態遷移を待機状態にする

					}
					anime_timer = 70;
					anime_flg = true;
				}
				player_pos.y += 0.1f;
				player_pos.x -= 0.5f;
				/*if (player_direction == false)
				{
				player_obj->SetMotion(MOVE);
				anime_timer = 60;
				move_flg = 2;
				}*/

				if (floor_flg == true)
				{

					anime_timer = 0;
					move_flg = 0;

					anime_flg = false;
				}
			}

			if (move_flg == 2)
			{

				if (anime_flg == false)
				{
					if (virtual_flg == false)
					{
						player_obj->SetMotion(MOVE);
						//player_anime = STAND;			//プレイヤーの状態遷移を待機状態にする

					}
					else if (virtual_flg == true)
					{
						player_obj->SetMotion(6);
						//player_anime = VIRTUAL_STAND;			//プレイヤーの状態遷移を待機状態にする

					}
					anime_timer = PLAYER_ANIME_TIEMR;
					anime_flg = true;
				}
				player->player_pos.x += 0.85f;
				player->player_pos.y += 1.5f;
				/*if(player_direction==true)
				{
				player_obj->SetMotion(MOVE);
				anime_timer = 50;
				move_flg = 1;
				}
				*/

			}

			if (move_flg == 3)
			{
				if (anime_flg == false)
				{
					if (virtual_flg == false)
					{
						player_obj->SetMotion(MOVE);
						//player_anime = STAND;			//プレイヤーの状態遷移を待機状態にする

					}
					else if (virtual_flg == true)
					{
						player_obj->SetMotion(6);
						//player_anime = VIRTUAL_STAND;			//プレイヤーの状態遷移を待機状態にする

					}
					anime_timer = 70;
					anime_flg = true;
				}
				player_pos.y += 0.1f;
				player_pos.x += 0.5f;

				/*if (player_direction == true)
				{
				player_obj->SetMotion(MOVE);
				anime_timer = 60;
				move_flg = 4;
				}*/

				if (floor_flg == true)
				{
					anime_timer = 0;
					move_flg = 0;

					anime_flg = false;
				}
			}

			if (move_flg == 4)
			{
				if (anime_flg == false)
				{
					if (virtual_flg == false)
					{
						player_obj->SetMotion(MOVE);
						//player_anime = STAND;			//プレイヤーの状態遷移を待機状態にする

					}
					else if (virtual_flg == true)
					{
						player_obj->SetMotion(6);
						//player_anime = VIRTUAL_STAND;			//プレイヤーの状態遷移を待機状態にする

					}
					anime_timer = PLAYER_ANIME_TIEMR;
					anime_flg = true;
				}
				player->player_pos.x -= 0.85f;
				player->player_pos.y += 1.5f;
				/*if (player_direction == false)
				{
				player_obj->SetMotion(MOVE);
				anime_timer = 60;
				move_flg = 3;
				}*/
			}


			if (move_flg == 0)
			{
				floor_flg = false;
				anime_flg = false;
				anime_timer = 0;
				move_flg = 0;
				player_anime = MOVE;
				if (virtual_flg == false)
				{
					player_obj->SetMotion(MOVE);
					//player_anime = STAND;			//プレイヤーの状態遷移を待機状態にする

				}
				else if (virtual_flg == true)
				{
					player_obj->SetMotion(6);
					//player_anime = VIRTUAL_STAND;			//プレイヤーの状態遷移を待機状態にする

				}
			}

			--anime_timer;
			if (anime_timer < 0)
			{

				anime_flg = false;
				anime_timer = 0;
				move_flg = 0;
				player_anime = MOVE;
				if (virtual_flg == false)
				{
					player_obj->SetMotion(MOVE);
					//player_anime = STAND;			//プレイヤーの状態遷移を待機状態にする

				}
				else if (virtual_flg == true)
				{
					player_obj->SetMotion(6);
					//player_anime = VIRTUAL_STAND;			//プレイヤーの状態遷移を待機状態にする

				}
			}
			break;
			//player_obj->SetScale(player_scale);

		case VIRTUAL_STAND:

			if (player_move.x > 0 && floor_flg == true && virtual_flg == false)
			{
				player_anime = MOVE;
				player_obj->SetMotion(MOVE);
			}
			else if (player_move.x > 0 && floor_flg == true && virtual_flg == true)
			{
				player_anime = VIRTUAL_MOVE;
				player_obj->SetMotion(6);
			}
			if (KEY_Get(KEY_B) == 3 && floor_flg == true)//TODO追加　virtual_flg=trueの時はジャンプしない
			{
				player_obj->SetMotion(TAKE);
				player_anime = TAKE;						//プレイヤーの状態遷移を移動状態にする
				player_take_timer = 65;
			}


			break;

		case VIRTUAL_MOVE:
			push_jump_key = false;
			if (player_move.x == 0 && virtual_flg == false)
			{
				player_anime = STAND;
				player_obj->SetMotion(STAND);
			}
			else if (player_move.x == 0 && virtual_flg == true)
			{
				player_anime = VIRTUAL_STAND;
				player_obj->SetMotion(5);
			}
			break;
		}



	}
	//return true;
}


void Player::Anime()
{

	if (floor_flg == false)
	{
		player_move.y = 0.2f;
		player_pos.y -= player_move.y;

	}
	reset_flg = false;
	floor_flg = false;
	player_angle = Vector3(0.0f, -1.56f, 0.0f);	//プレイヤーの向きを変える;
	player_obj->SetAngle(player_angle);
	player_obj->SetPos(player_pos);
	player_obj->Update();

	switch (player_anime)
	{
	case STAND://待機中
			   //待機中はプレイヤーの移動量を止める(X軸)
		player_move.x = 0.0f;
		break;
	case MOVE://移動中
		break;
	case JUMP://ジャンプ中
		if (floor_flg == true)
		{
			player_obj->SetMotion(STAND);
			player_anime = STAND;			//プレイヤーの状態遷移を待機状態にする
		}
		//}
		break;
	}

	player_obj->Animation();

}

void Player::Goal_Anime()
{
	player_angle = Vector3(0.0f, 0.0f, 0.0f);
	player_obj->SetAngle(player_angle);
	player_obj->Update();
	switch (player_anime)
	{
	case STAND:
		player_anime = GOAL_ANIME;
		//player_obj->SetMotion(GOAL);
		break;
	case MOVE:

		break;
	case GOAL_ANIME:
		player_obj->SetMotion(MOVE);
		player_anime = GOAL_ANIME2;
		break;
	case GOAL_ANIME2:
		player_obj->SetPos(player_pos);
		player_obj->SetScale(player_scale);
		player->Update();
		//player_anime = MOVE;
		//player_obj->SetMotion(MOVE);
		break;
	}

	player_obj->Animation();
}

void Player::Get_Virtual_Obj()
{
	Matrix Player_Mat = *player_obj->GetBone(14)*player_obj->TransMatrix;

	Matrix Virtual_Mat;
	const float scale = 2.0f;

	Virtual_Mat._11 = Player_Mat._11*scale;
	Virtual_Mat._12 = Player_Mat._12*scale;
	Virtual_Mat._13 = Player_Mat._13*scale;
	Virtual_Mat._14 = Player_Mat._14*scale;

	Virtual_Mat._21 = Player_Mat._21*scale;
	Virtual_Mat._22 = Player_Mat._22*scale;
	Virtual_Mat._23 = Player_Mat._23*scale;
	Virtual_Mat._24 = Player_Mat._24*scale;

	Virtual_Mat._31 = Player_Mat._31*scale;
	Virtual_Mat._32 = Player_Mat._32*scale;
	Virtual_Mat._33 = Player_Mat._33*scale;
	Virtual_Mat._34 = Player_Mat._34*scale;

	Virtual_Mat._41 = Player_Mat._41 + Virtual_Mat._11 * -1 + Player_Mat._21 * -3 + Virtual_Mat._31*2;
	Virtual_Mat._42 = Player_Mat._42 + Virtual_Mat._12 * -1 + Player_Mat._22 * -3 + Virtual_Mat._32*2;
	Virtual_Mat._43 = Player_Mat._43 + Virtual_Mat._13 * -1 + Player_Mat._23 * -3 + Virtual_Mat._33*2;
	Virtual_Mat._44 = Player_Mat._44 + Virtual_Mat._14 * -1 + Player_Mat._24 * -3 + Virtual_Mat._34*2;


	virtual_obj->TransMatrix = Virtual_Mat;

}