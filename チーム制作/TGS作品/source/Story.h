#pragma once

class Story : public Scene
{
private:
	iex2DObj* s_01;
	iex2DObj* s_02;
	iex2DObj* s_03;
	iex2DObj* s_04;
	iex2DObj* s_03_2;

	iex2DObj* skip;

	//bool flg;
	//bool flg_2;
	//bool flg_3;
	//bool flg_4;

	int alpha;
	int story_alpha1;
	int story_alpha2;
	int story_alpha3;
	int story_alpha4;
	int story_alpha3_2;
	int story_timer;
public:
	~Story();
	bool Initialize();
	void Update();
	void Render();
};