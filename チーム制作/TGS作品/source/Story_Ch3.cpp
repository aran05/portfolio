#include "iextreme.h"
#include "system\Scene.h"
#include "system\Framework.h"

#include "source\Story_Ch3.h"
#include "sceneMain.h"

bool Story_Ch3::Initialize()
{
	s_08 = new iex2DObj("DATA\\Story\\08.png");
	s_09 = new iex2DObj("DATA\\Story\\09.png");
	s_10 = new iex2DObj("DATA\\Story\\10.png");
	s_11 = new iex2DObj("DATA\\Story\\11.png");

	skip = new iex2DObj("DATA/skip.png");
	story_alpha8 = 255;
	story_alpha9 = 0;
	story_alpha10 = 0;
	story_alpha11 = 0;

	alpha = 255;
	story_timer = 0;

	//flg_5 = false;
	//flg_6 = false;
	//flg_7 = false;
	//flg_11 = false;
	//flg_12 = false;
	//flg_13 = false;
	//flg_14 = false;
	//flg_15 = false;
	//flg_16 = false;
	//flg_17 = false;
	//flg_18 = false;
	//flg_19 = false;

	return true;
}

Story_Ch3::~Story_Ch3()
{
	if (s_08)
	{
		delete s_08;
		s_08 = NULL;
	}

	if (s_09)
	{
		delete s_09;
		s_09 = NULL;
	}

	if (s_10)
	{
		delete s_10;
		s_10 = NULL;
	}
	if (s_11)
	{
		delete s_11;
		s_11 = NULL;
	}

	if (skip)
	{
		delete skip;
		skip = NULL;
	}

	//if (s_11)
	//{
	//	delete s_11;
	//	s_11 = NULL;
	//}

	//if (s_12)
	//{
	//	delete s_12;
	//	s_12 = NULL;
	//}

	//if (s_13)
	//{
	//	delete s_13;
	//	s_13 = NULL;
	//}
}

void Story_Ch3::Update()
{
	//flg_5 = true;
	story_timer++;

	if (KEY_Get(9) == 3)
	{
		//MainFrame->ChangeScene(new sceneMain());
		MainFrame->Pop_Scene();
	}

	if (story_timer >= 120 && story_timer <= 140)
	{
		story_alpha8-=20;
		story_alpha9+=20;
		//flg_6 = true;
		if (story_alpha8 <= 0)
		{
			story_alpha8 = 0;
		}
		if (story_alpha9 >= 255)
		{
			story_alpha9 = 255;
		}
		//flg_8 = false;
		//flg_9 = true;

		//count++;
	}

	if (story_timer >= 160 && story_timer <= 180)
	{
		story_alpha9-=25;
		story_alpha10+=25;
		//flg_7 = true;
		if (story_alpha9 <= 0)
		{
			story_alpha9 = 0;
		}
		if (story_alpha10 >= 255)
		{
			story_alpha10 = 255;
		}

		//flg_9 = false;
		//flg_10 = true;
	}

	if (story_timer >= 200)
	{
		story_alpha10-=25;
		story_alpha11+=25;
		//flg_7 = true;
		if (story_alpha10 <= 0)
		{
			story_alpha10 = 0;
		}
		if (story_alpha11 >= 255)
		{
			story_alpha11 = 255;
		}

		//flg_9 = false;
		//flg_10 = true;
	}

	//if (story_timer == 240)
	//{
	//	flg_10 = false;
	//	flg_11 = true;
	//}

	//if (story_timer == 250)
	//{
	//	flg_11 = false;
	//	flg_12 = true;
	//}

	//if (story_timer == 310)
	//{
	//	flg_12 = false;
	//	flg_13 = true;
	//}

	//if (story_timer == 420)
	//{
	//	flg_13 = false;
	//	flg_14 = true;
	//}

	//if (story_timer == 480)
	//{
	//	flg_14 = false;
	//	flg_15 = true;
	//}

	//if (story_timer == 540)
	//{
	//	flg_15 = false;
	//	flg_16 = true;
	//}

	//if (story_timer == 600)
	//{
	//	flg_16 = false;
	//	flg_17 = true;
	//}

	//if (story_timer == 660)
	//{
	//	flg_17 = false;
	//	flg_18 = true;
	//}

	//if (story_timer == 780)
	//{
	//	flg_18 = false;
	//	flg_19 = true;
	//}

	if (story_timer == 250)
	{
		MainFrame->Pop_Scene();
	}
	alpha -= 3;
}

void Story_Ch3::Render()
{
	s_11->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha11, 255, 255, 255));

	s_10->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha10, 255, 255, 255));

	s_09->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha9, 255, 255, 255));

	s_08->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha8, 255, 255, 255));

	//if (flg_11 == true) s_09->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_12 == true) s_10->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_13 == true) s_09->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_14 == true) s_08->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_15 == true) s_11->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_16 == true) s_12->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_17 == true) s_11->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_18 == true) s_12->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_19 == true) s_13->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);

	skip->Render(1000, 600, 300, 128, 0, 0, 512, 128, 0, ARGB(alpha, 255, 255, 255));
}