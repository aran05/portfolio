#pragma once
class Camera : public iexView
{
private:
	Vector3 camera_pos;
	Vector3 stage_pos;
	Vector3 camera_pos_start;
	Vector3 camera_pos_end;
	Vector3 camera_pos2;
	Vector3 camera_target;
	Vector3 camera_move;
	int camera_state;
	bool push_x;
	bool Book_flg;
public:
	int goal_timer;
	bool goal_flg;

	void Initialize(int state);
	void Update(bool left, bool right, bool book);
	int Get_Camera_State() { return camera_state; }
	void Set_Camera_State(int state) { camera_state = state; }
	void Set_Stage_Pos(Vector3 p) { stage_pos = p; }
	Vector3 Get_Stage_Pos() { return stage_pos; }
	Vector3 Get_Camera_Pos() { return camera_pos; }
	Vector3 Get_Camera_Target() { return camera_target; }
	Vector3 Camera_Move(Vector3 pos, Vector3 target, float speed);
};