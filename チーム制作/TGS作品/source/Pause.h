#pragma once

class Pause : public Scene
{
private:
	iex2DObj* pause;

	iex2DObj* font;
	iex2DObj* font2;
	int state;
	bool up, down;
	int key_timer;
public:
	~Pause();

	bool Initialize();
	void Update();
	void Render();
	void Move(float max);
	
};