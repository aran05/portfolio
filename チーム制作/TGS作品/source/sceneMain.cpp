﻿#include	"iextreme.h"
#include	"system/system.h"
#include  "system\Framework.h"

#include	"Player.h"
#include	"Camera.h"
#include	"sceneMain.h"
#include	"Stage.h"
#include	"mirror.h"
#include	"mirror_point.h"
#include	"Goal.h"
#include	"Sound.h"
#include	"tutorial.h"

#include	"Effect.h"
#include	"Item.h"
#include  "Pause.h"
#include  "Global.h"
#include  "Story.h"
#include  "Story_Ch2.h"
#include  "Story_Ch3.h"
#include  "Story_Ch4.h"
#include  "Story_Ch5.h"
#include  "sceneFade.h"

//*****************************************************************************************************************************
//
//	グローバル変数
//
//*****************************************************************************************************************************
//	カメラ用
//iexView*	view;
Vector3		cpos, ctarget;
int goal_count;
bool reset;
//	ステージ用
//*****************************************************************************************************************************
//
//	初期化
//
//*****************************************************************************************************************************

bool sceneMain::Initialize()
{
	jack = new iex2DObj("DATA\\jack.png");

	stage_num = new iex2DObj("DATA\\stage_num.png");

	//	環境設定
	iexLight::SetAmbient(0x505050);
	iexLight::SetFog(800, 1000, 0);

	Vector3 dir(1.0f, -1.0f, -0.5f);
	iexLight::DirLight(shader, 1, &dir, 1.0f, 1.0f, 1.0f);
	story_count = 0;
	/*if (goal_count != 0)
	{
	Reset();
	}*/


	goal_count = STAGE_NUM;
	tuto_flg = false;
	reset = false;
	story_flg = false;

	eff_flg = false;
	eff_flg2 = false;
	eff_flg3 = false;

	fe_2 = false;
	fe_2_timer = 0;
	//	環境光

	//プレイヤー初期化
	player = new Player();
	player->Initialize();

	//	カメラ設定
	camera = new Camera();
	camera->Initialize(0);



	//鏡の初期化
	mirror = new MIRROR();
	mirror->Initialize();
	//鏡の位置を決める//xを30ずつ変化する-25〜0の値を入れる
	mirror->set_pos(Vector3(0, -MIRROR_SET_Y, -MIRROR_SET_Z));
	//鏡のカウントをセット0〜5まで上でセットした値によって変化
	mirror->set_count(0);

	mirror_point = new Mirror_Point();
	mirror_point->Initialize(mirror->get_pos());
	mirror_point->Update(camera->Get_Camera_State(), mirror->Get_Mirror_Count());

	//	ステージ読み込み
	stage_manager = new Stage_Manager();
	stage_manager->Initialize(goal_count, mirror->get_pos(), mirror->Get_Mirror_Count());//TODO引数を追加鏡の位置とカウント
	stage_manager->Update(0, mirror->Get_Left(), mirror->Get_Right());//TODO初期設定で値を変化させる用の引数を追加 0なら初期化 1なら通常
																	  //goal_manager = new Goal_Manager();
	GOAL_MANAGER->Initialize(goal_count);

	item_manager = new Item_Manager();
	item_manager->Initialize(goal_count, mirror->get_pos(), mirror->Get_Mirror_Count());
	item_manager->Update(1);
	PARTICLEMANAGER->Effect_Init();
	SOUNDMANAGER->Se_Initialize();
	SOUNDMANAGER->Sound_Main();
	anime_count = 0;
	goal_timer = 0;

	x = new iexMesh("DATA/StageDATA/x.IMO");
	x->SetPos(Vector3(0, 0, 0));
	x->SetScale(Vector3(6.0f, 6.0f, 6.0f));
	x->SetAngle(Vector3(0.0f, 3.58f, 0.0f));
	x->Update();

	aplha = 255;
	aplha_flg = false;

	fe = false;
	fe_2 = false;
	fe_image[0] = new iex2DObj("DATA/feid.png");
	fe_image[1] = new iex2DObj("DATA/Story/16.png");
	fe_image[2] = new iex2DObj("DATA/BG/r1.png");
	fe_image[3] = new iex2DObj("DATA/BG/r2.png");
	//fe_image[1] = new iex2DObj("DATA/1.png");
	pause = new iex2DObj("DATA/pause.png");
	y = 0;
	Story_Set();
	return true;
}

void sceneMain::Reset()
{
	{
		if(player)
		{
			delete player;
			player = NULL;
		}
		if (camera)
		{
			delete camera;
			camera = NULL;
		}
		if (stage_manager)
		{
			delete stage_manager;
			stage_manager = NULL;
		}


		if (mirror)
		{
			delete mirror;
			mirror = NULL;
		}

		if (mirror_point)
		{
			delete mirror_point;
			mirror_point = NULL;
		}

		if (goal_manager)
		{
			delete goal_manager;
			goal_manager = NULL;
		}

		if (item_manager)
		{
			delete item_manager;
			item_manager = NULL;
		}

		if (jack)
		{
			delete jack;
			jack = NULL;
		}

		if (x)
		{
			delete x;
			x = NULL;
		}
	}
	reset = false;
	fe_2 = false;
	//if (tuto)
	//{
	//	delete tuto;
	//	tuto = NULL;;
	//}

	//	カメラ設定
	camera = new Camera();
	camera->Initialize(0);

	//プレイヤー初期化
	player = new Player();
	player->Initialize();
	
	//鏡の初期化
	mirror = new MIRROR();
	mirror->Initialize();
	mirror->set_pos(Vector3(0, -MIRROR_SET_Y, -MIRROR_SET_Z));
	//鏡のカウントをセット0〜5まで上でセットした値によって変化
	mirror->set_count(0);
	mirror_point = new Mirror_Point();
	mirror_point->Initialize(mirror->get_pos());

	//	ステージ読み込み
	stage_manager = new Stage_Manager();
	stage_manager->Initialize(goal_count, mirror->get_pos(), mirror->Get_Mirror_Count());//TODO引数を追加鏡の位置とカウント
	stage_manager->Update(0, mirror->Get_Left(), mirror->Get_Right());//TODO初期設定で値を変化させる用の引数を追加 0なら初期化 1なら通常

	player->player_scale = Vector3(PLAYER_SCALE, PLAYER_SCALE, PLAYER_SCALE);
	//goal_manager = new Goal_Manager();
	GOAL_MANAGER->~Goal_Manager();
	GOAL_MANAGER->Initialize(goal_count);

	item_manager = new Item_Manager();
	item_manager->Initialize(goal_count, mirror->get_pos(), mirror->Get_Mirror_Count());
	item_manager->Update(1);

	tuto_flg = false;
	story_flg = false;
	eff_flg = false;
	eff_flg2 = false;
	eff_flg3 = false;

	PARTICLEMANAGER->Effect_Release();
	PARTICLEMANAGER->Effect_Init();
	anime_count = 0;
	goal_timer = 0;

	x = new iexMesh("DATA/StageDATA/x.IMO");
	x->SetPos(Vector3(0, 0, 0));
	x->SetScale(Vector3(6.0f, 6.0f, 6.0f));
	x->SetAngle(Vector3(0.0f, 3.58f, 0.0f));
	x->Update();
	jack = new iex2DObj("DATA\\jack.png");
	SOUNDMANAGER->Sound_Main_Release();
	SOUNDMANAGER->Sound_Main();

	left_1 = true;
	right_1 = true;
	y = 0;
	Story_Set();
	//tuto = new Tutorial();
	//tuto->Initialize();
}

sceneMain::~sceneMain()
{
	//	プレイヤー解放								
	if (player)
	{
		delete player;
		player = NULL;
	}

	if (camera)
	{
		delete camera;
		camera = NULL;
	}

	if (stage_manager)
	{
		delete stage_manager;
		stage_manager = NULL;
	}


	if (mirror)
	{
		delete mirror;
		mirror = NULL;;
	}

	if (mirror_point)
	{
		delete mirror_point;
		mirror_point = NULL;
	}


	if (item_manager)
	{
		delete item_manager;
		item_manager = NULL;
	}

	GOAL_MANAGER->~Goal_Manager();
	PARTICLEMANAGER->Effect_Release();
	SOUNDMANAGER->Sound_Main_Release();
	SOUNDMANAGER->~Sound();

	if (pause)
	{
		delete pause;
		pause = NULL;
	}

	if (jack)
	{
		delete jack;
		jack = NULL;
	}

	if (x)
	{
		delete x;
		x = NULL;
	}

	if (fe_image[0])
	{
		delete fe_image[0];
		fe_image[0] = NULL;
	}

	if (stage_num)
	{
		delete stage_num;
		stage_num = NULL;
	}

	//SOUNDMANAGER->~Sound();
}

//*****************************************************************************************************************************
//
//		更新
//
//*****************************************************************************************************************************


void	sceneMain::Update()
{
	Story_Set();
	if (player->player_pos.y >= -100)
	{


		stage_manager->Hit_block(player->Get_Pos(), local);
		//	プレイヤー更新
		if (camera->Get_Camera_State() == 0 && stage_manager->tutorial->Book_Flg == false)
		{

			player->Update();
			stage_manager->tutorial->Update();
		}


		//	カメラ更新

		camera->Update(mirror->Get_Left(), mirror->Get_Right(), stage_manager->tutorial->Book_Flg);

		//プレイヤーとアイテムの当たり判定の処理
		if (camera->Get_Camera_State() == 0)
		{
			Vector3	pc = player->Get_Pos();
			Vector3	ps = player->player_scale;
			if (player->move_flg == 0)
			{
				for (int i = 0; i < ITEM_MAX; i++)
				{
					Vector3	ic = item_manager->ITEM_OBJ[i]->item_pos;
					Vector3	is = item_manager->ITEM_OBJ[i]->item_scale + Vector3(7, 7,7);

					if (item_manager->MIRROR_ITEM_OBJ[i]->item_type == ITEM_1&&ColAABB(pc, ps, ic, is))
					{
						if (item_manager->ITEM_OBJ[i]->delete_flg == false && item_manager->ITEM_OBJ[i]->item_display_flg == false && item_manager->MIRROR_ITEM_OBJ[i]->mirror_check_flg == false)
						{
							SOUNDMANAGER->Se_Play(SeNum::SE_Get, FALSE);
							player->Item_count -= 2;
							item_manager->ITEM_OBJ[i]->delete_flg = true;
							item_manager->MIRROR_ITEM_OBJ[i]->delete_flg = true;
						}
						if (item_manager->ITEM_OBJ[i]->item_display_flg == false && item_manager->MIRROR_ITEM_OBJ[i]->mirror_check_flg == false)
						{
							delete	item_manager->ITEM_OBJ[i]->item_obj;
							item_manager->ITEM_OBJ[i]->item_obj = NULL;
							delete	item_manager->MIRROR_ITEM_OBJ[i]->item_obj;
							item_manager->MIRROR_ITEM_OBJ[i]->item_obj = NULL;
						}


					}
				}
			}
			if (player->move_flg == 0)
			{
				for (int i = 0; i < ITEM_MAX; i++)
				{
					Vector3	mc = item_manager->MIRROR_ITEM_OBJ[i]->item_pos;
					Vector3	ms = item_manager->MIRROR_ITEM_OBJ[i]->item_scale + Vector3(7,7,7);

					if (item_manager->MIRROR_ITEM_OBJ[i]->item_type == ITEM_1&&ColAABB(pc, ps, mc, ms))
					{
						if (item_manager->MIRROR_ITEM_OBJ[i]->delete_flg == false && item_manager->MIRROR_ITEM_OBJ[i]->item_display_flg == false && item_manager->MIRROR_ITEM_OBJ[i]->mirror_check_flg == false)
						{
							SOUNDMANAGER->Se_Play(SeNum::SE_Get, FALSE);
							player->Item_count -= 2;
							item_manager->ITEM_OBJ[i]->delete_flg = true;
							item_manager->MIRROR_ITEM_OBJ[i]->delete_flg = true;
						}
						if (item_manager->ITEM_OBJ[i]->item_display_flg == false && item_manager->MIRROR_ITEM_OBJ[i]->mirror_check_flg == false)
						{
							delete	item_manager->ITEM_OBJ[i]->item_obj;
							item_manager->ITEM_OBJ[i]->item_obj = NULL;
							delete	item_manager->MIRROR_ITEM_OBJ[i]->item_obj;
							item_manager->MIRROR_ITEM_OBJ[i]->item_obj = NULL;
						}
					}
				}
			}
			//プレイヤーと本の当たり判定
			{
				Vector3	pc = player->Get_Pos();
				Vector3	ps = player->player_scale + Vector3(3, 4, 3);

				Vector3 bc = stage_manager->tutorial->Book->GetPos() + Vector3(-2, 0, 0);
				Vector3 bs = stage_manager->tutorial->Book->GetScale() + Vector3(15, 15, 15);


				if (KEY_Get(KEY_B) == 3 && ColAABB(pc, ps, bc, bs) && stage_manager->tutorial->Book_Flg == false)
				{
					stage_manager->tutorial->Book_Flg = true;
					SOUNDMANAGER->Se_Play(SeNum::SE_Book, FALSE);
				}
				else if (KEY_Get(KEY_B) == 3 && stage_manager->tutorial->Book_Flg == true)
				{
					stage_manager->tutorial->Book_Flg = false;
				}

			}

		}


		stage_manager->Update(1, mirror->Get_Left(), mirror->Get_Right());
		if (camera->Get_Camera_State() == 0)
		{
			stage_manager->Up_Hit_block(player->Get_Pos(), local);
		}
		item_manager->Update(camera->Get_Camera_State());
		item_manager->Set_Mirrror_Count(mirror->Get_Mirror_Count());
		item_manager->Set_Mirrror_Pos(mirror->get_pos());



		//鏡側の区別モデル
		if (camera->Get_Camera_State() == 1 || camera->Get_Camera_State() == 4)
		{
			mirror->Update(camera->Get_Camera_State());
			mirror_point->Update(camera->Get_Camera_State(), mirror->Get_Mirror_Count());
		}


		
	

		stage_manager->Set_Mirrror_Pos(mirror->get_pos());
		stage_manager->Set_Mirrror_Count(mirror->Get_Mirror_Count());
		stage_manager->Set_Camer_Type(camera->Get_Camera_State());

		GOAL_MANAGER->Update();

		if (camera->Get_Camera_State() == 0 && player->push_jump_key == false)
		{
			{
				Vector3	pc = player->Get_Pos();
				Vector3	ps = player->player_scale + Vector3(3, 4, 3);

				for (int i = 0; i < ITEM_MAX; i++)
				{
					if (GOAL_MANAGER->STAGE_OBJ[i]->stage_obj == NULL) continue;
					if (GOAL_MANAGER->STAGE_OBJ[i]->stage_type == Goal)
					{
						Vector3	gc = GOAL_MANAGER->STAGE_OBJ[i]->goal_pos;
						Vector3	gs = GOAL_MANAGER->STAGE_OBJ[i]->goal_scale;
						if (player->Item_count == 0 && ColAABB(pc, ps, gc, gs))
						{
							player->goal_flg = true;
						}
					}

				}


			}
		}

		if (player->goal_flg == true && player->push_jump_key == false)
		{

			if (anime_count == 0)
			{
				player->player_anime = GOAL_ANIME;
				player->player_angle = Vector3(0, 0, 0);
				anime_count = 1;
				goal_timer = 60;
			}

			player->Goal_Anime();
			player->player_pos.z += 0.15f;
			player->player_scale.x -= 0.002f;
			player->player_scale.y -= 0.002f;
			player->player_scale.z -= 0.002f;

			--goal_timer;
			//次のステージへ
			if (goal_count > 15)
			{
				goal_count = 0;
			}

			if (goal_timer < 10)
			{
				if (goal_count != 14)
				{
					fe = true;
				}
				else if (goal_count == 14)
				{
					goal_count += 1;
				}

			}


			if (goal_timer < 0)
			{
				goal_count += 1;
				goal_timer = 10;
				Reset();//プレイヤーとカメラ以外をデリートして初期化する
			}
		}
		if (fe == true)
		{
			--goal_timer;
			if (goal_timer < 0)
			{
				fe = false;
			}
		}


		if (camera->Get_Camera_State() == 0)
		{
			left_1 = false;
			right_1 = false;
			stage_manager->L_Hit_block(player->Get_Pos(), local);
			stage_manager->R_Hit_block(player->Get_Pos(), local);
		}
		GOAL_MANAGER->Set_Mirrror_Pos(mirror->get_pos());
		GOAL_MANAGER->Set_Mirrror_Count(mirror->Get_Mirror_Count());
		GOAL_MANAGER->Set_Camer_Type(camera->Get_Camera_State());



		if (KEY_Get(KEY_ENTER) == 3 && player->reset_flg == false || reset == true)
		{
			Reset();//プレイヤーとカメラ以外をデリートして初期化する
		}



		if (player->Item_count == 0)
		{
			camera->goal_timer--;
			camera->goal_flg = true;
		}
		if (camera->goal_timer >= 0 && camera->goal_flg == true)
		{
			camera->Set_Camera_State(3);
		}
		/*::cpos = camera->Get_Camera_Pos();
		::ctarget = camera->Get_Camera_Target();*/
		PARTICLEMANAGER->Effect_Update();
		eff_flg2 = false;
		if (player->Get_Pos().x >= mirror->get_pos().x - 0.5f && player->Get_Pos().x <= mirror->get_pos().x + 0.6f && camera->Get_Camera_State() != 1)
		{
			//timer--;
			eff_flg2 = true;
		}
		else if (player->Get_Pos().x <= mirror->get_pos().x - 0.5f && player->Get_Pos().x >= mirror->get_pos().x + 0.6f && camera->Get_Camera_State() != 1)
		{
			//timer--;
			eff_flg4 = true;
		}
		if (eff_flg2 && !eff_flg)
		{
			eff_flg3 = !eff_flg3;
		}
		if (eff_flg4 && !eff_flg)
		{
			eff_flg3 = !eff_flg3;
		}

		eff_flg = eff_flg2;

		if (eff_flg3 == true && player->player_angle == Vector3(0.0f, 1.56f, 0.0f))
		{
			PARTICLEMANAGER->Effect_2(player->player_pos.x, player->player_pos.y + 10, player->player_pos.z);
			eff_flg3 = false;
			SOUNDMANAGER->Se_Play(SeNum::SE_Mirror, FALSE);
		}
		else if (eff_flg3 == true && player->player_angle == Vector3(0.0f, -1.56f, 0.0f))
		{
			PARTICLEMANAGER->Effect_2(player->player_pos.x, player->player_pos.y + 10, player->player_pos.z);
			eff_flg3 = false;
			SOUNDMANAGER->Se_Play(SeNum::SE_Mirror, FALSE);

		}
	}
	else
	{

		if (fe_2 == false)
		{
			player->jack_flg = false;
			fe_2_timer = 16;

			
			fe_2 = true;
		}

		

		if (fe_2 == true)
		{
			y += 15;
			--fe_2_timer;
			if (fe_2_timer < 0)
			{
				Reset();//プレイヤーとカメラ以外をデリートして初期化する
			}
		}
		
	}

	// ポーズじゃない時は、ボリュームは普通
	volume_flg = false;
	if (volume_flg == false)
	{
		SOUNDMANAGER->Sound_Main_Volume();
	}

	// ポーズ
	if (KEY_Get(9) == 3 && volume_flg == false)
	{
		volume_flg = true;
		MainFrame->Push_Scene(new Pause);
	}

	// ポーズならば、ボリューム小さく
	if (volume_flg == true)
	{
		SOUNDMANAGER->Sound_Main_Volume2();
		volume_flg = false;
	}

	if (aplha >= 0 && aplha_flg == false)
	{
		aplha -= 3;
	}
	if (aplha == 0)
	{
		aplha_flg = true;
	}
	if (aplha <= 255 && aplha_flg == true)
	{
		aplha += 3;
	}
	if (aplha == 255)
	{
		aplha_flg = false;
	}
}

//*****************************************************************************************************************************
//
//		描画関連
//
//*****************************************************************************************************************************

void	sceneMain::Render()
{
	//	画面クリア
	//view->Activate();
	//view->Clear();
	camera->Activate();
	camera->Clear();


	//	プレイヤー描画
	player->Render();
	//	ステージ描画
	stage_manager->Render();
	if (stage_manager->stage_state == 0 || stage_manager->stage_state == 1 || stage_manager->stage_state == 3)
	{
		stage_manager->tutorial->Book_Render();
	}
	GOAL_MANAGER->Render();

	item_manager->Render();


	mirror_point->Render();

	// 鏡描画
	mirror->Render();
	PARTICLEMANAGER->Effect_Render();

	stage_manager->tutorial->Render();


	if (fe == true)
	{
		fe_image[0]->Render(0, 0, 1300, 720, 0, 0, 2048, 1024);
	}

	if (fe_2 == true)
	{

		fe_image[1]->Render(0, 0, 1300, 720, 0, 0, 2048, 1024);
		
		fe_image[2]->Render(550, y, 256, 256, 0, 0, 256, 256);

		//
	}

	if (player->virtual_flg == true && player->jack_flg == true)
	{
		jack->Render(0, 0, 150, 150, 0, 0, 256, 256, RS_COPY, ARGB(aplha, 255, 255, 255));
	}
	/*if (player->reset_flg == false)
	{
	char	str[64];
	wsprintf(str, "Reset_ENTER");
	IEX_DrawText(str, 10, 200, 300, 180, 0xFFFFFF00);
	}*/
	if (fe == false&&fe_2==false)
	{
		if (goal_count == 0) stage_num->Render(10, 670, 256, 30, 0, 0, 256, 32);
		if (goal_count == 1) stage_num->Render(10, 670, 256, 30, 0, 32, 256, 32);
		if (goal_count == 2) stage_num->Render(10, 670, 256, 30, 0, 64, 256, 32);
		if (goal_count == 3) stage_num->Render(10, 670, 256, 30, 0, 96, 256, 32);
		if (goal_count == 4) stage_num->Render(10, 670, 256, 30, 0, 128, 256, 32);
		if (goal_count == 5) stage_num->Render(10, 670, 256, 30, 0, 160, 256, 32);
		if (goal_count == 6) stage_num->Render(10, 670, 256, 30, 0, 192, 256, 32);
		if (goal_count == 7) stage_num->Render(10, 670, 256, 30, 0, 224, 256, 32);
		if (goal_count == 8) stage_num->Render(10, 670, 256, 30, 0, 256, 256, 32);
		if (goal_count == 9) stage_num->Render(10, 670, 256, 30, 0, 288, 256, 32);
		if (goal_count == 10) stage_num->Render(10, 670, 256, 30, 0, 320, 256, 32);
		if (goal_count == 11) stage_num->Render(10, 670, 256, 30, 0, 352, 256, 32);
		if (goal_count == 12) stage_num->Render(10, 670, 256, 30, 0, 384, 256, 32);
		if (goal_count == 13) stage_num->Render(10, 670, 256, 30, 0, 416, 256, 32);
		if (goal_count == 14) stage_num->Render(10, 670, 256, 30, 0, 448, 256, 32);
		pause->Render(500, 650, 512, 64, 0, 0, 1024, 128, 0, ARGB(50, 255, 255, 255));
	}
	
#ifdef _DEBUG
	char	st[64];
	wsprintf(st, "%d", (int)player->Item_count);
	IEX_DrawText(st, 10, 210, 300, 180, 0xFFFFFF00);

	char	st[64];
	wsprintf(st, "%d%d", (int)right_1, (int)left_1);
	IEX_DrawText(st, 10, 210, 300, 180, 0xFFFFFF00);
#endif		
}

void sceneMain::Story_Set()
{
	//TODOストーリを追加する
	if (story_count == 0 && goal_count == 0 && story_flg == false)
	{
		MainFrame->Push_Scene(new sceneFade(sceneFade::fade_in, 60, 0x00000000, new Story));

		//MainFrame->Push_Scene(new Story);
		story_flg = true;
		story_count = 1;
	}

	if (story_count == 1 && goal_count == 4 && story_flg == false)
	{
		MainFrame->Push_Scene(new sceneFade(sceneFade::fade_in, 60, 0x00000000, new Story_Ch2));

		//MainFrame->Push_Scene(new Story_Ch2);
		story_flg = true;
		story_count = 2;
	}


	//if (story_count == 2 && goal_count == 6 && story_flg == false)
	if (story_count == 2 && goal_count == 8 && story_flg == false)
	{
		MainFrame->Push_Scene(new sceneFade(sceneFade::fade_in, 60, 0x00000000, new Story_Ch3));

		//MainFrame->Push_Scene(new Story);
		story_flg = true;
		story_count = 3;
	}

	if (story_count == 3 && goal_count == 12 && story_flg == false)
	{
		MainFrame->Push_Scene(new sceneFade(sceneFade::fade_in, 60, 0x00000000, new Story_Ch4));
		story_flg = true;
		story_count = 4;
	}

	if (story_count == 4 && goal_count == 15 && story_flg == false)
	{

		MainFrame->Push_Scene(new sceneFade(sceneFade::fade_in, 60, 0x00000000, new Story_Ch5));

		//MainFrame->Push_Scene(new Story);
		story_flg = true;

		//MainFrame->Push_Scene(new sceneFade(sceneFade::fade_in, 60, 0x00000000, new Story_Ch5));

		////MainFrame->Push_Scene(new Story);
		//story_flg = true;
		//story_count = 5;
	}

	//if (story_count == 5 && goal_count == 15 && story_flg == false)
	//{
	//	MainFrame->Push_Scene(new sceneFade(sceneFade::fade_in, 60, 0x00000000, new Story_Ch6));

	//	//MainFrame->Push_Scene(new Story);
	//	story_flg = true;
	//}
}

bool	sceneMain::ColAABB(Vector3 c1, Vector3 s1, Vector3 c2, Vector3 s2)
{
	Vector3	min1(c1 - s1 / 2.0f), max1(c1 + s1 / 2.0f);
	Vector3	min2(c2 - s2 / 2.0f), max2(c2 + s2 / 2.0f);

	if (max1.x < min2.x)	return	false;
	if (max2.x < min1.x)	return	false;
	if (max1.y < min2.y)	return	false;
	if (max2.y < min1.y)	return	false;
	if (max1.z < min2.z)	return	false;
	if (max2.z < min1.z)	return	false;

	return	true;
}
