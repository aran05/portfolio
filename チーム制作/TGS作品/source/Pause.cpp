#include "iextreme.h"
#include "system\Scene.h"
#include "system\Framework.h"

#include "source\Pause.h"
#include "source\sceneTitle.h"
#include "source\sceneMain.h"
#include "Global.h"

bool Pause::Initialize()
{
	iexLight::SetFog(800, 1000, 0);

	pause = new iex2DObj("DATA\\PAUSE.png");
	font = new iex2DObj("DATA\\font2.png");
	font2 = new iex2DObj("DATA/back.png");
	up = down = false;
	state = 0;
	key_timer = 0;
	return true;
}

Pause::~Pause()
{
	if (pause)
	{
		delete pause;
		pause = NULL;
	}

	if (font)
	{
		delete font;
		font = NULL;
	}
	if (font2)
	{
		delete font2;
		font2 = NULL;
	}
}

void Pause::Update()
{

	switch (state)
	{
	case 0:
		if (KEY_Get(KEY_A) == 3)
		{
			reset = true;
			MainFrame->Pop_Scene();
		}
		--key_timer;
		if (key_timer < 0)
		{
			Move(0.4f);
		}
		if (up == true || down == true)
		{
			up = down = false;
			key_timer = 30;
			state = 1;
		}
		break;
	case 1:
		if (KEY_Get(KEY_A) == 3)
		{
			MainFrame->ChangeScene(new sceneTitle);
		}

		--key_timer;

		if (key_timer<0)
		{
			Move(0.4f);
		}

		if (up == true || down == true)
		{
			up = down = false;
			key_timer = 30;
			state = 0;
		}
		break;
	}

	if (KEY_Get(9) == 3)
	{
		MainFrame->Pop_Scene();
	}

	/*if (KEY_Get(KEY_A) == 3)
	{
	MainFrame->ChangeScene(new sceneTitle);
	}*/

	/*if (KEY_Get(KEY_B) == 3)
	{
	MainFrame->ChangeScene(new sceneMain);
	}*/
	return;
}

void Pause::Render()
{
	stack->Render();

	iexPolygon::Rect(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, RS_COPY, 0x80000000);

	//pause->Render(550, 300, iexSystem::ScreenWidth / 5, iexSystem::ScreenHeight / 5, 0, 0, 256, 64);

	switch (state)
	{
	case 0:
		font->Render(450, 400, 880, 96, 0, 0, 880, 96);
		font->Render(300, 250, 880, 96, 0, 96 * 3, 880, 96);
		break;
	case 1:
		font->Render(450, 400, 880, 96, 0, 96, 880, 96);
		font->Render(300, 250, 880, 96, 0, 96 * 2, 880, 96);
		break;
	}
	font2->Render(800, 550, 360, 64, 0, 0, 720, 128,0,ARGB(80,255,255,255));
	
	/*char	str[64];
	wsprintf(str, "%d",(int)reset);
	IEX_DrawText(str, 10, 200, 300, 180, 0xFFFFFF00);
	*/

}

void Pause::Move(float max)
{
	static float speed = 0;
	//	軸の傾きの取得
	float axisY = -KEY_GetAxisY()*0.001f;


	//	加速
	speed += 0.2f;
	if (speed > max) speed = max;

	//　軸の傾きと長さ
	float d = sqrtf(axisY*axisY);


	//	軸の傾きの遊び(適当)
	if (d < 0.2f) {
		speed = 0;
		//return false;
	}

	if (axisY>0.036f)
	{
		up = true;
	}
	else if (axisY<-0.036f)
	{
		down = true;
	}
}
