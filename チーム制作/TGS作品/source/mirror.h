#pragma once

//***************************************
//
//				鏡
//
//******************************************
//鏡サイズ

#define MIRROR_SCALE (Vector3(2.0f, 3.9f, 6.0f))
#define MIRROR_SCALE2 (Vector3(1.0f,0.6f,1.0f))
#define MIRROR_ANGLE (Vector3(0.0f,0.0f, 0.0f))
#define MIRROR_ANGLE2 (Vector3(0.0f,0.0f, 0.0f))
#define MIRROR_SPEED  1.0f
#define MIRROR_SET_Y  20.0f
#define MIRROR_SET_Z  0.0f
#define MIRROR_MIN_POS -180
#define MIRROR_MAX_POS 180
#define MIRROR_MAX_MOVE 60



class MIRROR
{
private:
	iexMesh* mirror_frame;
	iexMesh* mirror_obj;

	Vector3 pos;
	iexShader* kagami_shader;
	iexShader* kagami_shader2;
	int mirror_count; //鏡を動かす回数をカウント
	bool left;	//左方向に移動するフラグ/falseなら動かない　trueなら動く
	bool right; //右方向に移動するフラグ/falseなら動かない　trueなら動く
	float SetPos; //移動距離を保存する
	bool inverted; //反転フラグ falseの場合は左が現実,右が鏡,trueの場合は左が鏡,右が現実 
	float axisY;

public:
	void Initialize();
	void Update(int type);
	void Render();
	void Move(float max);
	Vector3 get_pos() { return pos; }
	void set_pos(Vector3 mirror_pos) { pos = mirror_pos; }
	void set_count(int c) { mirror_count = c; }
	int get_count() { return mirror_count; }

	int Get_Mirror_Count() { return mirror_count; }

	iexMesh* get_obj() { return mirror_obj; }
	bool Get_Left() { return left;}
	bool Get_Right() { return right; }
	void Set_Left(bool l) { left=l; }
	void Set_Right(bool r) { right=r; }
	MIRROR();
	~MIRROR();
};

extern MIRROR* mirror;
//鏡のモデル読み込み
static char *mirror_file[] =
{
	"DATA/StageDATA/kagami_frame.IMO",
	"DATA/StageDATA/test2.IMO",
};

#define S_COPY  "copy"
#define S_LCOPY "lcopy"
#define S_ADD  "add"
#define S_CFX  "copy_fx"
#define S_CFX2 "copy_fx2"


