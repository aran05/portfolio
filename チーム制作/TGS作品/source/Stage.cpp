﻿#include "iextreme.h"
#include "Stage.h"
#include "Player.h"
#include "mirror.h"
#include "Camera.h"
#include "Effect.h"
#include "tutorial.h"
#include "Sound.h"
#include "Global.h"
bool del;
bool left_1, right_1;

void Stage::Initialize(int type)
{
	//ステージモデルを読み込み
	stage_obj = new iexMesh(file_name[type]);
	x = new iexMesh("DATA/StageDATA/x.IMO");
	//pos.y *= -1;
	stage_type = type;
	//stage_display_flg = display_flg;

	//引数でもらった値をモデルに代入
	//stage_obj->SetPos(pos.x, pos.y, pos.z);
	//stage_obj->SetScale(scale);
	//stage_obj->SetAngle(angle);

	//x->SetPos(pos.x, pos.y+5, pos.z);
	//x->SetScale(scale);
	//x->SetAngle(Vector3(angle.x, angle.y + 3.58f, angle.z));

	//貼り付けたかを判定する
	//pass = pasting_flg;

	//position = pos;
	move = Vector3(0, 0, 0);
	mirror_check_flg = false;
	left = right = inverted = false;
	set_virtual_flg = false;
	push_x_flg = false;
	set_pos_flg = false;

	move_x = 0;

	SOUNDMANAGER->Se_Initialize();
}

void Stage::Stage_Inverted(Vector3 pos, Vector3 scale, Vector3 angle, bool display_flg, bool pasting_flg)
{
	pos.y *= -1;
	stage_display_flg = display_flg;
	//引数でもらった値をモデルに代入
	stage_obj->SetPos(pos.x, pos.y, pos.z);
	stage_obj->SetScale(scale);
	stage_obj->SetAngle(angle);
	stage_obj->Update();
	x->SetPos(pos.x, pos.y + 5, pos.z);
	x->SetScale(Vector3(10, 10, 10));
	x->SetAngle(Vector3(angle.x, angle.y + 3.58f, angle.z));
	x->Update();
	//貼り付けたかを判定する
	pass = pasting_flg;
	position = pos;
	model_scale = scale;

	check_obj = new iexMesh(file_name[7]);
	check_obj->SetPos(position);
	check_obj->SetScale(model_scale);

	check_obj2 = new iexMesh(file_name[9]);
	check_obj2->SetPos(position);
	check_obj2->SetScale(model_scale);
	//left_1 = false;
	//right_1 = false;
}

void Stage::Stage_Initialize(int type, Vector3 pos, Vector3 scale, Vector3 angle, bool display_flg, bool pasting_flg)
{
	//ステージモデルを読み込み
	stage_obj = new iexMesh(file_name[type]);
	x = new iexMesh("DATA/StageDATA/x.IMO");
	stage_type = type;
	move = Vector3(0, 0, 0);
	mirror_check_flg = false;
	set_virtual_flg = true;
	push_x_flg = false;
	set_pos_flg = false;

	left = right = inverted = false;

	move_x = 0;

	stage_display_flg = false;
	//引数でもらった値をモデルに代入
	stage_obj->SetPos(pos.x, pos.y, pos.z);
	stage_obj->SetScale(scale);
	stage_obj->SetAngle(angle);
	stage_obj->Update();
	x->SetPos(pos.x, pos.y + 5, pos.z);
	x->SetScale(Vector3(10, 10, 10));
	x->SetAngle(Vector3(angle.x, angle.y + 3.58f, angle.z));
	x->Update();
	//貼り付けたかを判定する
	pass = pasting_flg;
	position = pos;

	check_obj = new iexMesh(file_name[7]);
	check_obj->SetPos(position);
	check_obj->SetScale(scale);
	check_obj->Update();

	check_obj2 = new iexMesh(file_name[9]);
	check_obj2->SetPos(position);
	check_obj2->SetScale(scale);
	check_obj2->Update();
	left_1 = false;
	right_1 = false;
}

void Stage::Stage_Reset(int type, Vector3 pos, Vector3 scale, Vector3 angle, bool display_flg, bool pasting_flg)
{
	//ステージモデルを読み込み
	stage_obj = new iexMesh(file_name[type]);
	x = new iexMesh("DATA/StageDATA/x.IMO");
	stage_type = type;
	move = Vector3(0, 0, 0);
	mirror_check_flg = false;
	set_virtual_flg = true;
	push_x_flg = false;
	set_pos_flg = false;

	left = right = inverted = false;

	move_x = 0;
	del = true;

	stage_display_flg = false;
	//引数でもらった値をモデルに代入
	stage_obj->SetPos(pos.x, pos.y, pos.z);
	stage_obj->SetScale(scale);
	stage_obj->SetAngle(angle);
	stage_obj->Update();
	x->SetPos(pos.x, pos.y + 5, pos.z);
	x->SetScale(Vector3(10, 10, 10));
	x->SetAngle(Vector3(angle.x, angle.y + 3.58f, angle.z));
	x->Update();
	//貼り付けたかを判定する
	pass = pasting_flg;
	position = pos;

	check_obj = new iexMesh(file_name[7]);
	check_obj->SetPos(position);
	check_obj->SetScale(scale);

	check_obj2 = new iexMesh(file_name[9]);
	check_obj2->SetPos(position);
	check_obj2->SetScale(scale);
	left_1 = false;
	right_1 = false;

}


Stage::~Stage()
{
	//解放
	if (stage_obj)
	{
		delete stage_obj;
		stage_obj = NULL;
	}

	if (x)
	{
		delete x;
		x = NULL;
	}
	if (wall_obj)
	{
		delete wall_obj;
		wall_obj = NULL;
	}
	if (check_obj)
	{
		delete check_obj;
		check_obj = NULL;
	}
	if (check_obj2)
	{
		delete check_obj2;
		check_obj2 = NULL;
	}

	SOUNDMANAGER->~Sound();
}

void Stage::Virtual_Get(const Vector3& pos, Vector3& local)
{
	if (stage_type == VIRTUAL)
	{
		Matrix mat = stage_obj->TransMatrix;
		D3DXMatrixInverse(&mat, NULL, &mat);
		local.x = pos.x*mat._11 + pos.y*mat._21 + pos.z*mat._31 + mat._41;
		local.y = pos.x*mat._12 + pos.y*mat._22 + pos.z*mat._32 + mat._42;
		local.z = pos.x*mat._13 + pos.y*mat._23 + pos.z*mat._33 + mat._43;

		//ローカル座標での下向きレイピック
		Vector3 p = local + Vector3(1.0f, -0.5f, 0.0f);
		if (player->player_angle == Vector3(0.0f, -1.56f, 0.0f))
		{
			p = local + Vector3(-1.0f, -0.5f, 0.0f);
		}
		else if (player->player_angle == Vector3(0.0f, 1.56f, 0.0f))
		{
			p = local + Vector3(1.0f, -0.5f, 0.0f);
		}

		Vector3 v2(0.5f, 0.5f, 0.0f);
		Vector3 v3(-0.5f, 0.5f, 0.0f);
		float d2 = 1.0f;
		Vector3 out;


		Vector3 p2 = position - pos;

		float l = p2.Length();

		if (l>5)
		{
			push_x_flg = false;
		}

		if (stage_obj->RayPick(&out, &p, &v2, &d2) >= 0)
		{
			push_x_flg = true;
			Get_Virtual();
			//Set_Virtual();
		}

		if (stage_obj->RayPick(&out, &p, &v3, &d2) >= 0)
		{
			push_x_flg = true;
			Get_Virtual();
			//Set_Virtual();
		}
	}
	else
	{



		Matrix mat = stage_obj->TransMatrix;
		D3DXMatrixInverse(&mat, NULL, &mat);
		local.x = pos.x*mat._11 + pos.y*mat._21 + pos.z*mat._31 + mat._41;
		local.y = pos.x*mat._12 + pos.y*mat._22 + pos.z*mat._32 + mat._42;
		local.z = pos.x*mat._13 + pos.y*mat._23 + pos.z*mat._33 + mat._43;

		//ローカル座標での下向きレイピック
		Vector3 p = local + Vector3(1.0f, -0.5f, 0.0f);
		if (player->player_angle == Vector3(0.0f, -1.56f, 0.0f))
		{
			p = local + Vector3(1.0f, -0.5f, 0.0f);
		}
		else if (player->player_angle == Vector3(0.0f, 1.56f, 0.0f))
		{
			p = local + Vector3(0.5f, -0.5f, 0.0f);
		}
		Vector3 v2(0.5f, 0.3f, 0.0f);
		Vector3 v3(-0.5f, 0.3f, 0.0f);
		float d2 = 5.0f;
		Vector3 out;


		Vector3 p2 = position - pos;


		if (player->player_angle == Vector3(0.0f, 1.56f, 0.0f))
		{
			if (stage_obj->RayPick(&out, &p, &v2, &d2) >= 0)
			{
				push_x_flg = true;
				Get_Virtual();
				//Set_Virtual();
			}
		}
		else if (player->player_angle == Vector3(0.0f, -1.56f, 0.0f))
		{
			if (stage_obj->RayPick(&out, &p, &v3, &d2) >= 0)
			{
				push_x_flg = true;
				Get_Virtual();
				//Set_Virtual();
			}
		}
	}

}

void Stage::Virtual_Set(const Vector3& pos, Vector3& local)
{

	Matrix mat = stage_obj->TransMatrix;
	D3DXMatrixInverse(&mat, NULL, &mat);
	local.x = pos.x*mat._11 + pos.y*mat._21 + pos.z*mat._31 + mat._41;
	local.y = pos.x*mat._12 + pos.y*mat._22 + pos.z*mat._32 + mat._42;
	local.z = pos.x*mat._13 + pos.y*mat._23 + pos.z*mat._33 + mat._43;

	//ローカル座標での下向きレイピック
	Vector3 p = local + Vector3(1.0f, -0.5f, 0.0f);
	if (player->player_angle == Vector3(0.0f, -1.56f, 0.0f))
	{
		p = local + Vector3(-1.0f, -0.5f, 0.0f);
	}
	else if (player->player_angle == Vector3(0.0f, 1.56f, 0.0f))
	{
		p = local + Vector3(1.0f, -0.5f, 0.0f);
	}
	Vector3 v2(2.0f, 0.5f, 0.0f);
	Vector3 v3(-2.0f, 0.5f, 0.0f);
	float d2 = 3.0f;
	Vector3 out;


	Vector3 p2 = position - pos;

	float l = p2.Length();

	if (l>5)
	{
		push_x_flg = false;
	}

	if (player->player_angle == Vector3(0, 1.56f, 0))
	{
		if (stage_obj->RayPick(&out, &p, &v2, &d2) >= 0)
		{
			push_x_flg = true;
			Set_Virtual();
			//Set_Virtual();
		}
	}

	if (player->player_angle == Vector3(0, -1.56f, 0))
	{
		if (stage_obj->RayPick(&out, &p, &v3, &d2) >= 0)
		{
			push_x_flg = true;
			Set_Virtual();
			//Set_Virtual();
		}
	}

}
bool Stage::Hit_block(const Vector3& pos, Vector3 & local)
{

	Matrix mat = stage_obj->TransMatrix;
	D3DXMatrixInverse(&mat, NULL, &mat);
	local.x = pos.x*mat._11 + pos.y*mat._21 + pos.z*mat._31 + mat._41;
	local.y = pos.x*mat._12 + pos.y*mat._22 + pos.z*mat._32 + mat._42;
	local.z = pos.x*mat._13 + pos.y*mat._23 + pos.z*mat._33 + mat._43;

	//ローカル座標での下向きレイピック
	Vector3 p, p2, p3;

	p = local + Vector3(0.0f, -0.2f, 0.0f);
	Vector3 v = Vector3(0.3f, 0.5f, 0);

	p2 = local + Vector3(0.0f, 0.2f, 0.0f);
	Vector3 v2 = Vector3(0.50f, 0.0f, 0.0);

	p3 = local + Vector3(0.0f, 0.2f, 0.0f);
	Vector3 v3 = Vector3(-0.50f, 0.0f, 0.0);


	if (player->player_angle == Vector3(0.0f, -1.56f, 0.0f))
	{
		p = local + Vector3(0.0f, -0.2f, 0.0f);
		v = Vector3(0.0f, 0.4f, 0);
	}
	else if (player->player_angle == Vector3(0.0f, 1.56f, 0.0f))
	{
		p = local + Vector3(0.0f, -0.2f, 0.0f);
		v = Vector3(0.0f, 0.4f, 0);
	}



	float d = 2.0f;
	float d2 = 5.0f;
	float d3 = 5.0f;
	Vector3 out;

	if (stage_type == SCAFFOLD2)
	{
		if (stage_obj->RayPick(&out, &p2, &v2, &d2) >= 0)
		{
			local = out;
			player->player_pos.x += 1.48f;
			player->player_pos.y += 0.5f;
			//player->floor_flg = true;
			//player->reset_flg = true;
			player->floor_flg = true;
			return 0;
		}
	}


	if (stage_type == SCAFFOLD3)
	{
		if (stage_obj->RayPick(&out, &p3, &v3, &d3) >= 0)
		{
			local = out;
			player->player_pos.x -= 1.5f;
			player->player_pos.y += 0.5f;
			//player->floor_flg = true;
			//player->reset_flg = true;
			player->floor_flg = true;
			return 0;
		}
	}
	if (stage_type != L_STAIRS_VIRTUAL || stage_type != R_STAIRS_VIRTUAL || stage_type != STAIRS || stage_type != STAIRS2)
	{

		if (stage_obj->RayPick(&out, &p, &v, &d) >= 0)
		{
			local = out;
			//player->player_pos.y += 0.002f;
			player->floor_flg = true;
			if (player->push_jump_key == false)
			{
				player->reset_flg = true;
			}


			return 0;
		}
	}
	return 1;
}

bool Stage::Up_Hit_block(Vector3 pos, Vector3 & local)
{
	Matrix mat = stage_obj->TransMatrix;
	D3DXMatrixInverse(&mat, NULL, &mat);
	local.x = pos.x*mat._11 + pos.y*mat._21 + pos.z*mat._31 + mat._41;
	local.y = pos.x*mat._12 + pos.y*mat._22 + pos.z*mat._32 + mat._42;
	local.z = pos.x*mat._13 + pos.y*mat._23 + pos.z*mat._33 + mat._43;

	//ローカル座標での下向きレイピック
	Vector3 p;
	p = local + Vector3(0.0f, 2.0f, 0.0f);
	if (player->player_angle == Vector3(0.0f, -1.56f, 0.0f))
	{
		p = local + Vector3(0.0f, 2.0f, 0.0f);
	}
	else if (player->player_angle == Vector3(0.0f, 1.56f, 0.0f))
	{
		p = local + Vector3(0.0f, 2.0f, 0.0f);
	}

	Vector3 v(0.0f, 1.0f, 0);
	float d = 1.8f;
	Vector3 out;

	if (stage_obj->RayPick(&out, &p, &v, &d) >= 0)
	{
		local = out;
		player->player_pos.y -= 1.5f;
		player->floor_flg = false;

		return 0;
	}
	return 1;
}

bool Stage::L_Hit_block(const Vector3 & pos, Vector3 & local)
{
	Matrix mat = stage_obj->TransMatrix;
	D3DXMatrixInverse(&mat, NULL, &mat);

	local.x = pos.x*mat._11 + pos.y*mat._21 + pos.z*mat._31 + mat._41;
	local.y = pos.x*mat._12 + pos.y*mat._22 + pos.z*mat._32 + mat._42;
	local.z = pos.x*mat._13 + pos.y*mat._23 + pos.z*mat._33 + mat._43;

	if (mirror_check_flg == false)
	{

		if (stage_type == STAIRS2 || stage_type == L_STAIRS_VIRTUAL || stage_type == L_STAIRS_VIRTUAL2)
		{
			Vector3 p;
			Vector3 v;

			if (player->player_direction == false)
			{
				p = local + Vector3(-0.4f, 0.0f, 0.0f);
				v = Vector3(0.5f, 0.1f, 0);

			}
			else
			{
				p = local + Vector3(-0.4f, 0.0f, 0.0f);
				v = Vector3(1.0f, 0.1f, 0);
			}
			

			float d = 5.0f;
			Vector3 out;
			if (stage_obj->RayPick(&out, &p, &v, &d) >= 0)
			{
				if (player->player_direction == true)
				{
					//player->check_kaidan = true;
					//player->player_pos.x -= 0.1f;
					//player->player_pos.y -= 0.2f;
					player->move_flg = 1;
					//player->floor_flg = true;
					return 0;
				}
				else if (player->player_direction == false)
				{

					//player->check_kaidan = true;
					player->move_flg = 2;
					//player->player_pos.x += 0.5f;
					//player->player_pos.y += 1.05f;
					return 0;
				}
				if (player->player_move.x == 0)
				{

					//player->floor_flg = true;
					return 0;
				}

			}
		}
	}
	return 1;
}

bool Stage::R_Hit_block(const Vector3 & pos, Vector3 & local)
{

	Matrix mat = stage_obj->TransMatrix;
	D3DXMatrixInverse(&mat, NULL, &mat);

	local.x = pos.x*mat._11 + pos.y*mat._21 + pos.z*mat._31 + mat._41;
	local.y = pos.x*mat._12 + pos.y*mat._22 + pos.z*mat._32 + mat._42;
	local.z = pos.x*mat._13 + pos.y*mat._23 + pos.z*mat._33 + mat._43;


	if (mirror_check_flg == false)
	{


		if (stage_type == STAIRS || stage_type == R_STAIRS_VIRTUAL || stage_type == R_STAIRS_VIRTUAL2)
		{
			Vector3 p;
			Vector3 v;

			if (player->player_direction == false)
			{
				p = local + Vector3(0.5f, 0.0f, 0.0f);
				v = Vector3(-0.4f, 0.1f, 0);
			}
			else
			{
				p = local + Vector3(0.5f, 0.0f, 0.0f);
				v = Vector3(-0.4f, 0.1f, 0);
			}

			float d = 5.0f;
			Vector3 out;

			if (stage_obj->RayPick(&out, &p, &v, &d) >= 0)
			{

				if (player->player_direction == true)
				{
					//player->check_kaidan = true;
					player->move_flg = 4;

					return 0;
				}
				else if (player->player_direction == false)
				{
					player->move_flg = 3;
					//player->floor_flg = true;
					//player->player_pos.x += 0.4f;
					return 0;
				}
				if (player->player_move.x == 0)
				{
					//player->floor_flg = true;
					return 0;
				}

			}
		}
	}
	return 1;
}

void Stage::Check_Stage(Vector3 pos, int count)
{

	/*if (inverted == true)
	{

		move.x = 0.5f;

		if (count >= -4)
		{
			if (right == true)
			{
				position.x += move.x;
				move_x += move.x;

				if (move_x == 10)
				{
					right = false;
					move_x = 0;
				}


			}
			if (left == true)
			{
				position.x -= move.x;
				move_x -= move.x;
				if (move_x == -10)
				{
					left = false;
					move_x = 0;
				}
			}
		}
		else
		{

			if (left == true)
			{
				position.x -= move.x;
				move_x -= move.x;
				if (move_x == -10)
				{
					left = false;
					move_x = 0;
				}
			}
		}
	}*/


	if (position.x < pos.x)
	{
		mirror_check_flg = false;
		stage_display_flg = false;
	}
	else
	{
		mirror_check_flg = true;
		stage_display_flg = true;
		if (stage_type == VIRTUAL || stage_type == VIRTUAL2 || stage_type == L_STAIRS_VIRTUAL || stage_type == L_STAIRS_VIRTUAL2 || stage_type == R_STAIRS_VIRTUAL || stage_type == R_STAIRS_VIRTUAL2)
		{
			stage_display_flg = true;
			set_virtual_flg = false;
			pass = false;
		}

		if (stage_type == VIRTUAL3)
		{
			stage_display_flg = true;
			set_virtual_flg = false;
			pass = true;
			mirror_check_flg = false;
		}


	}

	stage_obj->SetPos(position);
	check_obj->SetPos(position);
	check_obj2->SetPos(position);
}

void Stage::Check_Mirror(Vector3 pos, int count, bool l, bool r)
{
	mirror_count = count;
	if (set_pos_flg == false && del == false)
	{
		position.x = position.x + (-60 * count);
		set_pos_flg = true;
	}


	/*if(KEY_Get(KEY_C)==3)
	{
	inverted = !inverted;
	}*/

	Move(0.4f);


	//反転フラグ inverted=falseの場合は左が現実,右が鏡
	if (inverted == false)
	{

		move.x = 2.0f;
		//鏡の動かせるカウントが4以下なら足場を動かす

		if (right == true)
		{
			//position.xを進める
			position.x += move.x;
			move_x += move.x;
			if (move_x == MIRROR_MAX_MOVE)
			{

				//position.x += 0.6f + (count*0.05f);
				right = false;
				right_1 = false;
				move_x = 0;
			}
		}
		if (left == true)
		{
			//position.xを戻す
			position.x -= move.x;
			move_x -= move.x;
			if (move_x == -MIRROR_MAX_MOVE)
			{
				//position.x -= 0.6f+(count*0.05f);
				left = false;
				left_1 = false;
				move_x = 0;
			}
		}



	}


	//足場のポジションと鏡のポジションの比較　
	if (position.x < pos.x)
	{
		mirror_check_flg = true; //足場を消す
		if (stage_type == VIRTUAL2)
		{

			pass = false; //虚像モデルの色を戻す

			mirror_check_flg = false;
		}

		else if (stage_type == VIRTUAL3)
		{
			pass = true;
			set_virtual_flg = true;
			mirror_check_flg = true;
		}
	}
	else
	{
		mirror_check_flg = false;//足場を表示する

	}
	x->SetPos(Vector3(position.x, position.y + 5, position.z));
	stage_obj->SetPos(position);
	check_obj->SetPos(position);
	check_obj2->SetPos(position);
}

void Stage::Update()
{
	stage_obj->Update();
	check_obj->Update();
	check_obj2->Update();


	if (stage_type == L_STAIRS_VIRTUAL || R_STAIRS_VIRTUAL)
	{
		static float y;
		y += 0.001f;
		x->SetPos(Vector3(position.x - 2, position.y + 15, position.z));
		x->SetAngle(Vector3(0, y, 0));
	}
	x->Update();


	iexLight::DirectionalLight(1, &Vector3(position.x, position.y, position.z), 0.3f, 0.3f, 0.3f, 5);

}


//TODO:ステージレンダー
void Stage::Render(iexShader* shader, iexShader*s)
{
	if (stage_type == VIRTUAL && pass == false || stage_type == L_STAIRS_VIRTUAL&& pass == false || stage_type == R_STAIRS_VIRTUAL && pass == false)
	{
		stage_obj->Render(shader, "copy");
	}
	else if (stage_type == VIRTUAL &&pass == true || stage_type == L_STAIRS_VIRTUAL&& pass == true || stage_type == R_STAIRS_VIRTUAL && pass == true)
	{
		stage_obj->Render(shader, "copy2");
	}
	if (stage_type == VIRTUAL3 && pass == false)
	{
		stage_obj->Render(shader, "copy2");
	}
	else if (stage_type == VIRTUAL3 && pass == true && set_virtual_flg == true)
	{
		stage_obj->Render(shader, "copy");
	}

	if (stage_type == VIRTUAL2 && pass == true)
	{
		stage_obj->Render(shader, "copy2");
	}

	if (stage_type == L_STAIRS_VIRTUAL2 && pass == true)
	{
		stage_obj->Render(shader, "copy2");
	}

	if (stage_type == R_STAIRS_VIRTUAL2 && pass == true)
	{
		stage_obj->Render(shader, "copy2");
	}

	if (stage_type != VIRTUAL&& stage_type != VIRTUAL2 && stage_type != VIRTUAL3&&stage_type != L_STAIRS_VIRTUAL&&stage_type != L_STAIRS_VIRTUAL2&&stage_type != R_STAIRS_VIRTUAL&&stage_type != R_STAIRS_VIRTUAL2)
	{
		stage_obj->Render(s, "copy_fx2");
	}


	//char	str[64];
	//wsprintf(str, "%d%d", (int)left, (int)right);
	//IEX_DrawText(str, 10, 200, 300, 180, 0xFFFFFF00);
}

void Stage::Botan_Render(iexShader * shader, int type)
{
	if (push_x_flg == true)
	{

		if (stage_type == VIRTUAL)
		{
			if (type == L_STAIRS_VIRTUAL)
			{
				check_obj->Render(shader, "copy3");
			}
			else
			{
				stage_obj->Render(shader, "copy5");
			}

		}

		if (stage_type == L_STAIRS_VIRTUAL)
		{
			if (type == L_STAIRS_VIRTUAL)
			{
				check_obj->Render(shader, "copy3");
			}
			else
			{
				stage_obj->Render(shader, "copy5");
			}
		}

		if (stage_type == L_STAIRS_VIRTUAL2)
		{
			if (check_obj != NULL)
			{
				check_obj->Render(shader, "copy3");
			}
			else
			{
				stage_obj->Render(shader, "copy3");
			}
		}

		if (stage_type == R_STAIRS_VIRTUAL)
		{
			if (type == R_STAIRS_VIRTUAL)
			{
				check_obj2->Render(shader, "copy3");
			}
			else
			{
				stage_obj->Render(shader, "copy3");
			}
		}

		if (stage_type == R_STAIRS_VIRTUAL2)
		{
			if (type == R_STAIRS_VIRTUAL)
			{
				check_obj2->Render(shader, "copy3");
			}
			else
			{
				stage_obj->Render(shader, "copy3");
			}
		}



		if (stage_type == VIRTUAL2)
		{
			if (type == L_STAIRS_VIRTUAL)
			{
				check_obj->Render(shader, "copy3");
			}
			else if (type == R_STAIRS_VIRTUAL)
			{
				check_obj2->Render(shader, "copy3");
			}
			else
			{
				stage_obj->Render(shader, "copy3");
			}
		}
		x->Render();
	}
}

void Stage::Get_Virtual()
{

	//虚像をとる
	if (KEY_Get(KEY_B) == 3 && stage_type == VIRTUAL&&player->virtual_flg == false && pass == false && stage_display_flg == false)
	{
		stage_display_flg = true;
		push_x_flg = false;
		//mirror_check_flg = true;
		player->virtual_flg = true;
		player->jack_flg = true;
		//pass = true;
		//stage_col_flg = false;
		//stage_col_mirror_flg = true;
		//player->Set_Virtual(true);

		SOUNDMANAGER->Se_Play(SeNum::SE_Kyozo_Take, FALSE);
		player->player_obj->SetMotion(TAKE);

	}
	else if (KEY_Get(KEY_B) == 3 && stage_type == VIRTUAL3&&player->virtual_flg == false && pass == true)
	{
		stage_display_flg = true;
		push_x_flg = false;
		//mirror_check_flg = true;
		player->virtual_flg = true;
		player->jack_flg = true;

		SOUNDMANAGER->Se_Play(SeNum::SE_Kyozo_Take, FALSE);
		player->player_obj->SetMotion(TAKE);

	}
	else if (KEY_Get(KEY_B) == 3 && stage_type == L_STAIRS_VIRTUAL&&player->virtual_flg == false && pass == false)
	{
		stage_display_flg = true;
		push_x_flg = false;
		//mirror_check_flg = true;
		player->virtual_flg = true;
		player->jack_flg = true;

		SOUNDMANAGER->Se_Play(SeNum::SE_Kyozo_Take, FALSE);
		player->player_obj->SetMotion(TAKE);

	}
	else if (KEY_Get(KEY_B) == 3 && stage_type == R_STAIRS_VIRTUAL&&player->virtual_flg == false && pass == false)
	{
		stage_display_flg = true;
		push_x_flg = false;
		//mirror_check_flg = true;
		player->virtual_flg = true;
		player->jack_flg = true;

		SOUNDMANAGER->Se_Play(SeNum::SE_Kyozo_Take, FALSE);
		player->player_obj->SetMotion(TAKE);

	}

}

void Stage::Set_Virtual()
{
	//虚像を置く
	if ((KEY_Get(KEY_B) == 3 && stage_type == VIRTUAL) || (KEY_Get(KEY_B) == 3 && stage_type == VIRTUAL2) && (KEY_Get(KEY_B) == 3 && player->virtual_flg == true))
	{

		stage_display_flg = true;
		set_virtual_flg = true;
		//stage_display_flg = false;
		mirror_check_flg = false;
		push_x_flg = false;
		pass = true;

		SOUNDMANAGER->Se_Play(SeNum::SE_Kyozo, FALSE);
		player->player_obj->SetMotion(TAKE);

	}


	else if ((KEY_Get(KEY_B) == 3 && stage_type == VIRTUAL3) && (KEY_Get(KEY_B) == 3 && player->virtual_flg == true))
	{

		stage_display_flg = true;
		set_virtual_flg = true;
		//stage_display_flg = false;
		mirror_check_flg = false;
		push_x_flg = false;
		pass = false;
		player->player_obj->SetMotion(TAKE);

		SOUNDMANAGER->Se_Play(SeNum::SE_Kyozo, FALSE);
	}

	else if ((KEY_Get(KEY_B) == 3 && stage_type == L_STAIRS_VIRTUAL) && (KEY_Get(KEY_B) == 3 && player->virtual_flg == true))
	{

		stage_display_flg = true;
		set_virtual_flg = true;
		//stage_display_flg = false;
		mirror_check_flg = false;
		push_x_flg = false;
		pass = false;
		SOUNDMANAGER->Se_Play(SeNum::SE_Kyozo, FALSE);
		player->player_obj->SetMotion(TAKE);

	}
	else if ((KEY_Get(KEY_B) == 3 && stage_type == R_STAIRS_VIRTUAL) && (KEY_Get(KEY_B) == 3 && player->virtual_flg == true))
	{

		stage_display_flg = true;
		set_virtual_flg = true;
		//stage_display_flg = false;
		mirror_check_flg = false;
		push_x_flg = false;
		pass = false;
		SOUNDMANAGER->Se_Play(SeNum::SE_Kyozo, FALSE);
		player->player_obj->SetMotion(TAKE);

	}


}

void Stage::Move(float max)
{
	static float speed = 0;
	//	軸の傾きの取得
	float axisX = KEY_GetAxisX()*0.001f;


	//	加速
	speed += 0.4f;
	if (speed > max) speed = max;

	//　軸の傾きと長さ
	float d = sqrtf(axisX*axisX);


	//	軸の傾きの遊び(適当)
	if (d < 0.4f) {
		speed = 0;
		//return false;
	}


	//	移動
	if (camer_timer==0&& mirror_count != 5 &&mirror->Get_Left()==false && axisX<=-0.1f&& left == false && right == false)
	{
		left = true;
		left_1 = true;
	}
	else if (camer_timer == 0 && mirror_count != 0 && mirror->Get_Right() == false && axisX>=0.1f&&left == false && right == false)
	{
		right = true;
		right_1 = true;

	}
}

void Stage_Manager::Initialize(int stage_num, Vector3 pos, int count)//TODO引数を追加鏡の位置とカウント
{
	stage_shader = new iexShader("DATA/SHADER/PhoneShader.fx");
	p_shader = new iexShader("DATA/SHADER/mirror_shader.fx");

	camera = new Camera();
	camera->Initialize(0);

	tutorial = new Tutorial();
	tutorial->Initialize();

	mirror_count = count;
	mirror_pos = pos;
	camer_type = 0;
	stage_state = stage_num;
	effect_flg = false;
	play_effect_flg = false;
	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{
		v_num[i] = -1;
	}

	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{

		new_mirror_set_num[i] = -1;//TODO新規追加
		stage_pos[i] = Vector3(0, 0, 0);
		mirror_stage_pos[i] = Vector3(0, 0, 0);
		stage_type = -1;
	}

	mirror_set_num = -1; //TODO新規追加

	del = false;
	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{

		//構造体データの初期化(現実世界)
		stage[i].pos = Vector3(-165.5f, -75.0f, 0.0f);
		stage[i].angle = Vector3(0.0f, 0.0f, 0.0f);
		stage[i].scale = Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE);
		stage[i].type = 0;
		stage[i].display_flg = false;
		stage[i].pasting_flg = false;

		STAGE_OBJ[i] = NULL;

	}

	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{
		//構造体データの初期化(鏡の世界)
		mirror_stage[i].pos = Vector3(165.0f, -75.0f, 0.0f);
		mirror_stage[i].angle = Vector3(0.0f, 0.0f, 0.0f);
		mirror_stage[i].scale = Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE);
		mirror_stage[i].type = 0;
		mirror_stage[i].display_flg = false;

		MIRROR_STAGE_OBJ[i] = NULL;

	}
	//足場ブロックの配置(引数のステイトはステイトの値でステージの種類が変わる)
	Stage_Map(stage_state);

	//EffectInit();
}

Stage_Manager::~Stage_Manager()
{
	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{
		delete STAGE_OBJ[i];
		STAGE_OBJ[i] = NULL;
	}
	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{
		delete MIRROR_STAGE_OBJ[i];
		MIRROR_STAGE_OBJ[i] = NULL;
	}

	if (stage_shader)
	{
		delete stage_shader;
		stage_shader = NULL;
	}

	if (camera)
	{
		delete camera;
		camera = NULL;
	}

	if (tutorial)
	{
		delete tutorial;
		tutorial = NULL;
	}

}

void Stage_Manager::Update(int init, bool left, bool right)//TODO初期設定で値を変化させる用の引数を追加 0なら初期化 1なら通常
{
	camera->Update(false, left, right);
	//tutorial->Set_Book_State(2);
	//tutorial->Set_Book_Pos(Vector3(0, -14, -5));
	//tutorial->Update();

	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{
		if (STAGE_OBJ[i] == NULL) continue;
		//iexLight::DirectionalLight(1, &Vector3(STAGE_OBJ[i]->Get_pos().x, STAGE_OBJ[i]->Get_pos().y, STAGE_OBJ[i]->Get_pos().z), 1.0f, 1.0f, 1.0f, 5);
		if (camer_type == 1 || camer_type == 4 || init == 0)
		{
			STAGE_OBJ[i]->Check_Stage(mirror_pos, mirror_count);
		}
		STAGE_OBJ[i]->Update();
	}

	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{

		if (MIRROR_STAGE_OBJ[i] == NULL) continue;


		if (camer_type == 1 || camer_type == 4 || init == 0)
		{

			MIRROR_STAGE_OBJ[i]->Check_Mirror(mirror_pos, mirror_count, left, right);
		}
		MIRROR_STAGE_OBJ[i]->Update();
	}

	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{
		if (MIRROR_STAGE_OBJ[i] == NULL) continue;
		if (STAGE_OBJ[i] == NULL) continue;
		//TODO 追加
		if (STAGE_OBJ[i]->Get_pass() == true && effect_flg == false && mirror_set_num != -1)
		{
			new_mirror_set_num[i] = i; //貼り付けた鏡の位置を保存する
			stage_pos[i] = STAGE_OBJ[i]->Get_pos();
			if (STAGE_OBJ[i])
			{
				delete STAGE_OBJ[i];
				STAGE_OBJ[i] = NULL;
			}
			STAGE_OBJ[i] = new Stage();
			if (MIRROR_STAGE_OBJ[mirror_set_num]->stage_type == VIRTUAL)
			{
				STAGE_OBJ[i]->Stage_Initialize(VIRTUAL2, stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, true);
			}
			if (MIRROR_STAGE_OBJ[mirror_set_num]->stage_type == VIRTUAL2)
			{
				STAGE_OBJ[i]->Stage_Initialize(VIRTUAL2, stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, true);
			}
			if (MIRROR_STAGE_OBJ[mirror_set_num]->stage_type == VIRTUAL3)
			{
				STAGE_OBJ[i]->Stage_Initialize(VIRTUAL2, stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, true);
			}

			if (MIRROR_STAGE_OBJ[mirror_set_num]->stage_type == L_STAIRS_VIRTUAL)
			{
				STAGE_OBJ[i]->Stage_Initialize(L_STAIRS_VIRTUAL2, stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, true);
			}

			if (MIRROR_STAGE_OBJ[mirror_set_num]->stage_type == R_STAIRS_VIRTUAL)
			{
				STAGE_OBJ[i]->Stage_Initialize(R_STAIRS_VIRTUAL2, stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, true);
			}




			if (STAGE_OBJ[i]->stage_type == L_STAIRS_VIRTUAL2)
			{
				mirror_stage_pos[i] = MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->Get_pos();
				if (MIRROR_STAGE_OBJ[i])
				{
					delete MIRROR_STAGE_OBJ[i];
					MIRROR_STAGE_OBJ[i] = NULL;
				}
				MIRROR_STAGE_OBJ[i] = new Stage();

				MIRROR_STAGE_OBJ[i]->Stage_Reset(R_STAIRS_VIRTUAL, mirror_stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, true);

			}

			if (STAGE_OBJ[i]->stage_type == R_STAIRS_VIRTUAL2)
			{
				mirror_stage_pos[i] = MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->Get_pos();

				if (MIRROR_STAGE_OBJ[i])
				{
					delete MIRROR_STAGE_OBJ[i];
					MIRROR_STAGE_OBJ[i] = NULL;
				}
				MIRROR_STAGE_OBJ[i] = new Stage();

				MIRROR_STAGE_OBJ[i]->Stage_Reset(L_STAIRS_VIRTUAL, mirror_stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, true);
			}


			if (STAGE_OBJ[i]->stage_type == VIRTUAL3)
			{
				mirror_stage_pos[i] = MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->Get_pos();
				if (MIRROR_STAGE_OBJ[i])
				{
					delete MIRROR_STAGE_OBJ[i];
					MIRROR_STAGE_OBJ[i] = NULL;
				}
				MIRROR_STAGE_OBJ[i] = new Stage();

				MIRROR_STAGE_OBJ[i]->Stage_Reset(VIRTUAL, mirror_stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, true);

			}

			MIRROR_STAGE_OBJ[i]->stage_display_flg = false;
			MIRROR_STAGE_OBJ[i]->Set_pass(true);
			MIRROR_STAGE_OBJ[i]->Set_Virtual_Flg(true);

			player->virtual_flg = false;
			effect_flg = true;

		}


	}

	//置いた場所にエフェクトを出す

	if (effect_flg == true && play_effect_flg == false)
	{
		for (int i = 0; i < STAGE_HALF_MAX; i++)
		{
			if (new_mirror_set_num[i] != -1)
			{
				PARTICLEMANAGER->Effect(STAGE_OBJ[new_mirror_set_num[i]]->stage_obj->GetPos().x, STAGE_OBJ[new_mirror_set_num[i]]->stage_obj->GetPos().y, STAGE_OBJ[new_mirror_set_num[i]]->stage_obj->GetPos().z);
				SOUNDMANAGER->Se_Play(SeNum::SE_Kyozo, FALSE);
			}

		}
		play_effect_flg = true;
	}
	{
		//for (int i = 0; i < STAGE_HALF_MAX; i++)
		//{
		//	
		//	if (v_num[i] != -1)
		//	{
		//		if (MIRROR_STAGE_OBJ[v_num[i]]->stage_type == VIRTUAL)
		//		{

		//			if (MIRROR_STAGE_OBJ[v_num[i]]->Get_Left() == false && MIRROR_STAGE_OBJ[v_num[i]]->Get_Right() == false)
		//			{
		//				mirror_stage_pos[i] = MIRROR_STAGE_OBJ[v_num[i]]->Get_pos();
		//				//mirror_stage_pos.x -= 20.0f;

		//				if (MIRROR_STAGE_OBJ[v_num[i]])
		//				{
		//					delete MIRROR_STAGE_OBJ[v_num[i]];
		//					MIRROR_STAGE_OBJ[v_num[i]] = NULL;
		//				}
		//				MIRROR_STAGE_OBJ[v_num[i]] = new Stage();

		//				MIRROR_STAGE_OBJ[v_num[i]]->Stage_Reset(VIRTUAL, mirror_stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, false);
		//			
		//				stage_type = -1;
		//				mirror_set_num = -1;
		//				new_mirror_set_num[i] = -1;
		//				effect_flg = false;
		//				play_effect_flg = false;
		//			}

		//		}
		//	}
		//}
	}
	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{
		if (new_mirror_set_num[i] != -1)
		{
			//TODO 追加 ステージのセットしたオブジェクトが消えたら虚像を元に戻す
			if (STAGE_OBJ[new_mirror_set_num[i]]->Get_pass() == false)
			{

				stage_pos[i] = STAGE_OBJ[new_mirror_set_num[i]]->Get_pos();
				if (STAGE_OBJ[new_mirror_set_num[i]])
				{
					delete STAGE_OBJ[new_mirror_set_num[i]];
					STAGE_OBJ[new_mirror_set_num[i]] = NULL;
				}
				STAGE_OBJ[new_mirror_set_num[i]] = new Stage();
				if (MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->stage_type == VIRTUAL)
				{
					STAGE_OBJ[new_mirror_set_num[i]]->Stage_Initialize(3, stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, true);
				}
				if (MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->stage_type == VIRTUAL2)
				{
					STAGE_OBJ[new_mirror_set_num[i]]->Stage_Initialize(VIRTUAL2, stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, true);
				}
				if (MIRROR_STAGE_OBJ[v_num[i]]->stage_type == VIRTUAL3)
				{
					STAGE_OBJ[v_num[i]]->Stage_Initialize(VIRTUAL2, stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, true);
				}
				if (MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->stage_type == L_STAIRS_VIRTUAL)
				{
					STAGE_OBJ[new_mirror_set_num[i]]->Stage_Initialize(VIRTUAL2, stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, true);
				}
				if (MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->stage_type == R_STAIRS_VIRTUAL)
				{
					STAGE_OBJ[new_mirror_set_num[i]]->Stage_Initialize(VIRTUAL2, stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, true);
				}

				if (MIRROR_STAGE_OBJ[mirror_set_num]->stage_type != stage_type)
				{
					if (MIRROR_STAGE_OBJ[mirror_set_num]->Get_Left() == false && MIRROR_STAGE_OBJ[mirror_set_num]->Get_Right() == false)
					{

						mirror_stage_pos[i] = MIRROR_STAGE_OBJ[mirror_set_num]->Get_pos();

						//mirror_stage_pos.x -= 20.0f;

						if (MIRROR_STAGE_OBJ[mirror_set_num])
						{
							delete MIRROR_STAGE_OBJ[mirror_set_num];
							MIRROR_STAGE_OBJ[mirror_set_num] = NULL;
						}
						MIRROR_STAGE_OBJ[mirror_set_num] = new Stage();

						MIRROR_STAGE_OBJ[mirror_set_num]->Stage_Reset(stage_type, mirror_stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), true, false);
					}


				}


				MIRROR_STAGE_OBJ[mirror_set_num]->stage_display_flg = false;
				MIRROR_STAGE_OBJ[mirror_set_num]->Set_pass(false);
				MIRROR_STAGE_OBJ[mirror_set_num]->Set_Virtual_Flg(false);

				//mirror_stage_pos.x -= 16.0f;
				if (MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->stage_type == L_STAIRS_VIRTUAL)
				{
					if (mirror_set_num != new_mirror_set_num[i])
					{
						if (MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->Get_Left() == false && MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->Get_Right() == false)
						{

							mirror_stage_pos[i] = MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->Get_pos();

							//mirror_stage_pos.x -= 20.0f;

							if (MIRROR_STAGE_OBJ[new_mirror_set_num[i]])
							{
								delete MIRROR_STAGE_OBJ[new_mirror_set_num[i]];
								MIRROR_STAGE_OBJ[new_mirror_set_num[i]] = NULL;
							}
							MIRROR_STAGE_OBJ[new_mirror_set_num[i]] = new Stage();

							MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->Stage_Reset(3, mirror_stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, false);

						}
					}
				}

				if (MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->stage_type == R_STAIRS_VIRTUAL)
				{
					if (mirror_set_num != new_mirror_set_num[i])
					{
						if (MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->Get_Left() == false && MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->Get_Right() == false)
						{
							mirror_stage_pos[i] = MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->Get_pos();
							//mirror_stage_pos.x -= 20.0f;

							if (MIRROR_STAGE_OBJ[new_mirror_set_num[i]])
							{
								delete MIRROR_STAGE_OBJ[new_mirror_set_num[i]];
								MIRROR_STAGE_OBJ[new_mirror_set_num[i]] = NULL;
							}
							MIRROR_STAGE_OBJ[new_mirror_set_num[i]] = new Stage();

							MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->Stage_Reset(3, mirror_stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, false);
						}
					}
				}
				if (MIRROR_STAGE_OBJ[mirror_set_num]->stage_type == VIRTUAL3)
				{

					if (MIRROR_STAGE_OBJ[v_num[i]]->Get_Left() == false && MIRROR_STAGE_OBJ[mirror_set_num]->Get_Right() == false)
					{
						mirror_stage_pos[i] = MIRROR_STAGE_OBJ[v_num[i]]->Get_pos();
						//mirror_stage_pos.x -= 20.0f;

						if (MIRROR_STAGE_OBJ[v_num[i]])
						{
							delete MIRROR_STAGE_OBJ[v_num[i]];
							MIRROR_STAGE_OBJ[v_num[i]] = NULL;
						}
						MIRROR_STAGE_OBJ[v_num[i]] = new Stage();

						MIRROR_STAGE_OBJ[v_num[i]]->Stage_Reset(VIRTUAL, mirror_stage_pos[i], Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE), Vector3(0, 0, 0), false, false);
					}
				}

				MIRROR_STAGE_OBJ[v_num[i]]->stage_display_flg = false;
				MIRROR_STAGE_OBJ[v_num[i]]->Set_pass(false);
				MIRROR_STAGE_OBJ[v_num[i]]->Set_Virtual_Flg(false);

				if (MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->Get_Left() == false && MIRROR_STAGE_OBJ[new_mirror_set_num[i]]->Get_Right() == false)
				{
					v_num[i] = -1;
					stage_type = -1;
					mirror_set_num = -1;
					new_mirror_set_num[i] = -1;
					effect_flg = false;
					play_effect_flg = false;
				}
			}
		}
	}
}

void Stage_Manager::Render()
{
	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{
		if (STAGE_OBJ[i] == NULL) continue;
		if (STAGE_OBJ[i]->stage_display_flg == false && STAGE_OBJ[i]->Get_Mirror_Flg() == false)
		{
			STAGE_OBJ[i]->Render(stage_shader, p_shader);
		}
		if (camera->Get_Camera_State() == 0 && STAGE_OBJ[i]->stage_type == VIRTUAL2 && STAGE_OBJ[i]->stage_display_flg == false && STAGE_OBJ[i]->Get_Mirror_Flg() == false && STAGE_OBJ[i]->Get_pass() == false && player->virtual_flg == true)
		{
			if (player->push_jump_key == false)
			{
				STAGE_OBJ[i]->Botan_Render(stage_shader, MIRROR_STAGE_OBJ[mirror_set_num]->stage_type);
			}
		}
		else if (camera->Get_Camera_State() == 0 && STAGE_OBJ[i]->stage_type == VIRTUAL2 && STAGE_OBJ[i]->stage_display_flg == true && STAGE_OBJ[i]->Get_Mirror_Flg() == false)
		{
			STAGE_OBJ[i]->Render(stage_shader, p_shader);
		}
	}

	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{
		if (MIRROR_STAGE_OBJ[i] == NULL) continue;
		if (MIRROR_STAGE_OBJ[i]->stage_display_flg == false && MIRROR_STAGE_OBJ[i]->Get_Mirror_Flg() == false)
		{
			MIRROR_STAGE_OBJ[i]->Render(stage_shader, p_shader);

		}
		if (camera->Get_Camera_State() == 0 && MIRROR_STAGE_OBJ[i]->stage_type == L_STAIRS_VIRTUAL || MIRROR_STAGE_OBJ[i]->stage_type == R_STAIRS_VIRTUAL || MIRROR_STAGE_OBJ[i]->stage_type == VIRTUAL&& player->virtual_flg == false && MIRROR_STAGE_OBJ[i]->Get_pass() == false && MIRROR_STAGE_OBJ[i]->stage_display_flg == false && MIRROR_STAGE_OBJ[i]->set_virtual_flg == false && MIRROR_STAGE_OBJ[i]->Get_Mirror_Check_Flg() == false)
		{
			if (player->push_jump_key == false)
			{
				MIRROR_STAGE_OBJ[i]->Botan_Render(stage_shader, MIRROR_STAGE_OBJ[mirror_set_num]->stage_type);
			}
		}
	}
	//EffectDraw();
	//tutorial->Render();

}

bool Stage_Manager::Hit_block(Vector3 pos, Vector3 & local)
{
	player->reset_flg = false;
	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{
		if (STAGE_OBJ[i] == NULL) continue;
		if (STAGE_OBJ[i]->stage_type != VIRTUAL2  &&STAGE_OBJ[i]->stage_display_flg == false && STAGE_OBJ[i]->Get_Mirror_Flg() == false)
		{
			STAGE_OBJ[i]->Hit_block(pos, local);
		}

		if (STAGE_OBJ[i]->stage_type == VIRTUAL2  && STAGE_OBJ[i]->set_virtual_flg == true)//TODO追加　
		{
			STAGE_OBJ[i]->Hit_block(pos, local);
		}

		if (MIRROR_STAGE_OBJ[i] == NULL) continue;

		if (MIRROR_STAGE_OBJ[i]->stage_type == VIRTUAL3)
		{
			v_num[i] = i;
			mirror_set_num = i;
			stage_type = MIRROR_STAGE_OBJ[i]->stage_type;
		}
		if (camera->Get_Camera_State() == 0 && MIRROR_STAGE_OBJ[i]->stage_display_flg == true)
		{
			mirror_set_num = i;//元々あった虚像の位置を保存する
			stage_type = MIRROR_STAGE_OBJ[i]->stage_type;
			if (STAGE_OBJ[i]->stage_display_flg == false && STAGE_OBJ[i]->stage_type == VIRTUAL2 && STAGE_OBJ[i]->stage_display_flg == false && STAGE_OBJ[i]->Get_Mirror_Flg() == false && STAGE_OBJ[i]->Get_pass() == false && player->virtual_flg == true)
			{
				for (int n = 0; n < STAGE_HALF_MAX; n++)
				{
					if (STAGE_OBJ[n]->stage_display_flg == false && STAGE_OBJ[n]->stage_type == VIRTUAL2 && STAGE_OBJ[n]->stage_display_flg == false && STAGE_OBJ[n]->Get_Mirror_Flg() == false && STAGE_OBJ[n]->Get_pass() == false && player->virtual_flg == true)
					{
						if (player->push_jump_key == false)
						{
							STAGE_OBJ[n]->Virtual_Set(pos, local);
						}
					}
				}
			}
		}

	}

	if (camera->Get_Camera_State() == 0 && player->push_jump_key == false)
	{
		Vector3	pc = player->Get_Pos();
		if (player->player_angle == Vector3(0.0f, -1.56f, 0.0f))
		{
			pc = player->Get_Pos() + Vector3(-20.0f, 0.0f, 0.0f);
		}
		else if (player->player_angle == Vector3(0.0f, 1.56f, 0.0f))
		{
			pc = player->Get_Pos() + Vector3(20.0f, 0.0f, 0.0f);
		}
		Vector3	ps = player->player_scale + Vector3(6, 6, 6);

		for (int i = 0; i < STAGE_HALF_MAX; i++)
		{
			if (MIRROR_STAGE_OBJ[i]->stage_obj == NULL) continue;
			if (MIRROR_STAGE_OBJ[i]->stage_type == L_STAIRS_VIRTUAL || MIRROR_STAGE_OBJ[i]->stage_type == R_STAIRS_VIRTUAL || MIRROR_STAGE_OBJ[i]->stage_type == L_STAIRS_VIRTUAL2 || MIRROR_STAGE_OBJ[i]->stage_type == R_STAIRS_VIRTUAL2)
			{
				Vector3	gc = MIRROR_STAGE_OBJ[i]->Get_pos();
				Vector3	gs = MIRROR_STAGE_OBJ[i]->Get_scale();
				MIRROR_STAGE_OBJ[i]->push_x_flg = false;
				if (camera->Get_Camera_State() == 0 && MIRROR_STAGE_OBJ[i]->Get_pass() == false && ColAABB(pc, ps, gc, gs) && mirror_set_num == -1 && MIRROR_STAGE_OBJ[i]->stage_display_flg == false && MIRROR_STAGE_OBJ[i]->set_virtual_flg == false && MIRROR_STAGE_OBJ[i]->Get_Mirror_Check_Flg() == false)
				{
					if (player->virtual_flg == false)
					{
						MIRROR_STAGE_OBJ[i]->push_x_flg = true;
					}
					MIRROR_STAGE_OBJ[i]->Get_Virtual();
				}
			}
		}
	}

	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{

		if (MIRROR_STAGE_OBJ[i] == NULL) continue;

		if (MIRROR_STAGE_OBJ[i]->stage_type != VIRTUAL2 &&MIRROR_STAGE_OBJ[i]->stage_display_flg == false && MIRROR_STAGE_OBJ[i]->Get_Mirror_Flg() == false)
		{
			MIRROR_STAGE_OBJ[i]->Hit_block(pos, local);
		}

		if (MIRROR_STAGE_OBJ[i]->stage_type == VIRTUAL2  && MIRROR_STAGE_OBJ[i]->set_virtual_flg == true && MIRROR_STAGE_OBJ[i]->Get_pass() == true)
		{
			MIRROR_STAGE_OBJ[i]->Hit_block(pos, local);
		}


		if (camera->Get_Camera_State() == 0 && mirror_set_num == -1 && MIRROR_STAGE_OBJ[i]->stage_display_flg == false && MIRROR_STAGE_OBJ[i]->set_virtual_flg == false && MIRROR_STAGE_OBJ[i]->Get_Mirror_Check_Flg() == false)
		{
			if (player->push_jump_key == false)
			{
				MIRROR_STAGE_OBJ[i]->Virtual_Get(pos, local);
			}
		}
	}

	return true;
}


bool Stage_Manager::Up_Hit_block(Vector3 pos, Vector3 & local)
{
	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{
		if (STAGE_OBJ[i] == NULL) continue;
		if (STAGE_OBJ[i]->stage_type != 3 && STAGE_OBJ[i]->stage_display_flg == false && STAGE_OBJ[i]->Get_Mirror_Flg() == false && STAGE_OBJ[i]->set_virtual_flg == false)
		{
			STAGE_OBJ[i]->Up_Hit_block(pos, local);
		}

	}
	for (int i = 0; i < STAGE_HALF_MAX; i++)
	{

		if (MIRROR_STAGE_OBJ[i] == NULL) continue;

		if (MIRROR_STAGE_OBJ[i]->stage_type != 3 && MIRROR_STAGE_OBJ[i]->stage_display_flg == false && MIRROR_STAGE_OBJ[i]->Get_Mirror_Flg() == false)
		{
			MIRROR_STAGE_OBJ[i]->Up_Hit_block(pos, local);
		}

	}

	return true;
}

bool Stage_Manager::R_Hit_block(const Vector3 & pos, Vector3 & local)
{
	
		for (int i = 0; i < STAGE_HALF_MAX; i++)
		{
			if (STAGE_OBJ[i] == NULL) continue;


			if (STAGE_OBJ[i]->stage_type == STAIRS || STAGE_OBJ[i]->stage_type == STAIRS2 || STAGE_OBJ[i]->stage_type == L_STAIRS_VIRTUAL2 || STAGE_OBJ[i]->stage_type == R_STAIRS_VIRTUAL2&& STAGE_OBJ[i]->stage_display_flg == false && STAGE_OBJ[i]->Get_Mirror_Flg() == false)
			{
				STAGE_OBJ[i]->R_Hit_block(pos, local);
			}

		}

		for (int i = 0; i < STAGE_HALF_MAX; i++)
		{

			if (MIRROR_STAGE_OBJ[i] == NULL) continue;

			if (MIRROR_STAGE_OBJ[i]->stage_type != VIRTUAL2 && MIRROR_STAGE_OBJ[i]->stage_display_flg == false && MIRROR_STAGE_OBJ[i]->Get_Mirror_Flg() == false)
			{
				MIRROR_STAGE_OBJ[i]->R_Hit_block(pos, local);
			}

		}
	
	return true;
}

bool Stage_Manager::L_Hit_block(const Vector3 & pos, Vector3 & local)
{
	
		for (int i = 0; i < STAGE_HALF_MAX; i++)
		{
			if (STAGE_OBJ[i] == NULL) continue;

			if (STAGE_OBJ[i]->stage_type != VIRTUAL2 && STAGE_OBJ[i]->stage_display_flg == false && STAGE_OBJ[i]->Get_Mirror_Flg() == false)
			{
				STAGE_OBJ[i]->L_Hit_block(pos, local);
				//STAGE_OBJ[i]->L_Hit_block(pos, local);
			}
		}

		for (int i = 0; i < STAGE_HALF_MAX; i++)
		{

			if (MIRROR_STAGE_OBJ[i] == NULL) continue;

			if (MIRROR_STAGE_OBJ[i]->stage_type != VIRTUAL2 &&MIRROR_STAGE_OBJ[i]->stage_display_flg == false && MIRROR_STAGE_OBJ[i]->Get_Mirror_Flg() == false)
			{
				MIRROR_STAGE_OBJ[i]->L_Hit_block(pos, local);
			}

		}
	
	return true;
}

void Stage_Manager::Stage_Map(int state)
{
	//ステージ切り替え
	//TODO ステージ変更
	switch (state)
	{
		//ステージ１
	case 0:
	{
		//チュートリアルステージ、歩く、ジャンプ
		//現実世界の足場の配置
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{

			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ SCAFFOLD2, SCAFFOLD ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD },

		};

		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{

			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ SCAFFOLD3, SCAFFOLD ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD },
		};
		tutorial->Set_Book_State(0);
		tutorial->Set_Book_Pos(Vector3(-130, -70, -5));
		player->Set_Pos(Vector3(-150, -65, -5));
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);
	}
	break;
	case 1:
	{
		//チュートリアルステージ鏡を動かす
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{

			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ SCAFFOLD2, SCAFFOLD ,VIRTUAL2 ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD },
		};
		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ SCAFFOLD3, SCAFFOLD ,VIRTUAL2 ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD },
		};
		tutorial->Set_Book_State(1);
		tutorial->Set_Book_Pos(Vector3(-130, -70, -5));
		player->Set_Pos(Vector3(-150, -65, -5));
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);

	}
	break;
	case 2:
	{
		//鏡を動かす応用
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,SCAFFOLD ,SCAFFOLD ,VIRTUAL2 ,SCAFFOLD },
			{ SCAFFOLD2, SCAFFOLD ,SCAFFOLD ,SCAFFOLD ,STAIRS2, VIRTUAL2 },

		};
		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,SCAFFOLD ,SCAFFOLD ,VIRTUAL2 ,SCAFFOLD },
			{ SCAFFOLD3, SCAFFOLD ,SCAFFOLD ,SCAFFOLD ,STAIRS, VIRTUAL2 },
		};


		player->Set_Pos(Vector3(-150, -65, -5));
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);
	}
	break;
	case 3:
	{
		//虚像のチュートリアル
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{

			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ SCAFFOLD2, SCAFFOLD ,VIRTUAL2 ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD },
		};

		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{

			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ SCAFFOLD3, SCAFFOLD ,VIRTUAL ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD },

		};
		tutorial->Set_Book_State(2);
		tutorial->Set_Book_Pos(Vector3(-50, -70, -5));
		player->Set_Pos(Vector3(-80, -65, -5));
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);
	}
	break;
	case 4:
	{
		//鏡を動かす応用
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{

			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, SCAFFOLD ,SCAFFOLD ,VIRTUAL2 ,SCAFFOLD ,SCAFFOLD },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,STAIRS ,SCAFFOLD ,VIRTUAL2 },
		};
		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL, SCAFFOLD ,SCAFFOLD ,VIRTUAL2 ,SCAFFOLD ,SCAFFOLD },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,STAIRS2 ,SCAFFOLD ,VIRTUAL2 },
		};


		player->Set_Pos(Vector3(-50, -65, -5));
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);
	}
	break;

	case 5:
	{
		//鏡を動かす応用
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,SCAFFOLD2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD2 ,VIRTUAL2, VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD2 }

		};
		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,SCAFFOLD3 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD3 ,VIRTUAL2, VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD3,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD3 }
		};


		player->Set_Pos(Vector3(-10, -65, -5));
		//enum定義の種類を読み込みstage_objに反映する用の関数

		Stage_Set(stage_map, mirror_stage_map);
	}
	break;
	case 6:
	{
		//鏡を動かす応用
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ SCAFFOLD2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD, SCAFFOLD },
			{ VIRTUAL2, VIRTUAL2 ,SCAFFOLD , VIRTUAL2,VIRTUAL2 ,VIRTUAL2 },
			{ SCAFFOLD2, SCAFFOLD ,SCAFFOLD ,STAIRS ,SCAFFOLD ,SCAFFOLD }

		};
		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ SCAFFOLD3, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD, SCAFFOLD },
			{ VIRTUAL2, L_STAIRS_VIRTUAL ,SCAFFOLD , VIRTUAL2,VIRTUAL2 ,VIRTUAL2 },
			{ SCAFFOLD3, SCAFFOLD ,SCAFFOLD ,STAIRS2 ,SCAFFOLD ,SCAFFOLD }
		};


		player->Set_Pos(Vector3(-10, -50, -5));
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);
	}
	break;
	case 7:
	{
		//鏡を動かす応用
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{

			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD ,SCAFFOLD ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,SCAFFOLD ,VIRTUAL2 ,SCAFFOLD ,SCAFFOLD },
		};
		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{

			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL ,SCAFFOLD ,SCAFFOLD ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,SCAFFOLD ,VIRTUAL2 ,SCAFFOLD ,SCAFFOLD },
		};


		player->Set_Pos(Vector3(-50, -65, -5));
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);
	}
	break;

	case 8:
	{
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{

			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD ,VIRTUAL2 ,SCAFFOLD },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD ,SCAFFOLD },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD ,VIRTUAL2 ,SCAFFOLD }

		};

		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{

			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL ,SCAFFOLD ,VIRTUAL2 ,SCAFFOLD },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD ,SCAFFOLD },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD ,VIRTUAL2 ,SCAFFOLD }
		};
		player->Set_Pos(Vector3(-80, 0, -5));
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);
		break;
	}


	case 9:
	{
		//鏡を動かす応用
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ SCAFFOLD2, SCAFFOLD ,VIRTUAL3 ,SCAFFOLD ,SCAFFOLD, SCAFFOLD },
			{ VIRTUAL2, SCAFFOLD ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD },

		};
		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ SCAFFOLD3, SCAFFOLD ,VIRTUAL3 ,SCAFFOLD ,SCAFFOLD, SCAFFOLD },
			{ VIRTUAL2, SCAFFOLD ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD },
		};


		player->Set_Pos(Vector3(-150, -30, -5));
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);
	}
	break;
	case 10:
	{
		//鏡を動かす応用
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, SCAFFOLD ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2, VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2,SCAFFOLD ,SCAFFOLD ,VIRTUAL3 ,SCAFFOLD },
			{ SCAFFOLD2, STAIRS2 ,VIRTUAL2 ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD }

		};
		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, SCAFFOLD ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2, VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,SCAFFOLD ,SCAFFOLD ,VIRTUAL3 ,SCAFFOLD },
			{ SCAFFOLD, STAIRS   ,VIRTUAL2 ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD }
		};


		player->Set_Pos(Vector3(-170, -50, -5));

		//かける値を0〜5までセットする
		mirror->set_pos(Vector3(MIRROR_POS * 0, -MIRROR_SET_Y, -MIRROR_SET_Z));
		//鏡のカウントをセット0〜5まで上でセットした値によって変化
		mirror->set_count(0);
		mirror_pos = mirror->get_pos();
		mirror_count = mirror->get_count();
		mirror->Update(1);
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);
		break;
	}
	case 11:
	{
		//鏡を動かす応用
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD ,VIRTUAL2, SCAFFOLD },
			{ VIRTUAL2, SCAFFOLD ,VIRTUAL2 ,VIRTUAL2   ,STAIRS ,SCAFFOLD },
			{ VIRTUAL2, VIRTUAL2 ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD ,VIRTUAL2 }

		};
		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL ,SCAFFOLD ,VIRTUAL2 ,SCAFFOLD },
			{ VIRTUAL2, SCAFFOLD ,VIRTUAL2 ,VIRTUAL2 ,STAIRS2  ,SCAFFOLD },
			{ VIRTUAL2, VIRTUAL2 ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD ,VIRTUAL2 }
		};


		player->Set_Pos(Vector3(-80, 0, -5));
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);

	}
	break;
	case 12:
	{
		//鏡を動かす応用
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD, VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2,SCAFFOLD ,STAIRS2 ,VIRTUAL2 ,SCAFFOLD },
			{ SCAFFOLD2, STAIRS2 ,VIRTUAL2 ,SCAFFOLD ,STAIRS2 ,SCAFFOLD2 }

		};
		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL ,SCAFFOLD ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD, VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,SCAFFOLD ,STAIRS ,VIRTUAL2 ,SCAFFOLD },
			{ SCAFFOLD3, STAIRS ,VIRTUAL2 ,SCAFFOLD ,  STAIRS ,SCAFFOLD3 }
		};


		player->Set_Pos(Vector3(-170, -50, -5));

		//かける値を0〜5までセットする
		mirror->set_pos(Vector3(MIRROR_POS * 5, -MIRROR_SET_Y, -MIRROR_SET_Z));
		//鏡のカウントをセット0〜5まで上でセットした値によって変化
		mirror->set_count(5);
		mirror_pos = mirror->get_pos();
		mirror_count = mirror->get_count();
		mirror->Update(1);
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);
		break;
	}
	case 13:
	{
		//鏡を動かす応用
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2, VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2, VIRTUAL2 },
			{ SCAFFOLD, SCAFFOLD ,VIRTUAL2 ,SCAFFOLD ,VIRTUAL2 ,SCAFFOLD },
			{ SCAFFOLD2,SCAFFOLD,STAIRS ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD }

		};
		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2, VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2, VIRTUAL2 },
			{ SCAFFOLD, SCAFFOLD ,VIRTUAL2 ,SCAFFOLD ,L_STAIRS_VIRTUAL ,SCAFFOLD },
			{ SCAFFOLD3,SCAFFOLD,STAIRS2 ,SCAFFOLD ,SCAFFOLD ,SCAFFOLD }
		};


		player->Set_Pos(Vector3(-170, -65, -5));

		//かける値を0〜5までセットする
		//mirror->set_pos(Vector3(MIRROR_POS * 5, -MIRROR_SET_Y, -MIRROR_SET_Z));
		////鏡のカウントをセット0〜5まで上でセットした値によって変化
		//mirror->set_count(5);
		//mirror_pos = mirror->get_pos();
		//mirror_count = mirror->get_count();
		//mirror->Update(1);
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);
		break;
	}

	case 14:
	{
		//鏡を動かす応用
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ SCAFFOLD, SCAFFOLD ,VIRTUAL2 ,SCAFFOLD ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, SCAFFOLD ,VIRTUAL2 ,VIRTUAL2,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, SCAFFOLD ,SCAFFOLD ,VIRTUAL2 ,VIRTUAL2, VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2,VIRTUAL2 ,SCAFFOLD ,VIRTUAL2 ,SCAFFOLD },
			{ VIRTUAL2, VIRTUAL2 ,SCAFFOLD ,VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD }

		};
		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ SCAFFOLD, SCAFFOLD ,VIRTUAL2 ,SCAFFOLD ,VIRTUAL2 ,VIRTUAL2 },
			{ L_STAIRS_VIRTUAL, SCAFFOLD ,VIRTUAL2 ,VIRTUAL2,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, SCAFFOLD ,SCAFFOLD ,VIRTUAL2 ,VIRTUAL2, VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2,VIRTUAL2 ,SCAFFOLD ,VIRTUAL2 ,SCAFFOLD },
			{ VIRTUAL2, VIRTUAL2 ,SCAFFOLD ,VIRTUAL2 ,VIRTUAL2 ,SCAFFOLD }
		};


		player->Set_Pos(Vector3(-100, -65, -5));

		//かける値を0〜5までセットする
		//mirror->set_pos(Vector3(MIRROR_POS * 5, -MIRROR_SET_Y, -MIRROR_SET_Z));
		//鏡のカウントをセット0〜5まで上でセットした値によって変化
		//mirror->set_count(5);
		//mirror_pos = mirror->get_pos();
		//mirror_count = mirror->get_count();
		//mirror->Update(1);
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);
		break;
	}
	case 15:
	{
		//鏡を動かす応用
		int stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2, VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 }

		};
		int mirror_stage_map[STAGE_MAX][STAGE_MAX] =
		{
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2, VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 },
			{ VIRTUAL2, VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 ,VIRTUAL2 }
		};


		player->Set_Pos(Vector3(-100, 0, -5));

		//かける値を0〜5までセットする
		//mirror->set_pos(Vector3(MIRROR_POS * 5, -MIRROR_SET_Y, -MIRROR_SET_Z));
		//鏡のカウントをセット0〜5まで上でセットした値によって変化
		//mirror->set_count(5);
		//mirror_pos = mirror->get_pos();
		//mirror_count = mirror->get_count();
		//mirror->Update(1);
		//enum定義の種類を読み込みstage_objに反映する用の関数
		Stage_Set(stage_map, mirror_stage_map);
		break;
	}
	}

}
void Stage_Manager::Stage_Set(int map[STAGE_MAX][STAGE_MAX], int mirror_map[STAGE_MAX][STAGE_MAX])
{

	for (int y = 0; y < STAGE_MAX; y++)
	{
		for (int x = 0; x < STAGE_MAX; x++)
		{
			//enumで判断してモデルを変える
			if (map[y][x] == SCAFFOLD)
			{
				//二次元配列を一次元配列にする
				//stage[y * STAGE_MAX + x].pos = map[y][x];
				//STAGE_OBJ[y * STAGE_MAX + x]->Set(Vector3((map[y][x]) + (x *1.0f), (map[y][x]) + (y * 1.0f), 0));
				//MIRROR_STAGE_OBJ[y * STAGE_MAX + x] = new Stage();
				STAGE_OBJ[y * STAGE_MAX + x] = new Stage();
				stage[y * STAGE_MAX + x].type = SCAFFOLD;
				STAGE_OBJ[y * STAGE_MAX + x]->Initialize(stage[y * STAGE_MAX + x].type);
				STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z), stage[y * STAGE_MAX + x].scale, stage[y*STAGE_MAX + x].angle, stage[y * STAGE_MAX + x].display_flg, stage[y * STAGE_MAX + x].pasting_flg);

			}
			else if (map[y][x] == BOX)
			{
				//二次元配列を一次元配列にする
				//stage[y * STAGE_MAX + x].pos = map[y][x];
				STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				stage[y * STAGE_MAX + x].type = BOX;
				STAGE_OBJ[y * STAGE_MAX + x]->Initialize(stage[y * STAGE_MAX + x].type);
				STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z), stage[y * STAGE_MAX + x].scale, stage[y*STAGE_MAX + x].angle, stage[y * STAGE_MAX + x].display_flg, stage[y * STAGE_MAX + x].pasting_flg);
			}

			else if (map[y][x] == VIRTUAL)
			{
				//二次元配列を一次元配列にする
				//stage[y * STAGE_MAX + x].pos = map[y][x];
				STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				//stage[y * STAGE_MAX + x].display_flg = true;
				stage[y * STAGE_MAX + x].type = VIRTUAL;
				STAGE_OBJ[y * STAGE_MAX + x]->Initialize(stage[y * STAGE_MAX + x].type);
				STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z), stage[y * STAGE_MAX + x].scale, stage[y*STAGE_MAX + x].angle, stage[y * STAGE_MAX + x].display_flg, stage[y * STAGE_MAX + x].pasting_flg);

			}
			else if (map[y][x] == VIRTUAL2)//TODO 追加
			{
				//二次元配列を一次元配列にする
				//stage[y * STAGE_MAX + x].pos = map[y][x];
				STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				//stage[y * STAGE_MAX + x].display_flg = true;
				stage[y * STAGE_MAX + x].type = VIRTUAL2;
				STAGE_OBJ[y * STAGE_MAX + x]->Initialize(stage[y * STAGE_MAX + x].type);
				STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z), stage[y * STAGE_MAX + x].scale, stage[y*STAGE_MAX + x].angle, stage[y * STAGE_MAX + x].display_flg, stage[y * STAGE_MAX + x].pasting_flg);

			}
			else if (map[y][x] == STAIRS)
			{
				stage[y * STAGE_MAX + x].type = STAIRS;

				//stage[y * STAGE_MAX + x].pos = map[y][x];
				STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				//stage[y * STAGE_MAX + x].display_flg = true;
				STAGE_OBJ[y * STAGE_MAX + x]->Initialize(stage[y * STAGE_MAX + x].type);
				STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z), stage[y * STAGE_MAX + x].scale, stage[y*STAGE_MAX + x].angle, stage[y * STAGE_MAX + x].display_flg, stage[y * STAGE_MAX + x].pasting_flg);

			}
			else if (map[y][x] == STAIRS2)
			{
				stage[y * STAGE_MAX + x].type = STAIRS2;

				//stage[y * STAGE_MAX + x].pos = map[y][x];
				STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				//stage[y * STAGE_MAX + x].display_flg = true;
				STAGE_OBJ[y * STAGE_MAX + x]->Initialize(stage[y * STAGE_MAX + x].type);
				STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z), stage[y * STAGE_MAX + x].scale, stage[y*STAGE_MAX + x].angle, stage[y * STAGE_MAX + x].display_flg, stage[y * STAGE_MAX + x].pasting_flg);

			}
			else if (map[y][x] == VIRTUAL3)
			{
				//二次元配列を一次元配列にする
				//stage[y * STAGE_MAX + x].pos = map[y][x];
				STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				//stage[y * STAGE_MAX + x].display_flg = true;
				stage[y * STAGE_MAX + x].type = VIRTUAL3;
				STAGE_OBJ[y * STAGE_MAX + x]->Initialize(stage[y * STAGE_MAX + x].type);
				STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z), stage[y * STAGE_MAX + x].scale, stage[y*STAGE_MAX + x].angle, stage[y * STAGE_MAX + x].display_flg, stage[y * STAGE_MAX + x].pasting_flg);

			}
			else if (map[y][x] == L_STAIRS_VIRTUAL)
			{
				//二次元配列を一次元配列にする
				//stage[y * STAGE_MAX + x].pos = map[y][x];
				STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				//stage[y * STAGE_MAX + x].display_flg = true;
				stage[y * STAGE_MAX + x].type = L_STAIRS_VIRTUAL;
				STAGE_OBJ[y * STAGE_MAX + x]->Initialize(stage[y * STAGE_MAX + x].type);
				STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z), stage[y * STAGE_MAX + x].scale, stage[y*STAGE_MAX + x].angle, stage[y * STAGE_MAX + x].display_flg, stage[y * STAGE_MAX + x].pasting_flg);

			}
			else if (map[y][x] == R_STAIRS_VIRTUAL)
			{
				//二次元配列を一次元配列にする
				//stage[y * STAGE_MAX + x].pos = map[y][x];
				STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				//stage[y * STAGE_MAX + x].display_flg = true;
				stage[y * STAGE_MAX + x].type = L_STAIRS_VIRTUAL;
				STAGE_OBJ[y * STAGE_MAX + x]->Initialize(stage[y * STAGE_MAX + x].type);
				STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z), stage[y * STAGE_MAX + x].scale, stage[y*STAGE_MAX + x].angle, stage[y * STAGE_MAX + x].display_flg, stage[y * STAGE_MAX + x].pasting_flg);

			}
			else if (map[y][x] == SCAFFOLD2)
			{
				stage[y * STAGE_MAX + x].type = SCAFFOLD2;

				//stage[y * STAGE_MAX + x].pos = map[y][x];
				STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				//stage[y * STAGE_MAX + x].display_flg = true;
				STAGE_OBJ[y * STAGE_MAX + x]->Initialize(stage[y * STAGE_MAX + x].type);
				STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z), stage[y * STAGE_MAX + x].scale, stage[y*STAGE_MAX + x].angle, stage[y * STAGE_MAX + x].display_flg, stage[y * STAGE_MAX + x].pasting_flg);

			}
			else if (map[y][x] == SCAFFOLD3)
			{
				stage[y * STAGE_MAX + x].type = SCAFFOLD3;

				//stage[y * STAGE_MAX + x].pos = map[y][x];
				STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				//stage[y * STAGE_MAX + x].display_flg = true;
				STAGE_OBJ[y * STAGE_MAX + x]->Initialize(stage[y * STAGE_MAX + x].type);
				STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(stage[y * STAGE_MAX + x].pos.x + (30.0f * x), stage[y * STAGE_MAX + x].pos.y + (30.0f * y), stage[y * STAGE_MAX + x].pos.z), stage[y * STAGE_MAX + x].scale, stage[y*STAGE_MAX + x].angle, stage[y * STAGE_MAX + x].display_flg, stage[y * STAGE_MAX + x].pasting_flg);

			}



			//STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

			if (mirror_map[y][x] == SCAFFOLD)
			{
				mirror_stage[y * STAGE_MAX + x].type = SCAFFOLD;
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Initialize(mirror_stage[y * STAGE_MAX + x].type);
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(mirror_stage[y * STAGE_MAX + x].pos.x + (-30.0f * x), mirror_stage[y * STAGE_MAX + x].pos.y + (30.0f * y), mirror_stage[y * STAGE_MAX + x].pos.z), mirror_stage[y * STAGE_MAX + x].scale, mirror_stage[y*STAGE_MAX + x].angle, mirror_stage[y * STAGE_MAX + x].display_flg, mirror_stage[y * STAGE_MAX + x].pasting_flg);
			}

			else if (mirror_map[y][x] == BOX)
			{
				mirror_stage[y * STAGE_MAX + x].type = BOX;
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Initialize(mirror_stage[y * STAGE_MAX + x].type);
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(mirror_stage[y * STAGE_MAX + x].pos.x + (-30.0f * x), mirror_stage[y * STAGE_MAX + x].pos.y + (30.0f * y), mirror_stage[y * STAGE_MAX + x].pos.z), mirror_stage[y * STAGE_MAX + x].scale, mirror_stage[y*STAGE_MAX + x].angle, mirror_stage[y * STAGE_MAX + x].display_flg, mirror_stage[y * STAGE_MAX + x].pasting_flg);
			}

			else if (mirror_map[y][x] == VIRTUAL)
			{
				mirror_stage[y * STAGE_MAX + x].type = VIRTUAL;
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Initialize(mirror_stage[y * STAGE_MAX + x].type);
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(mirror_stage[y * STAGE_MAX + x].pos.x + (-30.0f * x), mirror_stage[y * STAGE_MAX + x].pos.y + (30.0f * y), mirror_stage[y * STAGE_MAX + x].pos.z), mirror_stage[y * STAGE_MAX + x].scale, mirror_stage[y*STAGE_MAX + x].angle, mirror_stage[y * STAGE_MAX + x].display_flg, mirror_stage[y * STAGE_MAX + x].pasting_flg);
			}
			else if (mirror_map[y][x] == VIRTUAL2)//TODO 追加
			{
				mirror_stage[y * STAGE_MAX + x].type = VIRTUAL2;
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Initialize(mirror_stage[y * STAGE_MAX + x].type);
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(mirror_stage[y * STAGE_MAX + x].pos.x + (-30.0f * x), mirror_stage[y * STAGE_MAX + x].pos.y + (30.0f * y), mirror_stage[y * STAGE_MAX + x].pos.z), mirror_stage[y * STAGE_MAX + x].scale, mirror_stage[y*STAGE_MAX + x].angle, mirror_stage[y * STAGE_MAX + x].display_flg, mirror_stage[y * STAGE_MAX + x].pasting_flg);

			}
			else if (mirror_map[y][x] == STAIRS)
			{
				mirror_stage[y * STAGE_MAX + x].type = STAIRS;
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Initialize(mirror_stage[y * STAGE_MAX + x].type);
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(mirror_stage[y * STAGE_MAX + x].pos.x + (-30.0f * x), mirror_stage[y * STAGE_MAX + x].pos.y + (30.0f * y), mirror_stage[y * STAGE_MAX + x].pos.z), mirror_stage[y * STAGE_MAX + x].scale, mirror_stage[y*STAGE_MAX + x].angle, mirror_stage[y * STAGE_MAX + x].display_flg, mirror_stage[y * STAGE_MAX + x].pasting_flg);
			}
			else if (mirror_map[y][x] == STAIRS2)
			{
				mirror_stage[y * STAGE_MAX + x].type = STAIRS2;
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Initialize(mirror_stage[y * STAGE_MAX + x].type);
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(mirror_stage[y * STAGE_MAX + x].pos.x + (-30.0f * x), mirror_stage[y * STAGE_MAX + x].pos.y + (30.0f * y), mirror_stage[y * STAGE_MAX + x].pos.z), mirror_stage[y * STAGE_MAX + x].scale, mirror_stage[y*STAGE_MAX + x].angle, mirror_stage[y * STAGE_MAX + x].display_flg, mirror_stage[y * STAGE_MAX + x].pasting_flg);
			}

			else if (mirror_map[y][x] == VIRTUAL3)//TODO 追加
			{
				mirror_stage[y * STAGE_MAX + x].type = VIRTUAL3;
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Initialize(mirror_stage[y * STAGE_MAX + x].type);
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(mirror_stage[y * STAGE_MAX + x].pos.x + (-30.0f * x), mirror_stage[y * STAGE_MAX + x].pos.y + (30.0f * y), mirror_stage[y * STAGE_MAX + x].pos.z), mirror_stage[y * STAGE_MAX + x].scale, mirror_stage[y*STAGE_MAX + x].angle, mirror_stage[y * STAGE_MAX + x].display_flg, mirror_stage[y * STAGE_MAX + x].pasting_flg);

			}
			else if (mirror_map[y][x] == L_STAIRS_VIRTUAL)
			{
				mirror_stage[y * STAGE_MAX + x].type = L_STAIRS_VIRTUAL;
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Initialize(mirror_stage[y * STAGE_MAX + x].type);
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(mirror_stage[y * STAGE_MAX + x].pos.x + (-30.0f * x), mirror_stage[y * STAGE_MAX + x].pos.y + (30.0f * y), mirror_stage[y * STAGE_MAX + x].pos.z), mirror_stage[y * STAGE_MAX + x].scale, mirror_stage[y*STAGE_MAX + x].angle, mirror_stage[y * STAGE_MAX + x].display_flg, mirror_stage[y * STAGE_MAX + x].pasting_flg);

			}
			else if (mirror_map[y][x] == R_STAIRS_VIRTUAL)
			{
				mirror_stage[y * STAGE_MAX + x].type = R_STAIRS_VIRTUAL;
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Initialize(mirror_stage[y * STAGE_MAX + x].type);
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(mirror_stage[y * STAGE_MAX + x].pos.x + (-30.0f * x), mirror_stage[y * STAGE_MAX + x].pos.y + (30.0f * y), mirror_stage[y * STAGE_MAX + x].pos.z), mirror_stage[y * STAGE_MAX + x].scale, mirror_stage[y*STAGE_MAX + x].angle, mirror_stage[y * STAGE_MAX + x].display_flg, mirror_stage[y * STAGE_MAX + x].pasting_flg);

			}

			else if (mirror_map[y][x] == SCAFFOLD2)
			{
				mirror_stage[y * STAGE_MAX + x].type = SCAFFOLD2;
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Initialize(mirror_stage[y * STAGE_MAX + x].type);
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(mirror_stage[y * STAGE_MAX + x].pos.x + (-30.0f * x), mirror_stage[y * STAGE_MAX + x].pos.y + (30.0f * y), mirror_stage[y * STAGE_MAX + x].pos.z), mirror_stage[y * STAGE_MAX + x].scale, mirror_stage[y*STAGE_MAX + x].angle, mirror_stage[y * STAGE_MAX + x].display_flg, mirror_stage[y * STAGE_MAX + x].pasting_flg);
			}
			else if (mirror_map[y][x] == SCAFFOLD3)
			{
				mirror_stage[y * STAGE_MAX + x].type = SCAFFOLD3;
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x] = new Stage();

				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Initialize(mirror_stage[y * STAGE_MAX + x].type);
				MIRROR_STAGE_OBJ[y * STAGE_MAX + x]->Stage_Inverted(Vector3(mirror_stage[y * STAGE_MAX + x].pos.x + (-30.0f * x), mirror_stage[y * STAGE_MAX + x].pos.y + (30.0f * y), mirror_stage[y * STAGE_MAX + x].pos.z), mirror_stage[y * STAGE_MAX + x].scale, mirror_stage[y*STAGE_MAX + x].angle, mirror_stage[y * STAGE_MAX + x].display_flg, mirror_stage[y * STAGE_MAX + x].pasting_flg);
			}

		}
	}
}


bool	Stage_Manager::ColAABB(Vector3 c1, Vector3 s1, Vector3 c2, Vector3 s2)
{
	Vector3	min1(c1 - s1 / 2.0f), max1(c1 + s1 / 2.0f);
	Vector3	min2(c2 - s2 / 2.0f), max2(c2 + s2 / 2.0f);

	if (max1.x < min2.x)	return	false;
	if (max2.x < min1.x)	return	false;
	if (max1.y < min2.y)	return	false;
	if (max2.y < min1.y)	return	false;
	if (max1.z < min2.z)	return	false;
	if (max2.z < min1.z)	return	false;

	return	true;
}
