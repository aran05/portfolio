#pragma once
#define STAGE_MAX 6			//x軸とy軸の最大数は6ずつ
#define OBJ_MAX 14			//足場の種類の最大数(まだ増えるかも...)
#define STAGE_SCALE 6.0f	//ステージのサイズ(ここで変更)
#define STAGE_HALF_MAX 36	//ステージの最大数の半分
#define MIRROR_POS -30

class Camera;
class Tutorial;
extern int goal_count;
//描画するステージの形
enum
{
	SCAFFOLD,//足場
	BOX,	 //ハーフブロック
	VIRTUAL, //虚像の足場
	VIRTUAL2,//虚像をどこでも置ける
	STAIRS,	 //左上り階段
	STAIRS2, //右上り階段
	VIRTUAL3,//虚像が最初から実像になっている
	L_STAIRS_VIRTUAL,//左上り階段虚像
	L_STAIRS_VIRTUAL2,//左上り階段現実
	R_STAIRS_VIRTUAL,//左上り階段虚像
	R_STAIRS_VIRTUAL2,//左上り階段現実
	SCAFFOLD2,//足場
	SCAFFOLD3,//足場
	OBJ_NONE,//モデルなし
};

class Stage
{
private:
	Vector3 position;
	Vector3 move;
	Vector3 model_scale;

	bool mirror_check_flg; //鏡を消すフラグ　//falseなら表示 true
	bool left;	  //左方向に移動するフラグ/falseなら動かない　trueなら動く
	bool right;   //右方向に移動するフラグ/falseなら動かない　trueなら動く
	bool inverted; //反転フラグ falseの場合は左が現実,右が鏡,trueの場合は左が鏡,右が現実 
	float move_x;


	int mirror_count;
	bool pass; //貼り付けたかフラグ
public:
	iexMesh* stage_obj;			//メッシュオブジェ
	iexMesh* check_obj;			//メッシュオブジェ
	iexMesh* check_obj2;			//メッシュオブジェ
	int stage_type;				//マネージャーから受け取る変数
	bool stage_display_flg;
	bool set_virtual_flg; //足場を現実に貼り付けた時のフラグ falseならおいていない,trueなら置く
	bool push_x_flg;  //xの文字の表示フラグ　trueなら表示, falseなら非表示
	bool set_pos_flg; //足場の位置を補正する
	float axisY;

	bool Get_Mirror_Check_Flg()
	{
		return mirror_check_flg;
	}
	iexMesh* wall_obj;
	iexMesh* x;
	//モデルデータ
	char* file_name[OBJ_MAX]
	{
		"DATA/StageDATA/floor.IMO",
		"DATA/StageDATA/box_4.IMO",
		"DATA/StageDATA/floor.IMO",
		"DATA/StageDATA/floor.IMO",
		"DATA/StageDATA/stairs.IMO",
		"DATA/StageDATA/stairs2.IMO",
		"DATA/StageDATA/floor.IMO",
		"DATA/StageDATA/stairs2.IMO",
		"DATA/StageDATA/stairs2.IMO",
		"DATA/StageDATA/stairs.IMO",
		"DATA/StageDATA/stairs.IMO",
		"DATA/StageDATA/floor2.IMO",
		"DATA/StageDATA/floor3.IMO",
		"",
	};

	~Stage();
	//ステージの位置、大きさ、向き、種類をここで決める
	void Initialize(int type);
	void Stage_Inverted(Vector3 pos, Vector3 scale, Vector3 angle, bool display_flg, bool pasting_flg);
	void Stage_Initialize(int type, Vector3 pos, Vector3 scale, Vector3 angle, bool display_flg, bool pasting_flg);
	void Stage_Reset(int type, Vector3 pos, Vector3 scale, Vector3 angle, bool display_flg, bool pasting_flg);

	void Set(Vector3 pos)
	{
		stage_obj->SetPos(pos);
	}

	Vector3 Get_pos()
	{
		return position;
	}

	Vector3 Get_scale()
	{
		return model_scale;
	}
	//貼り付けたフラグをセットする
	bool Get_pass()
	{
		return pass;
	}

	void Set_pass(bool p)
	{
		pass = p;
	}

	bool Get_Left()
	{
		return left;
	}

	bool Get_Right()
	{
		return right;
	}

	bool Get_Mirror_Flg() { return mirror_check_flg; }
	void Set_Virtual_Flg(bool f) { set_virtual_flg = f; }

	bool Get_Virtual_Flg() { return stage_display_flg; }

	bool Hit_block(const Vector3& pos, Vector3& local);//足元判定

	bool Up_Hit_block(Vector3 pos, Vector3& local);   //頭判定
	bool L_Hit_block(const Vector3& pos, Vector3& local);//右側判定
	bool R_Hit_block(const Vector3& pos, Vector3& local);//左側判定

	void Check_Stage(Vector3 pos, int count); //鏡の位置と現実側ポジション比較して描画するか判断する
	void Check_Mirror(Vector3 pos, int count, bool left, bool right);//鏡の位置とミラーポジション比較して描画するか判断する

	void Update();
	void Render(iexShader* shader, iexShader*s);
	void Botan_Render(iexShader* shader, int type);

	void Virtual_Set(const Vector3& pos, Vector3& local);//TODO 追加 現実側で虚像を取る処理
	void Virtual_Get(const Vector3& pos, Vector3& local);//TODO 追加 鏡側で虚像を取る処理
	void Get_Virtual();
	void Set_Virtual();
	void Move(float max);
};


class Stage_Manager
{
	//TODO 鏡の位置
	Vector3 mirror_pos;
	int mirror_count;
	int camer_type;
	int mirror_set_num; //TODO鏡の状態を保存する
	int new_mirror_set_num[STAGE_HALF_MAX]; //貼り付けた鏡の状態を保存する
	bool effect_flg;
	bool play_effect_flg;
	int stage_type;
public:
	Camera* camera;

	Stage* STAGE_OBJ[STAGE_HALF_MAX];

	Stage* MIRROR_STAGE_OBJ[STAGE_HALF_MAX];

	Vector3 stage_pos[STAGE_HALF_MAX];
	Vector3 mirror_stage_pos[STAGE_HALF_MAX];
	int v_num[STAGE_HALF_MAX];

	iexShader* stage_shader;	//シェーダ
	iexShader* p_shader;	//シェーダ

	Tutorial* tutorial;

	int stage_state;		//ステージの数
							//int stage_type;
							//現実ステージ構造体
	struct STAGE
	{
		Vector3 pos;		//足場の位置
		Vector3 scale;		//足場のサイズ
		Vector3 angle;		//足場の向き
		int type;			//足場の種類
		bool display_flg;	//足場が表示しているかを判断するフラグ
		bool pasting_flg;	//貼り付けた時のフラグ
	}stage[STAGE_HALF_MAX];

	struct MIRROR_STAGE
	{
		Vector3 pos;		//足場の位置
		Vector3 scale;		//足場のサイズ
		Vector3 angle;		//足場の向き
		int type;			//足場の種類
		bool display_flg;	//足場が表示しているかを判断するフラグ
		bool pasting_flg;	//貼り付けた時のフラグ
	}mirror_stage[STAGE_HALF_MAX];


	//鏡の位置を取得
	void Set_Mirrror_Pos(Vector3 pos)
	{
		mirror_pos = pos;
	}

	//鏡の進む方向を決める
	void Set_Mirrror_Count(int count)
	{
		mirror_count = count;
	}

	//カメラのタイプを取得
	void Set_Camer_Type(int type)
	{
		camer_type = type;
	}

	//int stage_map[STAGE_MAX][STAGE_MAX];		//ステージの位置

	void Initialize(int stage_num, Vector3 pos, int count);
	~Stage_Manager();
	void Update(int init, bool left, bool right);
	void Render();
	bool Hit_block(Vector3 pos, Vector3& local);//足元判定
	bool Up_Hit_block(Vector3 pos, Vector3& local);//頭判定
	bool R_Hit_block(const Vector3& pos, Vector3& local);//右側判定
	bool L_Hit_block(const Vector3& pos, Vector3& local);//左側判定

	void Stage_Map(int state);
	void Stage_Set(int map[STAGE_MAX][STAGE_MAX], int mirror_map[STAGE_MAX][STAGE_MAX]);

	bool ColAABB(Vector3 c1, Vector3 s1, Vector3 c2, Vector3 s2);
};







