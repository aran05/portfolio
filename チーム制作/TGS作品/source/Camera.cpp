#include "iextreme.h"
#include "system\System.h"
#include "Player.h"
#include "Camera.h"
#include "Goal.h"
#include "mirror.h"
#include "Global.h"

int camer_timer;
void Camera::Initialize(int state)
{
	camera_pos = Vector3(-40.0f, 40.0f, -80.0f);
	//camera_pos2 = Vector3(-45.0f, 0.0f, -45.0f);
	camera_target = Vector3(0.0f, -20.0f, 0.0f);
	camera_move = Vector3(0.0f, 0.0f, 0.0f);
	camera_state = state;
	stage_pos = Vector3(0.0f, 0.0f, 0.0f);
	goal_timer = 90;
	goal_flg = false;
	Book_flg = false;
	push_x = false;
	camer_timer = 10;
}

void Camera::Update(bool left, bool right, bool book)
{
	Book_flg = book;
	--camer_timer;
	//goal->Update();
	if(camer_timer<=0)
	{
		camer_timer = 0;
		if (KEY_Get(KEY_A) == 3 && right == false && left == false && left_1 == false && right_1 == false && Book_flg != true && player->player_anime != TAKE && camera_state == 0)
		{
			if (push_x == false && mirror != NULL)
			{

				Vector3 p2 = player->player_pos - mirror->get_pos();

				float l = p2.Length();

				if (player->Get_Pos().x <= mirror->get_pos().x)
				{
					camera_state = 1;
				}

				if (player->Get_Pos().x >= mirror->get_pos().x)
				{
					camera_state = 4;
				}
			}
			push_x = true;
		}
		else if (KEY_Get(KEY_A) == 3 && left == false && right == false && left_1 == false && right_1 == false && Book_flg != true && player->player_anime != TAKE && camera_state != 0)
		{
			camera_state = 0;
			push_x = false;
		}
	}
	

	
	
	/*if (push_x == true)
	{
		if (mirror != NULL)
		{
			Vector3 p2 = player->player_pos - mirror->get_pos();

			float l = p2.Length();

			if (player->Get_Pos().x <= mirror->get_pos().x)
			{
				camera_state = 1;
			}

			if (player->Get_Pos().x >= mirror->get_pos().x)
			{
				camera_state = 4;
			}
		}
	}*/
	

	switch (camera_state)
	{
	case 0:
		camera_pos_end = Vector3(-40.0f, 40.0f, -80.0f) + player->Get_Pos();
		Set(camera_pos, player->Get_Pos());
		camera_pos += Camera_Move(camera_pos, camera_pos_end, 25.0f);

		break;
	case 1:
		camera_pos_end = Vector3(-300.0f, 30.0f, -180.0f);
		Set(camera_pos, camera_target + Vector3(0.0f, 10.0f, 0.0f));
		camera_pos += Camera_Move(camera_pos, camera_pos_end, 25.0f);

		break;
	case 2:
		camera_pos_end = Vector3(-250.0f, -30.0f, 0.0f);
		Set(camera_pos, camera_target + Vector3(0.0f, -30.0f, 0.0f));
		camera_pos += Camera_Move(camera_pos, camera_pos_end, 25.0f);

		break;
	case 3:
		for (int i = 0; i < GOAL_HALF_MAX; i++)
		{
			if (GOAL_MANAGER->STAGE_OBJ[i]->stage_type == Goal)
			{
				camera_pos_end = GOAL_MANAGER->STAGE_OBJ[i]->goal_pos + Vector3(0.0f, 50.0f, -250.0f);
				Set(camera_pos, GOAL_MANAGER->STAGE_OBJ[i]->goal_pos + Vector3(0.0f, 25.0f, 0.0f));
				camera_pos += Camera_Move(camera_pos, camera_pos_end, 40.0f);

			}
		}
		break;
	case 4:
		if (mirror->Get_Mirror_Count() == 0)
		{
			camera_pos_end = Vector3(300.0f, 30.0f, -180.0f);
		}
		else if (mirror->Get_Mirror_Count() == 1)
		{
			camera_pos_end = Vector3(270.0f, 30.0f, -180.0f);
		}
		else if (mirror->Get_Mirror_Count() == 2)
		{
			camera_pos_end = Vector3(240.0f, 30.0f, -180.0f);
		}
		else if (mirror->Get_Mirror_Count() == 3)
		{
			camera_pos_end = Vector3(210.0f, 30.0f, -180.0f);
		}
		else if (mirror->Get_Mirror_Count() == 4)
		{
			camera_pos_end = Vector3(180.0f, 30.0f, -180.0f);
		}
		else if (mirror->Get_Mirror_Count() == 5)
		{
			camera_pos_end = Vector3(150.0f, 30.0f, -180.0f);
		}
		Set(camera_pos, camera_target + Vector3(0.0f, 10.0f, 0.0f));
		camera_pos += Camera_Move(camera_pos, camera_pos_end, 25.0f);

		break;
	}

	if (goal_timer <= 0 && goal_flg == true && camera_state == 3)
	{
		camera_state = 0;
	}
	//camera_pos += Camera_Move(camera_pos, camera_pos_end, 25.0f);
	SetProjection(D3DX_PI / 4.2f, 1.0f, 2000.0f);

}

Vector3 Camera::Camera_Move(Vector3 pos, Vector3 target, float speed)
{
	Vector3 move = target - pos;
	move /= speed;
	return move;
}
