//*****************************************************************************************************************************
//
//		メインシーン
//
//*****************************************************************************************************************************
class Player;
class Camera;
class Stage_Manager;
class MIRROR;
class Mirror_Point;
class Goal_Manager;
class Tutorial;
class	sceneTitle : public Scene
{
private:
	iex2DObj* title;
	iex2DObj* push;
	//iexView*	view;
	Camera* camera;
	//Vector3 a;
	//Player*	player;
	Vector3 local;
	Stage_Manager* stage_manager;
	MIRROR* mirror;
	Mirror_Point *mirror_point;
	//DWORD color;
	int aplha;
	bool aplha_flg;
	int red;
	int green;
	int blue;
	bool flg;
	int image_x, image_y;
	int pos_x, pos_y;
	bool flg2;
	int se_count;
public:

	~sceneTitle();
	//	初期化
	bool Initialize();
	//	更新・描画
	void Update();	//	更新
	void Render();	//	描画
};


