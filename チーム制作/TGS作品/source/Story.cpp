#include "iextreme.h"
#include "system\Scene.h"
#include "system\Framework.h"

#include "source\Story.h"
#include "source\Sound.h"
#include "sceneMain.h"

bool Story::Initialize()
{
	s_01 = new iex2DObj("DATA\\Story\\01.png");
	s_02 = new iex2DObj("DATA\\Story\\02.png");
	s_03 = new iex2DObj("DATA\\Story\\03.png");
	s_04 = new iex2DObj("DATA\\Story\\04.png");
	s_03_2 = new iex2DObj("DATA\\Story\\03.png");

	skip = new iex2DObj("DATA/skip.png");

	//flg = false;
	//flg_2 = false;
	//flg_3 = false;
	alpha = 255;
	story_alpha1 = 255;
	story_alpha2 = 0;
	story_alpha3 = 0;
	story_alpha4 = 0;
	story_alpha3_2 = 0;
	story_timer = 0;
	return true;
}

Story::~Story()
{
	if (s_01)
	{
		delete s_01;
		s_01 = NULL;
	}

	if (s_02)
	{
		delete s_02;
		s_02 = NULL;
	}

	if (s_03)
	{
		delete s_03;
		s_03 = NULL;
	}
	if (s_04)
	{
		delete s_04;
		s_04 = NULL;
	}

	if (s_03_2)
	{
		delete s_03_2;
		s_03_2 = NULL;
	}
	if (skip)
	{
		delete skip;
		skip = NULL;
	}

	//if (s_03)
	//{
	//	delete s_03;
	//	s_03 = NULL;
	//}

	//if (s_04)
	//{
	//	delete s_04;
	//	s_04 = NULL;
	//}

}

void Story::Update()
{
	//flg = true;
	story_timer++;
	if (KEY_Get(9) == 3)
	{
		MainFrame->Pop_Scene();
	}
	if (story_timer >= 120 && story_timer <= 230)
	{
		story_alpha1-=4;
		story_alpha2+=4;
		//flg_2 = true;
		if (story_alpha1 <= 0)
		{
			story_alpha1 = 0;
			//flg = false;
		}
		if (story_alpha2 >= 255)
		{
			story_alpha2 = 255;
		}
	}

	if (story_timer >= 250 && story_timer <= 380)
	{
		story_alpha2 -= 4;
		story_alpha3 += 4;
		//flg_2 = true;
		if (story_alpha2 <= 0)
		{
			story_alpha2 = 0;
			//flg = false;
		}
		if (story_alpha3 >= 255)
		{
			story_alpha3 = 255;
		}
	}

	if (story_timer >= 400 && story_timer <= 510)
	{
		story_alpha3-=8;
		story_alpha4+=8;
		//flg_2 = true;
		if (story_alpha3 <= 0)
		{
			story_alpha3 = 0;
			//flg = false;
		}
		if (story_alpha4 >= 255)
		{
			story_alpha4 = 255;
		}
	}

	if (story_timer >= 530)
	{
		story_alpha4 -= 8;
		story_alpha3_2 += 8;
		//flg_2 = true;
		if (story_alpha4 <= 0)
		{
			story_alpha4 = 0;
			//flg = false;
		}
		if (story_alpha3_2 >= 255)
		{
			story_alpha3_2 = 255;
		}
	}




	//if (story_timer==120) // 初期60
	//{
	//	flg = false;
	//	story_alpha1--;
	//	story_alpha2++;
	//	//flg_2 = true;

	//	//count++;
	//}

	//if (story_timer == 240) // 初期120
	//{
	//	flg_2 = false;
	//	//flg_3 = true;
	//}

	////if (story_timer == 360) // 初期180
	////{
	//	//flg_3 = false;
	//	//flg_4 = true;
	////}

	if (story_timer == 600) // 初期240
	{
		MainFrame->Pop_Scene();
	}
	alpha -= 3;
}

void Story::Render()
{
	s_03_2->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha3_2, 255, 255, 255));
	s_04->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha4, 255, 255, 255));
	s_03->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha3, 255, 255, 255));
	s_02->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha2, 255, 255, 255));
	s_01->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720, 0, ARGB(story_alpha1, 255, 255, 255));

	//if (flg_3 == true) s_03->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);
	//if (flg_4 == true) s_04->Render(0, 0, iexSystem::ScreenWidth, iexSystem::ScreenHeight, 0, 0, 1280, 720);

	skip->Render(1000, 600, 300, 128, 0, 0, 512, 128, 0, ARGB(alpha, 255, 255, 255));
}