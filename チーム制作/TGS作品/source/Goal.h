#pragma once
#define STAGE_MAX 6			//x軸とy軸の最大数は6ずつ
#define GOAL_OBJ_MAX 10			//足場の種類の最大数(まだ増えるかも...)
#define GOAL_SCALE 4.0f	//ステージのサイズ(ここで変更)
#define GOAL_HALF_MAX 36	//ステージの最大数の半分


//描画するステージの形


enum
{
	Goal,	//足場
	NONE,	//ハーフブロック
};

class GOAL
{
private:
	Vector3 position;
	Vector3 move;
	bool mirror_check_flg;
	bool left, right, inverted; //右か左かを判定するフラグ
	float move_x;
	int count;
	bool goal_flg;

public:
	iexMesh* stage_obj;			//メッシュオブジェ
	int stage_type;				//マネージャーから受け取る変数
	bool stage_display_flg;
	iexMesh* wall_obj;
	iexMesh* goal;
	Vector3 goal_pos;
	Vector3 goal_scale;
	//モデルデータ
	char* file_name[GOAL_OBJ_MAX]
	{
		"DATA/StageDATA/mirror_frame.IMO",
		"",
	};

	~GOAL();
	//ステージの位置、大きさ、向き、種類をここで決める
	void Initialize(int type);

	void Set(Vector3 pos)
	{
		stage_obj->SetPos(pos);
	}

	Vector3 Get_Pos()
	{
		return goal_pos;
	}

	bool Get_Mirror_Flg() { return mirror_check_flg; }

	bool Get_Virtual_Flg() { return stage_display_flg; }

	//bool Hit_block(const Vector3& pos, Vector3& local);//足元判定
	//bool L_Hit_block(const Vector3& pos, Vector3& local);//右側判定
	//bool R_Hit_block(const Vector3& pos, Vector3& local);//左側判定

	void Set_Goal(Vector3 pos, Vector3 scale, Vector3 angle, bool display_flg);

	void Check_Stage(Vector3 pos, int count); //鏡の位置と現実側ポジション比較して描画するか判断する
	void Check_Mirror(Vector3 pos, int count);//鏡の位置とミラーポジション比較して描画するか判断する

	void Update();
	void Render(iexShader* shader);
	void Goal_Render(iexShader* shader);

};


class Goal_Manager
{
	//ミラー側
	iexMesh * mirror_point;
	//TODO 鏡の位置
	Vector3 mirror_pos;
	int mirror_count;
	int camer_type;

public:
	GOAL* STAGE_OBJ[GOAL_HALF_MAX];

	iexShader* stage_shader;	//シェーダ

	int stage_state;		//ステージの数
							//int stage_type;
							//現実ステージ構造体
	struct STAGE
	{
		Vector3 pos;		//足場の位置
		Vector3 scale;		//足場のサイズ
		Vector3 angle;		//足場の向き
		int type;			//足場の種類
		bool display_flg;	//足場が表示しているかを判断するフラグ
	}stage[GOAL_HALF_MAX];

	//鏡の位置を取得
	void Set_Mirrror_Pos(Vector3 pos)
	{
		mirror_pos = pos;
	}

	//鏡の進む方向を決める
	void Set_Mirrror_Count(int count)
	{
		mirror_count = count;
	}

	//カメラのタイプを取得
	void Set_Camer_Type(int type)
	{
		camer_type = type;
	}

	//int stage_map[STAGE_MAX][STAGE_MAX];		//ステージの位置

	void Initialize(int stage_num);
	~Goal_Manager();
	void Update();
	void Render();
	//bool Hit_block(Vector3 pos, Vector3& local);//足元判定
	//bool R_Hit_block(const Vector3& pos, Vector3& local);//右側判定
	//bool L_Hit_block(const Vector3& pos, Vector3& local);//左側判定
	void Stage_Map(int state);
	void Stage_Set(int map[STAGE_MAX][STAGE_MAX]);
	Vector3 goal_pos[GOAL_HALF_MAX];
	/*Vector3 Get_Pos()
	{
	for (int i = 0; i < GOAL_HALF_MAX; i++)
	{
	if(STAGE_OBJ[i]->stage_type==Goal)
	{
	return goal_pos[i];
	}
	}
	}*/

	static Goal_Manager *getInstance() {
		static Goal_Manager instance;
		return &instance;
	}
};

#define GOAL_MANAGER (Goal_Manager::getInstance())