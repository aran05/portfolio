#ifndef __SCENE_H__
#define __SCENE_H__

//*****************************************************************************
//
//		シーン基底クラス
//
//*****************************************************************************

class Scene {

protected:
	Scene* stack;

public:
	//	生成・解放
	Scene() : stack(NULL) {}
	virtual ~Scene() { if (stack) delete stack; }
	//	初期化
	virtual bool Initialize(){ return true; }
	//	更新・描画
	virtual void Update(){}
	virtual void Render(){}


	Scene* Get_Stack() { return stack; }
	void Set_Stack(Scene* s) { stack = s; }

	void Stack_In(Scene* s)
	{
		s->stack = stack;
		stack = s;
	}

};

//*****************************************************************************
#endif
