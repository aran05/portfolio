#include "wave.h"
#include <Windows.h>

#define _USE_MATH_DEFINES
#include <math.h>

void wave()
{
	int t;
	short* wave_data = (short*)GlobalAlloc(GPTR, sizeof(short) * 44100);
	
	float frq = 2.f / 440.f;

	for (t = 0; t<44100; t++) {
		wave_data[t] = (short)(32767 * sinf((FLOAT)(2.0f*M_PI*frq*t / 44100)));
		//else								wave_data[t] = -32767;
	}
	


	WAVEFORMATEX wf;                              //WAVEFORMATEX 構造体
	wf.wFormatTag = WAVE_FORMAT_PCM;                //これはこのまま
	wf.nChannels = 1;                               //モノラル ステレオなら'2'
	wf.nSamplesPerSec = 44100;                      //44100Hz
	wf.wBitsPerSample = 16;                         //16ビット
	wf.nBlockAlign = wf.nChannels*wf.wBitsPerSample / 8;       //計算
	wf.nAvgBytesPerSec = wf.nSamplesPerSec*wf.nBlockAlign;   //計算
	wf.cbSize = 0;                                           //計算
	HWAVEOUT hWOut;
	waveOutOpen(&hWOut, WAVE_MAPPER, &wf, 0, 0, CALLBACK_NULL);

	WAVEHDR wh;
	wh.lpData = (LPSTR)wave_data;
	wh.dwBufferLength = sizeof(short) * 44100;
	wh.dwFlags = 0;
	wh.dwLoops = 1;//1回だけ再生
	wh.dwBytesRecorded = 0;
	wh.dwUser = 0;
	wh.lpNext = NULL;
	wh.reserved = 0;

	//  再生
	waveOutPrepareHeader(hWOut, &wh, sizeof(WAVEHDR));
	waveOutWrite(hWOut, &wh, sizeof(WAVEHDR));


	//ExitProcess(0);
}