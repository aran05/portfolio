cbuffer CBPerMesh : register(b0)
{
	matrix  World;
	matrix	mWVP;
};


cbuffer CBPerFrame : register(b1)
{
	float4  LightColor;		//ライトの色
	float4	LightDir;		//ライトの方向
	float4  AmbientColor;	//環境光
	float4	EyePos;			//カメラ位置
};

struct VS_OUTPUT
{
	float4 Pos : POSITION;
	float4 Color : COLOR;
	float2 Tex0 :TEXCOORD0;
	float2 Tex1 :TEXCOORD1;
	float2 Normal :TEXCOORD2;
	float2 DecaleTex :TEXCOORD3;
};


float4 paraboloid(float4 vPosition,float4x4 mWV)
{
	float4 Out = (float4)0;
	float3 pos;
	float l;

	float z_min = 0.1f;		//Near plane
	float z_max = 100.0f;	//Far plane

	pos = mul(vPosition, mWV).xyz;
	l = length(pos);
	Out.xy = pos.xy*l*(l - pos.z) / dot(pos.xy, pos.xy);

	if(0<=pos.z)
	{
		Out.z = (l - z_min)*(z_max - z_min);
		Out.w = l;
	}
	else
	{
		Out.z = 0;
		Out.w = -0.00001;
	}
	return Out;
}