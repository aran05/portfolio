#include "Sprite.hlsli"

struct GSOutput
{
	float4 pos : SV_POSITION;
};

[maxvertexcount(3)]
void main(
	triangle VS_OUT input[3],
	uint id : SV_PrimitiveID,
	inout TriangleStream<VS_OUT> TriStream
)
{
	VS_OUT output = (VS_OUT)0;

	for (uint i = 0; i < 3; i++)
	{
		output.position = input[v].position;
		output.color = input[v].color;

		//1頂点データをストリームに追加
		TriStream.Append(output);
	}
}