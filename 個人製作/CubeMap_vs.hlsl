#include "CubeMap.hlsli"

VS_OUTPUT main(float4 Pos:POSITION,float4 Normal:NORMAL,float2 Tex0 :TEXCOORD0)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	Out.Pos = mul(Pos, mWVP);
	Out.Tex0 = Tex0;

	return Out;
}