//
// Generated by Microsoft (R) HLSL Shader Compiler 6.3.9600.16384
//
//
// Buffer Definitions: 
//
// cbuffer CONSTANT_BUFFER
// {
//
//   row_major float4x4 matWVP;         // Offset:    0 Size:    64
//   row_major float4x4 World;          // Offset:   64 Size:    64
//   float4 material_color;             // Offset:  128 Size:    16 [unused]
//   float4 light_direction;            // Offset:  144 Size:    16 [unused]
//   row_major float4x4 bone_transforms[32];// Offset:  160 Size:  2048 [unused]
//
// }
//
//
// Resource Bindings:
//
// Name                                 Type  Format         Dim Slot Elements
// ------------------------------ ---------- ------- ----------- ---- --------
// CONSTANT_BUFFER                   cbuffer      NA          NA    0        1
//
//
//
// Input signature:
//
// Name                 Index   Mask Register SysValue  Format   Used
// -------------------- ----- ------ -------- -------- ------- ------
// SV_POSITION              0   xyzw        0      POS   float   xyzw
// NORMAL                   0   xyz         1     NONE   float       
// COLOR                    1      w        1     NONE   float       
// COLOR                    0   xyzw        2     NONE   float   xyzw
// TEXCOORD                 0   xy          3     NONE   float   xy  
// POSITION                 0   xyzw        4     NONE   float       
// TEXCOORD                 2   xyz         5     NONE   float       
// TEXCOORD                 1   xyz         6     NONE   float       
// SV_PrimitiveID           0    N/A   primID   PRIMID    uint     NO
//
//
// Output signature:
//
// Name                 Index   Mask Register SysValue  Format   Used
// -------------------- ----- ------ -------- -------- ------- ------
// SV_POSITION              0   xyzw        0      POS   float   xyzw
// NORMAL                   0   xyz         1     NONE   float   xyz 
// COLOR                    1      w        1     NONE   float      w
// COLOR                    0   xyzw        2     NONE   float   xyzw
// TEXCOORD                 0   xy          3     NONE   float   xy  
// POSITION                 0   xyzw        4     NONE   float   xyzw
// TEXCOORD                 2   xyz         5     NONE   float   xyz 
// TEXCOORD                 1   xyz         6     NONE   float   xyz 
//
gs_5_0
dcl_globalFlags refactoringAllowed
dcl_constantbuffer cb0[7], immediateIndexed
dcl_input_siv v[3][0].xyzw, position
dcl_input v[3][1].xyz
dcl_input v[3][1].w
dcl_input v[3][2].xyzw
dcl_input v[3][3].xy
dcl_input v[3][4].xyzw
dcl_input v[3][5].xyz
dcl_input v[3][6].xyz
dcl_input vPrim
dcl_temps 7
dcl_inputprimitive triangle 
dcl_stream m0
dcl_outputtopology trianglestrip 
dcl_output_siv o0.xyzw, position
dcl_output o1.xyz
dcl_output o1.w
dcl_output o2.xyzw
dcl_output o3.xy
dcl_output o4.xyzw
dcl_output o5.xyz
dcl_output o6.xyz
dcl_maxout 6
add r0.xyz, -v[0][0].zxyz, v[1][0].zxyz
add r1.xyz, -v[0][0].yzxy, v[2][0].yzxy
mul r2.xyz, r0.xyzx, r1.xyzx
mad r0.xyz, r0.zxyz, r1.yzxy, -r2.xyzx
dp3 r0.w, r0.xyzx, r0.xyzx
rsq r0.w, r0.w
mul r0.xyz, r0.wwww, r0.xyzx
mad r1.xyz, v[1][0].xyzx, l(2.000000, 2.000000, 2.000000, 0.000000), v[2][0].xyzx
mad r1.xyz, r1.xyzx, l(0.333333, 0.333333, 0.333333, 0.000000), r0.xyzx
add r2.xyz, -r1.yzxy, v[0][0].yzxy
add r3.xyz, -r1.zxyz, v[1][0].zxyz
add r4.xyz, -r1.zxyz, v[2][0].zxyz
mul r5.xyz, r2.yzxy, r3.zxyz
mad r5.xyz, r2.xyzx, r3.xyzx, -r5.xyzx
dp3 r0.w, r5.xyzx, r5.xyzx
rsq r0.w, r0.w
mul r5.xyz, r0.wwww, r5.xyzx
mul r6.xyz, r3.xyzx, r4.zxyz
mad r3.xyz, r3.zxyz, r4.xyzx, -r6.xyzx
dp3 r0.w, r3.xyzx, r3.xyzx
rsq r0.w, r0.w
mul r3.xyz, r0.wwww, r3.xyzx
mul r6.xyz, r2.xyzx, r4.xyzx
mad r2.xyz, r4.zxyz, r2.yzxy, -r6.xyzx
dp3 r0.w, r2.xyzx, r2.xyzx
rsq r0.w, r0.w
mul r2.xyz, r0.wwww, r2.xyzx
dp3 r0.w, cb0[4].xyzx, r0.xyzx
dp3 r2.w, cb0[5].xyzx, r0.xyzx
dp3 r0.x, cb0[6].xyzx, r0.xyzx
mov r0.y, l(0)
loop 
  ige r0.z, r0.y, l(3)
  breakc_nz r0.z
  dp4 r0.z, cb0[0].xyzw, v[r0.y + 0][0].xyzw
  dp4 r3.w, cb0[1].xyzw, v[r0.y + 0][0].xyzw
  dp4 r4.x, cb0[2].xyzw, v[r0.y + 0][0].xyzw
  dp4 r4.y, cb0[3].xyzw, v[r0.y + 0][0].xyzw
  dp4 r4.z, cb0[4].xyzw, v[r0.y + 0][0].xyzw
  dp4 r4.w, cb0[5].xyzw, v[r0.y + 0][0].xyzw
  dp4 r5.w, cb0[6].xyzw, v[r0.y + 0][0].xyzw
  mov o0.x, r0.z
  mov o0.y, r3.w
  mov o0.z, r4.x
  mov o0.w, r4.y
  mov o1.x, r0.w
  mov o1.y, r2.w
  mov o1.z, r0.x
  mov o1.w, l(0)
  mov o2.xyzw, v[r0.y + 0][2].xyzw
  mov o3.xy, v[r0.y + 0][3].xyxx
  mov o4.xyzw, l(0,0,0,0)
  mov o5.xyz, l(0,0,0,0)
  mov o6.x, r4.z
  mov o6.y, r4.w
  mov o6.z, r5.w
  emit_stream m0
  iadd r0.y, r0.y, l(1)
endloop 
mov r1.w, l(1.000000)
dp4 r0.y, cb0[0].xyzw, r1.xyzw
dp4 r0.z, cb0[1].xyzw, r1.xyzw
dp4 r3.w, cb0[2].xyzw, r1.xyzw
dp4 r1.x, cb0[3].xyzw, r1.xyzw
dp3 r1.y, cb0[4].xyzx, r5.xyzx
dp3 r1.z, cb0[5].xyzx, r5.xyzx
dp3 r1.w, cb0[6].xyzx, r5.xyzx
dp4 r4.x, cb0[4].xyzw, v[0][0].xyzw
dp4 r4.y, cb0[5].xyzw, v[0][0].xyzw
dp4 r4.z, cb0[6].xyzw, v[0][0].xyzw
mov o0.x, r0.y
mov o0.y, r0.z
mov o0.z, r3.w
mov o0.w, r1.x
mov o1.x, r1.y
mov o1.y, r1.z
mov o1.z, r1.w
mov o1.w, l(0)
mov o2.xyzw, v[0][2].xyzw
mov o3.xy, v[0][3].xyxx
mov o4.xyzw, l(0,0,0,0)
mov o5.xyz, l(0,0,0,0)
mov o6.x, r4.x
mov o6.y, r4.y
mov o6.z, r4.z
emit_stream m0
mov r1.y, l(0)
loop 
  ige r1.z, r1.y, l(3)
  breakc_nz r1.z
  dp4 r1.z, cb0[0].xyzw, v[r1.y + 0][0].xyzw
  dp4 r1.w, cb0[1].xyzw, v[r1.y + 0][0].xyzw
  dp4 r4.x, cb0[2].xyzw, v[r1.y + 0][0].xyzw
  dp4 r4.y, cb0[3].xyzw, v[r1.y + 0][0].xyzw
  dp4 r4.z, cb0[4].xyzw, v[r1.y + 0][0].xyzw
  dp4 r4.w, cb0[5].xyzw, v[r1.y + 0][0].xyzw
  dp4 r5.x, cb0[6].xyzw, v[r1.y + 0][0].xyzw
  mov o0.x, r1.z
  mov o0.y, r1.w
  mov o0.z, r4.x
  mov o0.w, r4.y
  mov o1.x, r0.w
  mov o1.y, r2.w
  mov o1.z, r0.x
  mov o1.w, l(0)
  mov o2.xyzw, v[r1.y + 0][2].xyzw
  mov o3.xy, v[r1.y + 0][3].xyxx
  mov o4.xyzw, l(0,0,0,0)
  mov o5.xyz, l(0,0,0,0)
  mov o6.x, r4.z
  mov o6.y, r4.w
  mov o6.z, r5.x
  emit_stream m0
  iadd r1.y, r1.y, l(1)
endloop 
dp3 r1.y, cb0[4].xyzx, r3.xyzx
dp3 r1.z, cb0[5].xyzx, r3.xyzx
dp3 r1.w, cb0[6].xyzx, r3.xyzx
dp4 r3.x, cb0[4].xyzw, v[1][0].xyzw
dp4 r3.y, cb0[5].xyzw, v[1][0].xyzw
dp4 r3.z, cb0[6].xyzw, v[1][0].xyzw
mov o0.x, r0.y
mov o0.y, r0.z
mov o0.z, r3.w
mov o0.w, r1.x
mov o1.x, r1.y
mov o1.y, r1.z
mov o1.z, r1.w
mov o1.w, l(0)
mov o2.xyzw, v[1][2].xyzw
mov o3.xy, v[1][3].xyxx
mov o4.xyzw, l(0,0,0,0)
mov o5.xyz, l(0,0,0,0)
mov o6.x, r3.x
mov o6.y, r3.y
mov o6.z, r3.z
emit_stream m0
mov r1.y, l(0)
loop 
  ige r1.z, r1.y, l(3)
  breakc_nz r1.z
  dp4 r1.z, cb0[0].xyzw, v[r1.y + 0][0].xyzw
  dp4 r1.w, cb0[1].xyzw, v[r1.y + 0][0].xyzw
  dp4 r3.x, cb0[2].xyzw, v[r1.y + 0][0].xyzw
  dp4 r3.y, cb0[3].xyzw, v[r1.y + 0][0].xyzw
  dp4 r3.z, cb0[4].xyzw, v[r1.y + 0][0].xyzw
  dp4 r4.x, cb0[5].xyzw, v[r1.y + 0][0].xyzw
  dp4 r4.y, cb0[6].xyzw, v[r1.y + 0][0].xyzw
  mov o0.x, r1.z
  mov o0.y, r1.w
  mov o0.z, r3.x
  mov o0.w, r3.y
  mov o1.x, r0.w
  mov o1.y, r2.w
  mov o1.z, r0.x
  mov o1.w, l(0)
  mov o2.xyzw, v[r1.y + 0][2].xyzw
  mov o3.xy, v[r1.y + 0][3].xyxx
  mov o4.xyzw, l(0,0,0,0)
  mov o5.xyz, l(0,0,0,0)
  mov o6.x, r3.z
  mov o6.y, r4.x
  mov o6.z, r4.y
  emit_stream m0
  iadd r1.y, r1.y, l(1)
endloop 
dp3 r0.x, cb0[4].xyzx, r2.xyzx
dp3 r0.w, cb0[5].xyzx, r2.xyzx
dp3 r1.y, cb0[6].xyzx, r2.xyzx
dp4 r1.z, cb0[4].xyzw, v[2][0].xyzw
dp4 r1.w, cb0[5].xyzw, v[2][0].xyzw
dp4 r2.x, cb0[6].xyzw, v[2][0].xyzw
mov o0.x, r0.y
mov o0.y, r0.z
mov o0.z, r3.w
mov o0.w, r1.x
mov o1.x, r0.x
mov o1.y, r0.w
mov o1.z, r1.y
mov o1.w, l(0)
mov o2.xyzw, v[2][2].xyzw
mov o3.xy, v[2][3].xyxx
mov o4.xyzw, l(0,0,0,0)
mov o5.xyz, l(0,0,0,0)
mov o6.x, r1.z
mov o6.y, r1.w
mov o6.z, r2.x
emit_stream m0
cut_stream m0
ret 
// Approximately 190 instruction slots used
