#include "CubeMap.hlsli"
// UNIT.17
Texture2D diffuse_map : register(t0);
SamplerState diffuse_map_sampler_state : register(s0);

float4 main(VS_OUTPUT In) : SV_TARGET
{
	float4 Out;
	Out= diffuse_map.Sample(diffuse_map_sampler_state, In.Tex0);
	return Out;
}