#include "OBJ_Mesh.h"
#include <DirectXMath.h>
#include "Shader.h"
#include "Texture.h"

using namespace DirectX;

OBJ_Mesh::OBJ_Mesh(const wchar_t* name):static_mesh(name,true)
{
	pos = { 0,0,0 };
	scale = { 1.0f,1.0f,1.0f };
	angle = { 0,0,0 };
}


OBJ_Mesh::~OBJ_Mesh()
{
}

void OBJ_Mesh::Update()
{
	//ワールド変換行列作成
	matworld = DirectX::XMMatrixIdentity();
	DirectX::XMMATRIX work;
	{//拡大
		work = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
		matworld *= work;
	}
	{//回転
		work = XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);
		matworld *= work;
	}
	DirectX::XMMATRIX invMatW = matworld;
	{
		work = XMMatrixTranslation(pos.x, pos.y, pos.z);
		matworld *= work;
	}
}

void OBJ_Mesh::Render(const XMMATRIX & view, const XMMATRIX & projection, const XMFLOAT4 & light, const XMFLOAT4 & matColor, const bool flag)
{
	DirectX::XMMATRIX matWVP = matworld*view*projection;
	DirectX::XMFLOAT4X4 wvp;
	DirectX::XMStoreFloat4x4(&wvp, matWVP);


	DirectX::XMFLOAT4X4 mw;
	DirectX::XMStoreFloat4x4(&mw, matworld);

	static_mesh::render(wvp, mw, light, matColor, flag);
}

Skin_Mesh::Skin_Mesh(const char * name):skinned_mesh(name)
{
	position = { 0.0f,0.0f,0.0f };
	scales = { 1.0f,1.0f,1.0f };
	angles = { 0.0f,0.0f,0.0f };

	matWorld = XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
}

Skin_Mesh::Skin_Mesh(const char * name, const char * vs, const char * ps, const char* geo):skinned_mesh(name,vs,ps,geo)
{
	//pos = { 0.0f,0.0f,0.0f };
	//scale = { 1.0f,1.0f,1.0f };
	//angle = { 0.0f,0.0f,0.0f };

	position = { 0.0f,0.0f,0.0f };
	scales = { 1.0f,1.0f,1.0f };
	angles = { 0.0f,0.0f,0.0f };

	matWorld = XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
}

Skin_Mesh::Skin_Mesh(const char * name, const char * vs, const char * ps) :skinned_mesh(name, vs, ps)
{
	//pos = { 0.0f,0.0f,0.0f };
	//scale = { 1.0f,1.0f,1.0f };
	//angle = { 0.0f,0.0f,0.0f };

	position = { 0.0f,0.0f,0.0f };
	scales = { 1.0f,1.0f,1.0f };
	angles = { 0.0f,0.0f,0.0f };

	matWorld = XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
}

Skin_Mesh* Skin_Mesh::Clone()
{
	Skin_Mesh* obj = new Skin_Mesh(*this);

	if (obj == NULL)
	{
		delete obj;
	}
	return obj;
}
void Skin_Mesh::Update()
{
	//ワールド変換行列作成
	XMMATRIX mw = XMMatrixIdentity();
	//拡大
	DirectX::XMMATRIX work1 = DirectX::XMMatrixScaling(scales.x, scales.y, scales.z);

	//回転
	DirectX::XMMATRIX work2 = DirectX::XMMatrixRotationRollPitchYaw(angles.x, angles.y, angles.z);

	//移動
	DirectX::XMMATRIX work3 = DirectX::XMMatrixTranslation(position.x, position.y, position.z);

	mw = work1*work2*work3;
	XMStoreFloat4x4(&matWorld, mw);

}

void Skin_Mesh::Render(const XMMATRIX & view, const  XMMATRIX & projection, const XMFLOAT4 & light, const XMFLOAT4 & matColor, const XMFLOAT4& Edge_type, int blender_type)
{
	XMMATRIX mw = XMLoadFloat4x4(&matWorld);
	XMMATRIX mwvp = mw *view * projection;

	XMFLOAT4X4 fwvp;
	XMStoreFloat4x4(&fwvp, mwvp);

	//ワールド変換行列
	XMStoreFloat4x4(&matWorld, mw);

	skinned_mesh::render(fwvp, matWorld, light, matColor, Edge_type, framework::elapsed_time , motion, blender_type);

}

void Skin_Mesh::Render(const XMMATRIX & view, const XMMATRIX & projection, const XMFLOAT4 & light, const XMFLOAT4 & matColor, int blender_type)
{
	XMMATRIX mw = XMLoadFloat4x4(&matWorld);
	XMMATRIX mwvp = mw *view * projection;

	XMFLOAT4X4 fwvp;
	XMStoreFloat4x4(&fwvp, mwvp);

	//ワールド変換行列
	XMStoreFloat4x4(&matWorld, mw);

	skinned_mesh::render(fwvp, matWorld, light, matColor, framework::elapsed_time, motion, blender_type);

}

//void Skin_Mesh::Render(Shader * shader, Texture * tex, const XMMATRIX & view, const XMMATRIX & projection, int blender_type,float speed)
//{
//	XMMATRIX mw = XMLoadFloat4x4(&matWorld);
//	XMMATRIX mwvp = mw *view * projection;
//
//	XMFLOAT4X4 fwvp;
//	XMStoreFloat4x4(&fwvp, mwvp);
//
//	//ワールド変換行列
//	XMStoreFloat4x4(&matWorld, mw);
//
//	light_direction = {1,1,1,1};
//	material_color = {1,1,1,1};
//
//
//	skinned_mesh::render(shader,tex,fwvp, matWorld, light_direction, material_color, speed, motion, blender_type);
//}

void Skin_Mesh::Render(Shader * shader, Texture * tex, const XMMATRIX & view, const XMMATRIX & projection, const XMFLOAT4 & light, const XMFLOAT4 & matColor, int blender_type, float speed)
{

	XMMATRIX mw = XMLoadFloat4x4(&matWorld);
	XMMATRIX mwvp = mw *view * projection;

	XMFLOAT4X4 fwvp;
	XMStoreFloat4x4(&fwvp, mwvp);

	//ワールド変換行列
	XMStoreFloat4x4(&matWorld, mw);

	skinned_mesh::render(shader,tex,fwvp, matWorld, light, matColor, speed, motion, blender_type);

}

void Skin_Mesh::Render(Shader * shader,const XMMATRIX & view, const XMMATRIX & projection, const XMFLOAT4 & light, const XMFLOAT4 & matColor, int blender_type, float speed)
{

	XMMATRIX mw = XMLoadFloat4x4(&matWorld);
	XMMATRIX mwvp = mw *view * projection;

	XMFLOAT4X4 fwvp;
	XMStoreFloat4x4(&fwvp, mwvp);

	//ワールド変換行列
	XMStoreFloat4x4(&matWorld, mw);

	skinned_mesh::render(shader,fwvp, matWorld, light, matColor, speed, motion, blender_type);

}


void Skin_Mesh::setMotion(int m)
{
	{
		motion = m;

		for (mesh &mesh : meshes)
		{
			mesh.skeletal_animations.at(motion).animation_tick = 0;
		}

	}
}

float Skin_Mesh::getHeight(const Vector3 & pos,Vector3& local,const Vector3& angle, Vector3& localangle)
{
	Vector3 Pos = pos;
	Pos.y += 2.0f;
	Vector3 vec(0, -1, 0);
	Vector3 out;
	float dist = 100.0f;
	if (raypick(&out, &Pos, &vec, &dist) == -1) return -10000.0f;

	return out.y;
}

int Skin_Mesh::checkRide(const Vector3 & pos, Vector3 & local, const Vector3 & angle, Vector3 & localangle)
{
	XMFLOAT4X4  fmat = getWorld();
	XMVECTOR vpos = XMLoadFloat3((XMFLOAT3*)&pos);
	XMMATRIX mat = XMLoadFloat4x4(&fmat);
	//ワールド変換の逆行列算出
	mat = XMMatrixInverse(nullptr, mat);
	//レイピック位置をローカル位置に変換
	vpos = XMVector3Transform(vpos, mat);
	Vector3 p;
	XMStoreFloat3((XMFLOAT3*)&p, vpos);
	//ローカル座標でレイピック
	Vector3 pp = p;
	pp.y += 2.0f;
	Vector3 vec(0, -1, 0);
	float d = 100.0f;
	Vector3 out;
	if (raypick(&out, &pp, &vec, &d) >= 0) {
		//当たりの位置保存
		if (p.y < out.y) {
			local = out;
			localangle.x = angle.x - this->angle.x;
			localangle.y = angle.y - this->angle.y;
			localangle.z = angle.z - this->angle.z;
			return 0;
		}
	}
	return -1;
}

int Skin_Mesh::checkFrontward(const Vector3 & pos, Vector3 & local, const Vector3 & angle, Vector3 & localangle)
{
	XMFLOAT4X4  fmat = getWorld();
	XMVECTOR vpos = XMLoadFloat3((XMFLOAT3*)&pos);
	XMMATRIX mat = XMLoadFloat4x4(&fmat);
	//ワールド変換の逆行列算出
	mat = XMMatrixInverse(nullptr, mat);
	//レイピック位置をローカル位置に変換
	vpos = XMVector3Transform(vpos, mat);
	Vector3 p;
	XMStoreFloat3((XMFLOAT3*)&p, vpos);
	//ローカル座標でレイピック
	Vector3 pp = p;

	pp.z += 2.0f;
	Vector3 vec(0, 1.5f, -2);
	float d =2000.0f;
	Vector3 out,out2;
	

	if (raypick(&out, &pp, &vec, &d) >= 0) {
		//当たりの位置保存
		if (p.z < out.z) {
			local = out;
			localangle.x = angle.x - this->angle.x;
			localangle.y = angle.y - this->angle.y;
			localangle.z = angle.z - this->angle.z;
			return 0;
		}
	}

	return -1;
}

int Skin_Mesh::checkFrontward2(const Vector3 & pos, Vector3 & local, const Vector3 & angle, Vector3 & localangle)
{
	XMFLOAT4X4  fmat = getWorld();
	XMVECTOR vpos = XMLoadFloat3((XMFLOAT3*)&pos);
	XMMATRIX mat = XMLoadFloat4x4(&fmat);
	//ワールド変換の逆行列算出
	mat = XMMatrixInverse(nullptr, mat);
	//レイピック位置をローカル位置に変換
	vpos = XMVector3Transform(vpos, mat);
	Vector3 p;
	XMStoreFloat3((XMFLOAT3*)&p, vpos);
	//ローカル座標でレイピック
	Vector3 pp = p;

	pp.z += 1.0f;
	Vector3 vec(0,4.0f,-0.5f);
	float d = 80.0f;
	Vector3 out, out2;


	if (raypick(&out, &pp, &vec, &d) >= 0) {
		//当たりの位置保存
		if (p.z < out.z) {
			local = out;
			localangle.x = angle.x - this->angle.x;
			localangle.y = angle.y - this->angle.y;
			localangle.z = angle.z - this->angle.z;
			return 0;
		}
	}
	return -1;
}

int Skin_Mesh::checkLeft(const Vector3 & pos, Vector3 & local, const Vector3 & angle, Vector3 & localangle)
{
	XMFLOAT4X4  fmat = getWorld();
	XMVECTOR vpos = XMLoadFloat3((XMFLOAT3*)&pos);
	XMMATRIX mat = XMLoadFloat4x4(&fmat);
	//ワールド変換の逆行列算出
	mat = XMMatrixInverse(nullptr, mat);
	//レイピック位置をローカル位置に変換
	vpos = XMVector3Transform(vpos, mat);
	Vector3 p;
	XMStoreFloat3((XMFLOAT3*)&p, vpos);
	//ローカル座標でレイピック
	Vector3 pp = p;
	pp.x -= 1.0f;
	Vector3 vec(0.5f, 0, 0);
	float d = 5.0f;
	Vector3 out;
	if (raypick(&out, &pp, &vec, &d) >= 0) {
		//当たりの位置保存
		if (p.x > out.x) {
			local = out;
			localangle.x = angle.x - this->angle.x;
			localangle.y = angle.y - this->angle.y;
			localangle.z = angle.z - this->angle.z;
			return 0;
		}
	}
	return -1;
}

int Skin_Mesh::checkRight(const Vector3 & pos, Vector3 & local, const Vector3 & angle, Vector3 & localangle)
{
	XMFLOAT4X4  fmat = getWorld();
	XMVECTOR vpos = XMLoadFloat3((XMFLOAT3*)&pos);
	XMMATRIX mat = XMLoadFloat4x4(&fmat);
	//ワールド変換の逆行列算出
	mat = XMMatrixInverse(nullptr, mat);
	//レイピック位置をローカル位置に変換
	vpos = XMVector3Transform(vpos, mat);
	Vector3 p;
	XMStoreFloat3((XMFLOAT3*)&p, vpos);
	//ローカル座標でレイピック
	Vector3 pp = p;
	pp.x += 1.5f;
	Vector3 vec(-0.5f, 0, 0);
	float d = 10.0f;
	Vector3 out;
	if (raypick(&out, &pp, &vec, &d) >= 0) {
		//当たりの位置保存
		if (p.x < out.x) {
			local = out;
			localangle.x = angle.x - this->angle.x;
			localangle.y = angle.y - this->angle.y;
			localangle.z = angle.z - this->angle.z;
			return 0;
		}
	}
	return -1;
}

int Skin_Mesh::checkLeft2(const Vector3 & pos, Vector3 & local, const Vector3 & angle, Vector3 & localangle)
{
	XMFLOAT4X4  fmat = getWorld();
	XMVECTOR vpos = XMLoadFloat3((XMFLOAT3*)&pos);
	XMMATRIX mat = XMLoadFloat4x4(&fmat);
	//ワールド変換の逆行列算出
	mat = XMMatrixInverse(nullptr, mat);
	//レイピック位置をローカル位置に変換
	vpos = XMVector3Transform(vpos, mat);
	Vector3 p;
	XMStoreFloat3((XMFLOAT3*)&p, vpos);
	//ローカル座標でレイピック
	Vector3 pp = p;
	pp.x -= 1.0f;
	Vector3 vec(1.0f, 1.5f, 0);
	float d = 20.0f;
	Vector3 out;
	if (raypick(&out, &pp, &vec, &d) >= 0) {
		//当たりの位置保存
		if (p.x > out.x) {
			local = out;
			localangle.x = angle.x - this->angle.x;
			localangle.y = angle.y - this->angle.y;
			localangle.z = angle.z - this->angle.z;
			return 0;
		}
	}
	return -1;
}

int Skin_Mesh::checkRight2(const Vector3 & pos, Vector3 & local, const Vector3 & angle, Vector3 & localangle)
{
	XMFLOAT4X4  fmat = getWorld();
	XMVECTOR vpos = XMLoadFloat3((XMFLOAT3*)&pos);
	XMMATRIX mat = XMLoadFloat4x4(&fmat);
	//ワールド変換の逆行列算出
	mat = XMMatrixInverse(nullptr, mat);
	//レイピック位置をローカル位置に変換
	vpos = XMVector3Transform(vpos, mat);
	Vector3 p;
	XMStoreFloat3((XMFLOAT3*)&p, vpos);
	//ローカル座標でレイピック
	Vector3 pp = p;
	pp.x += 1.0f;
	Vector3 vec(-1.0f, 1.5f, 0);
	float d = 100.0f;
	Vector3 out;
	if (raypick(&out, &pp, &vec, &d) >= 0) {
		//当たりの位置保存
		if (p.x < out.x) {
			local = out;
			localangle.x = angle.x - this->angle.x;
			localangle.y = angle.y - this->angle.y;
			localangle.z = angle.z - this->angle.z;
			return 0;
		}
	}
	return -1;
}

void Skin_Mesh::carrierToWorld(Vector3 & inout, Vector3 & inoutangle)
{
	
	XMFLOAT4X4  fmat = getWorld();
	XMMATRIX mat = XMLoadFloat4x4(&fmat);
	XMVECTOR vpos = XMLoadFloat3((XMFLOAT3*)&inout);
	//ローカル座標をワールド座標へ変換
	vpos = XMVector3Transform(vpos, mat);
	
	XMStoreFloat3((XMFLOAT3*)&inout, vpos);
	
	inoutangle.x += this->angle.x;
	inoutangle.y += this->angle.y;
	inoutangle.z += this->angle.z;

}

int Skin_Mesh::raypick(Vector3 * out, Vector3 * pos, Vector3 * vec, float * dist)
{
	return skinned_mesh::raypick(out, pos, vec, dist);
}
