#include "Sprite.hlsli"

[maxvertexcount(3)]
void main(
	triangle PSInput input[3],
	uint id : SV_PrimitiveID,
	inout TriangleStream<PSInput> TriStream
)
{
	PSInput output = (PSInput)0;
	int v;
	for (v = 0; v < 3; v++)
	{
		output.position = input[v].position;
		output.color = input[v].color;

		//1頂点データをストリームに追加
		TriStream.Append(output);

	}
	TriStream.RestartStrip();
}