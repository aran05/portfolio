#include "shadow2.hlsli"

float4 main(PSInputShadow input) : SV_TARGET
{
	float4 color = input.Depth.z / input.Depth.w;
	color.a = 1.0f;
	return color;
}