#include "Toon.hlsli"

struct GS_INPUT
{
	float4 position: SV_POSITION;
	float edge : COLOR1;
};
[maxvertexcount(6)]
void main(
	/*lineadj GS_INPUT input[4]*/triangle VS_INPUT input[3],
	uint id : SV_PrimitiveID,
	inout TriangleStream<VS_INPUT> Stream
)
{
	VS_INPUT output = (VS_INPUT)0;

	float3 v1 = input[1].position.xyz - input[0].position.xyz;
	float3 v2 = input[2].position.xyz - input[0].position.xyz;
	float3 N = normalize(cross(v1, v2));

	float3 G = (input[1].position.xyz + input[1].position.xyz + input[2].position.xyz) / 3;
	float4 P = float4(G + N, 1.0f);
	float3 vf[3] = { input[0].position.xyz - P.xyz,input[1].position.xyz - P.xyz ,input[2].position.xyz - P.xyz };
	float3 nf[3] = { normalize(cross(vf[0],vf[1])),normalize(cross(vf[1],vf[2])) ,normalize(cross(vf[2],vf[0])) };

	for (int i = 0; i<3; i++)
	{

		for (int j = 0; j < 3; j++)
		{
			output.tex = input[j].tex;
			output.color = input[j].color;
			output.position = mul(matWVP, input[j].position);
			output.normal = mul((float3x3)World, N);
			output.wPos = mul(World, input[j].position).xyz;

			Stream.Append(output);
		}

		output.position = mul(matWVP, P);
		output.color = input[i].color;
		output.tex = input[i].tex;
		output.normal = mul((float3x3)World, nf[i]);
		output.wPos = mul(World, input[i].position).xyz;
		Stream.Append(output);

	}

	Stream.RestartStrip();
}