#include "Toon.hlsli"
// 入力制御点
struct VS_CONTROL_POINT_OUTPUT
{
	float3 vPosition : WORLDPOS;
	// TODO: 他のスタッフの変更/追加
};

// 出力制御点
struct HS_CONTROL_POINT_OUTPUT
{
	float3 vPosition : WORLDPOS; 
};

// 出力パッチ定数データ。
struct HSConstantOutput
{
	float factor[3]  : SV_TessFactor;
	float inner_factor : SV_InsideTessFactor;
};

#define NUM_CONTROL_POINTS 3

// パッチ定数関数
HSConstantOutput HSConstant(
	InputPatch<Input, 3> ip,
	uint pid : SV_PrimitiveID)
{
	HSConstantOutput output = (HSConstantOutput)0;

	float devide = 2;
	output.factor[0] = devide;
	output.factor[1] = devide;
	output.factor[2] = devide;
	output.inner_factor = devide;

	return output;
}

[domain("tri")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("HSConstant")]
DSInput main(
	InputPatch<Input, 3> input,
	uint cpid : SV_OutputControlPointID,
	uint PatchID : SV_PrimitiveID )
{
	DSInput output = (DSInput)0;
	output.position = input[cpid].position;
	output.color = input[cpid].color;
	output.tex = input[cpid].tex;
	output.normal = input[cpid].normal;

	return output;
}
