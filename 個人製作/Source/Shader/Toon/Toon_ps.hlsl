#include "Toon.hlsli"
Texture2D diffuse_map : register(t0);
SamplerState diffuse_map_sampler_state : register(s0);


Texture2D ToonTex : register(t1);
SamplerState ToonSampler : register(s1);


float4 main(VS_INPUT input) : SV_TARGET
{

	// ����
	float3 Ambient = AmbientColor.rgb;//��
	// �g�U����
	float3 C = normalize(LightColor.rgb);//�����ˌ�(�F�Ƌ���)
	float3 L = normalize(LightDir.rgb);	//�����ˌ��̌���
	float3 N = normalize(input.normal);
	
	float3 K = float3(1, 1, 1);	//���˗�
	
	

	// ���ʔ���
	float3 E = normalize(EyePos.xyz - input.wPos);
	E = normalize(E);
	float3 D = Diffuse(N, L, C, K);
	float3 S = Specular(N, L, C, E, K, 20);
	
	
	////�n�[�t�����o�[�g
	float p = dot(input.normal,L);
	p = (p + 1.0f) * 0.5f;
	p = p*p;

	ToonTex.Sample(ToonSampler, float2(p,0));
	
	
	float4 diffuseColor = diffuse_map.Sample(diffuse_map_sampler_state,input.tex)*input.color;
	float4 d;

	
	d = diffuseColor*float4(Ambient + D + S, 1.0f);
	
	return d;
}
