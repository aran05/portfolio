struct VS_INPUT
{
	float4 position : SV_POSITION;
	float3 normal   : NORMAL;
	float4 color : COLOR;
	float  edge : COLOR1;
	float2 tex : TEXCOORD;
	float4 subpos : POSITION0;
	float3 ToonUV: TEXCOORD2;
	float3 wPos	  :TEXCOORD1;
};

struct Input
{
	float4 position : POSITION;
	float3 normal   : NORMAL;
	float2 tex      : TEXCOORD;
	float4 color    : COLOR;
};

struct PS_OUTPUT
{
	float4 color  : COLOR;
	float4 color2 : COLOR1;
};

struct DSInput
{
	float4 position : POSITION;
	float3 normal   : NORMAL;
	float2 tex      : TEXCOORD;
	float4 color    : COLOR;

};

#define MAX_BONES 32

cbuffer CONSTANT_BUFFER : register(b0)
{
	row_major float4x4 matWVP;
	row_major float4x4 World;
	float4 material_color;
	float4 light_direction;
	row_major float4x4 bone_transforms[MAX_BONES];
};




struct POINTLIGHT
{
	float4 index_range_type;
	float4 pos;
	float4 color;
};

#define POINTMAX 32

cbuffer CBPerFrame : register(b1)
{
	float4  LightColor;		//ライトの色
	float4	LightDir;		//ライトの方向
	float4  AmbientColor;	//環境光
	float4	EyePos;			//カメラ位置
	//float4  UV;
};

#define DiffuseTest  0
#define SpecularTest 0


//--------------------------------------------
//	拡散反射関数
//--------------------------------------------
// N:法線(正規化済み)
// L:入射ベクトル(正規化済み)
// C:入射光(色・強さ)
// K:反射率(0〜1.0)
#if DiffuseTest
//ランバートシェーディング
float3 Diffuse(float3 N, float3 L, float3 C, float3 K)
{
	float D = dot(N, -L);
	D = max(0, D);			// 負の値を０にする
	return K * C * D;
}
#else
//ハーフランバートシェーディング
float3 Diffuse(float3 N, float3 L, float3 C, float3 K)
{
	float D = dot(N, -L);
	D = (D + 1.0) * 0.5;	// 0〜1.0までの範囲
	D = D * D;
	return K * C * D;
}
#endif


//--------------------------------------------
//	鏡面反射関数
//--------------------------------------------
// N:法線(正規化済み)
// L:入射ベクトル(正規化済み)
// C:入射光(色・強さ)
// E:視点方向ベクトル(正規化済み)
// K:反射率(0〜1.0)
// Power:ハイライトの強さ(輝き度)

#if SpecularTest
//フォンシェーディング
float3 Specular(float3 N, float3 L, float3 C, float3 E,
	float3 K, float Power)
{
	float3 R = reflect(L, N);
	R = normalize(R);
	float3 S = dot(R, E);
	S = max(0, S);
	S = pow(S, Power);
	S = S*K*C;
	return S;
}
#else
//ブリン・フォンシェーディング
float3 Specular(float3 N, float3 L, float3 C, float3 E,
	float3 K, float Power)
{
	float3 H = E + (-L);
	H = normalize(H);
	float3 S = dot(H, N);
	S = max(0, S);
	S = pow(S, Power);
	S = S*K*C;
	return S;
}
#endif