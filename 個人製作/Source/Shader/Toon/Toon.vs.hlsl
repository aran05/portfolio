#include "Toon.hlsli"

VS_INPUT main(float4 position : POSITION, float4 normal : NORMAL, float2 tex : TEXCOORD,float4 bone_weights : WEIGHTS, uint4  bone_indices : BONES)
{
	VS_INPUT vout = (VS_INPUT)0;
	

	float3 p = { 0, 0, 0 };
	float3 n = { 0, 0, 0 };
	
	for (int i = 0; i < 4; i++)
	{
		p += (bone_weights[i] * mul(position, bone_transforms[bone_indices[i]])).xyz;
		n += (bone_weights[i] * mul(float4(normal.xyz, 0), bone_transforms[bone_indices[i]])).xyz;
	}
	
	position = float4(p, 1.0f);
	normal = float4(n, 1.0f);
	
	

	// 頂点座標、テクスチャ座標
	float4 w_pos= mul(position, matWVP);
	vout.position = w_pos;
	vout.tex = tex;

	// トゥーンテクスチャ座標（ペイント）
	float3 w_normal = normalize(mul(normal, (float3x3)World));

	vout.ToonUV.x = max(0, dot(w_normal, -LightDir)) + AmbientColor;

	// トゥーンテクスチャ座標（インク）
	float3 wv_pos = normalize(mul(position, matWVP));
	float3 wv_normal = normalize(mul(normal, (float3x3)matWVP));
	vout.ToonUV.y = dot(wv_pos, wv_normal)*0.5f + 0.5f;

	vout.subpos = mul(w_pos + w_normal*10*length(EyePos-vout.wPos), matWVP);

	vout.color = material_color;
	vout.color.a = material_color.a;
	
	vout.normal = normal;
	
	return vout;
} 
