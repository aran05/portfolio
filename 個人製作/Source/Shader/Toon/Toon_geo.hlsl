#include "Toon.hlsli"
struct GS_INPUT
{
	float4 position: SV_POSITION;
	float edge : COLOR1;
};
[maxvertexcount(9)]
void main(
	triangle VS_INPUT input[3]/*triangle VS_INPUT input[3]*/,
	uint id : SV_PrimitiveID,
	inout TriangleStream<VS_INPUT> Stream
)
{
	
	VS_INPUT output = (VS_INPUT)0;

	const float normal_size = 0.1f;
	float3 dx;
	float3 dy;


	dx = input[1].position.xyz - input[0].position.xyz;
	dy = input[2].position.xyz - input[0].position.xyz;

	float3 N = normalize(cross(normalize(dx), normalize(dy)));


	for (int v = 0; v < 3; v++)
	{
		output.position = input[v].position;
		output.color    = input[v].color;
		output.tex      = input[v].tex;
		output.normal   = N;
		//1頂点データをストリームに追加
		Stream.Append(output);
	}
	Stream.RestartStrip();
}