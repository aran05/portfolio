#include "Toon.hlsli"

struct HSConstantOutput
{
	float factor[3]  : SV_TessFactor;
	float inner_factor : SV_InsideTessFactor;
};


#define NUM_CONTROL_POINTS 3

[domain("tri")]
VS_INPUT main(
	HSConstantOutput input,
	float3 UV : SV_DomainLocation,
	const OutputPatch<DSInput, 3> patch)
{
	VS_INPUT output = (VS_INPUT)0;
	float4 pos = patch[0].position*UV.x + patch[1].position*UV.y + patch[2].position*UV.z;
	output.position = pos;

	//uv座標算出
	float2 Tex = patch[0].tex*UV.x + patch[1].tex*UV.y + patch[2].tex*UV.z;
	output.tex = Tex;

	//法線取得
	float3 N = patch[0].normal*UV.x + patch[1].normal*UV.y + patch[2].normal*UV.z;
	N = normalize(N);
	output.normal = N;

	float4 C = patch[0].color.x*UV.x + patch[1].color.y*UV.y + patch[2].color.z*UV.z;
	output.color = C;



	return output;
}
