#include "shadow.hlsli"

float4 main(PSInput input) : SV_TARGET
{
	// ����
	float3 Ambient = AmbientColor.rgb;//��
									  // �g�U����
	float3 C = LightColor.rgb;	//�����ˌ�(�F�Ƌ���)
	float3 L = LightDir.rgb;	//�����ˌ��̌���
	L = normalize(L);//���K��
					 // �@��
	float3 N = input.wNormal;
	N = normalize(N);//���K��

	float3 Kd = float3(1, 1, 1);	//���˗�
	float3 D = Diffuse(N, L, C, Kd);

	// ���ʔ�
	float3 E = input.wEyeDir;
	E = normalize(E);
	float3 Ks = float3(1, 1, 1);
	float3 S = Specular(N, L, C, E, Ks, 20);
	float4 color = DiffuseTexture.Sample(DecalSampler, input.tex)
		* input.color/**float4(Ambient + D + S, 1.0f)*/;
	//�V���h�[�}�b�v�K�p
	color.rgb *= GetShadow(input.vShadow);

	return color;
}