Texture2D DiffuseTexture : register(t0);
SamplerState DecalSampler : register(s0);

Texture2D ShadowTexture : register(t3);
SamplerState ShadowSampler : register(s3);



cbuffer CBPerMesh : register(b0)
{
	matrix  World;
	matrix	matWVP;
};

//--------------------------------------------
//	グローバル変数
//------------------------------------------
cbuffer CBPerFrame : register(b1)
{
	float4  LightColor;		//ライトの色
	float4	LightDir;		//ライトの方向
	float4  AmbientColor;	//環境光
	float4	EyePos;			//カメラ位置
	float4  uv;
	float4x4  ShadowViewProjection;
};

static const float AdjustValue = 0.003f;
static const float3 ShadowColor = { 0.8f,0.6f,0.6f };

//--------------------------------------------
//	データーフォーマット
//--------------------------------------------
struct VSInput
{
	float3 position : POSITION;
	float3 normal   : NORMAL;
	float2 tex      : TEXCOORD;
	float4 color    : COLOR;
};

struct PSInput
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD;
	float4 color : COLOR;
	float3 wNormal: TEXCOORD1;
	float3 wEyeDir: TEXCOORD2;
	float3 vShadow: TEXCOORD5;
};



//--------------------------------------------
//	頂点シェーダー
//--------------------------------------------
inline float3 GetShadowTex(float3 wPos)
{
	//シャドウ空間座標
	float4 svpPos;
	svpPos.xyz = wPos;
	svpPos.w = 1;
	svpPos = mul(ShadowViewProjection, svpPos);
	//シャドウマップ座標に変換
	svpPos /= svpPos.w;
	svpPos.y = -svpPos.y;
	svpPos.xy = 0.5f*svpPos.xy + 0.5f;
	return svpPos.xyz;
}
inline float3 GetShadow(float3 Tex)
{
	//シャドウマップから深度を取り出す
	float d = ShadowTexture.Sample(ShadowSampler, Tex.xy).r;

	float3 l = (Tex.z - d > AdjustValue) ? ShadowColor : 1.0;
	return l;
}

#define DiffuseTest 0
#define SpecularTest 0

//--------------------------------------------
//	拡散反射関数
//--------------------------------------------
// N:法線(正規化済み)
// L:入射ベクトル(正規化済み)
// C:入射光(色・強さ)
// K:反射率(0〜1.0)
#if DiffuseTest
//ランバートシェーディング
float3 Diffuse(float3 N, float3 L, float3 C, float3 K)
{
	float D = dot(N, -L);
	D = max(0, D);			// 負の値を０にする
	return K * C * D;
}
#else
//ハーフランバートシェーディング
float3 Diffuse(float3 N, float3 L, float3 C, float3 K)
{
	float D = dot(N, -L);
	D = (D + 1.0) * 0.5;	// 0〜1.0までの範囲
	D = D * D;
	return K * C * D;
}
#endif


//--------------------------------------------
//	鏡面反射関数
//--------------------------------------------
// N:法線(正規化済み)
// L:入射ベクトル(正規化済み)
// C:入射光(色・強さ)
// E:視点方向ベクトル(正規化済み)
// K:反射率(0〜1.0)
// Power:ハイライトの強さ(輝き度)

#if SpecularTest
//フォンシェーディング
float3 Specular(float3 N, float3 L, float3 C, float3 E,
	float3 K, float Power)
{
	float3 R = reflect(L, N);
	R = normalize(R);
	float3 S = dot(R, E);
	S = max(0, S);
	S = pow(S, Power);
	S = S*K*C;
	return S;
}
#else
//ブリン・フォンシェーディング
float3 Specular(float3 N, float3 L, float3 C, float3 E,
	float3 K, float Power)
{
	float3 H = E + (-L);
	H = normalize(H);
	float3 S = dot(H, N);
	S = max(0, S);
	S = pow(S, Power);
	S = S*K*C;
	return S;
}
#endif