#include "skinned_mesh.hlsli"
Texture2D diffuse_map : register(t0);
SamplerState diffuse_map_sampler_state : register(s0);


float4 main(VS_INPUT input) : SV_TARGET
{
	// ����
	float3 Ambient = AmbientColor.rgb; // �g�U����

	float3 C = normalize(LightColor.rgb);//�����ˌ�(�F�Ƌ���)
	float3 L = normalize(LightDir.rgb);	//�����ˌ��̌���
	float3 N = normalize(input.normal);

	float3 K = float3(1, 1, 1);	//���˗�


								// ���ʔ���
	float3 E = normalize(EyePos.xyz - input.wPos);
	E = normalize(E);
	float3 D = Diffuse(N, L, C, K);
	float3 S = Specular(N, L, C, E, K, 20);



	float4 diffuseColor = diffuse_map.Sample(diffuse_map_sampler_state,input.tex)*input.color;
	float4 color;

	color = diffuseColor*float4(Ambient + D + S, 1.0f);


	return color;

}
