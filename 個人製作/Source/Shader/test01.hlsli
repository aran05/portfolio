struct VS_OUT
{
	float4 position : SV_POSITION;
	float3 normal   : NORMAL;
	float4 color : COLOR;
	float2 tex : TEXCOORD;
};

#define MAX_BONES 32

cbuffer CONSTANT_BUFFER : register(b0)
{

	row_major float4x4 World_Proj;
	row_major float4x4 matWVP;
	float4 material_color; //マテリアルカラー
	float4 light_direction;//
	row_major float4x4 bone_transforms[MAX_BONES];
};

#define POINTMAX 32

cbuffer CBPerFrame : register(b1)
{
	float4  LightColor;		//ライトの色
	float4	LightDir;		//ライトの方向
	float4  AmbientColor;	//環境光
	float4	EyePos;			//カメラ位置
	float4  UV;
};

Texture2D DiffuseTexture : register(t0);
SamplerState DiffuseSampler : register(s0);

