#include "shadow2.hlsli"
PSInputShadow main(VSInput input)
{
	PSInputShadow output = (PSInputShadow)0;

	float4 P = float4(input.position, 1.0);

	// ワールド変換座標
	output.Position = mul(matWVP, P);

	//出力値設定
	output.Depth = output.Position;
	return output;

}