#include "skinned_shadow.hlsli"
PSInputShadow main(float4 position : POSITION, float4 normal : NORMAL, float2 texcoord : TEXCOORD, float4 bone_weights : WEIGHTS, uint4  bone_indices : BONES)
{
	float3 p = { 0, 0, 0 };
	for (int i = 0; i < 4; i++)
	{
		p += (bone_weights[i] * mul(position, bone_transforms[bone_indices[i]])).xyz;
	}
	position = float4(p, 1.0f);
	PSInputShadow vout;
	vout.position = mul(position, World_Proj);
	vout.Depth = vout.position;

	return vout;
}
