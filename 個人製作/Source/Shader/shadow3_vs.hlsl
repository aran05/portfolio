#include "shadow3.hlsli"

VS_OUT main(float4 position : POSITION, float4 normal : NORMAL, float2 tex : TEXCOORD/*UNIT.17*/, float4 bone_weights : WEIGHTS, uint4  bone_indices : BONES)
{

	// UNIT.21
	float3 p = { 0, 0, 0 };
	float3 n = { 0, 0, 0 };
	for (int i = 0; i < 4; i++)
	{
		p += (bone_weights[i] * mul(position, bone_transforms[bone_indices[i]])).xyz;
		n += (bone_weights[i] * mul(float4(normal.xyz, 0), bone_transforms[bone_indices[i]])).xyz;
	}

	position = float4(p, 1.0f);
	normal = float4(n, 1.0f);

	VS_OUT vout = (VS_OUT)0;
	float4 worldPos = mul(position, matWVP);
	float4 wPos = mul(position, World);


	//normal.w = 0;
	float4 N = normalize(mul(normal, World));
	float3 E = EyePos.xyz;				// カメラ位置

	E = normalize(E - worldPos.xyz);	// 視点方向ベクトル

	//出力値設定.
	vout.position = worldPos;
	vout.color = material_color;
	vout.color.a = material_color.a;
	vout.tex = tex;
	vout.wNormal = N;
	vout.wEyeDir = E;
	float4 ps= mul(position, matWVP);
	ps = mul(ps, World);
	//シャドーマップ空間座標に変換
	vout.vShadow = GetShadowTex(ps.xyz);

	return vout;
}
