#include "Sea.hlsli"
// 入力制御点
struct VS_CONTROL_POINT_OUTPUT
{
	float3 vPosition : WORLDPOS;
	// TODO: 他のスタッフの変更/追加
};

// 出力制御点
struct HS_CONTROL_POINT_OUTPUT
{
	float3 vPosition : WORLDPOS;
};

// 出力パッチ定数データ。
struct HSConstantOutput
{
	float factor[3]  : SV_TessFactor;
	float inner_factor : SV_InsideTessFactor;
};

#define NUM_CONTROL_POINTS 3

// パッチ定数関数
HSConstantOutput HSConstant(
	InputPatch<HSInput, 3> ip,
	uint pid : SV_PrimitiveID)
{
	HSConstantOutput output = (HSConstantOutput)0;

	float devide = 32;
	output.factor[0] = DivideOut_In;
	output.factor[1] = DivideOut_In;
	output.factor[2] = DivideOut_In;
	output.inner_factor = DivideOut_In;

	return output;
}

[domain("tri")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("HSConstant")]
DSInput main(
	InputPatch<HSInput, 3> input,
	uint cpid : SV_OutputControlPointID,
	uint PatchID : SV_PrimitiveID)
{
	DSInput output = (DSInput)0;
	output.Position = input[cpid].Position;
	output.Color = input[cpid].Color;
	output.Tex = input[cpid].Tex;
	output.Normal = input[cpid].Normal;

	return output;
}