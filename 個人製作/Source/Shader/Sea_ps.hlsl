#include "Sea.hlsli"

float4 main(GSPSInput input) : SV_TARGET
{
	return DiffuseTexture.Sample(DecalSampler, input.Tex)
	* input.Color;
}