Texture2D DiffuseTexture : register(t0);
SamplerState DecalSampler : register(s0);

Texture2D NormalTexture : register(t4);
SamplerState NormalSampler : register(s4);

Texture2D HeightTexture : register(t5);
SamplerState HeightSampler : register(s5);


//--------------------------------------------
//	グローバル変数
//--------------------------------------------
cbuffer CBPerMesh : register(b0)
{
	matrix  World;
	matrix	matWVP;
};


struct VSInput
{
	float4 Position : POSITION;
	float3 Normal   : NORMAL;
	float2 Tex      : TEXCOORD;
	float4 Color    : COLOR;
};

struct HSInput
{
	float4 Position : POSITION;
	float3 Normal   : NORMAL;
	float2 Tex      : TEXCOORD;
	float4 Color    : COLOR;

};

struct DSInput
{
	float4 Position : POSITION;
	float3 Normal   : NORMAL;
	float2 Tex      : TEXCOORD;
	float4 Color    : COLOR;

};


struct GSPSInput
{
	float4 Position : SV_POSITION;
	float3 wNormal	: NORMAL;
	float2 Tex		: TEXCOORD;
	float4 Color	: COLOR;
	float3 wPos		: TEXCOORD1;
};

cbuffer CBPerFrame : register(b2)
{
	float4  DivideOut_In;
	float4  adjustUV;
};

struct POINTLIGHT
{
	float4 index_range_type;
	float4 pos;
	float4 color;
};

#define POINTMAX 32

#define DiffuseTest  0
#define SpecularTest 0


//--------------------------------------------
//	拡散反射関数
//--------------------------------------------
// N:法線(正規化済み)
// L:入射ベクトル(正規化済み)
// C:入射光(色・強さ)
// K:反射率(0〜1.0)
#if DiffuseTest
//ランバートシェーディング
float3 Diffuse(float3 N, float3 L, float3 C, float3 K)
{
	float D = dot(N, -L);
	D = max(0, D);			// 負の値を０にする
	return K * C * D;
}
#else
//ハーフランバートシェーディング
float3 Diffuse(float3 N, float3 L, float3 C, float3 K)
{
	float D = dot(N, -L);
	D = (D + 1.0) * 0.5;	// 0〜1.0までの範囲
	D = D * D;
	return K * C * D;
}
#endif


//--------------------------------------------
//	鏡面反射関数
//--------------------------------------------
// N:法線(正規化済み)
// L:入射ベクトル(正規化済み)
// C:入射光(色・強さ)
// E:視点方向ベクトル(正規化済み)
// K:反射率(0〜1.0)
// Power:ハイライトの強さ(輝き度)

#if SpecularTest
//フォンシェーディング
float3 Specular(float3 N, float3 L, float3 C, float3 E,
	float3 K, float Power)
{
	float3 R = reflect(L, N);
	R = normalize(R);
	float3 S = dot(R, E);
	S = max(0, S);
	S = pow(S, Power);
	S = S*K*C;
	return S;
}
#else
//ブリン・フォンシェーディング
float3 Specular(float3 N, float3 L, float3 C, float3 E,
	float3 K, float Power)
{
	float3 H = E + (-L);
	H = normalize(H);
	float3 S = dot(H, N);
	S = max(0, S);
	S = pow(S, Power);
	S = S*K*C;
	return S;
}
#endif