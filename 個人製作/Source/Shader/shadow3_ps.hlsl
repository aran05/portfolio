#include "shadow3.hlsli"



static const float viewOffset = 0.08f;

float4 main(VS_OUT input) : SV_TARGET
{
	// ����
	float3 Ambient = AmbientColor.rgb;//��
									  // �g�U����
	float3 C = normalize(LightColor.rgb);//�����ˌ�(�F�Ƌ���)
	float3 L = normalize(LightDir.rgb);	//�����ˌ��̌���
	float3 N = normalize(input.wNormal);

	float3 Kd = float3(1, 1, 1);	//���˗�
	float3 D = Diffuse(N, L, C, Kd);

	// ���ʔ�
	float3 E = input.wEyeDir;
	E = normalize(E);
	float3 Ks = float3(1, 1, 1);
	float3 S = Specular(N, L, C, E, Ks, 20);
	float4 color = DiffuseTexture.Sample(DecalSampler, input.tex)
		* input.color*float4(Ambient + D + S, 1.0f);
	//�V���h�[�}�b�v�K�p
	color.rgb *= GetShadow(input.vShadow);

	return color; 
}