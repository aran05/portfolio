struct VSInput
{
	float3 position : POSITION;
	float3 normal   : NORMAL;
	float2 tex      : TEXCOORD;
	float4 color    : COLOR;
};

struct PSInputShadow
{
	float4 Position : SV_POSITION;
	float4 Depth: TEXCOORD2;//�[�x�l
};


cbuffer CONSTANT_BUFFER : register(b0)
{
	matrix  World;
	matrix	matWVP;
};