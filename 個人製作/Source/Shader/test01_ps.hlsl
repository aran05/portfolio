#include "test01.hlsli"

float4 main(VS_OUT pin) : SV_TARGET
{
	return DiffuseTexture.Sample(DiffuseSampler, pin.tex)* pin.color;
}