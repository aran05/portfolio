#include "Sprite.hlsli"

Texture2D diffuse_map : register(t0);
SamplerState diffuse_map_sampler_state : register(s0);

float4 main(PSInput input) : SV_TARGET
{
	return diffuse_map.Sample(diffuse_map_sampler_state, input.tex)*input.color;
}