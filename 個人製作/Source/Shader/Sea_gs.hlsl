#include "Sea.hlsli"
struct GS_INPUT
{
	float4 position: SV_POSITION;
	float edge : COLOR1;
};
[maxvertexcount(3)]
void main(triangle GSPSInput input[3],
	uint id : SV_PrimitiveID,
	inout TriangleStream<GSPSInput> TriStream) // inout TriangleStream
{
	GSPSInput output = (GSPSInput)0;

	for (int v = 0; v < 3; v++)
	{
		output.Position = mul(matWVP, input[v].Position);
		output.wPos = mul(World, input[v].Position).xyz;
		output.wNormal = mul((float3x3)World, input[v].wNormal);
		output.Tex = input[v].Tex;
		output.Color = input[v].Color;
		TriStream.Append(output);
	}
	TriStream.RestartStrip();
}
