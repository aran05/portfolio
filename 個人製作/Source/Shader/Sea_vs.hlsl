#include "Sea.hlsli"

HSInput main(VSInput input)
{
	HSInput output = (HSInput)0;

	float4 P = input.Position;

	// 出力値設定
	output.Position = P;
	output.Color = input.Color;
	output.Tex = input.Tex;
	output.Normal = input.Normal;

	return output;
}