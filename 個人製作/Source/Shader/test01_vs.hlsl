#include "test01.hlsli"
VS_OUT main( float4 position : POSITION,float4 normal:NORMAL,float2 tex:TEXCOORD,float4 bone_weights:WEIGHTS,uint4 bone_indices:BONES)
{
	float3 p = { 0,0,0 };
	float3 n = { 0,0,0 };

	for (int i = 0; i < 4;i++)
	{
		p += (bone_weights[i] * mul(position, bone_transforms[bone_indices[i]])).xyz;
		n += (bone_weights[i] * mul(normal, bone_transforms[bone_indices[i]])).xyz;
	}

	position = float4(p, 1.0f);
	normal = float4(n, 1.0f);

	VS_OUT vsout = (VS_OUT)0;

	float4 worldPos = mul(position, World_Proj);

	float4 N = normalize(mul(normal, matWVP));
	
	vsout.position = worldPos;
	vsout.color = material_color;
	vsout.color.a = material_color.a;
	vsout.tex = tex;
	return vsout;
}