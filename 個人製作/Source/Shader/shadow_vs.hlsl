#include "shadow.hlsli"
PSInput main(float4 position : POSITION, float4 normal : NORMAL, float2 tex : TEXCOORD,float4 color:COLOR)
{
	PSInput output = (PSInput)0;

	float4 P = position;
	// ワールド変換座標
	float4 worldPos = mul(World, P);
	// 射影空間に変換(列優先)
	P = mul(matWVP, P);
	// ワールド法線算出
	float3 N = mul(World, normal);
	N = normalize(N);//正規化
	
	// 視線ベクトル算出
	float3 E = EyePos.xyz;				// カメラ位置
	E = normalize(E - worldPos.xyz);	// 視点方向ベクトル

	//出力値設定.
	output.position = P;
	output.color = color;
	output.tex = tex;
	output.wNormal = N;
	output.wEyeDir = E;
	//シャドーマップ空間座標に変換
	output.vShadow = GetShadowTex(worldPos.xyz);

	return output;
}
