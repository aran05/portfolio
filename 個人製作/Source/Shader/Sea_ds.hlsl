#include "Sea.hlsli"
struct HSConstantOutput
{
	float factor[3]  : SV_TessFactor;
	float inner_factor : SV_InsideTessFactor;
};

#define NUM_CONTROL_POINTS 3

[domain("tri")]
GSPSInput main(
	HSConstantOutput input,
	float3 UV : SV_DomainLocation,
	const OutputPatch<DSInput, 3> patch)
{
	GSPSInput output = (GSPSInput)0;

	float4 pos = patch[0].Position*UV.x + patch[1].Position*UV.y + patch[2].Position*UV.z;
	output.Position = pos;

	//uv座標算出
	float2 Tex = patch[0].Tex*UV.x + patch[1].Tex*UV.y + patch[2].Tex*UV.z;
	output.Tex = Tex;

	//法線取得
	float3 N = patch[0].Normal*UV.x + patch[1].Normal*UV.y + patch[2].Normal*UV.z;
	N = normalize(N);
	output.wNormal = N;

	// 頂点色
	float4 C = patch[0].Color.x*UV.x + patch[1].Color.y*UV.y + patch[2].Color.z*UV.z;
	output.Color = C;

	// 高さ情報取得
	float H = HeightTexture.SampleLevel(HeightSampler, output.Tex + float2(adjustUV.x, adjustUV.y), 0).x;
	H = H * 2.0f - 1.0f;

	float H2 = HeightTexture.SampleLevel(HeightSampler, output.Tex + float2(adjustUV.z, adjustUV.w), 0).x;
	H2 = H2 * 2.0f - 1.0f;

	// 合成
	float h = (H + H2);
	output.Position.xyz += N * h * 0.02f;

	return output;
}