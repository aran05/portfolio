#include <windows.h>
#include <memory>
#include <assert.h>
#include <tchar.h>

#include "A_System.h"
#include "KeyInput.h"
//#include "..\\Source\システム\A_System.h"
#include "FrameWork.h"
#include "resource.h"

BOOL	bFullScreen = FALSE;
DWORD	ScreenMode = SCREEN720p;

LRESULT CALLBACK fnWndProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	framework *f = reinterpret_cast<framework*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));
	return f ? f->handle_message(hwnd, msg, wparam, lparam) : DefWindowProc(hwnd, msg, wparam, lparam);
}

HWND InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	HWND			hWnd;

	//	スクリーンサイズ取得
	RECT	WindowSize;
	A_System::GetScreenRect(ScreenMode, WindowSize);

	//	ウィンドウクラス設定
	WNDCLASS	wc;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = fnWndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = "Run";
	RegisterClass(&wc);


	//	ウィンドウ作成
	if (!bFullScreen) {
		AdjustWindowRect(&WindowSize, WS_OVERLAPPEDWINDOW, FALSE);
		hWnd = CreateWindow("Run", "Run", WS_OVERLAPPEDWINDOW,
			0, 0, WindowSize.right - WindowSize.left, WindowSize.bottom - WindowSize.top,
			NULL, NULL, hInstance, NULL);
	}
	else {
		hWnd = CreateWindowEx(WS_EX_COMPOSITED,"Run", "Run", WS_POPUP, 0, 0, WindowSize.right, WindowSize.bottom, NULL, NULL, hInstance, NULL);
		ShowCursor(FALSE);
	}
	if (!hWnd) return NULL;

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return hWnd;
}
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	HWND	hWnd;
	//メモリーリークチェック
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtDumpMemoryLeaks();

	if (GetAsyncKeyState(VK_CONTROL) & 0x8000) bFullScreen = TRUE;

	hWnd = InitWindow(hInstance, nCmdShow);
	framework f(hWnd);

	SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(&f));

	//	全解放	
	//A_System::CloseDebugWindow();

	return f.run();

}