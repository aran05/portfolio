#pragma once

#define EFFECT_MAX 10
#include "Blend.h"
#include <string>
#include <memory>
#include "OBJ_Mesh.h"
//
//using namespace DirectX;
//
//
//class View;
//class effect
//{
//protected:
//	Microsoft::WRL::ComPtr<ID3D11Buffer> vertex_buffer;
//	Microsoft::WRL::ComPtr<ID3D11PixelShader> pixel_shader;
//	Microsoft::WRL::ComPtr<ID3D11InputLayout> input_layout;
//	
//	Microsoft::WRL::ComPtr<ID3D11VertexShader> vertex_shader;
//	unsigned int numVerteices;
//	Microsoft::WRL::ComPtr<ID3D11Buffer> index_buffer;
//	unsigned int numIndeices;
//
//	Microsoft::WRL::ComPtr<ID3D11RasterizerState> rasterizer_states[2];
//	Microsoft::WRL::ComPtr<ID3D11DepthStencilState> depth_stencil_state;
//	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> shader_resource_view;
//
//	D3D11_TEXTURE2D_DESC texture2d_desc;
//
//	Microsoft::WRL::ComPtr<ID3D11SamplerState> sampler_state;
//	
//
//	Microsoft::WRL::ComPtr<ID3D11Buffer> constant_buffer;
//
//
//	struct cbuffer
//	{
//		XMMATRIX matWVP;
//		XMFLOAT4 type;
//		XMFLOAT4 matColor;
//	};
//
//	Vector3 position;
//	Vector3 scale;
//	Vector3 angle;
//	float maxTime;
//	int type;
//
//public:
//	struct vertex
//	{
//		XMFLOAT3 position;
//		XMFLOAT4 color;
//		XMFLOAT2 texcord;
//	};
//
//	effect(const wchar_t *filename);
//	virtual ~effect(){}
//
//	void render(int type, const XMFLOAT4X4& view, XMFLOAT4X4& projection, const XMFLOAT4&light, const XMFLOAT4&matcolor, const bool flg);
//	void setPos(const Vector3 p) { position = p; }
//	void setScale(const Vector3 s) { scale = s; }
//	void setAngle(const Vector3 a) { angle = a; }
//
//};
//
//typedef struct
//{
//	int type;
//	Vector3 position;
//	Vector3 vec;
//	Vector3 power;
//	Vector3 scale;
//	XMFLOAT4 color;
//	int maxTimer, timer;
//	bool isAlive;
//}effectData;
//
//
//
//class effects
//{
//private:
//	std::unique_ptr<effect> obj;
//	std::unique_ptr<effectData[]> eff;
//	int max;
//public:
//	effects(const wchar_t *filename, int max);
//	void Update();
//	void Render(const XMMATRIX& view, const XMMATRIX& projection);
//
//	void set(int type, Vector3 position,
//		Vector3 vec,
//		Vector3 power,
//		Vector3 scale, int timer);
//	
//	void PlayEffect(Vector3 position);
//};

using namespace DirectX;

class Skin_Mesh;

//class particle
//{
//private:
//protected:
//	Microsoft::WRL::ComPtr<ID3D11VertexShader> vertex_shader;
//	Microsoft::WRL::ComPtr<ID3D11PixelShader> pixel_shader;
//	Microsoft::WRL::ComPtr<ID3D11InputLayout> input_layout;
//	Microsoft::WRL::ComPtr<ID3D11Buffer> vertex_buffer;
//	Microsoft::WRL::ComPtr<ID3D11Buffer> index_buffer;
//	Microsoft::WRL::ComPtr<ID3D11RasterizerState> rasterizer_states[2];
//	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> shader_resource_view;
//	Microsoft::WRL::ComPtr<ID3D11SamplerState> sampler_state;
//	Microsoft::WRL::ComPtr<ID3D11DepthStencilState> depth_stencil_state;
//	Microsoft::WRL::ComPtr<ID3D11Buffer> constant_buffer;
//
//	struct cbuffer
//	{
//		XMMATRIX matWVP;
//		XMFLOAT4 TYPE;
//		XMFLOAT4 matColor;
//	};
//
//	D3D11_TEXTURE2D_DESC texture2d_desc;
//	unsigned int numVerteices;
//	unsigned int numIndices;
//
//	Vector3 pos;
//	Vector3 scale;
//	Vector3 angle;
//
//	float maxTime;
//	int type;
//
//public:
//	struct vertex
//	{
//		XMFLOAT3 position;
//		XMFLOAT4 color;
//		XMFLOAT2 texcoord;
//	};
//
//	particle(const wchar_t *filename);
//	virtual ~particle() {}
//
//	void render(int type, const XMFLOAT4X4& view, const XMFLOAT4X4& projection, const XMFLOAT4& light, const XMFLOAT4& matColor, const bool flg);
//	void setPos(const Vector3 p) { pos = p; }
//	void setScale(const Vector3 s) { scale = s; }
//	void setAngle(const Vector3 a) { angle = a; }
//};

typedef struct {
	int type;
	Vector3 pos;
	Vector3 vec;
	Vector3 power;
	Vector3 scale;
	XMFLOAT4 color;
	int maxTimer, timer;
	bool isAlive;
}particleData;


class Particles
{
private:
	std::unique_ptr<Skin_Mesh> obj;
	std::unique_ptr<particleData[]> p;

	int max;
	XMFLOAT4 light_direction;
	XMFLOAT4 material_color;
public:
	Particles(const char *filename, int max);
	void update();
	void render(XMMATRIX& view,XMMATRIX& projection);
	void set(int type, Vector3 pos, Vector3 vec, Vector3 power, Vector3 scale, int timer);
	void test(Vector3 p);
	void coin(Vector3 p);
};


