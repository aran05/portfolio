#pragma once

#define	SCREEN640	0
#define	SCREEN800	1
#define	SCREEN1024	2
#define SCREENT     3

#define	SCREEN720p	11
#define	SCREEN800p	12

#define	null	NULL

#define	s8		signed char
#define	s16		short
#define	s32		int

#define	u8		unsigned char
#define	u16		unsigned short
#define	u32		unsigned long
