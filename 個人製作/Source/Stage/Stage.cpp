#include "Stage.h"
#include "KeyInput.h"
#include "Mouse\Mouse.h"
#include "loadtext\loadtext.h"
#include "Score.h"
#include	<process.h>
#include "Constant.h"
#include "sceneClear.h"


Stage::SET_DATA stage1_set00[] =
{
	{ "DATA/stage/s1.fbx"},
	{ "DATA/stage/s2.fbx"},
	{ "DATA/stage/s3.fbx"},
	{ "DATA/stage/s4.fbx" },
	{ "DATA/stage/s5.fbx" },

};




Stage::Stage()
{
}

Stage::~Stage()
{
	SAFE_DELETE(obj);
	SAFE_DELETE(line);
	SAFE_DELETE(shader);
	//SAFE_DELETE(name);
}
bool	Stage::bActive;
void Stage::Initialize(float n,Vector3 p)
{
	shader = new Shader();

#if SHADER_CHANGE
	shader->Create("DATA/Shader/test01_vs.cso", "DATA/Shader/test01_ps.cso");
#else
	shader->Create("DATA/Shader/Toon_vs.cso", "DATA/Shader/Toon_ps.cso", "DATA/Shader/Toon_gs.cso");
#endif 

	pos = p;
	move = Vector3(1.0f, 0, 0.1f);
	angle = Vector3(0, 3, 0);
	

	name = stage1_set00[(int)n].name;

	obj = new Skin_Mesh(name);
	obj->SetPos(pos);
	obj->SetScalse(Vector3(STAGE_SCALE, STAGE_SCALE, STAGE_SCALE));
	obj->Update();

	line = new Skin_Mesh(name);
	line->SetPos(pos);
	line->SetScalse(Vector3(STAGE_LINE_SCALE, STAGE_LINE_SCALE, STAGE_LINE_SCALE));
	line->Update();


}


void Stage::Update(Player& player, Vector3& local, Vector3&localangle, bool& clear_flg)
{

	
	if (obj != null)
	{
		
		//プレイヤーの位置から見てステージのz軸が長ければステージを消していく
		if (player.Get_Pos().z < obj->GetPos().z - 180)
		{
			SAFE_DELETE(obj);
			SAFE_DELETE(line);
			return;
		}
		//ゴール判定
		if (name == stage1_set00[4].name&&player.Get_Pos().z < obj->GetPos().z - 130)
		{
			clear_flg = true;
			return;
		}

		//各面の当たり判定
		if (obj != null)
		{

			//地面当たり判定
			int ret = obj->checkRide(player.Get_Pos(), local, player.Get_Angle(), localangle);
			//正面当たり判定
			int front = obj->checkFrontward2(player.Get_Pos(), local, player.Get_Angle(), localangle);
			//右当たり判定
			int right = obj->checkRight2(player.Get_Pos(), local, player.Get_Angle(), localangle);
			//左当たり判定
			int left = obj->checkLeft2(player.Get_Pos(), local, player.Get_Angle(), localangle);


			//右
			if (right >= 0 && player.Get_Jump_Flag() == false) {
				obj->carrierToWorld(local, localangle);

				player.Set_Pos(Vector3(local.x + 1.0f, player.Get_Pos().y, player.Get_Pos().z));
				player.Set_Move(move);

				if (GetKeyState(VK_LBUTTON) < MouseClick&&player.Get_Jump_Flag() == false)
				{
					A_PlaySound(SE::JUMP, false);

					player.Set_Push_Button(true);
					player.Set_Jump_Flag(true);
				}
			}
			//左
			else if (left >= 0 && player.Get_Jump_Flag() == false) {
				obj->carrierToWorld(local, localangle);
				//player.Set_Push_Button(false);
				//move.x = 0.0f;
				player.Set_Pos(Vector3(local.x - 1.0f, player.Get_Pos().y, player.Get_Pos().z));
				player.Set_Move(move);
				if (GetKeyState(VK_LBUTTON) < MouseClick&&player.Get_Jump_Flag() == false)
				{

					A_PlaySound(SE::JUMP, false);
					player.Set_Push_Button(true);
					player.Set_Jump_Flag(true);

				}
			}
			//正面
			else if (front >= 0 && player.Get_Jump_Flag() == false)
			{
				obj->carrierToWorld(local, localangle);
				player.Set_Push_Button(false);
				move.z = 5.0f;
				player.Set_Pos(Vector3(player.Get_Pos().x, player.Get_Pos().y, player.Get_Pos().z + move.z));
				player.Set_Move(move);
				player.Set_Hit_Flag(true);
				player.obj->setMotion(1);

			}
			//地面
			else if (ret >= 0 && player.Get_Jump_Flag() == false) {
				obj->carrierToWorld(local, localangle);
				player.Set_Pos(Vector3(player.Get_Pos().x, local.y - 0.01f, player.Get_Pos().z));
				player.Set_Angle(localangle);
				player.Set_Push_Button(false);

				if (GetKeyState(VK_LBUTTON) < MouseClick&&player.Get_Jump_Flag() == false)
				{
					A_PlaySound(SE::JUMP, false);

					player.Set_Push_Button(true);
					player.Set_Jump_Flag(true);
				}
				move.y = 0.0f;
				player.Set_Move(move);
			}
			else
			{
				move.x = 0.2f;
			}

			obj->Update();
			line->Update();
		}
	}
}

void Stage::Render(View * view)
{

	if (obj != NULL)
	{
#if SHADER_CHANGE
		obj->Render(shader, view->GetView(), view->GetProjection(), light_direction, material_color, framework::RS_FRONT);
#else
		line->Render(shader, view->GetView(), view->GetProjection(), light_direction,black_color, framework::RS_SOLID);
		obj->Render(shader, view->GetView(), view->GetProjection(), light_direction,material_color, framework::RS_FRONT);
#endif 
	}

}
