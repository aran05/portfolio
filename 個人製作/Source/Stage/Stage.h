#pragma once
#include "OBJ_Mesh.h"
#include "A_System.h"
#include "Player.h"
#include <sstream>
#include <memory>
#include "loadtext\loadtext.h"

#include"Math\math.h"
#include <list>
#include "effect.h"
class Player;
class Skin_Mesh;
class loadtext;
class Particles;



class Stage
{
public:
	Stage();
	~Stage();

	struct SET_DATA
	{
		const char* name;
	};

	void Initialize(float name,Vector3 p);
	void Update(Player& p,Vector3& local,Vector3&localangle,bool& clear_flg);//1:プレイヤー位置,2:ローカル座標,3ローカルアングル,4コイン枚数
	void Render(View* v);
	const char* Get_Name() { return name; }

private:
	const char* name;
	static bool	bActive;
	bool erase = false;
	int endless;

	Vector3 pos, angle, move;
	Shader* shader;
	Skin_Mesh* obj;
	Skin_Mesh* line;
	Vector3 player_pos;
};

