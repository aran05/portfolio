#pragma once

enum BGM
{
	SE_Atack,
	SE_Damage,
	SE_BossSuction,
	SE_BossDie,
	SE_EX,
};