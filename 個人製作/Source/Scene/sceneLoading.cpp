#include "Math\math.h"
#include	"FrameWork.h"
#include	"sceneLoading.h"
#include	<process.h>


bool	sceneLoading::bActive;

sceneLoading::sceneLoading(Scene* scene,int t):img(NULL), timer(t)
{
	newScene = scene;
}

sceneLoading::~sceneLoading()
{
	SAFE_DELETE(shader);
	SAFE_DELETE(tex);
	SAFE_DELETE(text);
}

int sceneLoading::waitcount;

bool	sceneLoading::Initialize()
{
	//	ロードシーン用の初期化				
	//view = new View();
	//view->SetPos(Vector3(0, 50, -50));

	g_pSolidBrush = NULL;
	text = new D2Render();
	text->setText(L"PixelMplus10", SIZE_100);

	//img = new sprite(L"DATA\\fireG.png");
	//2Dシェーダ
	shader = new Shader();
	shader->Create2("DATA/Shader/Sprite_vs.cso", "DATA/Shader/Sprite_ps.cso");
	
	tex = new Texture();
	tex->Load(L"DATA\\fireG.png");
	//	スレッド開始						
	count = 0;

	bActive = true;
	_beginthread(Thread, 0, newScene);
	return	true;
}



void	sceneLoading::Update()
{
	count = (count + 1) & 0xFFFFFF;

	--timer;
	if(timer<0)
	{
		timer = 0;
	}

	if (bActive == false)
	{

		scenemanager->changeScene(newScene);
		//delete newScene;
		return;
	}
}



void	sceneLoading::Render()
{
	

	framework::ClearView(1.0f, 1.0f, 1.0f, 1.0f);
	
	
	D2D1::ColorF color = D2D1::ColorF::White;
	std::wstring load = L"ロード中";


	if(count%30==0)
	{
		text->TextRender(300 + count, 0, 1240, 1240,L".");
	}

	text->TextRender(0,0,W,H, load);


	framework::Flip(1);
}




void	sceneLoading::Thread(void* arg)
{
	//	並列動作させる処理(後で追加)		
	Scene* scene = (Scene*)arg;
	scene->Initialize();
	

	//待ち時間
	//while (waitcount < 1000000000) waitcount++;

	//	スレッド終了						
	bActive = false;
	//delete scene;
	_endthread();

	
}
