
//数学用
#include "Math\math.h"

#include "FrameWork.h"
//**Scene
#include"Scene.h"
#include "sceneClear.h"
#include "sceneTitle.h"
#include "sceneLoading.h"
#include "sceneFade.h"
//*************************

//**カメラ：ライト
#include "View.h"
#include "Light\Light.h"
//*************************

//コントローラ設定
#include "KeyInput.h"
#include "Mouse\Mouse.h"
//フォント描画
#include "D2Render.h"

//3Dオブジェクト描画
#include "Score.h"
//*****************************************************************************************************************************
//
//	初期化
//
//*****************************************************************************************************************************

bool sceneClear::CreateConstantBuffer(ID3D11Buffer ** ppCB, u_int size)
{
	// 定数バッファ生成
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.ByteWidth = size;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags
		= D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;

	HRESULT hr = framework::device.Get()->CreateBuffer(&bd, NULL, ppCB);
	if (FAILED(hr))
	{
		assert(false && "ID3D11Device::CreateBuffer() Failed.");
		return false;
	}
	return true;
}

sceneClear::~sceneClear()
{
	GetScore->release();
}



bool sceneClear::Initialize()
{

	{
		text = std::make_unique<D2Render>();
		text->setText(L"PixelMplus10", SIZE_92);

		line = std::make_unique<D2Render>();
		line->setText(L"PixelMplus10", SIZE_90);
	}
	GetScore->HiScore_Load();
	return true;
}



void  sceneClear::Update()
{

	GetScore->sort();


	if (GetKeyState(VK_LBUTTON) < MouseClick)
	{
		scenemanager->changeScene(new sceneTitle());

	}
}



void  sceneClear::Render()
{

	framework::ClearView(RED2);


	ID2D1SolidColorBrush* g_pSolidBrush = NULL;
	D2D1::ColorF color = D2D1::ColorF::White;
	std::wstring score = L"スコア";

	text->TextRender(ZERO_CLEAR, ZERO_CLEAR, W, H, score, color);
	line->TextRender(ZERO_CLEAR, ZERO_CLEAR, W, H, score);


	std::wstring click = L"左クリックで\n      タイトルに戻る";
	text->TextRender(SC_POS_X, SC_POS_Y, W, H, click, color);
	line->TextRender(SC_POS_X, SC_POS_Y, W, H, click);


	//ブレンドステート設定
	framework::device_context.Get()->OMSetBlendState(framework::GetBlendState(framework::BS_ALPHA).Get(), nullptr, 0xFFFFFFFF);
	//ラスタライザ―設定
	framework::device_context.Get()->RSSetState(framework::GetRasterizerState(framework::RS_SOLID).Get());

	framework::device_context.Get()->OMSetDepthStencilState(framework::GetDephtStencilState(framework::DS_TRUE).Get(), 1);

	GetScore->RenderScore(GS_POS_X, GS_POS_Y, GS_SIZE, GS_SIZE);

	GetScore->RenderScore2(GS_POS_X, GS_POS_Y *2, GS_SIZE, GS_SIZE);

	GetScore->RenderScore3(GS_POS_X, GS_POS_Y * 3, GS_SIZE, GS_SIZE);
	framework::device_context.Get()->OMSetDepthStencilState(framework::GetDephtStencilState(framework::DS_FALSE).Get(), 1);


	framework::Flip(1);
}
