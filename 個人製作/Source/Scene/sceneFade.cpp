
#include	"sceneFade.h"

#include	"Framework.h"



sceneFade::sceneFade(int type, int timer, DWORD col, Scene* newScene) :
	type(type), timer(timer), timerMax(timer), pNext(newScene)
{
	this->col = col & 0x00FFFFFF;		//	引数の色に不透明度情報がある時は消す	
}

bool sceneFade::Initialize()
{
	if (bLoad)	return false;

	imge = std::make_unique<sprite>(L"DATA\\white.png");
	switch (type)
	{
	case FAED_IN:
		//this->insertStack(pNext);				//	スタック順を入れ替え
		pNext->Initialize();
		pNext = NULL;							//	リンク切り
		break;
	}

	bLoad = true;
	count = 0;
	return true;
};

void	sceneFade::Update()
{
	//pStack->Update();				//	プログラム内容によっては不要	
	count = (count + 1) & 0xFFFFFF;
	timer--;						//	残り時間の減少					
	if (timer < 0)
	{
		Scene*	work;
		switch (type)
		{
		case FAED_IN:
			//scenemanager->PopScene();
			break;
		case FAED_OUT:
			work = pNext;		//	一時退避
			pNext = NULL;			//	リンク切り(削除防止)
			scenemanager->changeScene(work);
			break;
		}
		return;
	}
}

void	sceneFade::Render()
{	
	imge->render(0, 0, 1280, 720, 0, 0, 256, 256);
}
