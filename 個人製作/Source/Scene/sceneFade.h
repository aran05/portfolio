#pragma once


#include	"Scene.h"
#include    "Sprite.h"
#include    "Shader.h"

class sceneFade : public Scene {
public:
	enum
	{
		FAED_IN,
		FAED_OUT
	};

protected:
	int		type;
	int		count;
	int		timer, timerMax;
	DWORD	col;
	Scene*	pNext;
	std::unique_ptr<sprite> imge; //画像ファイル
	std::unique_ptr<Shader> sp;   //シェーダ

public:
	sceneFade(int type, int timer, DWORD col, Scene* newScene);
	~sceneFade() { if (pNext)	delete pNext; };
	bool	Initialize();
	void	Update();
	void	Render();

};
