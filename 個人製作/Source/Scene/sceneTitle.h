#pragma once
//*****************************************************************************************************************************
//
//		タイトルシーン
//
//*****************************************************************************************************************************
#include "A_System.h"
#include <sstream>
#include <memory>


#include "Sprite.h"
#include "Scene.h"
#include "PlainMesh.h"
#include "OBJ_Mesh.h"
#include "..\movie.h"
#include "..\Light\Light.h"
#include "Sound.h"
#include "View.h"
#include "Player.h"
#include "Stage\Stage.h"
#include <list>
#include <iterator>
#include "Coin\Coin.h"
#include "loadtext\loadtext.h"

class FontTexture;
class D2Render;
class Skin_Mesh;
class Texture;
class Player;
class Stage;
class Coin;

extern int c_type;

#define ShadowSizeW 512
#define ShadowSizeH 512



typedef enum {
	Walk,      //歩き
	Stand,	   //待機
} Anime;


class sceneTitle : public Scene {
private:

	bool change = false;
	bool clear_flg = false;//ステージクリアフラグ
	std::unique_ptr<Particles> eff;
	bool change_se = true;
	int score;
	Vector3 light;
	Vector3 pos, angle, move;
	View* view;			 //カメラ
	Stage* stage[STAGE_MAX];
	
	D2Render* text[NUM_2];//文字描画	
	Player* player;

	Coin* coin[COIN_MAX];

	loadtext* load;
	loadtext* load_stage;

	LPDSSTREAM Mainbgm;

	float add_move;
	int coin_count;
	bool move_flag;
	
	Texture* normalMap;
	Texture* heightMap;

	
	std::unique_ptr<Shader> sp;
	std::unique_ptr<Texture> shadowMap;
	std::unique_ptr<sprite> shadow;
	std::unique_ptr<Shader> shadow_shader;
	std::unique_ptr<Shader> f_shadow_shader;



	ID2D1SolidColorBrush* g_pSolidBrush = NULL;
	XMFLOAT4X4 ShadowViewProjection;
	float speed;

	Vector3 cpos = { 0, 20, 0 };
	float cangle = 0.0f;
	float cdist = 15.0f;
	bool sound_flag=true;
	XMFLOAT4 edge[2];

	ID3D11Buffer* ConstantBuffer;


	struct ConstantBufferForPerFrame
	{
		XMFLOAT4	LightColor;		//ライトの色
		XMFLOAT4	LightDir;		//ライトの方向
		XMFLOAT4	AmbientColor;	//環境光
		XMFLOAT4	EyePos;			//カメラ位置
		XMFLOAT4	UV;
		XMFLOAT4X4	ShadowViewProjection;	//影用行列
	};

public:

	sceneTitle() 
	{
		
	};

	bool CreateConstantBuffer(ID3D11Buffer**ppCB, u_int size);
	~sceneTitle();


	bool Initialize();
	void Update();
	void ShadowRender();
	void Render();
};