#pragma once
//*****************************************************************************
//
//		シーン基底クラス
//
//*****************************************************************************
//#include "A_System.h"
#include <d3d11.h>
#include <memory>

class framework;

class Scene {
public:
	Scene*	pStack;
	bool bLoad;
	Scene() :pStack(NULL), bLoad(false) {}
	virtual ~Scene() {};


	virtual bool Initialize() = 0;
	virtual void Update() = 0;
	virtual void Render() = 0;
	Scene*	getStack() { return	pStack; }
	void	setStack(Scene* s) { pStack = s; }

	void	insertStack(Scene* s)
	{
		s->pStack = this->pStack;
		pStack = s;
	}

};


class Scene;

class sceneManager
{
private:
	std::unique_ptr<Scene> scene;
public:
	sceneManager() {}
	~sceneManager() {}

	void update();
	void render();
	void changeScene(Scene* newScene);
};


//Scen管理クラス
extern std::unique_ptr<sceneManager> scenemanager;
