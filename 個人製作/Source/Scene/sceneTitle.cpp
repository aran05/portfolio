#include <iostream>
#include <list>
#include <iterator>

//数学用
#include "Math\math.h"

#include "FrameWork.h"
//**Scene
#include"Scene.h"
#include "sceneTitle.h"
#include "sceneClear.h"
#include "sceneFade.h"
//*************************

//**カメラ：ライト
#include "View.h"
#include "Light\Light.h"
//*************************

//コントローラ設定
#include "KeyInput.h"
#include "Mouse\Mouse.h"
//フォント描画
#include "D2Render.h"


//3Dオブジェクト描画
#include "Player.h"

#include "Stage\Stage.h"
#include "Score.h"
#include "sceneLoading.h"

#include "Constant.h"
#include "Sound.h"
//*****************************************************************************************************************************
//
//	初期化
//
//*****************************************************************************************************************************

int c_type;



bool sceneTitle::CreateConstantBuffer(ID3D11Buffer ** ppCB, u_int size)
{
	// 定数バッファ生成
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.ByteWidth = size;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;

	HRESULT hr = framework::device.Get()->CreateBuffer(&bd, NULL, ppCB);
	if (FAILED(hr))
	{
		assert(false && "ID3D11Device::CreateBuffer() Failed.");
		return false;
	}
	return true;
}

sceneTitle::~sceneTitle()
{
	
	//コンスタントバッファをデリート
	SAFE_RELEASE(ConstantBuffer);

	if (Mainbgm)
	{
		A_StopStreamSound(Mainbgm);
		Mainbgm = NULL;
	}

	//view.reset(NULL);
	//text[0].reset(NULL);
	//text[1].reset(NULL);

	SAFE_DELETE(view);


	for (int i = 0; i < NUM_2; i++)
	{
		SAFE_DELETE(text[i]);

	}
	for (int i = COIN_MIN; i < COIN_MAX; i++)
	{
		SAFE_DELETE(coin[i]);
	}

	for (int i = STAGE_MIN; i < STAGE_MAX; i++)
	{
		SAFE_DELETE(stage[i]);
	}


	SAFE_DELETE(player);
}



bool sceneTitle::Initialize() 
{
	view = new View();
	//view->Set(VIEW_POS, VIEW_TPOS, VIEW_UPOS);
	//view->SetProjection(XM_PI / 6.0f, (FLOAT)framework::SCREEN_WIDTH / (FLOAT)framework::SCREEN_HEIGHT,C_NEAR,C_FAR);

	
	

	light =ZERO_CLEARV;
	pos = ZERO_CLEARV;
	
	//csvファイルの読み込み
	load = new loadtext();
	load->Load2("DATA/test.csv");

	load_stage = new loadtext();
	load_stage->Load("DATA/stage.csv");
	g_pSolidBrush = NULL;

	eff = std::make_unique<Particles>("DATA/eff2.fbx", PARTICLE60);
	//汎用シェーダの初期化
	CreateConstantBuffer(&ConstantBuffer, sizeof(ConstantBufferForPerFrame));
	A_SetWAV(SE::JUMP, "DATA/SE/jump.wav");
	A_SetWAV(SE::COIN, "DATA/SE/coin.wav");
	//フォントの初期化
	{

		text[NUM_0] = new D2Render();
		text[NUM_0]->setText(L"PixelMplus10", SIZE_100);

		text[NUM_1] = new D2Render();
		text[NUM_1]->setText(L"PixelMplus10", SIZE_96);
	}


	//シェーダーの初期化
	{

		shadow_shader = std::make_unique<Shader>();
		shadow_shader->Create("DATA/Shader/shadow_vs.cso", "DATA/Shader/shadow_ps.cso");


		sp = std::make_unique<Shader>();
		sp->Create2("DATA/Shader/Sprite_vs.cso", "DATA/Shader/Sprite_ps.cso");
	}

	
	//number = std::make_unique<sprite>();

	shadow = std::make_unique<sprite>();
	shadowMap = std::make_unique<Texture>();
	shadowMap->Create(1280,720,DXGI_FORMAT_R16G16B16A16_FLOAT);


	//プレイヤーの初期化
	player = new Player();
	player->Initialize();

	//プレイヤーの初期位置
	pos = START_POS;
	player->Set_Pos(pos);
	
	//移動量
	move = START_MOVE;
	player->Set_Move(move);
	angle = START_ANGLE;
	player->Set_Angle(angle);


	//ステージのオブジェクトの初期化
	for(int s=0;s<STAGE_MAX;s++)
	{
		stage[s] = new Stage();
		stage[s]->Initialize(load_stage->Get_pass(s,0),Vector3(load_stage->Get_pass(s, 1), load_stage->Get_pass(s, 2), load_stage->Get_pass(s, 3)));
	}
	SAFE_DELETE(load_stage);
	for (int c = 0; c<COIN_MAX; c++)
	{
		coin[c] = new Coin();
		coin[c]->Initialize(Vector3(load->Get_pass2(c, 1), load->Get_pass2(c, 2), load->Get_pass2(c, 3)));
	}
	SAFE_DELETE(load);
	//*****************************************************************

	move_flag = false;

	coin_count = ZERO_CLEAR;

	score = ZERO_CLEAR;
	GetScore->Initialize();
	sound_flag = true;
	change_se = true;
	return true;
}



void  sceneTitle::Update()
{
	eff->update();
	if(sound_flag)
	{
		Mainbgm = A_PlayStreamSound("DATA/BGM/bgm1.ogg");
		Mainbgm->SetVolume(BGM_VOLUME);
		sound_flag = false;
	}

	
	static float lightAngle = XM_PI;


	//アンビエントカラー
	Light::SetAmbient(AMBIENT_COLOR);

	//ライト方向
	light.x = sinf(lightAngle);
	light.y = -2.0f;
	light.z = cosf(lightAngle);
	Light::SetDirLight(light, LIGHT_COLOR);


	if (player->Get_Pos().y <= -20)
	{
		GetScore->newnum = score+(coin_count*COIN_ADD);
		GetScore->HiScore_Save(score + (coin_count*COIN_ADD));
		scenemanager->changeScene(new sceneClear());
		return;
	}

#ifdef _DEBUG
	if (KEY_Get(KEY_ENTER) == 3)
	{
		scenemanager->changeScene(new sceneClear());
		return;

		//scenemanager->PushScene(new sceneFade(sceneFade::FAED_IN, 260, 0x00FFFFFF, newScene));
	}
#endif // _DEBUG

	

	if (clear_flg)
	{
		
		if (change_se)
		{
			if (Mainbgm)
			{
				A_StopStreamSound(Mainbgm);
				Mainbgm = NULL;
			}

			Mainbgm = A_PlayStreamSound("DATA/BGM/bgm3.ogg");
			Mainbgm->SetVolume(BGM_VOLUME);
			change_se =false;
		}
		static int timer;
		timer++;

		player->Clear_Anime();

		eff->test(player->Get_Pos()+Vector3(0,10,0));
		if (timer > SECOND3)
		{
			
			GetScore->newnum = score + (coin_count * COIN_ADD);
			GetScore->HiScore_Save(score + (coin_count * COIN_ADD));
			scenemanager->changeScene(new sceneClear());
			timer = 0;
			return;
		}
	}
	

	//カメラアングル
	if (GetKeyState(VK_LBUTTON) < MouseClick&&player->Get_Start_Flag() == false)
	{

		if (Mainbgm)
		{
			A_StopStreamSound(Mainbgm);
			Mainbgm = NULL;
		}
		Mainbgm = A_PlayStreamSound("DATA/BGM/bgm2.ogg");
		Mainbgm->SetVolume(BGM_VOLUME);
		player->Set_Start_Flag(true);
	}


	Vector3 local;
	Vector3 localangle;

	if (player->Get_Start_Flag() == true)
	{
		//ステージ
		for (int s = STAGE_MIN; s < STAGE_MAX; s++)
		{
			stage[s]->Update(*player, local, localangle, clear_flg);

		}
		//コイン
		for (int c = COIN_MIN; c < COIN_MAX; c++)
		{
			coin[c]->Update(*player, local, localangle, coin_count);

		}
	}
	if (player->Get_Start_Flag() == true && player->Get_Hit_Flag() == false&&clear_flg==false)
	{

		player->Start_Angle();
		cpos.x = sinf(cangle)* cdist;
		cpos.z = cosf(cangle)* cdist;


		if (player->Get_Angle().y == ZERO_CLEARF)
		{
			static int time;
			time++;
			if(time%30==0)score+=SCORE_ADD;//10ポイントずつ加算
			player->Update();
		}
	}
	else if (player->Get_Hit_Flag() == true)
	{
		static int timer;

		timer++;

		if (GetKeyState(VK_LBUTTON) < MouseClick)
		{
			if (Mainbgm)
			{
				A_StopStreamSound(Mainbgm);
				Mainbgm = NULL;
			}
			Initialize();
		}

		if (timer > SECOND) //2秒ごscore画面へ
		{
			GetScore->newnum = score + (coin_count * COIN_ADD);
			GetScore->HiScore_Save(score + (coin_count * COIN_ADD));
			scenemanager->changeScene(new sceneClear());
			timer = 0;
		}

	}

}

//TODO 未実装　シャドーマップ
void sceneTitle::ShadowRender()
{

	ID3D11RenderTargetView* rtv = shadowMap->GetRenderTarget();

	framework::device_context.Get()->OMSetRenderTargets(1, &rtv, framework::depth_stencil_view.Get());

	float clearColor[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

	framework::device_context.Get()->ClearRenderTargetView(rtv, clearColor);
	framework::device_context.Get()->ClearDepthStencilView(framework::depth_stencil_view.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	framework::device_context.Get()->OMSetDepthStencilState(framework::GetDephtStencilState(framework::DS_FALSE).Get(), 1);

	
	Vector3 ShadowCamera;
	float d = sqrtf(light.x * light.x + light.y * light.y + light.z * light.z);
	if (d > 0.0f)
	{
		light.x /= d;		light.y /= d;	light.z /= d;
	}
	ShadowCamera.x = light.x * (-10.0f);
	ShadowCamera.y = light.y * (-10.0f);
	ShadowCamera.z = light.z * (-10.0f);


	view->Set(ShadowCamera,player->Get_Pos(),Vector3(0, 1, 0)+ player->Get_Pos());
	view->SetProjection(XM_PI / 2.0f, (FLOAT)(framework::SCREEN_WIDTH) / (FLOAT)(framework::SCREEN_HEIGHT), 0.1f, 1000.0f);


	framework::SetViewPort(framework::SCREEN_WIDTH, framework::SCREEN_HEIGHT);
	view->Activate();

	XMMATRIX work;
	work = view->GetView()*view->GetProjection();
	XMStoreFloat4x4(&ShadowViewProjection, work);

	ID3D11RenderTargetView* backbuffer = framework::render_target_view.Get();
	framework::device_context.Get()->OMSetRenderTargets(1, &backbuffer, framework::depth_stencil_view.Get());
}



void  sceneTitle::Render()
{
	//ShadowRender();
	if(player->Get_Start_Flag() == false)
	{
		framework::ClearView(LIGHT_BLUE);

	}
	else
	{
		framework::ClearView(GREEN2);
	}
	

	////カメラ


	if(player->Get_Angle().y==ZERO_CLEARF)
	{
		view->Set(Vector3(ZERO_CLEARF, player->Get_Pos().y, player->Get_Pos().z) + Vector3(cpos.x, C_Y15, cpos.z), Vector3(ZERO_CLEARF, player->Get_Pos().y, player->Get_Pos().z) + Vector3(ZERO_CLEARF, ZERO_CLEARF, -C_Z20), VIEW_UPOS);
	}
	else if (player->Get_Angle().y != 0.0f&&clear_flg == false)
	{
		view->Set(VIEW_POS, player->Get_Pos(), VIEW_UPOS);
	}
	else
	{
		view->Set(Vector3(0, player->Get_Pos().y, player->Get_Pos().z) + Vector3(cpos.x, C_Y10,cpos.z), Vector3(ZERO_CLEARF, ZERO_CLEARF, player->Get_Pos().z), VIEW_UPOS);
	}
	
	view->SetProjection(XM_PI / 2.0f, (FLOAT)(framework::SCREEN_WIDTH) / (FLOAT)(framework::SCREEN_HEIGHT), C_NEAR, C_FAR);
	view->Activate();

	// ビューポートの設定
	framework::SetViewPort(framework::SCREEN_WIDTH, framework::SCREEN_HEIGHT);
	{
		//ライト処理
		ConstantBufferForPerFrame cb;
		cb.AmbientColor = Light::Ambient;
		cb.LightDir = Light::LightDir;
		cb.LightColor = Light::DirLightColor;
		cb.EyePos = XMFLOAT4(view->GetPos().x, view->GetPos().y, view->GetPos().z, 1.0f);
		static float u = 0;
		static float v = 0;
		u += 0.01f * framework::elapsed_time;
		v += 0.01f * framework::elapsed_time;
		cb.UV = XMFLOAT4(u, v, 0, 0);
		cb.ShadowViewProjection = ShadowViewProjection;


		framework::device_context.Get()->UpdateSubresource(ConstantBuffer, 0, NULL, &cb, 0, 0);
		framework::device_context.Get()->VSSetConstantBuffers(1, 1, &ConstantBuffer);
		framework::device_context.Get()->GSSetConstantBuffers(1, 1, &ConstantBuffer);
		framework::device_context.Get()->PSSetConstantBuffers(1, 1, &ConstantBuffer);
		framework::device_context.Get()->DSSetConstantBuffers(1, 1, &ConstantBuffer);
		framework::device_context.Get()->HSSetConstantBuffers(1, 1, &ConstantBuffer);

	}

	

	//ブレンドステート設定
	framework::device_context.Get()->OMSetBlendState(framework::GetBlendState(framework::BS_ALPHA).Get(), nullptr, 0xFFFFFFFF);
	//ラスタライザ―設定
	framework::device_context.Get()->RSSetState(framework::GetRasterizerState(framework::RS_SOLID).Get());

	shadowMap->Set(3);

	player->Render(view);


	for (int s = STAGE_MIN; s<STAGE_MAX; s++)
	{
		stage[s]->Render(view);
	}

	for (int c = COIN_MIN; c < COIN_MAX; c++)
	{
		coin[c]->Render(view);
	}

	
	framework::device_context->OMSetDepthStencilState(framework::GetDephtStencilState(framework::DS_FALSE).Get(), 1);
	shadow->render(sp.get(), shadowMap.get(), ZERO_CLEAR, ZERO_CLEAR, ShadowSizeW, ShadowSizeH, ZERO_CLEAR, ZERO_CLEAR, ShadowSizeW, ShadowSizeH);
	framework::device_context->OMSetDepthStencilState(framework::GetDephtStencilState(framework::DS_TRUE).Get(), 1);

	std::wstring score_name = L"スコア:";
	std::wstring coin_num = L"コイン:";

#ifdef _DEBUG
	score_name += std::to_wstring(player->Get_Pos().z);
#else
	score_name += std::to_wstring(score);
#endif // _DEBUG

	coin_num += std::to_wstring(coin_count);


	if(player->Get_Start_Flag()==false)
	{
		text[NUM_1]->TextRender(ST_POS_X, ST_POS_Y,W, H, L"ゲームスタート\n\n          左クリック");
		text[NUM_0]->TextRender(ST_POS_X, ST_POS_Y,W, H,  L"ゲームスタート\n\n          左クリック",blue);
	}
	else
	{
		text[NUM_0]->TextRender(ZERO_CLEAR, ZERO_CLEAR, W,H, score_name, white);
		text[NUM_1]->TextRender(ZERO_CLEAR, ZERO_CLEAR, W,W, score_name);

		text[NUM_0]->TextRender(ST_POS_X2, ZERO_CLEAR, W, H, coin_num);
		text[NUM_1]->TextRender(ST_POS_X2, ZERO_CLEAR, W, H, coin_num,yellow);
	}

	eff->render(view->GetView(), view->GetProjection());

	framework::Flip(1);
}


