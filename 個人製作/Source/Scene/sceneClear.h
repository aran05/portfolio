#pragma once

#include "A_System.h"
#include <sstream>
#include <memory>


#include "Sprite.h"
#include "Scene.h"
#include "PlainMesh.h"
#include "OBJ_Mesh.h"
#include "..\movie.h"
#include "..\Light\Light.h"
#include "Sound.h"
#include "View.h"

class FontTexture;
class D2Render;
class Skin_Mesh;
class Texture;

class sceneClear :public Scene
{
	std::unique_ptr<D2Render> text;//文字描画
	std::unique_ptr<D2Render> line;//文字描画
public:
	sceneClear() {};
	~sceneClear();

	bool CreateConstantBuffer(ID3D11Buffer**ppCB, u_int size);

	bool Initialize();
	void Update();
	void Render();
};

