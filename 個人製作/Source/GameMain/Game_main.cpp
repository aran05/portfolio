#include "A_System.h"
#include"Scene.h"
#include "FrameWork.h"
#include "Game_main.h"
#include "sceneTitle.h"
#include "View.h"


#include "KeyInput.h"

#include "SpriteFont.h"
#include "movie.h"
#include "D2Render.h"
#include "Texture.h"
#include "Light\Light.h"
#include "..\\wave.h"


//*****************************************************************************************************************************
//
//	初期化
//
//*****************************************************************************************************************************
Vector3 LightDir;

static int poinrOffset[4][2] = {
	{ 0,-1 },//上
	{ 1,0 },//右
	{ 0,1 },//下
	{ -1,0 }//左
};


int map_data[5][5] =
{
	{ 0,1,1,1,1 },
	{ 1,1,1,1,1 },
	{ 1,1,1,1,1 },
	{ 1,1,1,1,1 },
	{ 1,1,1,1,1 }
};

//3Dキャラクターの描画
bool Game_Main::CreateConstantBuffer()
{
	// 定数バッファ生成
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.ByteWidth = sizeof(ConstantBufferForPerFrame);
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;

	HRESULT hr = framework::device.Get()->CreateBuffer(&bd, NULL, &ConstantBuffer);

	if (FAILED(hr))
	{
		assert(false && "ID3D11Device::CreateBuffer() Failed.");
		return false;
	}

	return true;

}
void Game_Main::move(int x, int y, int n)
{
	map_pos[y][x] = n;


	if (n == 0) return;

	if (map_pos[y - 1][x] < n) move(x, y - 1, n - 1);
	if (map_pos[y + 1][x] < n) move(x, y + 1, n - 1);
	if (map_pos[y][x + 1] < n)   move(x + 1, y, n - 1);
	if (map_pos[y][x - 1] < n)   move(x - 1, y, n - 1);



}

void Game_Main::set_pos_xy(float px, float py)
{


}


Game_Main::~Game_Main()
{


	//SAFE_DELETE(fbx_mesh);
	SAFE_DELETE(view);

	//SAFE_DELETE(Pchar);
	//SAFE_DELETE(PMap);

	//SAFE_DELETE(t);
	//SAFE_DELETE(t[1]);
}

//wchar_t* FONTNAME = L"mini-わくわく";

bool Game_Main::Initialize()
{

	view = new View();
	view->SetPos(Vector3(0, 50, -50));

	//Pchar = new CChara(L"DATA/uv_checker.png", 0, 0, 0);
	//PMap = new CMap();

	px = 1 * 385;
	py = 1 * 95;

	move_count = 0;
	state = 0;
	keyBuf = 0;
	push_key = false;

	for (int y = 0; y<5; y++)
	{
		for (int x = 0; x < 5; x++)
		{
			map_pos[y][x] = -1;
		}
	}





	{
		////m = new movie();
		////m->init(L"DATA/TVアニメ「だがしかし2」PV.wmv");
		//

		//fbx_mesh = new Skin_Mesh("DATA/doki/douki.fbx","DATA/Shader/skinned_light_mesh_vs.cso","DATA/Shader/skinned_light_mesh_ps.cso");
		//fbx_mesh->SetScalse(Vector3(0.1f, 0.1f, 0.1f));
		//fbx_mesh->SetPos(Vector3(0.0f, 0.0f, 0.0f));
		////fbx_mesh->setMotion(0);
		//
		//x = 0;
		//y = 0;


		//CreateConstantBuffer();

		//normal = new Texture();
		//normal->Load(L"DATA/Ndaiou.png");

		//hight = new Texture();
		//normal->Load(L"DATA/Hdaiou.png");

		//

	}

	t = new D2Render();
	t->setText(L"MS 明朝", 40);

	//wave();
	return true;
}



void  Game_Main::Update()
{

	{
		////m->update();
		//View::camera->Update(fbx_mesh->GetPos() + Vector3(0, 0.0f, 0.0));


		//static float x=0.0f;
		//static float y=0.0f;
		//static float z = 0.0f;
		//
		//
		//static float lightAngle = XM_PI;
		//if (GetKeyState('A') < 0) lightAngle += elapsed_time*1.0f;
		//if (GetKeyState('D') < 0) lightAngle -= elapsed_time*1.0f;

		//Light::SetAmbient(Vector3(0.6f, 0.6f, 0.6f));



		////ライト方向
		//LightDir.x = sinf(lightAngle);
		//LightDir.y = -0.5f;
		//LightDir.z = cosf(lightAngle);
		//Light::SetDirLight(LightDir, Vector3(0.1f, 0.1f, 0.1f));

		//

		////Light::SetPointLight(0, fbx_mesh->GetPos(), Vector3(0, 1, 0), 10.0f);
		////Light::SetPointLight(1, fbx_mesh->GetPos(), Vector3(1, 0, 0), 10.0f);
		////Light::SetPointLight(2, fbx_mesh->GetPos(), Vector3(0, 0, 1), 10.0f);


		//
		//if (KEY_Get(KEY_A))
		//{
		//	y += 1;
		//}
		//
		//if (KEY_Get(KEY_B))
		//{
		//	y -= 1;
		//}

		//if (KEY_Get(KEY_C))
		//{
		//	x -= 1;
		//}
		//if (KEY_Get(KEY_D))
		//{
		//	x += 1;
		//}
	}


	if (KEY_Get(KEY_START)==3)
	{
		scenemanager->changeScene(new sceneTitle());
	}

	int bx, by, x, y;

	bx = px;
	by = py;

	switch (state)
	{
	case 0:
		if (px>385)
		{
			if (KEY_Get(KEY_LEFT) == 3)
			{
				keyBuf = 3;
				//px -= 128;
				push_key = true;
			}

		}

		if (px<897)
		{
			if (KEY_Get(KEY_RIGHT) == 3)
			{
				keyBuf = 1;
				push_key = true;
				//px += 128;
			}
		}

		if (py > 95)
		{
			if (KEY_Get(KEY_UP) == 3)
			{
				keyBuf = 0;
				push_key = true;
				//py -= 128;
			}
		}

		if (py < 607)
		{
			if (KEY_Get(KEY_DOWN) == 3)
			{
				keyBuf = 2;
				push_key = true;
				//py += 128;
			}
		}



		if (keyBuf == 4) return;
		dir = keyBuf;

		y = by + poinrOffset[dir][1];		//移動後の場所の位置を調べる。
		x = bx + poinrOffset[dir][0];

		if (push_key == true)
		{
			move_count = 1;
			state++;
		}
		break;
	case 1:

		my = (float)(poinrOffset[dir][1]) * 128;		//向き番号から移動ベクトルを求める
		mx = (float)(poinrOffset[dir][0]) * 128;

		px += mx;									//座標に速度を加算
		py += my;

		move_count -= 4;
		if (move_count <= 0)
		{
			move_count = 0;
			state = 0;
			push_key = false;
			break;
		}
		break;
	}

	
}


void  Game_Main::Render()
{

	framework::ClearView(0.0f, 0.0f, 0.0f, 1.0f);
	//カメラ
	view->SetProjection(XM_PI / 2.0f, framework::SCREEN_WIDTH/ framework::SCREEN_HEIGHT, 0.1f, 1000.0f);
	view->Activate();



	//PMap->Render(framework::elapsed_time);
	//Pchar->Render(framework::elapsed_time);


	{
		//m->update();
		/*XMFLOAT3 xt = XMFLOAT3(0, 0, 0);


		if (GetKeyState('W') < 0) xt.y += 2.0f;
		if (GetKeyState('S') < 0) xt.y -= 2.0f;

		if (GetKeyState('Q') < 0) xt.x += 2.0f;
		if (GetKeyState('E') < 0) xt.x -= 2.0f;
		*/


		//// ライト設定
		//{
		//	Light::Light_Uadate(elapsed_time);
		//}

		//light_direction = XMFLOAT4(0, 0, 0, 0);
		//material_color = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);



		//normal->Set(1);
		//hight->Set(2);

		//fbx_mesh->Render(View::camera->GetView(), View::camera->GetProjection(), light_direction, material_color, elapsed_time);
	}

	ID2D1SolidColorBrush* g_pSolidBrush = NULL;
	D2D1::ColorF color = D2D1::ColorF::White;


	//D2D1::ColorF color2 = D2D1::ColorF::CornflowerBlue;

	//D2D1::ColorF color4 = D2D1::ColorF::DarkRed;
	//D2D1::ColorF color3 = D2D1::ColorF::DarkGoldenrod;
	//ID2D1SolidColorBrush* g_pSolidBrush;
	//*/

	for (int y = 0; y < 4; y++)
	{
		for (int x = 0; x < 4; x++)
		{
			t->SquareRender((450) + 128 * x, (160) + 128 * y, 128, 128, 1);
		}
	}



	set_pos_xy(px, py);





	t->Circle(px, py, 50, 50, 0);


	std::wstring x_pos = L"posX:";
	x_pos += std::to_wstring((int)(px));
	t->TextRender(g_pSolidBrush, 7, 50, 256, 20, x_pos, color);
	std::wstring y_pos = L"posY:";
	y_pos += std::to_wstring((int)(py));
	t->TextRender(g_pSolidBrush, 7, 100, 256, 20, y_pos, color);

	std::wstring c = L"count:";
	c += std::to_wstring(move_count);

	t->TextRender(g_pSolidBrush, 7, 150, 256, 20, c, color);

	t->TextRender(g_pSolidBrush,1000, 600, 512, 96, L"エンターで\nタイトルに戻る", color);

	framework::Flip(1);
}

//*****************************************************************************************************************************
//
// Map
//
//*****************************************************************************************************************************



CMap::CMap()
{
	map = new sprite(L"DATA\\map.png");
	mx = 0;
	my = 0;
	msize = 24;

}

CMap::~CMap()
{
	SAFE_DELETE(map);
}



void CMap::Update(float elapsed_time)
{

}

void CMap::Render(float elapsed_time)
{

	/*int x, y, block;
	int dx = mx / 64;
	int m =  mx % 32;
	*/
	for (int y = 0; y < 35; y++)
	{
		for (int x = 0; x < 55; x++)
		{
			map->render(x*msize, y*msize, msize, msize, 24 * 4, 0, msize, msize);
		}
	}

}



//*****************************************************************************************************************************
//
//	キャラクター
//
//*****************************************************************************************************************************
CChara::CChara(const wchar_t* file_name, float x, float y, int d)
{
	this->char_image = std::make_unique<sprite>(file_name);
	this->cx = x;
	this->cy = y;
	this->dir = d;
}

void CChara::Update(float elapsed_time)
{

}

void CChara::Render(float elapsed_time)
{
	char_image->render(cx, cy, 32, 32, 0, 0, 32, 32);
}

CChara::~CChara()
{

}