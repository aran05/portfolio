#pragma once
//*****************************************************************************************************************************
//
//		タイトルシーン
//
//*****************************************************************************************************************************
#include "A_System.h"
#include <sstream>
#include <memory>


#include "Sprite.h"
#include "Scene.h"
#include "OBJ_Mesh.h"
#include "..\movie.h"
#include "..\Light\Light.h"
#include "Sound.h"
#include "View.h"

class movie;
class FontTexture;
class D2Render;
class Skin_Mesh;
class Texture;
class CChara;
class CMap;
//class p
//{
//
//	XMFLOAT4X4 world_view_projection;
//	XMFLOAT4X4 world;
//	XMFLOAT4 light_direction;
//	XMFLOAT4 material_color;
//	std::unique_ptr<Skin_Mesh> fbx_mesh2;
//	Texture* normal;
//public:
//	void Initialize();
//	void Update(float elapsed_time);
//	void Render(float elapsed_time);
//
//	~p();
//};


class Game_Main : public Scene 
{
private:
	View* view;
	sprite* titleimage;		//	タイトル画像 
	std::unique_ptr<sprite> titlekey;		//	タイトルキー 
	Skin_Mesh* fbx_mesh;


	std::unique_ptr<sprite> f;
	D2Render* t;

	movie * m;
	Texture* normal;
	Texture* hight;

	int state;
	int px, py;	//移動位置
	int mx, my;
	int move_count; //移動量
	int dir;//移動方向
	bool push_key;

	int map_pos[5][5];

	void set_pos_xy(float px, float py); //移動位置の値をセットする　1:移動量 2:x軸 3:y軸

	LPDSSTREAM Mainbgm;
	XMFLOAT4X4 world_view_projection;
	XMFLOAT4X4 world;
	XMFLOAT4 light_direction;
	XMFLOAT4 material_color;
	Vector3 angles;

	CChara * Pchar;
	CMap *PMap;

	


	int timer;
	bool flag;

	int keyBuf;



	std::vector<ID3D11ShaderResourceView*> _textures; // モデルのテクスチャ

	//ID3D11Buffer* ConstantBuffer;
	ID3D11Buffer* ConstantBuffer;
	struct ConstantBufferForPerFrame
	{
		XMFLOAT4	LightColor;		//ライトの色
		XMFLOAT4	LightDir;		//ライトの方向
		XMFLOAT4	AmbientColor;	//環境光
		XMFLOAT4	EyePos;			//カメラ位置
		XMFLOAT4	UV;
		XMFLOAT4X4	ShadowViewProjection;	//影用行列
	};

public:
	Game_Main()
	{
		Mainbgm = NULL;
		titlekey = std::make_unique<sprite>(L"DATA/Picture/title_key.png");
	};


	~Game_Main();

	bool Initialize();
	void Update();
	void Render();
	bool CreateConstantBuffer();

	void move(int y, int x, int n);
};

//extern int	mapData[5][5];

class CMap
{
public:



	int mx, my;				//マップ位置
	int msize;				//マップサイズ
	int dist;

	enum //マップ識別
	{
		DEPTH_AREA,
		DEPTH_CUSOR,
		DEPTH_CHAR,
		DEPTH_OBJECT,
	};


	CMap();
	~CMap();
	void Update(float elapsed_time);
	void Render(float elapsed_time);
private:
	sprite * map;
};




class CChara :public CMap
{
public:
	std::unique_ptr<sprite> char_image; //キャラクターの画像　
	float cx, cy;						//キャラクター位置
	int dir;							//キャラクターの向き

	enum //向きの識別
	{
		LEFT,
		DOWN,
		RIGHT,
		UP,
	};


	CChara() {};
	CChara(const wchar_t* file_name, float x, float y, int d); //1.画像のパス名/2.x軸,3.y軸,4向き
	~CChara();
	void SetDirection(int d) { dir = d; }

	void SetX(float x) { cx = x; }
	void SetY(float y) { cy = y; }

	float GetX() { return cx; }
	float GetY() { return cy; }



	void Update(float elapsed_time);
	void Render(float elapsed_time);

};



