#pragma once
//*****************************************************************************
//		３Ｄベクトル
//*****************************************************************************

//------------------------------------------------------
//	３Ｄベクトル基本構造体
//------------------------------------------------------
#include	<windows.h>
#include	<d3dx9.h>

typedef D3DXMATRIX		Matrix;

typedef struct Vector {
	float	x, y, z;
} Vector;

//------------------------------------------------------
//	３Ｄベクトル構造体
//------------------------------------------------------

typedef struct Vector3 : public Vector
{
public:
	//	コンストラクタ
	Vector3() {};
	inline Vector3(float x, float y, float z) { this->x = x, this->y = y, this->z = z; }
	inline Vector3(CONST Vector& v) { this->x = v.x, this->y = v.y, this->z = v.z; }

	//	距離計算
	inline float Length() { return sqrtf(x*x + y*y + z*z); }
	inline float LengthSq() { return x*x + y*y + z*z; }

	//	正規化
	void Normalize()
	{
		float l = Length();
		if (l != .0f) { x /= l; y /= l; z /= l; }
	}

	//	オペレーター
	inline Vector3& operator = (CONST Vector& v) { x = v.x; y = v.y; z = v.z; return *this; }
	inline Vector3& operator += (CONST Vector3& v) { x += v.x; y += v.y; z += v.z; return *this; }
	inline Vector3& operator -= (CONST Vector3& v) { x -= v.x; y -= v.y; z -= v.z; return *this; }
	inline Vector3& operator *= (FLOAT v) { x *= v; y *= v; z *= v; return *this; }
	inline Vector3& operator /= (FLOAT v) { x /= v; y /= v; z /= v; return *this; }

	inline Vector3 operator + () const { Vector3 ret(x, y, z); return ret; }
	inline Vector3 operator - () const { Vector3 ret(-x, -y, -z); return ret; }

	inline Vector3 operator + (CONST Vector3& v) const { return Vector3(x + v.x, y + v.y, z + v.z); }
	inline Vector3 operator - (CONST Vector3& v) const { return Vector3(x - v.x, y - v.y, z - v.z); }
	inline Vector3 operator * (FLOAT v) const { Vector3 ret(x*v, y*v, z*v); return ret; }
	inline Vector3 operator / (FLOAT v) const { Vector3 ret(x / v, y / v, z / v); return ret; }

	BOOL operator == (CONST Vector3& v) const { return (x == v.x) && (y == v.y) && (z == v.z); }
	BOOL operator != (CONST Vector3& v) const { return (x != v.x) || (y != v.y) || (z != v.z); }

} Vector3, *LPVECTOR3;


//------------------------------------------------------
//	２Ｄベクトル構造体
//------------------------------------------------------
typedef struct Vector2
{
public:
	float	x, y;
	//	コンストラクタ
	Vector2() { this->x = 0.0f, this->y = 0.0f; }
	inline Vector2(float x, float y) { this->x = x, this->y = y; }
	inline Vector2(CONST Vector& v) { this->x = v.x, this->y = v.y; }

	//	距離計算
	inline float Length() { return sqrtf(x*x + y*y); }
	inline float LengthSq() { return x*x + y*y; }

	//	正規化
	void Normalize()
	{
		float l = Length();
		if (l != .0f) { x /= l; y /= l; }
	}

	//	オペレーター
	inline Vector2& operator = (CONST Vector2& v) { x = v.x; y = v.y; return *this; }
	inline Vector2& operator = (CONST Vector3& v) { x = v.x; y = v.y; return *this; }
	inline Vector2& operator += (CONST Vector2& v) { x += v.x; y += v.y;  return *this; }
	inline Vector2& operator -= (CONST Vector2& v) { x -= v.x; y -= v.y;  return *this; }
	inline Vector2& operator *= (FLOAT v) { x *= v; y *= v;  return *this; }
	inline Vector2& operator /= (FLOAT v) { x /= v; y /= v;  return *this; }

	inline Vector2 operator + () const { Vector2 ret(x, y); return ret; }
	inline Vector2 operator - () const { Vector2 ret(-x, -y); return ret; }

	inline Vector2 operator + (CONST Vector2& v) const { return Vector2(x + v.x, y + v.y); }
	inline Vector2 operator - (CONST Vector2& v) const { return Vector2(x - v.x, y - v.y); }
	inline Vector2 operator * (FLOAT v) const { Vector2 ret(x*v, y*v); return ret; }
	inline Vector2 operator / (FLOAT v) const { Vector2 ret(x / v, y / v); return ret; }

	BOOL operator == (CONST Vector2& v) const { return (x == v.x) && (y == v.y); }
	BOOL operator != (CONST Vector2& v) const { return (x != v.x) || (y != v.y); }


} Vector2, *LPVECTOR2;

//------------------------------------------------------
//	外積
//------------------------------------------------------
inline float Vector2Cross(const Vector2& v1, const Vector2& v2)
{
	return v1.x * v2.y - v1.y * v2.x;
}

//------------------------------------------------------
//	内積
//------------------------------------------------------
inline float Vector2Dot(const Vector2& v1, const Vector2& v2)
{
	return v1.x * v2.x + v1.y * v2.y;
}

inline Vector2 RotVectorR(Vector2 vec, float rad)
{
	Vector2 ret;
	ret.x = vec.x*cosf(rad) - vec.y*sinf(rad);
	ret.y = vec.x*sinf(rad) + vec.y*cosf(rad);
	return ret;
}


//*****************************************************************************
//		クォータニオン
//*****************************************************************************
typedef struct  Quaternion
{
public:
	//------------------------------------------------------
	//	パラメータ
	//------------------------------------------------------
	float	x, y, z, w;

	//------------------------------------------------------
	//	コンストラクタ
	//------------------------------------------------------
	Quaternion() {}
	Quaternion(float sx, float sy, float sz, float sw) : x(sx), y(sy), z(sz), w(sw) {}

	//------------------------------------------------------
	//	生成
	//------------------------------------------------------
	//	単位クォータニオン生成
	void Identity() { x = y = z = 0; w = 1; }

	//	正規化
	inline void normalize()
	{
		float legnth = getLength();
		if (legnth == .0f) return;
		float invL = 1.0f / legnth;
		(*this) *= invL;
	}

	//------------------------------------------------------
	//	情報取得
	//------------------------------------------------------
	inline float getLength() const { return sqrtf(x*x + y*y + z*z + w*w); }
	void toMatrix(Matrix& m);

	//------------------------------------------------------
	//	オペレーター
	//------------------------------------------------------
	inline Quaternion operator + () const { Quaternion ret(x, y, z, w); return ret; }
	inline Quaternion operator - () const { Quaternion ret(-x, -y, -z, -w); return ret; }

	//	VS クォータニオン
	inline Quaternion& operator +=(const Quaternion& v) { x += v.x; y += v.y; z += v.z; w += v.w; return *this; }
	inline Quaternion& operator -=(const Quaternion& v) { x -= v.x; y -= v.y; z -= v.z; w -= v.w; return *this; }
	inline Quaternion& operator *=(const Quaternion& v)
	{
		x = y * v.z - z * v.y + x * v.w + w * v.x;
		y = z * v.x - x * v.z + y * v.w + w * v.y;
		z = x * v.y - y * v.x + z * v.w + w * v.z;
		w = w * v.w - x * v.x - y * v.y - z * v.z;
		return *this;
	}

	inline Quaternion operator +(const Quaternion& v) const { return Quaternion(x + v.x, y + v.y, z + v.z, w + v.w); }
	inline Quaternion operator -(const Quaternion& v) const { return Quaternion(x - v.x, y - v.y, z - v.z, w - v.w); }
	inline Quaternion operator *(const Quaternion& v) const
	{
		return Quaternion(
			y * v.z - z * v.y + x * v.w + w * v.x,
			z * v.x - x * v.z + y * v.w + w * v.y,
			x * v.y - y * v.x + z * v.w + w * v.z,
			w * v.w - x * v.x - y * v.y - z * v.z
		);
	}

	//	VS 値
	inline Quaternion& operator *=(float v) { x *= v; y *= v; z *= v; w *= v; return *this; }
	inline Quaternion& operator /=(float v) { x /= v; y /= v; z /= v; w /= v; return *this; }

	inline Quaternion operator *(float v) const { return Quaternion(x*v, y*v, z*v, w*v); }
	inline Quaternion operator /(float v) const { return Quaternion(x / v, y / v, z / v, w / v); }

} Quaternion;

//------------------------------------------------------
//	球面線形補間
//------------------------------------------------------
Quaternion QuaternionSlerp(Quaternion& q, Quaternion& r, float a);