#pragma once
#include "Scene.h"
#include "FrameWork.h"
#include "View.h"
class skinned_mesh;
class ShotManager;
class Skin_Mesh;
class Camera;

class stage
{
public:
	stage();
	~stage();

	Vector3 position[2];
	Vector3 dimensions[2];
	Vector3 angles[2];
	XMVECTOR eye, focus, up;
	XMFLOAT4X4 world_view_projection;
	XMFLOAT4X4 world;
	XMFLOAT4 light_direction;
	XMFLOAT4 material_color;

	Skin_Mesh* pstage;
	Skin_Mesh* pstage2;
	int HP;

	void HitCheck(Vector3& pos);
	bool Initialize();
	void Update();
	void Render();
	bool shot_flag;
	static stage * getInstance() {
		static stage instance;
		return &instance;
	}
	
};
#define STAGEMANAGER (stage::getInstance())
