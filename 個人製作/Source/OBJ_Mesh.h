#pragma once
#include "static_mesh.h"
#include "skinned_mesh.h"
#include "Math\math.h"
#include "FrameWork.h"

class OBJ_Mesh : public static_mesh
{
private:
	XMFLOAT3 pos;
	XMFLOAT3 scale;
	XMFLOAT3 angle;
	XMMATRIX matworld;
public:
	OBJ_Mesh(const wchar_t* name);
	~OBJ_Mesh();
	void Update();
	void Render(const XMMATRIX& view,const XMMATRIX& projection,const XMFLOAT4& light,const XMFLOAT4& matColor,const bool flag);
	void SetPos(const XMFLOAT3& p) { pos = p; }
	void SetScalse(const XMFLOAT3& s) { scale = s; }
	void SetAngle(const XMFLOAT3& a) { angle = a; }
};

class skinned_mesh;
class Shader;
class Texture;

class Skin_Mesh : public skinned_mesh
{
private:
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT3 scale;
	DirectX::XMFLOAT3 angle;


	Vector3 position;
	Vector3 scales;
	Vector3 angles;

	XMFLOAT4 light_direction;
	XMFLOAT4 material_color;

	XMFLOAT4X4 matWorld;
	int motion;

public:
	Skin_Mesh(const char* name);
	Skin_Mesh(const char* name, const char* vs, const char* ps,const char* geo);
	Skin_Mesh(const char* name, const char* vs, const char* ps);
	~Skin_Mesh() {};
	Skin_Mesh* Clone();
	void Update();

	
	void Render(const XMMATRIX& view, const XMMATRIX& projection,
		const XMFLOAT4& light, const XMFLOAT4& matColor, const XMFLOAT4& Edge_type, int blender_type = 1);
	void Render(const XMMATRIX& view, const XMMATRIX& projection,
		const XMFLOAT4& light, const XMFLOAT4& matColor, int blender_type = 1);

	//void Render(Shader*shader,Texture* tex,const XMMATRIX& view, const XMMATRIX& projection,  int blender_type = 1,float speed=0.5f);

	void Render(Shader*shader, Texture* tex, const XMMATRIX& view, const XMMATRIX& projection, const XMFLOAT4& light, const XMFLOAT4& matColor,int blender_type = 1, float speed = 0.5f);
	void Render(Shader*shader, const XMMATRIX& view, const XMMATRIX& projection, const XMFLOAT4& light, const XMFLOAT4& matColor, int blender_type = 1, float speed = 0.5f);



	void setMotion(int m);


	void SetPos(XMFLOAT3 p) { pos = p; }
	void SetScalse(XMFLOAT3 s) { scale = s; }
	void SetAngle(XMFLOAT3 a) { angle = a; }

	void SetPos(Vector3 p) { position = p; }
	void SetScalse(Vector3 s) { scales = s; }
	void SetAngle(Vector3 a) { angles = a; }

	float getHeight(const Vector3& pos,Vector3& local, const Vector3& angle, Vector3& localangle);
	int checkRide(const Vector3& pos, Vector3& local, const Vector3& angle, Vector3& localangle);
	int checkFrontward(const Vector3 & pos, Vector3 & local, const Vector3 & angle, Vector3 & localangle);
	int checkFrontward2(const Vector3 & pos, Vector3 & local, const Vector3 & angle, Vector3 & localangle);
	int checkLeft(const Vector3 & pos, Vector3 & local, const Vector3 & angle, Vector3 & localangle);
	int checkRight(const Vector3 & pos, Vector3 & local, const Vector3 & angle, Vector3 & localangle);

	int checkLeft2(const Vector3 & pos, Vector3 & local, const Vector3 & angle, Vector3 & localangle);
	int checkRight2(const Vector3 & pos, Vector3 & local, const Vector3 & angle, Vector3 & localangle);
	void carrierToWorld(Vector3& inout, Vector3& inoutangle);

	int raypick(Vector3* out, Vector3* pos, Vector3* vec, float* dist);
	Vector3 GetPos() { return position; }
	XMFLOAT4X4 getWorld()
	{
		return matWorld;
	}

};
