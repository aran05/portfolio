#pragma once
#include "Sprite.h"
#include "A_System.h"
#include <sstream>
#include <memory>
#include "D2Render.h"
#define GetScore (Score::getInstance())
static const int KETA = 4;

class D2Render;

class Score {
private:
	D2Render* number[2];
	

public:
	int saveData[5];
	int saveData2[5];
	int runkScore;
	int num[KETA];//
	int num2[KETA];//
	int num3[KETA];//
	int newnum;
	Score();
	~Score();

	void Initialize();

	void setScore(int n);
	void setScore2(int n);
	void RenderScore(int x, int y, int w, int h);
	void RenderScore2(int x, int y, int w, int h);
	void RenderScore3(int x, int y, int w, int h);
	void Release();
	void release();
	void swap(int *a, int *b);
	

	void sort();
	

	int HiScore_Load(void);
	void HiScore_Save(int NewScore);

	static Score * getInstance() {
		static Score instance;
		return &instance;
	}
};



