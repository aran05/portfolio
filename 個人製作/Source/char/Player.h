#pragma once
#include "A_System.h"
#include <sstream>
#include <memory>

#include"Math\math.h"
#include"OBJ_Mesh.h"
#include"effect.h"

class Particles;
class Skin_Mesh;


class Player
{
private:
	Vector3 pos, angle, move;
	bool start_flag; //ゲーム開始フラグ
	bool jump_flag; //ジャンプしているか
	bool push_button;//ボタンが押されているか
	bool move_flag;//歩いているか
	bool hit_flag; //壁に当たったか
	bool no;
	float add_move;
	std::unique_ptr<Shader> shader;
	std::unique_ptr<Shader> shadow_shader;
	int coin_count;
	std::unique_ptr<Particles> eff;

public:
	std::unique_ptr<Skin_Mesh> obj;//キャラクターモデル

	Player();
	~Player();
	void Initialize();

	void Start_Angle();
	void Clear_Anime();
	void Update();
	void Render(View* v);

	//セッター
	void Set_Pos(Vector3 p) { pos = p; }
	void Set_Angle(Vector3 a) { angle = a; }
	void Set_Move(Vector3 m) { move = m; }

	void Set_Start_Flag(bool s) { start_flag = s; }
	void Set_Jump_Flag(bool j) { jump_flag = j; }
	void Set_Push_Button(bool b) { push_button = b; }

	void Set_Coin_Count(int c) { coin_count = c; }
	void Set_Move_Flag(bool f) { move_flag = f; }
	void Set_Hit_Flag(bool h) { hit_flag = h; }
	Skin_Mesh* Get_Obj() { return obj.get(); }


	//ゲッター
	Vector3 Get_Pos() { return pos; }
	Vector3 Get_Angle() { return angle; }
	Vector3 Get_Move() { return move; }

	bool Get_Start_Flag() { return start_flag; }
	bool Get_Jump_Flag() { return jump_flag; }
	bool Get_Push_Button() { return push_button; }
	bool Get_Hit_Flag() { return hit_flag; }
	

};
