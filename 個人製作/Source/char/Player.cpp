#include "Player.h"
#include "KeyInput.h"
#include "Mouse\Mouse.h"
#include "Constant.h"
#include "Sound.h"
Player::Player()
{
}
Player::~Player()
{
}


void Player::Initialize()
{

	shader = std::make_unique<Shader>();
	

	
	eff = std::make_unique<Particles>("DATA/eff2.fbx", PARTICLE60);

#if SHADER_CHANGE
	shader->Create("DATA/Shader/test01_vs.cso", "DATA/Shader/test01_ps.cso");

#else	
	shader->Create("DATA/Shader/Toon_vs.cso", "DATA/Shader/Toon_ps.cso", "DATA/Shader/Toon_gs.cso");
#endif 

	
	shadow_shader = std::make_unique<Shader>();
	shadow_shader->Create("DATA/Shader/shadow_vs.cso", "DATA/Shader/shadow_ps.cso");
	//***キャラクターオブジェクトの初期化***

	obj = std::make_unique<Skin_Mesh>("DATA/guriguri/test.fbx");
	obj->SetScalse(Vector3(PLYAER_SCALE, PLYAER_SCALE, PLYAER_SCALE));
	obj->SetPos(pos);
	obj->SetAngle(angle);
	obj->setMotion(Walk);
	obj->Update();

	add_move = ZERO_CLEARF;

	coin_count = ZERO_CLEAR;
	move.x = MOVE_X;

	//ゲームスタートフラグ
	start_flag = false;

	jump_flag = false;

	push_button = false;

	hit_flag = false;
	no = false;
}

void Player::Start_Angle()
{
	angle.y -= framework::elapsed_time*ANGLE_SPEED;
	
	
	if (angle.y <= ZERO_CLEARF)
	{
		angle.y = ZERO_CLEARF;
	}
}

void Player::Clear_Anime()
{
	angle.y += framework::elapsed_time*ANGLE_SPEED;


	if (angle.y >= MAX_ANGLE_Y)
	{
		angle.y = MAX_ANGLE_Y;

	}

	obj->SetAngle(Vector3(ZERO_CLEARF, angle.y, ZERO_CLEARF));
	obj->Update();
}

void Player::Update()
{

	if (coin_count != ZERO_CLEAR && coin_count % 10 == ZERO_CLEAR && move_flag == false)
	{
		static int add = 0.2f;
		add_move += add;
		move_flag = true;
	}

	move.z = -MOVE_Z - add_move;
	move.y = -MOVE_Y;

	pos.y += move.y;
	pos.z += move.z;

	if (KEY_Get(KEY_LEFT) && push_button == false)
	{
		pos.x += move.x;
	}

	if (KEY_Get(KEY_RIGHT) && push_button == false)
	{
		pos.x -= move.x;
	}



	if (jump_flag == true)
	{
		pos.y += JUMP_POW;
		if (pos.y > MAX_Y)
		{
			jump_flag = false;
		}
	}

	//プレイヤーのx方向移動範囲
	if (pos.x > MAX_X)
	{
		pos.x = MAX_X;
	}
	else if (pos.x < MIN_X)
	{
		pos.x = MIN_X;
	}

	eff->update();
	eff->test(pos);

}

//void Player::ShadowRender(View * view)
//{
//	
//
//	//obj->SetAngle(angle);
//	//obj->SetPos(pos);
//	//obj->Update();
//	//obj->Render(shadow_shader.get(), view->GetView(), view->GetProjection(), light_direction, material_color2, framework::RS_CULL_NONE , 0.2f);
//	//
//	//obj->SetAngle(angle);
//	//obj->SetPos(pos);
//	//obj->SetScalse(Vector3(PLYAER_LINE_SCALE, PLYAER_LINE_SCALE, PLYAER_LINE_SCALE));
//	//obj->Update();
//	//obj->Render(shadow_shader.get(), view->GetView(), view->GetProjection(), light_direction, material_color2, framework::RS_CULL_NONE, 0.2f);
//}

void Player::Render(View* view)
{


#if SHADER_CHANGE

	obj->SetAngle(angle);
	obj->SetPos(pos);
	obj->SetScalse(Vector3(PLYAER_SCALE, PLYAER_SCALE, PLYAER_SCALE));
	obj->Update();
	obj->Render(shader.get(), view->GetView(), view->GetProjection(), light_direction, material_color, framework::RS_FRONT,1.0f);


#else
	//シェイダーあり
	obj->SetAngle(angle);
	obj->SetPos(pos);
	obj->SetScalse(Vector3(PLYAER_SCALE, PLYAER_SCALE, PLYAER_SCALE));
	obj->Update();
	obj->Render(shader.get(), view->GetView(), view->GetProjection(), light_direction, material_color, framework::RS_FRONT, PLAYER_ANIME_SPEED);

	
	obj->SetAngle(angle);
	obj->SetPos(pos);
	obj->SetScalse(Vector3(PLYAER_LINE_SCALE, PLYAER_LINE_SCALE, PLYAER_LINE_SCALE));
	obj->Update();
	obj->Render(shader.get(), view->GetView(), view->GetProjection(), light_direction,black_color, framework::RS_SOLID, PLAYER_ANIME_SPEED);
#endif 
	
	eff->render(view->GetView(), view->GetProjection());
}