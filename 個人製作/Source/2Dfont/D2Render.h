#pragma once
#include "FrameWork.h"

class D2Render
{
public:
	ID2D1Factory*         g_pD2DFactory;
	IDWriteFactory *      g_pDWriteFactory;
	IDWriteTextFormat*    g_pTextFormat;
	ID2D1SolidColorBrush* g_pSolidBrush;
	Microsoft::WRL::ComPtr<IDWriteTextLayout> pTextLayout;

	~D2Render()
	{


		/*if (g_pDWriteFactory)
		{
			g_pDWriteFactory->Release();
			g_pDWriteFactory = NULL;
		}*/
		/*if (g_pTextFormat)
		{
		g_pTextFormat->Release();
		g_pTextFormat = NULL;
		}*/


	};
	//フォントと大きさの設定:(1:フォントの名 2:大きさ)
	void setText(WCHAR *c, int size);

	//文字描画(1:横位置 2:縦位置 3:幅 4:高さ 5:文字 6:color)
	void TextRender(int x, int y, int hx, int hy, std::wstring wst, D2D1::ColorF color = D2D1::ColorF::Black);
	//四角描画(1:横位置 2:縦位置 3:大きさX 4:大きさY,5:type(0か1)0:四角を塗りつぶす:1枠のみ,6:カラー)
	void SquareRender(int x, int y, int sizex, int sizey, int type, D2D1::ColorF c = D2D1::ColorF::White);

	//円描画(1:横位置 2:縦位置 3:大きさX 4:大きさY 5:type(0か1)0:円を塗りつぶす:1枠のみ)
	void Circle(int x, int y, int sizex, int sizey, int type, D2D1::ColorF color = D2D1::ColorF::White);
private:

};
