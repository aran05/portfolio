#include "A_System.h"
#include <Shlwapi.h>
#include "D2Render.h"
#include "FrameWork.h"

template <class T> inline void SafeRelease(T **ppT)
{
	if (*ppT)
	{
		(*ppT)->Release();
		*ppT = NULL;
	}
}


void D2Render::setText(WCHAR *c, int size)
{

	HRESULT hr = S_OK;

	// DirectWriteのファクトリの作成
	DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown**>(&g_pDWriteFactory));

	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));


	// Get the system font collection.
	if (SUCCEEDED(hr))
	{
		g_pDWriteFactory->GetSystemFontCollection(framework::pFontCollection.GetAddressOf());
	}

	UINT32 familyCount = 0;

	// Get the number of font families in the collection.
	if (SUCCEEDED(hr))
	{
		familyCount = framework::pFontCollection.Get()->GetFontFamilyCount();
	}

	for (UINT32 i = 0; i < familyCount; ++i)
	{
		IDWriteFontFamily* pFontFamily = NULL;

		// Get the font family.
		if (SUCCEEDED(hr))
		{
			hr = framework::pFontCollection.Get()->GetFontFamily(i, &pFontFamily);
		}

		IDWriteLocalizedStrings* pFamilyNames = NULL;

		// Get a list of localized strings for the family name.
		if (SUCCEEDED(hr))
		{
			hr = pFontFamily->GetFamilyNames(&pFamilyNames);
		}

		UINT32 index = 0;
		BOOL exists = false;

		wchar_t localeName[LOCALE_NAME_MAX_LENGTH];

		if (SUCCEEDED(hr))
		{
			// Get the default locale for this user.
			int defaultLocaleSuccess = GetUserDefaultLocaleName(localeName, LOCALE_NAME_MAX_LENGTH);

			// If the default locale is returned, find that locale name, otherwise use "en-us".
			if (defaultLocaleSuccess)
			{
				hr = pFamilyNames->FindLocaleName(localeName, &index, &exists);
			}
			if (SUCCEEDED(hr) && !exists) // if the above find did not find a match
			{
				hr = pFamilyNames->FindLocaleName(c, &index, &exists);
			}
		}

		// If the specified locale doesn't exist, select the first on the list.
		if (!exists) index = 0;

		UINT32 length = 0;

		// Get the string length.
		if (SUCCEEDED(hr))
		{
			hr = pFamilyNames->GetStringLength(index, &length);
		}

		// Allocate a string big enough to hold the name.
		wchar_t* name = new (std::nothrow) wchar_t[length + 1];
		if (name == NULL)
		{
			hr = E_OUTOFMEMORY;
		}

		// Get the family name.
		if (SUCCEEDED(hr))
		{
			hr = pFamilyNames->GetString(index, name, length + 1);
		}
		if (SUCCEEDED(hr))
		{
			// Print out the family name.
			wprintf(L"%s\n", name);
		}

		SafeRelease(&pFontFamily);
		SafeRelease(&pFamilyNames);

		delete[] name;
	}

	g_pTextFormat = NULL;
	//g_pDWriteFactory = NULL;
	//テキストフォーマットの作成
	g_pDWriteFactory->CreateTextFormat(c, NULL, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, (float)size, L"", &g_pTextFormat);
	g_pDWriteFactory->Release();
	//_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));


	//文字の位置の設定
	g_pTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
	g_pTextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
	//_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
}



void D2Render::TextRender(int x, int y, int hx, int hy, std::wstring wst, D2D1::ColorF color)
{

	framework::d2dCont_.Get()->BeginDraw();

	static float	angle = 0.0f;
	const float		strokeWidth = 16.0f;

	framework::d2dCont_.Get()->CreateSolidColorBrush(color, &g_pSolidBrush);



	//レイアウト
	float width = (float)framework::SCREEN_WIDTH;
	float height = (float)framework::SCREEN_HEIGHT;

	//描画位置
	D2D1_RECT_F rect;
	rect.left = (float)x;
	rect.top = (float)y;
	rect.right = rect.left + width;
	rect.bottom = rect.top + height;
	//DrawText


	g_pDWriteFactory->CreateTextLayout(
		wst.c_str(),
		wst.size(),
		g_pTextFormat,
		(float)hx, //レイアウト幅
		(float)hy,//レイアウト高さ
		&pTextLayout
	);

	D2D1_POINT_2F points;
	points.x = (float)x;
	points.y = (float)y;


	framework::d2dCont_.Get()->DrawTextLayout(points, pTextLayout.Get(), g_pSolidBrush);
	framework::d2dCont_.Get()->EndDraw();
}

void D2Render::SquareRender(int x, int y, int sizex, int sizey, int type, D2D1::ColorF color)
{
	D2D1::ColorF c = color;
	framework::d2dCont_.Get()->BeginDraw();
	framework::d2dCont_.Get()->CreateSolidColorBrush(c, &g_pSolidBrush);

	//// 中心
	//D2D1_POINT_2F tCenter = D2D1::Point2F(
	//	framework::SCREEN_WIDTH / 2
	//	, framework::SCREEN_HEIGHT / 2
	//	);

	// 描画矩形
	D2D1_RECT_F tRectF = D2D1::RectF(
		(float)(x - sizex)
		, (float)(y - sizey)
		, (float)(x + sizex)
		, (float)(y + sizey)
	);


	// 線の幅
	float fStrokeWidth = 5;

	// 四角形の描画
	if (type == 0)
	{
		framework::d2dCont_->FillRectangle(&tRectF, g_pSolidBrush);
	}
	else
	{
		framework::d2dCont_->DrawRectangle(&tRectF, g_pSolidBrush, fStrokeWidth/*,framework::m_pStrokeStyle.Get()*/);
	}

	framework::d2dCont_.Get()->EndDraw();
}

void D2Render::Circle(int x, int y, int sizex, int sizey, int type, D2D1::ColorF color)
{


	//D2D1::ColorF color = D2D1::ColorF::Black;
	D2D1::ColorF color2 = color;
	framework::d2dCont_.Get()->BeginDraw();
	framework::d2dCont_.Get()->CreateSolidColorBrush(color, &g_pSolidBrush);
	ID2D1SolidColorBrush* g_pSolidBrush2;
	framework::d2dCont_.Get()->CreateSolidColorBrush(color2, &g_pSolidBrush2);
	D2D1_ELLIPSE e;
	e.point = D2D1::Point2F(
		(float)x
		, (float)y
	);
	e.radiusX = (float)sizex;
	e.radiusY = (float)sizey;


	if (type == 0)
	{
		framework::d2dCont_->FillEllipse(&e, g_pSolidBrush2);
	}
	else
	{
		framework::d2dCont_->DrawEllipse(&e, g_pSolidBrush, 2.0f/*,framework::m_pStrokeStyle.Get()*/);
	}


	framework::d2dCont_.Get()->EndDraw();
}

