#include <windows.h>  
#include <stdio.h>  
#include <stdarg.h>  
#include <assert.h>
#include <DxErr.h>


#define _DEBUG_OUT_BUFF_SIZE_ 2048  

 
//////////////////////////////////////////////////
///	printfと同じ使い方で”出力”に出力できる
/////////////////////////////////////////////////
bool DebugPrintf( const char *str, ...)  
{  
#ifdef _DEBUG
    char debugOutBuff[ _DEBUG_OUT_BUFF_SIZE_ ];  

     va_list ap;  
     va_start( ap, str );      

    if ( !vsprintf_s( debugOutBuff, _DEBUG_OUT_BUFF_SIZE_, str, ap ) ) {  

         OutputDebugStringA( "dprintf error." );  

         return false;  

     }  
	va_end( ap ); 

    OutputDebugStringA( debugOutBuff );    
 
#endif
     return true;  
} 

int MyAssert( char* str )
{
//#if _UNICODE
//	TCHAR msg[2048];
//	MultiByteToWideChar( CP_ACP, 0, str, -1, msg, sizeof(msg) );
//	return MessageBoxW( NULL, msg, NULL, MB_ABORTRETRYIGNORE );
//#else
	return MessageBoxA( NULL, str, NULL, MB_ABORTRETRYIGNORE );
//#endif
}
int MyAssertHR( char* str, HRESULT hr )
{
	DXTRACE_ERR_MSGBOX(TEXT("HRエラー"), hr);
	return MessageBoxA( NULL, str, NULL, MB_ABORTRETRYIGNORE );
}