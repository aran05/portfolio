#pragma once
#include "Math\math.h"
#include "Sprite.h"
#include <memory>


#define MouseClick 0x00
//#define WM_MOUSEWHELL 0x020a


class MouseProcess
{
private:
	POINT mouse;
	bool ClickCheak;
	int ClickState;
	int waitTimeMouseCursor;
	Vector2 SaveCursor;


	std::unique_ptr<sprite> mouseUi;
	Vector2 PastPos;

public:
	MouseProcess();
	~MouseProcess(){};

	void ClickProsess();
	Vector2 MouseMovePos();
	Vector2 ClickMouseMovePos();

	void SetPos(POINT P) { mouse = P; }
	POINT GetPos() { return mouse; }
	void SetCState(int CS) { CS= ClickState; }
	int GetCState() { return ClickState; }
	int GTimer() { return waitTimeMouseCursor;}
	bool ButtonCollision(int mx, int my);

	void Update();
	void Render();

};

extern MouseProcess*mouse;