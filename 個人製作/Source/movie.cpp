#include <windows.h>
#include "A_System.h"
#include"FrameWork.h"
#include "Player.h"
#include"Sprite.h"
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<vector>
#include <dshow.h>

#include "KeyInput.h"

#include "movie.h"
#include "sceneMain.h"

//動画再生実行
void movie::init(const wchar_t *file_name)
{
	hr = S_OK;

	::CoInitialize(NULL);

	pGraphBuilder = NULL;
	pMediaControl = NULL;
	if (SUCCEEDED(hr))
	{
		hr = ::CoCreateInstance(CLSID_FilterGraph,
		// インスタンス生成
			NULL,
			CLSCTX_INPROC,
			IID_IGraphBuilder,
			(LPVOID*)&pGraphBuilder);

		// 各インターフェイスの取得
		//hr = pGraphBuilder->QueryInterface(IID_IMediaEvent, (LPVOID*)&pMediaEvent);
		hr = pGraphBuilder->QueryInterface(IID_IVideoWindow, (LPVOID *)&pVideoWindow);
		hr = pGraphBuilder->QueryInterface(IID_IMediaControl, (LPVOID *)&pMediaControl);

		//動画ファイルの指定
		hr = pMediaControl->RenderFile((BSTR)file_name);

		//ビデオのウィンドウをメインウィンドウに設定する
		//hr = pVideoWindow->put_Owner((OAHWND)framework::Hwnd);
	}


	flag = false;
}

void movie::update()
{
	//再生
	if (SUCCEEDED(hr))
	{
		if (flag == false)
		{
			
			//hr = pVideoWindow->put_MessageDrain((OAHWND)wnd);
			hr = pVideoWindow->put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS);


			RECT rect;
			::GetClientRect(framework::Hwnd, &rect);

			// Full Screen 開始
			//pVideoWindow->put_FullScreenMode(OATRUE);

			//動画サイズ決定
			hr = pVideoWindow->SetWindowPosition(0,0,
				framework::SCREEN_WIDTH,framework::SCREEN_HEIGHT);

			//ウィンドウにフォーカスを与えるかどうかを示す値 //OATRUE=ウィンドウにフォーカスを与える;
			//hr = pVideoWindow->SetWindowForeground(OATRUE);
			//hr = pVideoWindow->put_Visible(OATRUE);

			//動画再生
			hr = pMediaControl->Run();

			if (KEY_Get(KEY_ENTER) == 3)
			{
				flag = true;
			}


		}

	}

	//Zキーで画面切り替え
	if (KEY_Get(KEY_B) == 3)
	{
		flag = false;
	}

	//ビデオを最後まで再生する
	//pMediaEvent->WaitForCompletion(-1, &eventCode);

	//flagがtrueならば動画を止める
	if (flag == true)
	{
		// Full Screen 終了
		//pVideoWindow->put_FullScreenMode(OAFALSE);
		//停止
		hr = pMediaControl->Stop();
		// 描画ウィンドウからの切り離し
		hr = pVideoWindow->put_Visible(OAFALSE);
		//hr = pVideoWindow->put_MessageDrain(NULL);
		hr = pVideoWindow->put_Owner(NULL);
		scenemanager->changeScene(new sceneTitle());
		
		return;
	}
}

movie::movie()
{
}

movie::~movie()
{
	pMediaEvent->Release();
	pMediaControl->Release();
	pGraphBuilder->Release();
	pVideoWindow->Release();
	CoUninitialize();
}
