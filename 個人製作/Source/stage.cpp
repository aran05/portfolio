

#include <Windows.h>
#include "A_System.h"
#include "Player.h"
#include "KeyInput.h"
#include "stage.h"
#include "Enemy.h"
#include "skinned_mesh.h"
#include "OBJ_Mesh.h"
#include "sceneMain.h"
#include "View.h"
#include "Sprite.h"

stage::stage()
{
}


stage::~stage()
{
	if(pstage)
	{
		delete pstage;
		pstage = NULL;
	}
	if (pstage)
	{
		delete pstage2;
		pstage2 = NULL;
	}
	
}

bool stage::Initialize()
{
	pstage = new Skin_Mesh("DATA/gumi/stage.fbx", "DATA/Shader/Toon_vs.cso", "DATA/Shader/Toon_ps.cso");
	//pstage2 = new Skin_Mesh("DATA/gumi/stage.fbx", "DATA/Shader/Toon_vs.cso", "DATA/Shader/Toon_ps.cso");
	position[0] = Vector3(0, -4, -10);
	dimensions[0] = Vector3(10.0f,40.0f, 4.0f);
	angles[0] = Vector3(4.7f, 0.0f, 0.0f);
	pstage->SetPos(position[0]);
	pstage->SetScalse(dimensions[0]);
	pstage->SetAngle(angles[0]);
	position[1] = Vector3(0, 0.0f,-10);
	dimensions[1] = Vector3(10.0f, 40.0f, 4.0f);
	angles[1] = Vector3(4.7f, 0.0f, 0.0f);
	//pstage2->SetPos(position[1]);
	//pstage2->SetScalse(dimensions[1]);
	//pstage2->SetAngle(angles[1]);

	pstage->Update();
	//stage2->Update();

	return true;
}

void stage::Update()
{

	position[0].y -= 0.5f*framework::elapsed_time;
	position[1].y -= 0.5f*framework::elapsed_time;
	

	if(position[0].y<=-5.0f)
	{
		position[0] = Vector3(0, 4.0f, -10);

	}
	if(position[1].y <= -5.0f)
	{
		position[1] = Vector3(0, 4.0f, -10);
	}
	//pstage->SetPos(position[0]);
	//pstage->SetScalse(dimensions[0]);
	//pstage->SetAngle(angles[0]);

	//pstage->SetPos(position[1]);
	//pstage2->SetScalse(dimensions[1]);
	//pstage->SetAngle(angles[1]);

	//pstage->Update();
}



void stage::Render()
{

	static bool wireframe = false;
	if (GetAsyncKeyState('T') & 1)
	{
		wireframe = !wireframe;
	}

	light_direction = XMFLOAT4(0, 0, 1, 0);
	material_color = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);

	if (pstage != NULL)
	{
		pstage->SetPos(position[0]);
		pstage->SetScalse(dimensions[0]);
		pstage->SetAngle(angles[0]);
		pstage->Render(MainView->GetView(), MainView->GetProjection(), light_direction, material_color);
		pstage->Update();
	}

	if(pstage != NULL)
	{
		pstage->SetPos(position[1]);
		pstage->SetScalse(dimensions[0]);
		pstage->SetAngle(angles[1]);
		pstage->Render(MainView->GetView(), MainView->GetProjection(), light_direction, material_color);
		pstage->Update();
	}
}
