#ifndef __FRAMEWORK_H__
#define __FRAMEWORK_H__

#include <windows.h>
#include <d3d11.h>
#pragma comment( lib, "d3dcompiler.lib" )
#include <wrl.h>
#include <tchar.h>
#include <sstream>
#include <memory>

#include "A_System.h"
#include "Sound.h"
#include "sceneTitle.h"
#include "Scene.h"

#include "misc.h"

#include "high_resolution_timer.h"

#include "General_Purpose.h"
#include "Blend.h"
#include "D2Render.h"
#include <float.h>

//*****************************************************************************
//
//		フレームワーク
//
//*****************************************************************************

//*****************************************************************************
//
//*****************************************************************************
#define	FPS_60		0	//	固定６０フレーム
#define	FPS_30		1	//	固定３０フレーム
#define	FPS_FLEX	2	//	可変フレーム

#define SAFE_RELEASE(x) if((x)){(x)->Release();(x)=NULL;}
#define SAFE_DELETE(x) if((x)){delete (x);(x)=NULL;}

class D2Render;
class sceneTitle;

class framework
{
private:
	//------------------------------------------------------
	//	シーン
	//------------------------------------------------------
	//------------------------------------------------------
	//	更新
	//------------------------------------------------------
	BOOL	bRender;

	//------------------------------------------------------
	//	フレームレート関連
	//------------------------------------------------------
	DWORD	dwGameFrame;	//	起動からのフレーム数

	BYTE	FPSMode;		//	フレーム制御モード
	DWORD	dwFrameTime;

	DWORD	dwFPS;			//	処理フレームレート
	DWORD	dwRenderFPS;	//	描画フレームレート

	DWORD	dwCurFrame;		//	処理カウンタ
	DWORD	dwRCurFrame;	//	描画カウンタ

	DWORD before = GetTickCount();
	
public:

	//static blender* blend;
	static HWND Hwnd;
	static int SCREEN_WIDTH;
	static int SCREEN_HEIGHT;
	static int GetScreenWidth() { return SCREEN_WIDTH; }
	static int GetScreenHeight() { return SCREEN_HEIGHT; }

	static float elapsed_time;
	static Microsoft::WRL::ComPtr<ID3D11Device> device; 
	static Microsoft::WRL::ComPtr<ID3D11DeviceContext> device_context;
	static Microsoft::WRL::ComPtr<ID3D11Texture2D> depth_stencil_texture;
	static Microsoft::WRL::ComPtr<IDXGISwapChain1>swap_chain;
	static Microsoft::WRL::ComPtr<ID3D11RenderTargetView> render_target_view;
	static Microsoft::WRL::ComPtr<ID3D11DepthStencilView> depth_stencil_view;
	static Microsoft::WRL::ComPtr<ID3D11Buffer> view_pos_constent_buffer;
	static Microsoft::WRL::ComPtr<ID3D11DepthStencilState> depth_stencil_state[2];


	static bool InitializeRenderTarget();
	//ラスタライズステート
	static const int RASTERIZE_TYPE = 4;
	static Microsoft::WRL::ComPtr<ID3D11RasterizerState>  rasterizer_state[RASTERIZE_TYPE];
	static bool CreateRasterizerState();

	//ブレンドステート
	static const int BLEND_TYPE = 9;
	static  Microsoft::WRL::ComPtr<ID3D11BlendState> blend_state[BLEND_TYPE];
	static bool CreateBlendState();


	static Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> shader_resource_view;	

	static ID3D11Texture2D *backBufferTex;
	static Microsoft::WRL::ComPtr<ID2D1Device>            d2dDev_;
	static Microsoft::WRL::ComPtr<IDXGIDevice1>           dxgiDev_;
	static Microsoft::WRL::ComPtr<ID2D1DeviceContext>     d2dCont_;
	static Microsoft::WRL::ComPtr<ID2D1Bitmap1>           d2dTargetBitmap_;
	static Microsoft::WRL::ComPtr<IDWriteTextLayout>	  pTextLayout;
	static Microsoft::WRL::ComPtr<IDWriteFontCollection>  pFontCollection;
	static Microsoft::WRL::ComPtr<ID2D1StrokeStyle>		  m_pStrokeStyle;

	static Microsoft::WRL::ComPtr<ID3D11DepthStencilState> GetDephtStencilState(int state) { return depth_stencil_state[state]; }
	D2Render* fps;
	std::unique_ptr<Scene>							scene;

	std::unique_ptr<sprite> txt;
	//------------------------------------------------------
	//	初期化・解放
	//------------------------------------------------------
	/*framework(int FPSMode = FPS_FLEX);
	~framework();*/
	framework(HWND hwnd)
	{
		Hwnd = hwnd;
		FPSMode = FPS_60;
	
		dwGameFrame = 0;
		dwFrameTime = clock();
		
	}
	~framework();
	int run() {
		A_InitAudio();
		MSG msg = {};
		
		if (!Initialize()) return 0;
		while (WM_QUIT != msg.message) {
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else {
				
				timer.tick();
				calculate_frame_stats();
				elapsed_time = timer.time_interval();
				Update();
				Render();
			}
		}
		return static_cast<int>(msg.wParam);
	}
	LRESULT CALLBACK handle_message(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
	{
		HDC hdc;
		switch (msg)
		{
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HRESULT hr = S_OK;
			
			hdc = BeginPaint(hwnd, &ps);

			EndPaint(hwnd, &ps);
			
			break;
		}
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		case WM_CREATE:
			break;
		case WM_KEYDOWN:
			if (wparam == VK_ESCAPE) PostMessage(hwnd, WM_CLOSE, 0, 0);
			break;
		case WM_ENTERSIZEMOVE:
			// WM_EXITSIZEMOVE is sent when the user grabs the resize bars.
			timer.stop();
			break;
		case WM_EXITSIZEMOVE:
			// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
			// Here we reset everything based on the new window dimensions.
			timer.start();
			break;
		default:
			return DefWindowProc(hwnd, msg, wparam, lparam);
		}
		return 0;
	}

	static bool CreateDepthStencil();
	static Microsoft::WRL::ComPtr<ID3D11RasterizerState> GetRasterizerState(int state) { return rasterizer_state[state]; }
	static Microsoft::WRL::ComPtr<ID3D11BlendState> GetBlendState(int state) { return blend_state[state]; }

	
	bool Initialize();
	void Update();
	static void ClearView(float r = 1.0f, float g = 1.0f, float b = 1.0f, float a = 1.0f);
	static void ClearView(XMFLOAT4 rgba = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f));
	static void Flip(int n = 0);
	void Render();

	static void SetViewPort(int width, int height);
	//DepthStencilState
	enum { DS_FALSE, DS_TRUE };
	//RasterizerState
	enum { RS_SOLID, RS_WIRE,RS_FRONT, RS_CULL_NONE };
	//BlendState
	enum { BS_NONE, BS_ALPHA, BS_ADD, BS_SUBTRACT, BS_REPLACE, BS_MULTIPLY, BS_LIGHTEN, BS_DARKEN, BS_SCREEN };


	//------------------------------------------------------
	//	シーンの切り替え
	//------------------------------------------------------
	void ChangeScene(Scene* newScene) {
		/* 本来は今のシーンを 解放するが Unique なので不要。*/
		/* 切り替え & 初期化 */
		scene.reset(newScene);
		scene->Initialize();
	}

private:
	high_resolution_timer timer;
	bool calculate_frame_stats()
	{

		

		static 	DWORD	dwSec = 0;
		DWORD CurrentTime = clock() * 10;

		//	フレーム制限
		if (CurrentTime < dwFrameTime + 167) return false;

		//	経過時間
		DWORD	dTime = CurrentTime - dwFrameTime;
		if (dTime > 2000) dwFrameTime = CurrentTime;

		//	スキップタイプ 
		switch (FPSMode) {
		case FPS_60:	bRender = TRUE;	break;
		case FPS_30:	if (dwGameFrame & 0x01) bRender = TRUE; else bRender = FALSE;
			break;
		case FPS_FLEX:	if (dTime > 167 * 2) bRender = FALSE; else bRender = TRUE;
			break;
		}

		//	フレーム時間更新
		if (GetKeyState(VK_LCONTROL) < 0) dwFrameTime += 300;
		dwFrameTime += 167;

		//	秒間フレーム数保存
		if (dwSec < CurrentTime) {
			dwFPS = dwCurFrame;
			dwRenderFPS = dwRCurFrame;
			dwCurFrame = dwRCurFrame = 0;
			dwSec += 10000;
		}
		dwCurFrame++;	//	フレーム数更新
		dwGameFrame++;	//	ゲームフレーム数更新


		float mspf= 1000.0f / dwFPS;
#ifdef _DEBUG
		//	更新処理
		std::ostringstream outs;
		outs.precision(6);
		outs << "FPS : " << dwFPS << " / " << "Frame Time : " << mspf << " (ms)";
		SetWindowTextA(Hwnd, outs.str().c_str());
#endif
		return true;

	} 
};

#endif