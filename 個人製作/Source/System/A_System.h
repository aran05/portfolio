
#ifndef __A_SYSTEM_H__
#define __A_SYSTEM_H__

#define _CRT_SECURE_NO_WARNINGS
#define	_CRT_SECURE_NO_DEPRECATE

#include	<windows.h>

#include <d3d11.h>

#include <wrl.h>

#include	<dsound.h>
#include	<codec.h>

//#define		DIRECTINPUT_VERSION	0x0800 

#include <string>

#pragma comment ( lib, "winmm.lib" )

#include <mmsystem.h>
#include "Define.h"


#define SAFE_RELEASE(x) if((x)){(x)->Release();(x)=NULL;}
#define SAFE_DELETE(x) if((x)){delete (x);(x)=NULL;}


extern BOOL		bFullScreen;
extern DWORD	ScreenMode;
//メインシステム
/*学内ライブラリー参考*/
class A_System {
private:
	
	static CONST LONG SCREEN_WIDTH = 640;
	static CONST LONG SCREEN_HEIGHT = 480;

public:

	Microsoft::WRL::ComPtr<ID3D11Device> device;
	Microsoft::WRL::ComPtr<ID3D11DeviceContext> device_context;
	Microsoft::WRL::ComPtr<IDXGISwapChain>swap_chain;
	Microsoft::WRL::ComPtr<ID3D11RenderTargetView> render_target_view;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilView> depth_stencil_view;


	static HWND	Window;				//	ウィンドウハンドル
	static void GetScreenRect(DWORD mode, RECT& rc);

	//------------------------------------------------------
	//	初期化・解放
	//------------------------------------------------------
	//bool Initialize(HWND hWnd, BOOL bFullScreen, DWORD ScreenMode);

	static void OpenDebugWindow();
	static void CloseDebugWindow();

};

#endif