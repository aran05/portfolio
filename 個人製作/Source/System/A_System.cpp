#include "A_System.h"
//*****************************************************************************
//
//	
//
//*****************************************************************************

//*****************************************************************************
//		モード別スクリーンサイズ取得
//*****************************************************************************
void A_System::GetScreenRect(DWORD mode, RECT& rc)
{
	rc.left = rc.top = 0;
//*****************************************************************************

//*****************************************************************************
//
//		初期化
//

	switch (mode) {
	case SCREEN640:	 rc.right = 640;  rc.bottom = 480;	break;
	case SCREEN800:	 rc.right = 800;  rc.bottom = 600;	break;
	case SCREENT:    rc.right = 512;  rc.bottom = 800;  break;
	case SCREEN1024: rc.right = 1024; rc.bottom = 720;	break;
	case SCREEN720p: rc.right = 1280; rc.bottom = 720;	break;

	default:
		rc.right = (mode >> 16);
		rc.bottom = (mode & 0xFFFF);
		break;
	}
}


static FILE* DebugFP = NULL;

void A_System::OpenDebugWindow()
{
#ifdef _DEBUG
	AllocConsole();
	freopen_s(&DebugFP, "CON", "w", stdout);
#endif
}

void A_System::CloseDebugWindow()
{
#ifdef _DEBUG
	if (DebugFP)
	{
		fclose(DebugFP);
		FreeConsole();
	}
#endif
}



