#include <DirectXMath.h>
#include "FrameWork.h"
#include "View.h"
#include "Math\math.h"
#include "KeyInput.h"

View::View()
{
	ZeroMemory(this, 0);
	matView = { 1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1 };
	matProjection = { 1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1 };
}

View::~View()
{
}

void View::Set(Vector3 p, Vector3 t,Vector3 u)
{
	pos = p;
	target = t;
	up = u;
}

void View::SetProjection(float fo, float aspect, float n, float f)
{
	bView = true;
	fov = fo;
	Aspect = aspect;
	nearPlane = n;
	farPlane = f;

}

void View::SetOrtho(float width, float height, float min, float max)
{
	bView = false;
	Width = width;
	Height = height;
	nearPlane = min;
	farPlane = max;
}	

void View::Activate()
{

	XMMATRIX mv, mp;
	XMVECTOR fp, ft, fu;
	
	fp = XMVectorSet(pos.x, pos.y, pos.z,0);
	ft = XMVectorSet(target.x,target.y,target.z,0);
	fu = XMVectorSet(up.x,up.y,up.z,0);

	XMMATRIX vm = XMMatrixLookAtLH(fp,ft, fu);
	XMStoreFloat4x4(&matView, vm);

	XMMATRIX pm = XMMatrixIdentity();
	if(bView)
	{
		pm = XMMatrixPerspectiveFovLH(
			fov,					//視野角(ラジアン度)
			Aspect,					//アスペクト比
			nearPlane,				//最近面
			farPlane				//最遠面
		);
		XMStoreFloat4x4(&matProjection, pm);
	}	
	else
	{
		pm = XMMatrixOrthographicLH(Width, Height, nearPlane,farPlane);
		XMStoreFloat4x4(&matProjection, mp);
	}	

}



void View::SetViewPort(int width, int height)
{
	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)width;
	vp.Height = (FLOAT)height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;

	framework::device_context.Get()->RSSetViewports(1, &vp);
}

void View::Side_Rotation()
{

	Matrix&	m = (Matrix&)matView;
	Vector3	r = Vector3(m._11, m._21, m._31);
	Vector3	r2 = Vector3(m._12, m._22, m._32);
	Vector3	f = target - pos;
	float		d = f.Length();						//	2点間の距離保存	
	//r.Normalize();
	r2.Normalize();
	f.Normalize();

	if (GetKeyState('L')<0)
	{
		pos += r * 0.5f;
	}

	if (GetKeyState('J')<0)
	{
		pos -= r * 0.5f;
	}

	if (GetKeyState('I')<0)
	{
		pos += r2 * 0.5f;
	}

	if (GetKeyState('K')<0)
	{
		pos -= r2 * 0.5f;
	}

	Vector3	v = target - pos;							//	移動後のベクトル	
	v.Normalize();

	pos = target - v * d;								//	元の距離に調整		
}


void View::Move(Vector3 t)
{
	target = t;						//	注視点の更新

	Vector3 v = target - pos;			//	ベクトル作成
	v.y = 0;							//	高さを考慮しない
	float len = v.Length();				//	距離測定
	v.Normalize();						//	正規化

	if (len < 20.0f)
	{
		pos.x = target.x - v.x * 20.0f;
		pos.z = target.z - v.z * 20.0f;
	}
	if (len > 100.0f)
	{
		pos.x = target.x - v.x * 100.0f;
		pos.z = target.z - v.z * 100.0f;
	}
}

void View::Update(Vector3 t, Vector3 u)
{
	Side_Rotation();

	Set(pos, t, u);
}

