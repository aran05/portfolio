#include "FrameWork.h"
#include "Shader.h"


HRESULT create_vs_from_cso(ID3D11Device *device,const char *cso_name,ID3D11VertexShader **vertex_shader,ID3D11InputLayout **input_layout,D3D11_INPUT_ELEMENT_DESC *input_element_desc,UINT num_elementse)
{
	FILE* fp = nullptr;
	fopen_s(&fp, cso_name, "rb");
	_ASSERT_EXPR_A(fp, "CSO File not found");

	fseek(fp, 0, SEEK_END);
	long cso_sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	std::unique_ptr<unsigned char[]>cso_data = std::make_unique<unsigned char[]>(cso_sz);
	fread(cso_data.get(), cso_sz, 1, fp);
	fclose(fp);

	HRESULT hr = device->CreateVertexShader(cso_data.get(), cso_sz, nullptr, vertex_shader);
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	hr = device->CreateInputLayout(input_element_desc, num_elementse, cso_data.get(),cso_sz, input_layout);
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	return hr;
}


HRESULT create_ps_from_cso(ID3D11Device *device,const char *cso_name,ID3D11PixelShader **pixel_shader)
{
	FILE* fp = nullptr;
	fopen_s(&fp, cso_name, "rb");
	_ASSERT_EXPR_A(fp, "CSO File not found");

	fseek(fp, 0, SEEK_END);
	long cso_sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	std::unique_ptr<unsigned char[]>cso_data = std::make_unique<unsigned char[]>(cso_sz);
	fread(cso_data.get(), cso_sz, 1, fp);
	fclose(fp);

	HRESULT hr = device->CreatePixelShader(cso_data.get(), cso_sz, nullptr,pixel_shader);
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	return hr;
}

HRESULT create_geo_from_cso(ID3D11Device * device, const char * cso_name, ID3D11GeometryShader ** geometry_shader)
{
	FILE* fp = nullptr;
	fopen_s(&fp, cso_name, "rb");
	_ASSERT_EXPR_A(fp, "CSO File not found");

	fseek(fp, 0, SEEK_END);
	long cso_sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	std::unique_ptr<unsigned char[]>cso_data = std::make_unique<unsigned char[]>(cso_sz);
	fread(cso_data.get(), cso_sz, 1, fp);
	fclose(fp);

	HRESULT hr = device->CreateGeometryShader(cso_data.get(), cso_sz, nullptr, geometry_shader);
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	return hr;
}



//Shader::~Shader()
//{
//	SAFE_RELEASE(VS);
//	SAFE_RELEASE(PS);
//	SAFE_RELEASE(VertexLayout);
//
//}
//
////****************************************************************
////
////
////
////****************************************************************
////------------------------------------------------
////	シェーダー単体コンパイル
////------------------------------------------------
//HRESULT Shader::Compile(WCHAR* filename, LPCSTR method, LPCSTR shaderModel, ID3DBlob** ppBlobOut)
//{
//	DWORD ShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
//	ShaderFlags |= D3DCOMPILE_DEBUG;
//	ShaderFlags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
//
//
//
//	ID3DBlob* BlobError = NULL;
//	// コンパイル
//	HRESULT hr = D3DCompileFromFile(
//		filename,
//		NULL,
//		D3D_COMPILE_STANDARD_FILE_INCLUDE,
//		method,
//		shaderModel,
//		ShaderFlags,
//		0,
//		ppBlobOut,
//		&BlobError
//	);
//
//	// エラー出力
//	if (BlobError != NULL)
//	{
//		//		OutputDebugStringA((char*)BlobError->GetBufferPointer());
//		MessageBoxA(0, (char*)BlobError->GetBufferPointer(), NULL, MB_OK);
//		BlobError->Release();
//		BlobError = NULL;
//	}
//
//	return hr;
//}
//
////------------------------------------------------
////	シェーダーセットコンパイル
////------------------------------------------------
//bool Shader::Create(WCHAR* filename, LPCSTR VSFunc, LPCSTR PSFunc)
//{
//	HRESULT hr = S_OK;
//
//	ID3DBlob* VSBlob = NULL;
//	// 頂点シェーダ
//	hr = Compile(filename, VSFunc, "vs_5_0", &VSBlob);
//	if (FAILED(hr))
//	{
//		return false;
//	}
//
//	// 頂点シェーダ生成
//	hr = framework::device.Get()->CreateVertexShader(VSBlob->GetBufferPointer(), VSBlob->GetBufferSize(), NULL, &VS);
//	if (FAILED(hr))
//	{
//		VSBlob->Release();
//		return false;
//	}
//
//	// 入力レイアウト
//	D3D11_INPUT_ELEMENT_DESC layout[] = {
//		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//	};
//	UINT numElements = ARRAYSIZE(layout);
//
//	// 入力レイアウト生成
//	hr = framework::device.Get()->CreateInputLayout(
//		layout,
//		numElements,
//		VSBlob->GetBufferPointer(),
//		VSBlob->GetBufferSize(),
//		&VertexLayout
//	);
//
//	VSBlob->Release();
//	if (FAILED(hr))
//	{
//		return false;
//	}
//
//	// 入力レイアウト設定
//	
//	framework::device_context.Get()->IASetInputLayout(VertexLayout);
//
//
//	// ピクセルシェーダ
//	ID3DBlob* PSBlob = NULL;
//	hr = Compile(filename, PSFunc, "ps_5_0", &PSBlob);
//	if (FAILED(hr))
//	{
//		return false;
//	}
//
//	// ピクセルシェーダ生成
//	hr = framework::device.Get()->CreatePixelShader(PSBlob->GetBufferPointer(), PSBlob->GetBufferSize(), NULL, &PS);
//	PSBlob->Release();
//	if (FAILED(hr))
//	{
//		return false;
//	}
//
//	return true;
//}
//
//
//
////****************************************************************
////
////
////
////****************************************************************
////------------------------------------------------
////	有効化
////------------------------------------------------
//void Shader::Activate()
//{
//	framework::device_context.Get()->VSSetShader(VS, NULL, 0);
//	framework::device_context.Get()->PSSetShader(PS, NULL, 0);
//
//}
//


Shader::~Shader()
{
	SAFE_RELEASE(VS);
	SAFE_RELEASE(PS);
	SAFE_RELEASE(GS);
	SAFE_RELEASE(HS);
	SAFE_RELEASE(DS);
	SAFE_RELEASE(VertexLayout);

}


HRESULT Shader::create_vs_from_cso(LPCSTR cso_name, D3D11_INPUT_ELEMENT_DESC * input_element_desc, UINT num_elementse)
{
	FILE* fp = nullptr;
	fopen_s(&fp, cso_name, "rb");
	_ASSERT_EXPR_A(fp, "CSO File not found");

	fseek(fp, 0, SEEK_END);
	long cso_sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	std::unique_ptr<unsigned char[]>cso_data = std::make_unique<unsigned char[]>(cso_sz);
	fread(cso_data.get(), cso_sz, 1, fp);
	fclose(fp);

	HRESULT hr = framework::device.Get() ->CreateVertexShader(cso_data.get(), cso_sz, nullptr, &VS);
	if (FAILED(hr))
	{
		cso_data.release();
		return false;
	}

	hr = framework::device.Get()->CreateInputLayout(input_element_desc, num_elementse, cso_data.get(), cso_sz, &VertexLayout);
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	framework::device_context.Get()->IASetInputLayout(VertexLayout);
	return hr;
}

HRESULT Shader::create_ps_from_cso(LPCSTR cso_name)
{
	FILE* fp = nullptr;
	fopen_s(&fp, cso_name, "rb");
	_ASSERT_EXPR_A(fp, "CSO File not found");

	fseek(fp, 0, SEEK_END);
	long cso_sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	std::unique_ptr<unsigned char[]>cso_data = std::make_unique<unsigned char[]>(cso_sz);
	fread(cso_data.get(), cso_sz, 1, fp);
	fclose(fp);

	HRESULT hr = framework::device.Get()->CreatePixelShader(cso_data.get(), cso_sz, nullptr, &PS);
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	return hr;
}

HRESULT Shader::create_geo_from_cso(LPCSTR cso_name)
{
	FILE* fp = nullptr;
	fopen_s(&fp, cso_name, "rb");
	_ASSERT_EXPR_A(fp, "CSO File not found");

	fseek(fp, 0, SEEK_END);
	long cso_sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	std::unique_ptr<unsigned char[]>cso_data = std::make_unique<unsigned char[]>(cso_sz);
	fread(cso_data.get(), cso_sz, 1, fp);
	fclose(fp);

	HRESULT hr = framework::device.Get()->CreateGeometryShader(cso_data.get(), cso_sz, nullptr, &GS);
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	return hr;
}

HRESULT Shader::create_hs_from_cso(LPCSTR cso_name)
{
	FILE* fp = nullptr;
	fopen_s(&fp, cso_name, "rb");
	_ASSERT_EXPR_A(fp, "CSO File not found");

	fseek(fp, 0, SEEK_END);
	long cso_sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	std::unique_ptr<unsigned char[]>cso_data = std::make_unique<unsigned char[]>(cso_sz);
	fread(cso_data.get(), cso_sz, 1, fp);
	fclose(fp);

	HRESULT hr = framework::device.Get()->CreateHullShader(cso_data.get(), cso_sz, nullptr, &HS);
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	return hr;
}

HRESULT Shader::create_ds_from_cso(LPCSTR cso_name)
{
	FILE* fp = nullptr;
	fopen_s(&fp, cso_name, "rb");
	_ASSERT_EXPR_A(fp, "CSO File not found");

	fseek(fp, 0, SEEK_END);
	long cso_sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	std::unique_ptr<unsigned char[]>cso_data = std::make_unique<unsigned char[]>(cso_sz);
	fread(cso_data.get(), cso_sz, 1, fp);
	fclose(fp);

	HRESULT hr = framework::device.Get()->CreateDomainShader(cso_data.get(), cso_sz, nullptr, &DS);
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	return hr;
}

void Shader::Activate()
{

	framework::device_context->VSSetShader(VS, NULL, 0);
	framework::device_context->PSSetShader(PS, NULL, 0);
	framework::device_context->GSSetShader(GS, NULL, 0);
	framework::device_context->HSSetShader(HS, NULL, 0);
	framework::device_context->DSSetShader(DS, NULL, 0);
}

bool Shader::Create(LPCSTR vs_filename, LPCSTR ps_filename, LPCSTR geo_filename,int num)
{

	HRESULT hr = S_OK;


	D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "WEIGHTS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONES", 0, DXGI_FORMAT_R32G32B32A32_SINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR",0,DXGI_FORMAT_R32G32B32A32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0 },
	};

	hr = create_vs_from_cso(vs_filename, input_element_desc, ARRAYSIZE(input_element_desc));
	hr = create_ps_from_cso(ps_filename);
	hr = create_geo_from_cso(geo_filename);

	

	return true;
}

bool Shader::Create(LPCSTR vs_filename, LPCSTR ps_filename)
{
	HRESULT hr = S_OK;

	D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "WEIGHTS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONES", 0, DXGI_FORMAT_R32G32B32A32_SINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR",0,DXGI_FORMAT_R32G32B32A32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0 },
	};

	hr = create_vs_from_cso(vs_filename, input_element_desc, ARRAYSIZE(input_element_desc));
	hr = create_ps_from_cso(ps_filename);
	
	return true;
}

bool Shader::Create(LPCSTR vs_filename, LPCSTR ps_filename, LPCSTR gs_filename)
{
	HRESULT hr = S_OK;

	D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "WEIGHTS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONES", 0, DXGI_FORMAT_R32G32B32A32_SINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR",0,DXGI_FORMAT_R32G32B32A32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0 },
	};

	hr = create_vs_from_cso(vs_filename, input_element_desc, ARRAYSIZE(input_element_desc));

	hr = create_ps_from_cso(ps_filename);

	hr = create_geo_from_cso(gs_filename);

	return false;
}

bool Shader::Create(LPCSTR vs_filename, LPCSTR ps_filename, LPCSTR gs_filename, LPCSTR hs_filename, LPCSTR ds_filename)
{
	HRESULT hr = S_OK;

	D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "WEIGHTS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONES", 0, DXGI_FORMAT_R32G32B32A32_SINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	hr = create_vs_from_cso(vs_filename, input_element_desc, ARRAYSIZE(input_element_desc));

	hr = create_ps_from_cso(ps_filename);

	hr = create_geo_from_cso(gs_filename);

	hr = create_hs_from_cso(hs_filename);

	hr = create_ds_from_cso(ds_filename);
	
	return false;
}

bool Shader::Create2(LPCSTR vs_filename, LPCSTR ps_filename)
{

	HRESULT hr = S_OK;

	D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	hr = create_vs_from_cso(vs_filename, input_element_desc, ARRAYSIZE(input_element_desc));

	hr = create_ps_from_cso(ps_filename);

	
	return true;
}

bool Shader::Create2(LPCSTR vs_filename, LPCSTR ps_filename, LPCSTR gs_filename, LPCSTR hs_filename, LPCSTR ds_filename)
{
	HRESULT hr = S_OK;

	D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	hr = create_vs_from_cso(vs_filename, input_element_desc, ARRAYSIZE(input_element_desc));

	hr = create_ps_from_cso(ps_filename);

	hr = create_geo_from_cso(gs_filename);

	hr = create_hs_from_cso(hs_filename);

	hr = create_ds_from_cso(ds_filename);

	return true;
}

bool Shader::Create3(LPCSTR vs_filename, LPCSTR ps_filename)
{
	HRESULT hr = S_OK;

	D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	
	hr = create_ps_from_cso(ps_filename);

	hr = create_vs_from_cso(vs_filename, input_element_desc, ARRAYSIZE(input_element_desc));


	return true;
}
