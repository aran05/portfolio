#include "Math\math.h"
#include "Particl.h"
#include "FrameWork.h"

Particle::Particle(const wchar_t *file_name, int max)
{
	obj = std::make_unique<sprite>(file_name);
	p = std::make_unique<ParticleData[]>(max);
	dataMax = max;
	ZeroMemory(p.get(), sizeof(ParticleData)*dataMax);
	for (int i = 0; i < dataMax;i++)
	{
		p[i].type = -1;
	}

}

Particle::~Particle()
{
	
}

void Particle::update(float elapsed_time)
{
	for(int i = 0; i < dataMax; i++)
	{
		if (p[i].timer > p[i].limit) p[i].type = -1;
		if (p[i].type == -1) continue;

		p[i].timer += elapsed_time;
		p[i].vec.x += p[i].power.x*p[i].timer;
		p[i].vec.y += p[i].power.y*p[i].timer;

		//
		p[i].pos.x += p[i].vec.x*p[i].timer;
		p[i].pos.y += p[i].vec.y*p[i].timer;

		p[i].color.w = p[i].alpha*(1.0f - p[i].timer / p[i].limit);

	}
}

void Particle::render(blender* blender) 
{

	
	for (int i = 0; i < dataMax; i++)
	{
		if (p[i].type == -1) continue;

		float sw = (float)texture2d_desc.Width / 4;
		float sh = (float)texture2d_desc.Height / 4;
		float sx = p[i].type % 4 * sw;
		float sy = p[i].type / 4 * sh;

		float dw = sw*p[i].scale.x;
		float dh = sh*p[i].scale.y;
		float dx = p[i].pos.x * 0.5f;
		float dy = p[i].pos.y * 0.5f;

		float angle = p[i].angle;
		float r = p[i].color.x;
		float g = p[i].color.y;
		float b = p[i].color.z;
		float a = p[i].color.w;


		framework::device_context.Get()->OMSetBlendState(blender->states[p[i].blend].Get(), nullptr, 0xFFFFFFFF);

		obj->render(dx, dy, dw, dh,sx,sy,sw,sh,angle,r,g,b,a);


	}
}

void Particle::set(int type, DirectX::XMFLOAT2 pos, DirectX::XMFLOAT2 vec, DirectX::XMFLOAT2 power, DirectX::XMFLOAT2 scale, DirectX::XMFLOAT4 color, float limit, int blend)
{
	for (int i = 0; i < dataMax; i++)
	{
		if (p[i].type >=0) continue;
		p[i].type = type;
		p[i].pos = pos;
		p[i].vec = vec;
		p[i].power = power;
		p[i].scale = scale;
		p[i].color = color;
		p[i].alpha = color.w;
		p[i].timer = 0;
		p[i].limit = limit;
		p[i].blend = blend;
		break;
	}
}


void Particle::heart(DirectX::XMFLOAT2 pos)
{
	for (int i = 0; i < 10;i++)
	{
		DirectX::XMFLOAT2 Pos, Vec,Power;
		Pos.x = pos.x + (rand() % 1000 - 500)*0.01f;
		Pos.y = pos.y + (rand() % 1000 - 500)*0.01f;
		Vec.x = (rand() % 1000 - 500)*0.01f;
		Vec.y = (rand() % 1000)*0.02f;
		Power.x = .0f;
		Power.y = 1.0f + (rand() % 1000 % 500)*0.001f;

		set(7, Pos, Vec, Power, DirectX::XMFLOAT2(.5f, .5f),DirectX::XMFLOAT4(1.0f,1.0f, 0.0f, 0.4f),1.0f, blender::BS_ADD);
	}
}
