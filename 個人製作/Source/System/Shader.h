#pragma once

#include <d3d11.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
using namespace DirectX;

//class Shader
//{
//protected:
//	ID3D11VertexShader*		VS; // 頂点シェーダ
//	ID3D11PixelShader*		PS; // ピクセルシェーダ
//
//	ID3D11InputLayout*		VertexLayout;
//
//	HRESULT Compile(WCHAR* filename, LPCSTR method, LPCSTR shaderModel, ID3DBlob** ppBlobOut);
//
//
//public:
//	Shader() { ZeroMemory(this, sizeof(Shader)); }
//	~Shader();
//
//	bool Create(WCHAR* filename, LPCSTR VSName, LPCSTR PSName);
//	void Activate();
//};
//
//struct VERTEX
//{
//	XMFLOAT3 Pos;	//位置
//	XMFLOAT3 Normal;//法線
//	XMFLOAT2 Tex;	//UV座標
//	XMFLOAT4 Color;	//頂点色
//};


class Shader
{
	int num;
protected:
	ID3D11VertexShader*		VS = nullptr; //頂点シェーダ
	ID3D11PixelShader*		PS = nullptr; //ピクセルシェーダ
	ID3D11GeometryShader*	GS = nullptr; //ジオメトリシェーダ
	ID3D11HullShader*		HS = nullptr; //ハルシェーダ
	ID3D11DomainShader*		DS = nullptr; //ドメインネームシェーダ
	ID3D11InputLayout*		VertexLayout;

public:
	Shader() { ZeroMemory(this, sizeof(Shader)); }
	~Shader();

	HRESULT create_vs_from_cso(
		LPCSTR cso_name,
		D3D11_INPUT_ELEMENT_DESC *input_element_desc,
		UINT num_elementse
	);

	HRESULT create_ps_from_cso(
		LPCSTR cso_name
	);


	HRESULT create_geo_from_cso(
		LPCSTR cso_name
	);

	HRESULT create_hs_from_cso(
		LPCSTR cso_name
	);

	HRESULT create_ds_from_cso(
		LPCSTR cso_name
	);
	void Activate();

	ID3D11VertexShader* Get_VS() { return VS; }
	ID3D11PixelShader * Get_PS() { return PS; }
	ID3D11GeometryShader *Get_GS() { return GS; }
	ID3D11HullShader *Get_HS() { return HS; }
	ID3D11DomainShader *Get_DS() { return DS; }
	ID3D11InputLayout * Get_InputLayout(){ return VertexLayout; }
	bool Create(LPCSTR vs_filename, LPCSTR ps_filename, LPCSTR gs_filename,int num);

	bool Create(LPCSTR vs_filename, LPCSTR ps_filename);
	bool Create(LPCSTR vs_filename, LPCSTR ps_filename, LPCSTR gs_filename);
	bool Create(LPCSTR vs_filename, LPCSTR ps_filename, LPCSTR gs_filename, LPCSTR hs_filename, LPCSTR ds_filename);
	bool Create2(LPCSTR vs_filename, LPCSTR ps_filename);
	bool Create2(LPCSTR vs_filename, LPCSTR ps_filename, LPCSTR gs_filename, LPCSTR hs_filename, LPCSTR ds_filename);
	bool Create3(LPCSTR vs_filename, LPCSTR ps_filename);


};

HRESULT create_vs_from_cso(
	ID3D11Device *device,
	const char *cso_name,
	ID3D11VertexShader **vertex_shader,
	ID3D11InputLayout **input_layout,
	D3D11_INPUT_ELEMENT_DESC *input_element_desc,
	UINT num_elementse
	);

HRESULT create_ps_from_cso(
	ID3D11Device *device,
	const char *cso_name,
	ID3D11PixelShader **pixel_shader
	);


HRESULT create_geo_from_cso(
	ID3D11Device *device,
	const char *cso_name,
	ID3D11GeometryShader **geometry_shader
);


struct VERTEX
{
	XMFLOAT3 Pos;	//位置
	XMFLOAT3 Normal;//法線
	XMFLOAT2 Tex;	//UV座標
	XMFLOAT4 Color;	//頂点色
};