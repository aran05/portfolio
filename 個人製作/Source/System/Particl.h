#pragma once
#include <directxmath.h>
#include <memory>

#include "sprite.h"
#include "Blend.h"

typedef struct
{
	DirectX::XMFLOAT2 pos;
	DirectX::XMFLOAT2 vec;
	DirectX::XMFLOAT2 power;
	DirectX::XMFLOAT2 scale;
	DirectX::XMFLOAT4 color;

	int type;
	float timer;
	float limit;
	float alpha;
	float angle;
	int blend;
}ParticleData;

class Particle :public sprite
{

	std::unique_ptr<ParticleData[]>p;
	std::unique_ptr<sprite> obj;
	int dataMax;
public:
	Particle(const wchar_t *file_name, int max);
	~Particle();
	void update(float elapsed_time);
	void render(blender* blender);
	void set(int type, DirectX::XMFLOAT2 pos, DirectX::XMFLOAT2 vec, DirectX::XMFLOAT2 power, DirectX::XMFLOAT2 scale, DirectX::XMFLOAT4 color, float limit,int blend = blender::BS_ALPHA);
	void set(int type, Vector3 pos, Vector3 vec, Vector3 power, DirectX::XMFLOAT2 scale, DirectX::XMFLOAT4 color, float limit, int blend = blender::BS_ALPHA);

	void heart(DirectX::XMFLOAT2 pos);
};