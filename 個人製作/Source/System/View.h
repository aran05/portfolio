#pragma once
#include <DirectXMath.h>
using namespace DirectX;

class View
{
private:

protected:
	XMFLOAT4X4 matView;			//カメラ行列
	XMFLOAT4X4 matProjection;	//プロジェクション行列

	//回転
	Vector3 front;
	Vector3 pos=Vector3(0,0,0);
	Vector3 target;
	Vector3 up;

	float fov;			 //視野角
	float Aspect;		 //アスペクト比
	float nearPlane;	 //二アプレーン
	float farPlane;		 //ファープレーン

	FLOAT Width;   //幅
	FLOAT Height;  //高さ
	bool bView;	   //投影方法フラグ

public:


	View();
	virtual ~View();
	void SetPos(Vector3 p) { pos = p; }
	void SetTarget(Vector3 t) { target = t; }
	void Set(Vector3 p, Vector3 t, Vector3 u);
	void SetProjection(float fov, float aspect, float n, float f);
	void SetOrtho(float width, float height, float min, float max);
	void Activate();
	void SetViewPort(int width, int height);
	void Side_Rotation();
	void Move(Vector3 t);

	void Update(Vector3 t, Vector3 u = Vector3(0, 1, 0));

	XMMATRIX GetView() { return XMLoadFloat4x4(&matView); }
	Vector3 GetPos() { return pos; }
	Vector3 GetTarget() { return target; }
	XMMATRIX GetProjection() { return XMLoadFloat4x4(&matProjection); }
};
