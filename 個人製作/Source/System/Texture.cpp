#include <WICTextureLoader.h>
#include <d3d11.h>
#include "Texture.h"
#include "FrameWork.h"
#include "Misc.h"

#include <map>
#include <wrl.h>


HRESULT load_texture_from_file(const wchar_t *file_name, ID3D11ShaderResourceView **shader_resource_view, D3D11_TEXTURE2D_DESC *texture2d_desc)
{

	HRESULT hr = S_OK;
	Microsoft::WRL::ComPtr<ID3D11Resource> resource;


	static std::map<std::wstring, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> cache;
	auto it = cache.find(file_name);
	if (it != cache.end())
	{
		//it->second.Attach(*shader_resource_view);
		*shader_resource_view = it->second.Get();
		(*shader_resource_view)->AddRef();
		(*shader_resource_view)->GetResource(resource.GetAddressOf());
	}
	else
	{
		hr = DirectX::CreateWICTextureFromFile(framework::device.Get(), file_name, resource.GetAddressOf(), shader_resource_view);
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
		cache.insert(std::make_pair(file_name, *shader_resource_view));
	}

	Microsoft::WRL::ComPtr<ID3D11Texture2D> texture2d;
	hr = resource.Get()->QueryInterface<ID3D11Texture2D>(texture2d.GetAddressOf());
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	texture2d->GetDesc(texture2d_desc);

	return hr;
}


HRESULT make_dummy_texture(ID3D11Device *device, ID3D11ShaderResourceView **shader_resource_view)
{
	HRESULT hr = S_OK;

	D3D11_TEXTURE2D_DESC texture2d_desc = {};
	texture2d_desc.Width = 1;
	texture2d_desc.Height = 1;
	texture2d_desc.MipLevels = 1;
	texture2d_desc.ArraySize = 1;
	texture2d_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texture2d_desc.SampleDesc.Count = 1;
	texture2d_desc.SampleDesc.Quality = 0;
	texture2d_desc.Usage = D3D11_USAGE_DEFAULT;
	texture2d_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texture2d_desc.CPUAccessFlags = 0;
	texture2d_desc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA subresource_data = {};
	u_int color = 0xFFFFFFFF;
	subresource_data.pSysMem = &color;
	subresource_data.SysMemPitch = 4;
	subresource_data.SysMemSlicePitch = 4;

	ID3D11Texture2D *texture2d;
	hr = device->CreateTexture2D(&texture2d_desc, &subresource_data, &texture2d);
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	D3D11_SHADER_RESOURCE_VIEW_DESC shader_resource_view_desc = {};
	shader_resource_view_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	shader_resource_view_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shader_resource_view_desc.Texture2D.MipLevels = 1;

	hr = device->CreateShaderResourceView(texture2d, &shader_resource_view_desc, shader_resource_view);
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	texture2d->Release();

	return hr;
}

/*
e.g.
referrer_filename <= L"data/bison.obj"
referent_filename <= L"/user/textures/bison.png"
combined_resource_path => L"/data/bison.png"
*/

void combine_resource_path(wchar_t(&combined_resource_path)[256], const wchar_t *referrer_filename, const wchar_t *referent_filename)
{
	const wchar_t delimiters[] = { L'\\', L'/' };
	// extract directory from obj_filename
	wchar_t directory[256] = {};
	for (wchar_t delimiter : delimiters)
	{
		wchar_t *p = wcsrchr(const_cast<wchar_t *>(referrer_filename), delimiter);
		if (p)
		{
			memcpy_s(directory, 256, referrer_filename, (p - referrer_filename + 1)*sizeof(wchar_t));
			break;
		}
	}
	// extract filename from resource_filename
	wchar_t filename[256];
	wcscpy_s(filename, referent_filename);
	for (wchar_t delimiter : delimiters)
	{
		wchar_t *p = wcsrchr(filename, delimiter);
		if (p)
		{
			wcscpy_s(filename, p + 1);
			break;
		}
	}
	// combine directory and filename
	wcscpy_s(combined_resource_path, directory);
	wcscat_s(combined_resource_path, filename);
};


Texture::Texture()
{
	ShaderResourceView = nullptr;
	sampler = nullptr;
	RenderTargetView = nullptr;
}


Texture::~Texture()
{
	SAFE_RELEASE(ShaderResourceView);
	SAFE_RELEASE(sampler);
	SAFE_RELEASE(RenderTargetView);
}

bool Texture::Load(const wchar_t * filename)
{
	//	シェーダーリソースビュー作成
	ID3D11Resource* resource;
	DirectX::CreateWICTextureFromFile(framework::device.Get(), filename, &resource, &ShaderResourceView);

	//テクスチャ情報取得
	ID3D11Texture2D* m_pTexture2D;
	resource->QueryInterface(&m_pTexture2D);
	m_pTexture2D->GetDesc(&texture2d_desc);
	SAFE_RELEASE(m_pTexture2D);
	SAFE_RELEASE(resource);



	//	サンプラステート作成
	D3D11_SAMPLER_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.Filter = D3D11_FILTER_MIN_MAG_POINT_MIP_LINEAR;
	sd.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sd.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	sd.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	sd.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sd.MinLOD = -D3D11_FLOAT32_MAX;
	sd.MaxLOD = D3D11_FLOAT32_MAX;

	framework::device.Get()->CreateSamplerState(
		&sd, &sampler);

	return true;
}
void Texture::Set(UINT Slot)
{
	framework::device_context.Get()->PSSetShaderResources(Slot, 1, &ShaderResourceView);
	framework::device_context.Get()->PSSetSamplers(Slot, 1, &sampler);

	framework::device_context.Get()->DSSetShaderResources(Slot, 1, &ShaderResourceView);
	framework::device_context.Get()->DSSetSamplers(Slot, 1, &sampler);
}

bool Texture::Create(u_int width, u_int height, DXGI_FORMAT format)
{
	ID3D11Texture2D* Texture2D;
	HRESULT hr = S_OK;
	//	テクスチャ作成
	ZeroMemory(&texture2d_desc, sizeof(texture2d_desc));
	texture2d_desc.Width = width;
	texture2d_desc.Height = height;
	texture2d_desc.MipLevels = 1;
	texture2d_desc.ArraySize = 1;
	texture2d_desc.Format = format;
	texture2d_desc.SampleDesc.Count = 1;
	texture2d_desc.Usage = D3D11_USAGE_DEFAULT;
	texture2d_desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;

	//	ID3D11Texture2D * texture;
	hr = framework::device.Get()->CreateTexture2D(&texture2d_desc, NULL, &Texture2D);
	if (FAILED(hr))
	{
		return false;
	}

	//	レンダーターゲットビュー作成
	D3D11_RENDER_TARGET_VIEW_DESC rtvd;
	ZeroMemory(&rtvd, sizeof(rtvd));
	rtvd.Format = format;
	rtvd.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	rtvd.Texture2D.MipSlice = 0;
	hr = framework::device.Get()->CreateRenderTargetView(Texture2D, &rtvd, &RenderTargetView);
	if (FAILED(hr))
	{
		return false;
	}


	//	シェーダーリソースビュー作成
	D3D11_SHADER_RESOURCE_VIEW_DESC srvd;
	ZeroMemory(&srvd, sizeof(srvd));
	srvd.Format = format;
	srvd.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvd.Texture2D.MostDetailedMip = 0;
	srvd.Texture2D.MipLevels = 1;
	hr = framework::device.Get()->CreateShaderResourceView(Texture2D, &srvd, &ShaderResourceView);
	if (FAILED(hr))
	{
		return false;
	}

	Texture2D->Release();

	//	サンプラステート作成
	D3D11_SAMPLER_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
	sd.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	sd.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	sd.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	sd.BorderColor[0] = 1.0f;
	sd.BorderColor[1] = 1.0f;
	sd.BorderColor[2] = 1.0f;
	sd.BorderColor[3] = 1.0f;
	sd.MaxAnisotropy = 1.0f;
	sd.MipLODBias = 0.0f;
	sd.ComparisonFunc = D3D11_COMPARISON_EQUAL;
	sd.MinLOD = 0;
	sd.MaxLOD = D3D11_FLOAT32_MAX;

	hr = framework::device.Get()->CreateSamplerState(&sd, &sampler);



	if (FAILED(hr))
	{
		return false;
	}

	return true;
}



