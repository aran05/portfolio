#pragma once



HRESULT load_texture_from_file(const wchar_t *file_name, ID3D11ShaderResourceView **shader_resource_view, D3D11_TEXTURE2D_DESC *texture2d_desc);
HRESULT make_dummy_texture(ID3D11Device *device, ID3D11ShaderResourceView **shader_resource_view);

void combine_resource_path(wchar_t(&combined_resource_path)[256], const wchar_t *referrer_filename, const wchar_t *referent_filename);


class Texture
{
	ID3D11ShaderResourceView* ShaderResourceView;
	
protected:
	
	ID3D11RenderTargetView* RenderTargetView;
	ID3D11SamplerState* sampler;

	// テクスチャ情報
	D3D11_TEXTURE2D_DESC texture2d_desc;

public:
	Texture();
	virtual ~Texture();
	bool Load(const wchar_t* filename);
	void Set(UINT Slot = 0);
	UINT GetWidth() { return texture2d_desc.Width; }
	UINT GetHeight() { return texture2d_desc.Height; }

protected:
	//ID3D11RenderTargetView* RenderTargetView;

public:
	bool Create(u_int whidth, u_int height, DXGI_FORMAT format);
	ID3D11RenderTargetView* GetRenderTarget() { return RenderTargetView; }

};
