#include "Sprite.h"
#include "FrameWork.h"
#include "Texture.h"


sprite::sprite(const wchar_t *file_name)
{
	HRESULT hr = S_OK;

	vertex v[] = {
		{ XMFLOAT3(-0.5f, 0.5f,0), XMFLOAT3(0,0,1), XMFLOAT2(0,0), XMFLOAT4(1,1,1,1) } , //左上
		{ XMFLOAT3(0.5f, 0.5f,0),  XMFLOAT3(0,0,1), XMFLOAT2(1,0), XMFLOAT4(1,1,1,1) } , //右上
		{ XMFLOAT3(-0.5f,-0.5f,0), XMFLOAT3(0,0,1), XMFLOAT2(0,1), XMFLOAT4(1,1,1,1) } , //左下
		{ XMFLOAT3(0.5f,-0.5f,0),  XMFLOAT3(0,0,1), XMFLOAT2(1,1), XMFLOAT4(1,1,1,1) } , //右下
	};

	//頂点バッファ
	D3D11_BUFFER_DESC buffer_desc = {};
	buffer_desc.ByteWidth = sizeof(v);
	buffer_desc.Usage = D3D11_USAGE_DYNAMIC;
	buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	buffer_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	buffer_desc.MiscFlags = 0;
	buffer_desc.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA subresource_data = {};
	subresource_data.pSysMem = v;
	subresource_data.SysMemPitch = 0;
	subresource_data.SysMemSlicePitch = 0;
	hr = framework::device.Get()->CreateBuffer(&buffer_desc, &subresource_data, vertex_buffer.GetAddressOf());
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	//インデクスバッファ
	unsigned int index[6] = { 0,1,2,2,1,3 };
	D3D11_BUFFER_DESC index_buffer_desc = {};
	index_buffer_desc.ByteWidth = sizeof(index);
	index_buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	index_buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	index_buffer_desc.CPUAccessFlags = 0;
	index_buffer_desc.MiscFlags = 0;
	index_buffer_desc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA index_subresource_data = {};
	index_subresource_data.pSysMem = index;
	index_subresource_data.SysMemPitch = 0;
	index_subresource_data.SysMemSlicePitch = 0;
	hr = framework::device.Get()->CreateBuffer(&index_buffer_desc, &index_subresource_data, index_buffer.GetAddressOf());
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));


	_ASSERT_EXPR(!input_layout, L"input_layout'must be uncreated.");
	D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	hr = create_vs_from_cso(framework::device.Get(), "DATA/Shader/Sprite_vs.cso", vertex_shader.GetAddressOf(), input_layout.GetAddressOf(), input_element_desc, ARRAYSIZE(input_element_desc));

	hr = create_ps_from_cso(framework::device.Get(), "DATA/Shader/Sprite_ps.cso", pixel_shader.GetAddressOf());


	hr = load_texture_from_file(file_name, &shader_resource_view, &texture2d_desc);

	D3D11_SAMPLER_DESC sampler_desc;

	sampler_desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	sampler_desc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	sampler_desc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	sampler_desc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	sampler_desc.MipLODBias = 0;
	sampler_desc.MaxAnisotropy = 16;
	sampler_desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	memcpy(sampler_desc.BorderColor, &DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f), sizeof(DirectX::XMFLOAT4));
	sampler_desc.MinLOD = 0;
	sampler_desc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = framework::device.Get()->CreateSamplerState(&sampler_desc, sampler_state.GetAddressOf());
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	D3D11_RASTERIZER_DESC rasterizer_desc = {};
	rasterizer_desc.FillMode = D3D11_FILL_SOLID;
	rasterizer_desc.CullMode = D3D11_CULL_NONE;
	rasterizer_desc.FrontCounterClockwise = FALSE;
	rasterizer_desc.DepthBias = 0;
	rasterizer_desc.DepthBiasClamp = 0;
	rasterizer_desc.SlopeScaledDepthBias = 0;
	rasterizer_desc.DepthClipEnable = FALSE;
	rasterizer_desc.ScissorEnable = FALSE;
	rasterizer_desc.MultisampleEnable = FALSE;
	rasterizer_desc.AntialiasedLineEnable = FALSE;

	hr = framework::device.Get()->CreateRasterizerState(&rasterizer_desc, rasterizer_state.GetAddressOf());
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));


	D3D11_DEPTH_STENCIL_DESC depth_stencil_desc;
	ZeroMemory(&depth_stencil_desc, sizeof(D3D11_DEPTH_STENCIL_DESC));
	depth_stencil_desc.DepthEnable = FALSE;
	depth_stencil_desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	depth_stencil_desc.DepthFunc = D3D11_COMPARISON_ALWAYS;
	depth_stencil_desc.StencilEnable = FALSE;
	depth_stencil_desc.StencilReadMask = 0xFF;
	depth_stencil_desc.StencilWriteMask = 0xFF;
	depth_stencil_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depth_stencil_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depth_stencil_desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depth_stencil_desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	hr = framework::device.Get()->CreateDepthStencilState(&depth_stencil_desc, depth_stencil_state.GetAddressOf());
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

}

sprite::sprite()
{
	vertex v[] = {
		{ XMFLOAT3(-0.5f, 0.5f,0), XMFLOAT3(0,0,1), XMFLOAT2(0,0), XMFLOAT4(1,1,1,1) } , //左上
		{ XMFLOAT3(0.5f, 0.5f,0),  XMFLOAT3(0,0,1), XMFLOAT2(1,0), XMFLOAT4(1,1,1,1) } , //右上
		{ XMFLOAT3(-0.5f,-0.5f,0), XMFLOAT3(0,0,1), XMFLOAT2(0,1), XMFLOAT4(1,1,1,1) } , //左下
		{ XMFLOAT3(0.5f,-0.5f,0),  XMFLOAT3(0,0,1), XMFLOAT2(1,1), XMFLOAT4(1,1,1,1) } , //右下
	};

	//	頂点バッファ作成
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd)); // 全０
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(v);
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA res;
	ZeroMemory(&res, sizeof(res));
	res.pSysMem = v;

	framework::device.Get()->CreateBuffer(&bd, &res, vertex_buffer.GetAddressOf());


	//デプスステンシルステート
	D3D11_DEPTH_STENCIL_DESC depth_stencil_desc;
	ZeroMemory(&depth_stencil_desc, sizeof(depth_stencil_desc));
	depth_stencil_desc.DepthEnable = FALSE;
	framework::device.Get()->CreateDepthStencilState(&depth_stencil_desc, depth_stencil_state.GetAddressOf());
}

void sprite::render(float dx, float dy, float dw, float dh, float sx, float sy, float sw, float sh, float angle, float r, float g, float b, float a)const {
	//４頂点の座標をスクリーン座標で計算する
	float x0, y0, x1, y1, x2, y2, x3, y3;

	x0 = dx;
	y0 = dy;

	x1 = dx + dw;
	y1 = dy;

	x2 = dx;
	y2 = dy + dh;

	x3 = dx + dw;
	y3 = dy + dh;

	//回転の中心を計算して４頂点を回転させる

	float mx, my;

	mx = dx + dw*0.5f;
	my = dy + dh*0.5f;


	x0 -= mx;
	y0 -= my;
	x1 -= mx;
	y1 -= my;
	x2 -= mx;
	y2 -= my;
	x3 -= mx;
	y3 -= my;

	//degree度からradian度への変換
	float rad = angle*PI / 180.0f;
	float c = cosf(rad);
	float s = sinf(rad);

	float wx = c*x0 + (-s)* y0;
	float wy = s*x0 + c* y0;
	x0 = wx;
	y0 = wy;

	wx = c*x1 + (-s)* y1;
	wy = s*x1 + c* y1;
	x1 = wx;
	y1 = wy;

	wx = c*x2 + (-s)* y2;
	wy = s*x2 + c* y2;
	x2 = wx;
	y2 = wy;

	wx = c*x3 + (-s)* y3;
	wy = s*x3 + c* y3;
	x3 = wx;
	y3 = wy;


	x0 += mx;
	y0 += my;
	x1 += mx;
	y1 += my;
	x2 += mx;
	y2 += my;
	x3 += mx;
	y3 += my;


	//４頂点をスクリーン座標から正規化デバイス座標(NDC)に変える
	x0 = x0*2.0f / 1280.0f - 1.0f;
	y0 = y0 *(-2.0f) / 720.0f + 1.0f;

	x1 = x1*2.0f / 1280.0f - 1.0f;
	y1 = y1 *(-2.0f) / 720.0f + 1.0f;

	x2 = x2*2.0f / 1280.0f - 1;
	y2 = y2 *(-2.0f) / 720.0f + 1.0f;

	x3 = x3*2.0f / 1280.0f - 1;
	y3 = y3 *(-2.0f) / 720.0f + 1.0f;



	HRESULT hr = S_OK;
	D3D11_MAP map = D3D11_MAP_WRITE_DISCARD;
	D3D11_MAPPED_SUBRESOURCE mapped_buffer;
	hr = framework::device_context.Get()->Map(vertex_buffer.Get(), 0, map, 0, &mapped_buffer);
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	vertex *verticer = static_cast<vertex *>(mapped_buffer.pData);
	verticer[0].position.x = x0;
	verticer[0].position.y = y0;
	verticer[1].position.x = x1;
	verticer[1].position.y = y1;
	verticer[2].position.x = x2;
	verticer[2].position.y = y2;
	verticer[3].position.x = x3;
	verticer[3].position.y = y3;
	verticer[0].position.z = verticer[1].position.z = verticer[2].position.z, verticer[3].position.z = 0.0f;

	DirectX::XMFLOAT4 color(r, g, b, a);

	verticer[0].color = verticer[1].color = verticer[2].color = verticer[3].color = color;

	verticer[0].texcoord.x = sx / texture2d_desc.Width;
	verticer[0].texcoord.y = sy / texture2d_desc.Height;

	verticer[1].texcoord.x = (sx + sw) / texture2d_desc.Width;
	verticer[1].texcoord.y = sy / texture2d_desc.Height;

	verticer[2].texcoord.x = sx / texture2d_desc.Width;
	verticer[2].texcoord.y = (sy + sh) / texture2d_desc.Height;

	verticer[3].texcoord.x = (sx + sw) / texture2d_desc.Width;
	verticer[3].texcoord.y = (sy + sh) / texture2d_desc.Height;


	framework::device_context.Get()->Unmap(vertex_buffer.Get(), 0);

	UINT stride = sizeof(vertex);
	UINT offset = 0;

	framework::device_context.Get()->IASetVertexBuffers(0, 1, vertex_buffer.GetAddressOf(), &stride, &offset);
	framework::device_context.Get()->IASetIndexBuffer(index_buffer.Get(), DXGI_FORMAT_R32_UINT, 0);
	framework::device_context.Get()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	framework::device_context.Get()->IASetInputLayout(input_layout.Get());
	framework::device_context.Get()->RSSetState(rasterizer_state.Get());
	framework::device_context.Get()->VSSetShader(vertex_shader.Get(), nullptr, 0);
	framework::device_context.Get()->PSSetShader(pixel_shader.Get(), nullptr, 0);

	framework::device_context.Get()->PSSetShaderResources(0, 1, shader_resource_view.GetAddressOf());
	framework::device_context.Get()->PSSetSamplers(0, 1, sampler_state.GetAddressOf());
	framework::device_context.Get()->OMSetDepthStencilState(depth_stencil_state.Get(), 1);
	framework::device_context.Get()->DrawIndexed(6, 0, 0);
}

void sprite::render(Shader* shader, Texture * tex, float dx, float dy, float dw, float dh, float sx, float sy, float sw, float sh, int blender_type, float angle, float r, float g, float b, float a)
{
	shader->Activate();
	//頂点データ設定
	vertex data[4];

	data[0].position.x = dx;
	data[0].position.y = dy;
	data[0].position.z = 0.0f;

	data[1].position.x = dx + dw;
	data[1].position.y = dy;
	data[1].position.z = 0.0f;

	data[2].position.x = dx;
	data[2].position.y = dy + dh;
	data[2].position.z = 0.0f;

	data[3].position.x = dx + dw;
	data[3].position.y = dy + dh;
	data[3].position.z = 0.0f;


	// 正規化デバイス座標系
	for (int i = 0; i < 4; i++) {
		data[i].position.x = 2.0f * data[i].position.x / framework::SCREEN_WIDTH - 1.0f;
		data[i].position.y = 1.0f - 2.0f * data[i].position.y / framework::SCREEN_HEIGHT;
		data[i].position.z = 0.0f;
	}


	//テクスチャ座標設定
	data[0].texcoord.x = sx;
	data[0].texcoord.y = sy;
	data[1].texcoord.x = sx + sw;
	data[1].texcoord.y = sy;
	data[2].texcoord.x = sx;
	data[2].texcoord.y = sy + sh;
	data[3].texcoord.x = sx + sw;
	data[3].texcoord.y = sy + sh;

	//UV座標
	for (int i = 0; i < 4; i++) {
		data[i].texcoord.x = data[i].texcoord.x / tex->GetWidth();
		data[i].texcoord.y = data[i].texcoord.y / tex->GetHeight();
	}
	//頂点カラー
	data[0].color = XMFLOAT4(1, 1, 1, 1);
	data[1].color = XMFLOAT4(1, 1, 1, 1);
	data[2].color = XMFLOAT4(1, 1, 1, 1);
	data[3].color = XMFLOAT4(1, 1, 1, 1);

	//法線
	data[0].normal = XMFLOAT3(0, 0, 1);
	data[1].normal = XMFLOAT3(0, 0, 1);
	data[2].normal = XMFLOAT3(0, 0, 1);
	data[3].normal = XMFLOAT3(0, 0, 1);

	//頂点データ更新
	framework::device_context.Get()->UpdateSubresource(vertex_buffer.Get(), 0, NULL, data, 0, 0);

	//	頂点バッファの指定
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	framework::device_context.Get()->IASetVertexBuffers(
		0, 1, vertex_buffer.GetAddressOf(), // スロット, 数, バッファ
		&stride,		// １頂点のサイズ
		&offset			// 開始位置
	);
	framework::device_context.Get()->IASetPrimitiveTopology(
		D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP
	);

	framework::device_context.Get()->OMSetDepthStencilState(depth_stencil_state.Get(), 1);

	//テクスチャの設定
	if (tex) tex->Set();

	framework::device_context.Get()->Draw(4, 0);
}


