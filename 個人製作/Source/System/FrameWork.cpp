#include "A_System.h"
#include "sceneTitle.h"
#include "GameMain\Game_main.h"

#include "FrameWork.h"

#include "KeyInput.h"

#include "General_Purpose.h"


//*****************************************************************************************************************************
//
//		フレームワーク
//
//*****************************************************************************************************************************

std::unique_ptr<sceneManager> scenemanager;
HWND		framework::Hwnd;

int framework::SCREEN_WIDTH = 1280;
int framework::SCREEN_HEIGHT= 720;

//directx3d	
WRL::ComPtr<ID3D11Device>  framework::device = nullptr;
WRL::ComPtr<ID3D11DeviceContext> framework::device_context = nullptr;
WRL::ComPtr<IDXGISwapChain1> framework::swap_chain = nullptr;
WRL::ComPtr<ID3D11RenderTargetView> framework::render_target_view=nullptr;
WRL::ComPtr<ID3D11DepthStencilView> framework::depth_stencil_view= nullptr;
WRL::ComPtr<ID3D11Buffer> framework::view_pos_constent_buffer = nullptr;
WRL::ComPtr<ID3D11DepthStencilState> framework::depth_stencil_state[2];
WRL::ComPtr<ID3D11RasterizerState>   framework::rasterizer_state[RASTERIZE_TYPE];
WRL::ComPtr<ID3D11BlendState>	     framework::blend_state[BLEND_TYPE];

WRL::ComPtr<ID3D11ShaderResourceView> framework::shader_resource_view;
WRL::ComPtr<ID3D11Texture2D> framework::depth_stencil_texture=nullptr;



//direct2d
WRL::ComPtr<ID2D1Device>           framework::d2dDev_ = nullptr;
WRL::ComPtr<IDXGIDevice1>          framework::dxgiDev_ = nullptr;
WRL::ComPtr<ID2D1DeviceContext>    framework::d2dCont_ = nullptr;
WRL::ComPtr<ID2D1Bitmap1>          framework::d2dTargetBitmap_ = nullptr;
WRL::ComPtr<IDWriteTextLayout>	   framework::pTextLayout = nullptr;
WRL::ComPtr<IDWriteFontCollection> framework::pFontCollection = nullptr;
WRL::ComPtr<ID2D1StrokeStyle>	   framework::m_pStrokeStyle = nullptr;

//blender* framework::blend;

float framework::elapsed_time=0;

bool framework::InitializeRenderTarget()
{
	// バックバッファ取得
	WRL::ComPtr<ID3D11Texture2D> BackBuffer;
	HRESULT hr =swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)BackBuffer.GetAddressOf());
	if (FAILED(hr))
	{
		return false;
	}

	// レンダーターゲットビュー生成
	hr = device->CreateRenderTargetView(BackBuffer.Get(), NULL,render_target_view.GetAddressOf());
	BackBuffer.Get()->Release();

	//  深度ステンシルバッファ生成
	CreateDepthStencil();

	// レンダーターゲットビュー設定
	device_context->OMSetRenderTargets(1,render_target_view.GetAddressOf(),depth_stencil_view.Get());

	// ビューポートの設定
	SetViewPort(SCREEN_WIDTH, SCREEN_HEIGHT);

	return true;
}

bool framework::CreateRasterizerState()
{
	D3D11_RASTERIZER_DESC rd;
	for (int state = 0; state < RASTERIZE_TYPE; state++) {
		switch (state) {
		case RS_SOLID:
			ZeroMemory(&rd, sizeof(rd));
			rd.FillMode = D3D11_FILL_SOLID;
			rd.CullMode = D3D11_CULL_BACK;
			rd.FrontCounterClockwise = FALSE;
			rd.DepthBias = 0;
			rd.DepthBiasClamp = 0;
			rd.SlopeScaledDepthBias = 0;
			rd.DepthClipEnable = TRUE;
			rd.ScissorEnable = FALSE;
			rd.MultisampleEnable = FALSE;
			rd.AntialiasedLineEnable = FALSE;

			break;

		case RS_WIRE:
			ZeroMemory(&rd, sizeof(rd));
			rd.FillMode = D3D11_FILL_WIREFRAME;
			rd.CullMode = D3D11_CULL_BACK;
			rd.FrontCounterClockwise = FALSE;
			rd.DepthBias = 0;
			rd.DepthBiasClamp = 0;
			rd.SlopeScaledDepthBias = 0;
			rd.DepthClipEnable = TRUE;
			rd.ScissorEnable = FALSE;
			rd.MultisampleEnable = FALSE;
			rd.AntialiasedLineEnable = FALSE;
			break;

		case RS_FRONT:
			ZeroMemory(&rd, sizeof(rd));
			rd.FillMode = D3D11_FILL_SOLID;
			rd.CullMode = D3D11_CULL_FRONT;
			rd.FrontCounterClockwise = FALSE;
			rd.DepthBias = 0;
			rd.DepthBiasClamp = 0;
			rd.SlopeScaledDepthBias = 0;
			rd.DepthClipEnable = TRUE;
			rd.ScissorEnable = FALSE;
			rd.MultisampleEnable = FALSE;
			rd.AntialiasedLineEnable = FALSE;
			break;

		case RS_CULL_NONE:
			ZeroMemory(&rd, sizeof(rd));
			rd.FillMode = D3D11_FILL_SOLID;
			rd.CullMode = D3D11_CULL_NONE;
			rd.FrontCounterClockwise = TRUE;
			rd.DepthBias = 0;
			rd.DepthBiasClamp = 0;
			rd.SlopeScaledDepthBias = 0;
			rd.DepthClipEnable = FALSE;
			rd.ScissorEnable = FALSE;
			rd.MultisampleEnable = FALSE;
			rd.AntialiasedLineEnable = FALSE;
			break;
		}
		HRESULT hr = device->CreateRasterizerState(&rd, &rasterizer_state[state]);
		if (FAILED(hr))
		{
			return false;
		}
	}

	return true;

}


bool framework::CreateBlendState()
{
	D3D11_BLEND_DESC bd;

	for (int state = 0; state < BLEND_TYPE; state++) {
		switch (state) {
		case BS_NONE:
			ZeroMemory(&bd, sizeof(bd));
			bd.IndependentBlendEnable = false;
			bd.AlphaToCoverageEnable = false;
			bd.RenderTarget[0].BlendEnable = false;
			bd.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
			bd.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
			bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

			bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
			bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
			bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
			bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

			break;

		case BS_ALPHA:
			ZeroMemory(&bd, sizeof(bd));
			bd.IndependentBlendEnable = false;
			bd.AlphaToCoverageEnable = false;
			bd.RenderTarget[0].BlendEnable = true;
			bd.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
			bd.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
			bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

			bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
			bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
			bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
			bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

			break;

		case BS_ADD:
			ZeroMemory(&bd, sizeof(bd));
			bd.IndependentBlendEnable = false;
			bd.AlphaToCoverageEnable = false;
			bd.RenderTarget[0].BlendEnable = true;
			bd.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
			bd.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
			bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

			bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
			bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
			bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
			bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

			break;

		case BS_SUBTRACT:
			ZeroMemory(&bd, sizeof(bd));
			bd.IndependentBlendEnable = false;
			bd.AlphaToCoverageEnable = false;
			bd.RenderTarget[0].BlendEnable = true;
			bd.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
			bd.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
			bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_REV_SUBTRACT;

			bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
			bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
			bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
			bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

			break;

		case BS_REPLACE:
			ZeroMemory(&bd, sizeof(bd));
			bd.IndependentBlendEnable = false;
			bd.AlphaToCoverageEnable = false;
			bd.RenderTarget[0].BlendEnable = true;
			bd.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
			bd.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
			bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

			bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_SRC_ALPHA;
			bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
			bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
			bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

			break;
		case BS_MULTIPLY:
			ZeroMemory(&bd, sizeof(bd));
			bd.IndependentBlendEnable = false;
			bd.AlphaToCoverageEnable = false;
			bd.RenderTarget[0].BlendEnable = true;
			bd.RenderTarget[0].SrcBlend = D3D11_BLEND_DEST_COLOR;
			bd.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
			bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

			bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_DEST_ALPHA;
			bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
			bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
			bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

			break;
		case BS_LIGHTEN:
			ZeroMemory(&bd, sizeof(bd));
			bd.IndependentBlendEnable = false;
			bd.AlphaToCoverageEnable = false;
			bd.RenderTarget[0].BlendEnable = true;
			bd.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
			bd.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
			bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_MAX;

			bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
			bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
			bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_MAX;
			bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

			break;

		case BS_DARKEN:
			ZeroMemory(&bd, sizeof(bd));
			bd.IndependentBlendEnable = false;
			bd.AlphaToCoverageEnable = false;
			bd.RenderTarget[0].BlendEnable = true;
			bd.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
			bd.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
			bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_MIN;

			bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
			bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
			bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_MIN;
			bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

			break;
		case BS_SCREEN:
			ZeroMemory(&bd, sizeof(bd));
			bd.IndependentBlendEnable = false;
			bd.AlphaToCoverageEnable = false;
			bd.RenderTarget[0].BlendEnable = true;
			bd.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
			bd.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_COLOR;
			bd.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

			bd.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
			bd.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
			bd.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
			bd.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

			break;

		}
		//ブレンドステートの作成
		HRESULT hr = device->CreateBlendState(&bd, &blend_state[state]);
		if (FAILED(hr))
		{
			return false;
		}
	}
	return true;
}


framework::~framework()
{
	ReleaseInput();
	
	//scenemanager->~sceneManager();
	/*if(scenemanager)
	{
		delete scenemanager;
		scenemanager = NULL;
	}*/
	scenemanager.reset(NULL);
	//scenemanager.release();

	d2dDev_.Reset();
	dxgiDev_.Reset();
	device.Reset();

	device_context.Reset();
	d2dCont_.Reset();
	swap_chain.Reset();
	render_target_view.Reset();
	depth_stencil_view.Reset();
	view_pos_constent_buffer.Reset();
	
	d2dTargetBitmap_.Reset();
	pTextLayout.Reset();
	pFontCollection.Reset();
	m_pStrokeStyle.Reset();

}



class d3d_exception : public std::exception
{
public:
	d3d_exception(const char *const msg) : std::exception(msg) {}
};



//------------------------------------------------
//	深度ステンシルバッファ生成
//------------------------------------------------
bool framework::CreateDepthStencil()
{
	// 深度ステンシル設定
	D3D11_TEXTURE2D_DESC td;
	ZeroMemory(&td, sizeof(D3D11_TEXTURE2D_DESC));
	td.Width = SCREEN_WIDTH;
	td.Height = SCREEN_HEIGHT;
	td.MipLevels = 1;
	td.ArraySize = 1;
	td.Format = DXGI_FORMAT_R24G8_TYPELESS;
	td.SampleDesc.Count = 1;
	td.SampleDesc.Quality = 0;
	td.Usage = D3D11_USAGE_DEFAULT;
	td.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	td.CPUAccessFlags = 0;
	td.MiscFlags = 0;

	// 深度ステンシルテクスチャ生成
	HRESULT hr = device->CreateTexture2D(&td, NULL, &depth_stencil_texture);
	if (FAILED(hr))
	{
		return false;
	}

	// 深度ステンシルビュー設定
	D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
	ZeroMemory(&dsvd, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	dsvd.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	dsvd.Texture2D.MipSlice = 0;

	// 深度ステンシルビュー生成
	hr = device->CreateDepthStencilView(depth_stencil_texture.Get(), &dsvd, &depth_stencil_view);
	if (FAILED(hr))
	{
		return false;
	}
	//デプスステンシルステート
	D3D11_DEPTH_STENCIL_DESC depth_stencil_desc;
	ZeroMemory(&depth_stencil_desc, sizeof(depth_stencil_desc));
	depth_stencil_desc.DepthEnable = FALSE;
	hr = device->CreateDepthStencilState(&depth_stencil_desc, &depth_stencil_state[DS_FALSE]);
	if (FAILED(hr))
	{
		return false;
	}
	ZeroMemory(&depth_stencil_desc, sizeof(depth_stencil_desc));
	depth_stencil_desc.DepthEnable = TRUE;
	hr = device->CreateDepthStencilState(&depth_stencil_desc, &depth_stencil_state[DS_TRUE]);
	if (FAILED(hr))
	{
		return false;
	}

	// シェーダリソースビュー設定
	D3D11_SHADER_RESOURCE_VIEW_DESC srvd;
	ZeroMemory(&srvd, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	srvd.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;

	srvd.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvd.Texture2D.MostDetailedMip = 0;
	srvd.Texture2D.MipLevels = 1;

	// シェーダリソースビュー生成
	hr = device->CreateShaderResourceView(depth_stencil_texture.Get(), &srvd,shader_resource_view.GetAddressOf());
	if (FAILED(hr))
	{
		return false;
	}

	return true;
}

bool framework::Initialize()
{
	HRESULT hr = S_OK;
	srand((unsigned int)time(NULL));
	{

		DXGI_SWAP_CHAIN_DESC1 sc;

		// 解像度
		sc.Width = SCREEN_WIDTH;
		sc.Height = SCREEN_HEIGHT;
		// バックバッファのテクスチャフォーマット
		sc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		// スケール方法
		sc.Scaling = DXGI_SCALING_STRETCH;
		// 立体視
		sc.Stereo = 0;
		// 半透明モード
		sc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;

		// 使用方法
		sc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		// バックバッファの数
		sc.BufferCount = 8;
		// 描画後のバッファの扱い
		sc.SwapEffect = DXGI_SWAP_EFFECT_SEQUENTIAL;
		// MSAA
		sc.SampleDesc.Count = 1;
		sc.SampleDesc.Quality = 0;
		// フラグ
		sc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH; // 解像度変更が有効
		Microsoft::WRL::ComPtr<ID3D11Texture2D> back_buffer;
		//D3D11_TEXTURE2D_DESC back_buffer_desc;
		UINT create_dever = 0;
		UINT d3dFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT; // BGRA テクスチャ有効

		D3D_DRIVER_TYPE drivar_types[] =
		{
			D3D_DRIVER_TYPE_UNKNOWN,
			D3D_DRIVER_TYPE_HARDWARE,
			D3D_DRIVER_TYPE_WARP,
			D3D_DRIVER_TYPE_REFERENCE,

		};

		D3D_FEATURE_LEVEL feature_level[] =
		{
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
			D3D_FEATURE_LEVEL_9_3,
			D3D_FEATURE_LEVEL_9_2,
			D3D_FEATURE_LEVEL_9_1,
		};

		D3D_FEATURE_LEVEL fature_level;
		for (auto i : drivar_types)
		{
			hr = D3D11CreateDevice
			(
				nullptr,
				drivar_types[i],
				nullptr,
				D3D11_CREATE_DEVICE_BGRA_SUPPORT,
				feature_level,
				ARRAYSIZE(feature_level),
				D3D11_SDK_VERSION,
				device.GetAddressOf(),
				&fature_level,
				device_context.GetAddressOf());
			if (SUCCEEDED(hr))
				break;
		}

		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
		_ASSERT_EXPR(!(fature_level < D3D_FEATURE_LEVEL_11_0), L"Direct3D Feature Lever11 unsupported.");



		BOOL enable_4x_msaa = TRUE;
		{
			DXGI_SWAP_CHAIN_DESC swap_chain_desc;
			ZeroMemory(&swap_chain_desc, sizeof(DXGI_SWAP_CHAIN_DESC));
			swap_chain_desc.BufferDesc.Width = SCREEN_WIDTH;
			swap_chain_desc.BufferDesc.Height = SCREEN_HEIGHT;
			swap_chain_desc.BufferDesc.RefreshRate.Numerator = 60;
			swap_chain_desc.BufferDesc.RefreshRate.Denominator = 1;
			swap_chain_desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			swap_chain_desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;

			UINT msaa_quality_level;
			device->CheckMultisampleQualityLevels
			(
				swap_chain_desc.BufferDesc.Format,
				4,
				&msaa_quality_level
			);
			swap_chain_desc.SampleDesc.Count = enable_4x_msaa ? 1 : 1;
			swap_chain_desc.SampleDesc.Quality = enable_4x_msaa ? msaa_quality_level - 1 : 0;
			swap_chain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			swap_chain_desc.BufferCount = 1;
			swap_chain_desc.OutputWindow = Hwnd;
			swap_chain_desc.Windowed = TRUE;
			swap_chain_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
			swap_chain_desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
			Microsoft::WRL::ComPtr<IDXGIFactory> factory;
			CreateDXGIFactory(__uuidof(IDXGIFactory), &factory);
			hr = factory->CreateSwapChain
			(
				device.Get(),
				&swap_chain_desc,
				reinterpret_cast<IDXGISwapChain**>(swap_chain.GetAddressOf())
			);
			_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
		}

		// DXGIデバイスの作成
		hr = device.Get()->QueryInterface<IDXGIDevice1>(dxgiDev_.GetAddressOf());
		if (FAILED(hr))
			throw d3d_exception("ID3D11Device#QueryInterface()");

		// キューに格納されていく描画コマンドをスワップ時に全てフラッシュする
		dxgiDev_->SetMaximumFrameLatency(1);



		FLOAT dpiX;
		FLOAT dpiY;

		//Direct2Dのファクトリーの作成
		D2D1_FACTORY_OPTIONS d2dOpt;
		ZeroMemory(&d2dOpt, sizeof d2dOpt);
		ID2D1Factory1 *d2dFactory;
		{
			hr = D2D1CreateFactory(
				D2D1_FACTORY_TYPE_SINGLE_THREADED,
				__uuidof(ID2D1Factory1),
				&d2dOpt,
				reinterpret_cast<void**>(&d2dFactory));
			if (hr == E_NOINTERFACE)
				throw d3d_exception("D2D1CreateFactory() : アップデートが必要です");
			if (FAILED(hr))
				throw d3d_exception("D2D1CreateFactory()");

			//DPIの取得
			d2dFactory->GetDesktopDpi(&dpiX, &dpiY);

			//Direct2Dデバイスの作成
			hr = d2dFactory->CreateDevice(dxgiDev_.Get(), &d2dDev_);
			d2dFactory->Release();
			if (FAILED(hr))
				throw d3d_exception("ID2DFactory#CreateDevice()");

			//Direct2Dデバイスコンテクストの作成
			hr = d2dDev_->CreateDeviceContext(
				D2D1_DEVICE_CONTEXT_OPTIONS_NONE,
				&d2dCont_);
			if (FAILED(hr))
				throw d3d_exception("ID2DFactory#CreateDeviceContext()");

			//DXGIアダプタ（GPU）の取得
			IDXGIAdapter *adapter;
			hr = dxgiDev_->GetAdapter(&adapter);
			if (FAILED(hr))
				throw d3d_exception("IDXGIDevice#GetAdapter()");


			// DXGIのファクトリの作成
			IDXGIFactory2 *factory;
			hr = adapter->GetParent(IID_PPV_ARGS(&factory));
			adapter->Release();
			if (FAILED(hr))
				throw d3d_exception("IDXGIAdapter#GetParent()");

			//スワップチェインをHWNDから作成
			hr = factory->CreateSwapChainForHwnd(device.Get(), Hwnd,
				&sc, nullptr, nullptr, swap_chain.GetAddressOf());
			factory->Release();
			if (FAILED(hr))
				throw d3d_exception("IDXGIFactory2#CreateSwapChainForHwnd()");

			ID3D11Texture2D* pBackBuffer = nullptr;
			hr = swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(back_buffer.GetAddressOf()));
			hr = device->CreateRenderTargetView
			(
				back_buffer.Get(),
				NULL,
				render_target_view.GetAddressOf()
			);
			//back_buffer->GetDesc(&back_buffer_desc);
			//back_buffer->Release();
			_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));


			//レンダーターゲットの取得（DXGI）
			IDXGISurface *surf;
			hr = swap_chain->GetBuffer(0, IID_PPV_ARGS(&surf));
			if (FAILED(hr))
				throw d3d_exception("IDXGISwapChain1#GetBuffer()");


			// Direct2D
			D2D1_BITMAP_PROPERTIES1 d2dProp =
				D2D1::BitmapProperties1(
					D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
					D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_PREMULTIPLIED),
					dpiX,
					dpiY);

			hr = d2dCont_->CreateBitmapFromDxgiSurface(
				surf, &d2dProp, &d2dTargetBitmap_);
			surf->Release();
			if (FAILED(hr))
				throw d3d_exception("ID2D1DeviceContext#CreateBitmapFromDxgiSurface()");

			//描画するDirect2Dビットマップの設定
			d2dCont_->SetTarget(d2dTargetBitmap_.Get());
		}


		D2D1_STROKE_STYLE_PROPERTIES strokeStyleProperties = D2D1::StrokeStyleProperties(
			D2D1_CAP_STYLE_FLAT,  // The start cap.
			D2D1_CAP_STYLE_FLAT,  // The end cap.
			D2D1_CAP_STYLE_ROUND, // The dash cap.
			D2D1_LINE_JOIN_MITER_OR_BEVEL, // The line join.
			10.0f, // The miter limit.
			D2D1_DASH_STYLE_DASH_DOT_DOT, // The dash style.
			0.0f // The dash offset.
		);

		hr = d2dFactory->CreateStrokeStyle(strokeStyleProperties, NULL, 0, &m_pStrokeStyle);

	}
	
	InitializeRenderTarget();
	CreateRasterizerState();
	CreateBlendState();
	
	

	InitInput();
	//	コントローラ設定
	SYSTEM_Initialize();

	//TODO シーンの切り替え
	scenemanager = std::make_unique<sceneManager>();
	scenemanager->changeScene(new sceneTitle());
	return true;
}

void framework::Update()
{

	//	更新処理
	KEY_SetInfo();
	scenemanager->update();	
	if (scene) scene->Update();
	
}

void framework::ClearView(float r, float g, float b, float a)
{
	FLOAT color[4] = { r,g,b,a };
	device_context->ClearRenderTargetView(render_target_view.Get(), color);
	device_context->ClearDepthStencilView(depth_stencil_view.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	device_context->OMSetDepthStencilState(depth_stencil_state[framework::DS_FALSE].Get(), 1);

}

void framework::ClearView(XMFLOAT4 c)
{
	
	FLOAT color[4] = { c.x,c.y,c.z,c.w };//RGBA
	device_context->ClearRenderTargetView(render_target_view.Get(), color);
	device_context->ClearDepthStencilView(depth_stencil_view.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	device_context->OMSetDepthStencilState(depth_stencil_state[framework::DS_FALSE].Get(), 1);

}

void framework::Flip(int n)
{
	swap_chain->Present(n, 0);
}



void framework::Render()
{
	HRESULT hr = S_OK;
	scenemanager->render();
}

void framework::SetViewPort(int width, int height)
{
	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)width;
	vp.Height = (FLOAT)height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	device_context->RSSetViewports(1, &vp);
}
