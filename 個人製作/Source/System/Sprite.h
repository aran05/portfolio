#pragma once


#include <directxmath.h>
#include "Shader.h"
#include <wrl.h>
#include "Misc.h"
#include <string>
#include "Texture.h"
class sprite
{

public:
	Microsoft::WRL::ComPtr<ID3D11VertexShader> vertex_shader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader> pixel_shader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>input_layout;

	Microsoft::WRL::ComPtr<ID3D11Buffer> vertex_buffer;
	Microsoft::WRL::ComPtr<ID3D11Buffer>index_buffer;

	Microsoft::WRL::ComPtr<ID3D11RasterizerState> rasterizer_state;

	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> shader_resource_view;
	D3D11_TEXTURE2D_DESC texture2d_desc;
	Microsoft::WRL::ComPtr<ID3D11SamplerState> sampler_state;

	Microsoft::WRL::ComPtr<ID3D11DepthStencilState> depth_stencil_state;
	Microsoft::WRL::ComPtr<ID3D11BlendState> blend_state;
	





	struct vertex
	{
		XMFLOAT3 position;//位置
		XMFLOAT3 normal;  //法線
		XMFLOAT2 texcoord;//UV座標
		XMFLOAT4 color;	  //頂点色
	};

	sprite(const wchar_t* file_name/*Texture file name*/);
	sprite();
	virtual ~sprite() {}
	//void render(ID3D11DeviceContext *immediate_context) const;
	//void render(ID3D11DeviceContext *immediata_context, float dx, float dy, float dw, float dh, float angle = 0, float r = 1, float g = 1, float b = 1, float a = 1) const;
	void render(float dx, float dy, float dw, float dh, float sx, float sy, float sw, float sh, float angle = 0, float r = 1, float g = 1, float d = 1, float a = 1)const;
	void render(Shader* shader, Texture* tex, float dx, float dy, float dw, float dh, float sx, float sy, float sw, float sh, int blender_type = 1, float angle = 0, float r = 1, float g = 1, float d = 1, float a = 1);

	//void textout(std::string s, float dx, float dy, float dw, float dh, float angle = 0, float r = 1, float g = 1, float b = 1, float a = 1) const;
};

#define PI 3.141592f


