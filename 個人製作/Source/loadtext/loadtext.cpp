#include "A_System.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <codecvt> 
#include <string>
#include <deque>
#include <iomanip>

#include "loadtext.h"
#include "D2Render.h"

int loadtext::count=1;

loadtext::~loadtext()
{
	//SAFE_DELETE(txt);
}

//TODO 二つのLOAD関数を作っているので1つにまとめる
bool loadtext::Load(char * file_name)
{


	//ifstream fp(file_name);
	FILE *fp;

	int i;
	//ファイル読み込み
	fp = fopen(file_name, "r");
	i = 0;


	//csvから値を取得してdataに保存	
	while (fscanf(fp, "%[^,],%f,%f,%f,%f", t, &data[i][0], &data[i][1], &data[i][2], &data[i][3])!=EOF)
	{
		i++;
	} 
	fclose(fp);
	return 0;
}

bool loadtext::Load2(char * file_name)
{
	int i;
	//ファイル読み込み
	fp = fopen(file_name, "r");
	i = 0;

	//csvから値を取得してdataに保存
	while (fscanf(fp, "%[^,],%f,%f,%f,%f", t2, &data2[i][0], &data2[i][1], &data2[i][2], &data2[i][3]) != EOF)
	{
		i++;
	}

	fclose(fp);
	return 0;
}






void loadtext::Initialize()
{
#ifdef _DEBUG
	txt = new D2Render();
	txt->setText(L"MS ゴシック", FONT_SIZE);
#endif // _DEBUG

}

void loadtext::Render()
{
	//デバッグ用レンダー
#ifdef _DEBUG
	ID2D1SolidColorBrush* g_pSolidBrush = NULL;
	D2D1::ColorF color = D2D1::ColorF::White;
	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> cv;
	std::wstring debug;
	std::wstring debug2=L"\n";

	//データに値が入っているか確認用レンダー
	for (int y = 0; y < STAGE_MAX; y++)
	{
		for (int x = 0; x < DATA; x++)
		{
			debug +=to_wstring(data[y][x])+debug2;
			txt->TextRender(g_pSolidBrush, 0, 20, 1024, 512, debug, color);
		}
	}
#endif

}

