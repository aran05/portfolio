#include "Math\math.h"
#include <directxmath.h>
#include "effect.h"
#include "FrameWork.h"
#include "sceneTitle.h"
#include "OBJ_Mesh.h"

#include "Texture.h"
#include "View.h"



//effect::effect(const wchar_t * filename)
//{
//	HRESULT hr = S_OK;
//
//	vertex vertices[4] = {
//		{ XMFLOAT3(-1.0f, +1.0f,+0.0f), DirectX::XMFLOAT4(+1.0f, +1.0f, +1.0f, +1.0f),DirectX::XMFLOAT2(0.0f,0.0f) },
//		{ XMFLOAT3(+1.0f, +1.0f,+0.0f), DirectX::XMFLOAT4(+1.0f, +1.0f, +1.0f, +1.0f),DirectX::XMFLOAT2(1.0f,0.0f) },
//		{ XMFLOAT3(-1.0f, -1.0f,+0.0f), DirectX::XMFLOAT4(+1.0f, +1.0f, +1.0f, +1.0f),DirectX::XMFLOAT2(0.0f,1.0f) },
//		{ XMFLOAT3(+1.0f, -1.0f,+0.0f), DirectX::XMFLOAT4(+1.0f, +1.0f, +1.0f, +1.0f),DirectX::XMFLOAT2(1.0f,1.0f) },
//	};
//	u_int indices[6] = { 0,1,2,1,3,2 };
//
//	//頂点バッファ
//
//	numVerteices = sizeof(vertices) / sizeof(vertices[0]);	
//	D3D11_BUFFER_DESC buffer_desc = {};
//	buffer_desc.ByteWidth = sizeof(vertices);
//	buffer_desc.Usage = D3D11_USAGE_DYNAMIC;
//	buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//	buffer_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//	buffer_desc.MiscFlags = 0;
//	buffer_desc.StructureByteStride = 0;
//	D3D11_SUBRESOURCE_DATA subresource_data = {};
//	subresource_data.pSysMem = vertices;
//	subresource_data.SysMemPitch = 0;
//	subresource_data.SysMemSlicePitch = 0;
//	hr = framework::device.Get()->CreateBuffer(&buffer_desc, &subresource_data, vertex_buffer.GetAddressOf());
//	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
//
//	//インデクスバッファ
//	numIndeices = sizeof(indices) / sizeof(indices[0]);
//
//	D3D11_BUFFER_DESC index_buffer_desc = {};
//	index_buffer_desc.ByteWidth = sizeof(indices);
//	index_buffer_desc.Usage = D3D11_USAGE_DEFAULT;
//	index_buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
//	index_buffer_desc.CPUAccessFlags = 0;
//	index_buffer_desc.MiscFlags = 0;
//	index_buffer_desc.StructureByteStride = 0;
//
//	D3D11_SUBRESOURCE_DATA index_subresource_data = {};
//	index_subresource_data.pSysMem = indices;
//	index_subresource_data.SysMemPitch = 0;
//	index_subresource_data.SysMemSlicePitch = 0;
//	hr = framework::device.Get()->CreateBuffer(&index_buffer_desc, &index_subresource_data, index_buffer.GetAddressOf());
//	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
//
//
//	_ASSERT_EXPR(!input_layout, L"input_layout'must be uncreated.");
//	D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
//	{
//		{ "POSITION",0,DXGI_FORMAT_R32G32B32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0 },
//		{ "COLOR",0,DXGI_FORMAT_R32G32B32A32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0 },
//		{ "TEXCOORD",0,DXGI_FORMAT_R32G32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0 },
//	};
//
//	hr = create_vs_from_cso(framework::device.Get(), "effect_vs.cso", vertex_shader.GetAddressOf(), input_layout.GetAddressOf(), input_element_desc, ARRAYSIZE(input_element_desc));
//
//	hr = create_ps_from_cso(framework::device.Get(), "effect_ps.cso", pixel_shader.GetAddressOf());
//
//
//	{
//		D3D11_RASTERIZER_DESC rasterizer_desc = {};
//		rasterizer_desc.FillMode = D3D11_FILL_SOLID; //D3D11_FILL_WIREFRAME, D3D11_FILL_SOLID
//		rasterizer_desc.CullMode = D3D11_CULL_BACK; //D3D11_CULL_NONE, D3D11_CULL_FRONT, D3D11_CULL_BACK   
//		rasterizer_desc.FrontCounterClockwise = FALSE;
//		rasterizer_desc.DepthBias = 0;
//		rasterizer_desc.DepthBiasClamp = 0;
//		rasterizer_desc.SlopeScaledDepthBias = 0;
//		rasterizer_desc.DepthClipEnable = TRUE;
//		rasterizer_desc.ScissorEnable = FALSE;
//		rasterizer_desc.MultisampleEnable = FALSE;
//		rasterizer_desc.AntialiasedLineEnable = FALSE;
//		hr = framework::device.Get()->CreateRasterizerState(&rasterizer_desc, rasterizer_states[0].GetAddressOf());
//		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
//	}
//	// create rasterizer state : wireframe mode
//	{
//		D3D11_RASTERIZER_DESC rasterizer_desc = {};
//		rasterizer_desc.FillMode = D3D11_FILL_WIREFRAME; //D3D11_FILL_WIREFRAME, D3D11_FILL_SOLID
//		rasterizer_desc.CullMode = D3D11_CULL_BACK; //D3D11_CULL_NONE, D3D11_CULL_FRONT, D3D11_CULL_BACK   
//		rasterizer_desc.FrontCounterClockwise = FALSE;
//		rasterizer_desc.DepthBias = 0;
//		rasterizer_desc.DepthBiasClamp = 0;
//		rasterizer_desc.SlopeScaledDepthBias = 0;
//		rasterizer_desc.DepthClipEnable = TRUE;
//		rasterizer_desc.ScissorEnable = FALSE;
//		rasterizer_desc.MultisampleEnable = FALSE;
//		rasterizer_desc.AntialiasedLineEnable = FALSE;
//		hr = framework::device.Get()->CreateRasterizerState(&rasterizer_desc, rasterizer_states[1].GetAddressOf());
//		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
//	}
//
//
//	hr = load_texture_from_file(framework::device.Get(), filename, &shader_resource_view, &texture2d_desc);
//
//
//	D3D11_SAMPLER_DESC sampler_desc;
//
//	sampler_desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
//	sampler_desc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
//	sampler_desc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
//	sampler_desc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
//	sampler_desc.MipLODBias = 0;
//	sampler_desc.MaxAnisotropy = 16;
//	sampler_desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
//	memcpy(sampler_desc.BorderColor, &DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f), sizeof(DirectX::XMFLOAT4));
//	sampler_desc.MinLOD = 0;
//	sampler_desc.MaxLOD = D3D11_FLOAT32_MAX;
//	hr = framework::device.Get()->CreateSamplerState(&sampler_desc, sampler_state.GetAddressOf());
//	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
//
//	// create depth stencil state
//	{
//		D3D11_DEPTH_STENCIL_DESC depth_stencil_desc;
//		depth_stencil_desc.DepthEnable = TRUE;
//		depth_stencil_desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
//		depth_stencil_desc.DepthFunc = D3D11_COMPARISON_LESS;
//		depth_stencil_desc.StencilEnable = FALSE;
//		depth_stencil_desc.StencilReadMask = 0xFF;
//		depth_stencil_desc.StencilWriteMask = 0xFF;
//		depth_stencil_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//		depth_stencil_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
//		depth_stencil_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//		depth_stencil_desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//		depth_stencil_desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//		depth_stencil_desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
//		depth_stencil_desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//		depth_stencil_desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//		hr = framework::device.Get()->CreateDepthStencilState(&depth_stencil_desc, depth_stencil_state.GetAddressOf());
//		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
//	}
//	// create constant buffer
//	{
//		D3D11_BUFFER_DESC buffer_desc = {};
//		buffer_desc.ByteWidth = sizeof(cbuffer);
//		buffer_desc.Usage = D3D11_USAGE_DEFAULT;
//		buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
//		buffer_desc.CPUAccessFlags = 0;
//		buffer_desc.MiscFlags = 0;
//		buffer_desc.StructureByteStride = 0;
//		hr = framework::device.Get()->CreateBuffer(&buffer_desc, nullptr, constant_buffer.GetAddressOf());
//		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
//	}
//}
//
//void effect::render(int type, const XMFLOAT4X4 & view, XMFLOAT4X4 & projection, const XMFLOAT4 & light, const XMFLOAT4 & matcolor, const bool flg)
//{
//	{
//		cbuffer data;
//		XMMATRIX matWorld = XMMatrixIdentity();
//		DirectX::XMMATRIX work;
//		{
//			//拡大
//			work = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
//			matWorld *= work;
//		}
//		{
//			//回転
//			work = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);
//			matWorld *= work;
//		}
//		{
//			//移動
//			XMMATRIX work = DirectX::XMMatrixTranslation(position.x, position.y, position.z);
//			matWorld *= work;
//		}
//
//		XMFLOAT4X4 v= view;
//
//		v._41 = 0.0f; v._42 = 0.0f; v._43 = 0.0f; v._44 = 1.0f;
//		XMMATRIX inv_viewv = XMLoadFloat4x4(&v);
//		inv_viewv = XMMatrixInverse(nullptr, inv_viewv);
//		matWorld=matWorld*inv_viewv;
//
//		XMMATRIX matV = XMLoadFloat4x4(&view);
//		XMMATRIX matProj = XMLoadFloat4x4(&projection);
//
//		matWorld *= matV*matProj;
//
//		data.matWVP = XMMatrixTranspose(matWorld);
//		data.type = XMFLOAT4(type, 0, 0, 0);
//		data.matColor = matcolor;
//
//		framework::device_context->UpdateSubresource(constant_buffer.Get(), 0, 0, &data, 0, 0);
//		framework::device_context->VSSetConstantBuffers(0, 1, constant_buffer.GetAddressOf());
//	}
//
//	UINT stride = sizeof(vertex);
//	UINT offset = 0;
//
//	framework::device_context.Get()->IASetVertexBuffers(0, 1, vertex_buffer.GetAddressOf(), &stride, &offset);
//	framework::device_context.Get()->IASetIndexBuffer(index_buffer.Get(), DXGI_FORMAT_R32_UINT, 0);
//	framework::device_context.Get()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
//	framework::device_context.Get()->IASetInputLayout(input_layout.Get());
//	
//	if(flg)
//	{
//		framework::device_context.Get()->RSSetState(rasterizer_states[1].Get());
//	}
//	else
//	{
//		framework::device_context.Get()->RSSetState(rasterizer_states[0].Get());
//	}
//	
//	framework::device_context.Get()->VSSetShader(vertex_shader.Get(), nullptr, 0);
//	framework::device_context.Get()->PSSetShader(pixel_shader.Get(), nullptr, 0);
//
//	framework::device_context.Get()->PSSetShaderResources(0, 1, shader_resource_view.GetAddressOf());
//	framework::device_context.Get()->PSSetSamplers(0, 1, sampler_state.GetAddressOf());
//	framework::device_context.Get()->OMSetDepthStencilState(depth_stencil_state.Get(), 1);
//
//	framework::device_context.Get()->DrawIndexed(numIndeices, 0, 0);
//	
//}
//
//effects::effects(const wchar_t* filename, int max)
//{
//	obj = std::make_unique<effect>(filename);
//	eff = std::make_unique<effectData[]>(max);
//	ZeroMemory(eff.get(), sizeof(effectData)*max);
//	this->max;
//}
//
//void effects::Update()
//{
//	float alpha;
//	for (int i = 0; i < max;i++)
//	{
//		if (!eff[i].isAlive)continue;
//
//		eff[i].vec.x += eff[i].power.x;
//		eff[i].vec.y += eff[i].power.y;
//		eff[i].vec.z += eff[i].power.z;
//
//		eff[i].position.x += eff[i].vec.x;
//		eff[i].position.y += eff[i].vec.y;
//		eff[i].position.z += eff[i].vec.z;
//
//		alpha = (float)eff[i].timer / eff[i].maxTimer;
//		eff[i].color.x = alpha;
//		eff[i].color.y = alpha;
//		eff[i].color.z = alpha;
//		eff[i].color.w = alpha;
//
//		eff[i].timer--;
//		if (eff[i].timer <= 0) eff[i].isAlive = false;
//	}
//}
//
//void effects::Render(const XMMATRIX & view, const XMMATRIX & projection)
//{
//	static blender blender(framework::device.Get());
//	framework::device_context->OMSetBlendState(blender.states[blender::BS_ADD].Get(), nullptr, 0xFFFFFFFF);
//	XMFLOAT4X4 fview;
//	XMStoreFloat4x4(&fview, view);
//	XMFLOAT4X4 fprojection;
//	XMStoreFloat4x4(&fprojection, projection);
//
//	for (int i = 0; i < max;i++)
//	{
//		if (!eff[i].isAlive) continue;
//		obj->setPos(eff[i].position);
//		obj->setScale(eff[i].scale);
//
//		obj->render(eff[i].type,fview,fprojection, XMFLOAT4(1, 1, 1, 1),eff[i].color,false);
//	}
//
//}
//
//void effects::set(int type, Vector3 position, Vector3 vec, Vector3 power, Vector3 scale, int timer)
//{
//	for (int i = 0; i < max;i++)
//	{
//		if (eff[i].isAlive) continue;
//		eff[i].isAlive = true;
//		eff[i].type = type;
//		eff[i].position = position;
//		eff[i].vec = vec;
//		eff[i].power = power;
//		eff[i].scale = scale;
//		eff[i].timer = eff[i].maxTimer = timer;
//		break;
//	}
//}
//
//void effects::PlayEffect(Vector3 position)
//{
//	for (int i = 0; i < 10;i++)
//	{
//		Vector3 pos, vec, power;
//		pos.x = position.x + (rand() % 1000 - 500)*0.001;
//		pos.y = position.y + (rand() % 1000 - 500)*0.001;
//		pos.z = position.z + (rand() % 1000 - 500)*0.001;
//
//		vec.x = (rand() % 1000 - 500)*0.0001f;
//		vec.y = (rand() % 1000 - 500)*0.0001f+0.1f;
//		vec.z = (rand() % 1000 - 500)*0.0001f;
//
//		power.x = 0.0f;
//		power.y = -0.001f;
//		power.z = 0.0f;
//		set(rand() % 12, pos, vec, power, Vector3(0.01f, 0.01f, 0.01f), 60);
//	}
//}



//particle::particle(const wchar_t *filename)
//{
//	HRESULT hr = S_OK;
//
//	vertex vertices[4] = {
//		{ XMFLOAT3(-1.0f, +1.0f, +0.0f), XMFLOAT4(+1.0f, +1.0f, +1.0f, +1.0f), XMFLOAT2(0.0f, 0.0f) },
//		{ XMFLOAT3(+1.0f, +1.0f, +0.0f), XMFLOAT4(+1.0f, +1.0f, +1.0f, +1.0f), XMFLOAT2(1.0f, 0.0f) },
//		{ XMFLOAT3(-1.0f, -1.0f, +0.0f), XMFLOAT4(+1.0f, +1.0f, +1.0f, +1.0f), XMFLOAT2(0.0f, 1.0f) },
//		{ XMFLOAT3(+1.0f, -1.0f, +0.0f), XMFLOAT4(+1.0f, +1.0f, +1.0f, +1.0f), XMFLOAT2(1.0f, 1.0f) },
//	};
//
//	u_int indices[6] = { 0, 1, 2, 1, 3, 2 };
//
//	{
//		// 頂点
//		numVerteices = sizeof(vertices) / sizeof(vertices[0]);
//
//		D3D11_BUFFER_DESC buffer_desc = {};
//		buffer_desc.ByteWidth = sizeof(vertices);
//		buffer_desc.Usage = D3D11_USAGE_DYNAMIC;
//		buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//		buffer_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//		buffer_desc.MiscFlags = 0;
//		buffer_desc.StructureByteStride = 0;
//
//		D3D11_SUBRESOURCE_DATA subresource_data = {};
//		subresource_data.pSysMem = vertices;
//		subresource_data.SysMemPitch = 0;
//		subresource_data.SysMemSlicePitch = 0;
//		hr = framework::device->CreateBuffer(&buffer_desc, &subresource_data, vertex_buffer.GetAddressOf());
//		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
//	}
//
//	{
//		// インデックス
//		numIndices = sizeof(indices) / sizeof(indices[0]);
//
//		D3D11_BUFFER_DESC buffer_desc = {};
//		buffer_desc.ByteWidth = sizeof(indices);
//		buffer_desc.Usage = D3D11_USAGE_DEFAULT;
//		buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
//		buffer_desc.CPUAccessFlags = 0;
//		buffer_desc.MiscFlags = 0;
//		buffer_desc.StructureByteStride = 0;
//
//		D3D11_SUBRESOURCE_DATA subresource_data = {};
//		subresource_data.pSysMem = indices;
//		subresource_data.SysMemPitch = 0;
//		subresource_data.SysMemSlicePitch = 0;
//		hr = framework::device->CreateBuffer(&buffer_desc, &subresource_data, index_buffer.GetAddressOf());
//		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
//	}
//
//	_ASSERT_EXPR(!input_layout, L"'input_layout' must be uncreated.");
//	D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
//	{
//		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//	};
//
//	create_vs_from_cso(framework::device.Get(), "DATA/Shader/effect_vs.cso", vertex_shader.GetAddressOf(), input_layout.GetAddressOf(), input_element_desc, ARRAYSIZE(input_element_desc));
//	create_ps_from_cso(framework::device.Get(), "DATA/Shader/effect_ps.cso", pixel_shader.GetAddressOf());
//
//	{
//		// solid mode
//		D3D11_RASTERIZER_DESC rasterizer_desc = {};
//		rasterizer_desc.FillMode = D3D11_FILL_SOLID;
//		rasterizer_desc.CullMode = D3D11_CULL_BACK;
//		rasterizer_desc.FrontCounterClockwise = FALSE;
//		rasterizer_desc.DepthBias = 0;
//		rasterizer_desc.DepthBiasClamp = 0;
//		rasterizer_desc.SlopeScaledDepthBias = 0;
//		rasterizer_desc.DepthClipEnable = TRUE;
//		rasterizer_desc.ScissorEnable = FALSE;
//		rasterizer_desc.MultisampleEnable = FALSE;
//		rasterizer_desc.AntialiasedLineEnable = FALSE;
//		hr = framework::device->CreateRasterizerState(&rasterizer_desc, rasterizer_states[0].GetAddressOf());
//		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
//	}
//
//	{
//		// wireframe mode
//		D3D11_RASTERIZER_DESC rasterizer_desc = {};
//		rasterizer_desc.FillMode = D3D11_FILL_WIREFRAME;
//		rasterizer_desc.CullMode = D3D11_CULL_NONE;
//		rasterizer_desc.FrontCounterClockwise = FALSE;
//		rasterizer_desc.DepthBias = 0;
//		rasterizer_desc.DepthBiasClamp = 0;
//		rasterizer_desc.SlopeScaledDepthBias = 0;
//		rasterizer_desc.DepthClipEnable = TRUE;
//		rasterizer_desc.ScissorEnable = FALSE;
//		rasterizer_desc.MultisampleEnable = FALSE;
//		rasterizer_desc.AntialiasedLineEnable = FALSE;
//		hr = framework::device->CreateRasterizerState(&rasterizer_desc, rasterizer_states[1].GetAddressOf());
//		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
//	}
//
//	hr = load_texture_from_file(filename, &shader_resource_view, &texture2d_desc);
//
//	D3D11_SAMPLER_DESC sampler_desc;
//	ZeroMemory(&sampler_desc, sizeof(D3D11_SAMPLER_DESC));
//	sampler_desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
//	sampler_desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
//	sampler_desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
//	sampler_desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
//	hr = framework::device->CreateSamplerState(&sampler_desc, sampler_state.GetAddressOf());
//
//	D3D11_DEPTH_STENCIL_DESC depth_stencil_desc;
//	depth_stencil_desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
//	depth_stencil_desc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
//	depth_stencil_desc.StencilEnable = FALSE;
//	depth_stencil_desc.StencilReadMask = 0xFF;
//	depth_stencil_desc.StencilWriteMask = 0xFF;
//	depth_stencil_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//	depth_stencil_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
//	depth_stencil_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	depth_stencil_desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//	depth_stencil_desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//	depth_stencil_desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
//	depth_stencil_desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	depth_stencil_desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//	hr = framework::device->CreateDepthStencilState(&depth_stencil_desc, depth_stencil_state.GetAddressOf());
//	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
//
//	{
//		// コンスタント
//		D3D11_BUFFER_DESC buffer_desc = {};
//		buffer_desc.ByteWidth = sizeof(cbuffer);
//		buffer_desc.Usage = D3D11_USAGE_DEFAULT;
//		buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
//		buffer_desc.CPUAccessFlags = 0;
//		buffer_desc.MiscFlags = 0;
//		buffer_desc.StructureByteStride = 0;
//		hr = framework::device->CreateBuffer(&buffer_desc, nullptr, constant_buffer.GetAddressOf());
//		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
//	}
//}
//
//void particle::render(int type, const XMFLOAT4X4& view, const XMFLOAT4X4& projection, const XMFLOAT4& light, const XMFLOAT4& matColor, const bool flg)
//{
//	// コンスタント更新
//	{
//		cbuffer data;
//		XMMATRIX matWorld = XMMatrixIdentity();
//		XMMATRIX work;
//		{
//			// 拡大
//			work = XMMatrixScaling(scale.x, scale.y, scale.z);
//			matWorld *= work;
//		}
//
//		{
//			// 回転
//			work = XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);
//			matWorld *= work;
//		}
//
//		{
//			// 移動
//			work = XMMatrixTranslation(pos.x, pos.y, pos.z);
//			matWorld *= work;
//		}
//
//		XMFLOAT4X4 v = view;
//		v._41 = 0.0f;
//		v._42 = 0.0f;
//		v._43 = 0.0f;
//		v._44 = 1.0f;
//
//		XMMATRIX inv_view = XMLoadFloat4x4(&v);
//		inv_view = XMMatrixInverse(nullptr, inv_view);
//		matWorld = inv_view * matWorld;
//		XMMATRIX matV = XMLoadFloat4x4(&view);
//		XMMATRIX matProj = XMLoadFloat4x4(&projection);
//		matWorld *= matV * matProj;
//
//		data.matWVP = XMMatrixTranspose(matWorld);
//		data.TYPE = XMFLOAT4((float)type, 0, 0, 0);
//		data.matColor = matColor;
//
//		framework::device_context->UpdateSubresource(constant_buffer.Get(), 0, 0, &data, 0, 0);
//		framework::device_context->VSSetConstantBuffers(0, 1, constant_buffer.GetAddressOf());
//	}
//
//	UINT stride = sizeof(vertex);
//	UINT offset = 0;
//
//	framework::device_context->IASetVertexBuffers(0, 1, vertex_buffer.GetAddressOf(), &stride, &offset);
//	framework::device_context->IASetIndexBuffer(index_buffer.Get(), DXGI_FORMAT_R32_UINT, 0);
//	framework::device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
//	framework::device_context->IASetInputLayout(input_layout.Get());
//
//	if (flg)
//	{
//		framework::device_context->RSSetState(rasterizer_states[1].Get());
//	}
//	else
//	{
//		framework::device_context->RSSetState(rasterizer_states[0].Get());
//	}
//
//	framework::device_context->VSSetShader(vertex_shader.Get(), nullptr, 0);
//	framework::device_context->PSSetShader(pixel_shader.Get(), nullptr, 0);
//	framework::device_context->PSSetShaderResources(0, 1, shader_resource_view.GetAddressOf());
//	framework::device_context->PSSetSamplers(0, 1, sampler_state.GetAddressOf());
//	framework::device_context->OMSetDepthStencilState(depth_stencil_state.Get(), 1);
//	framework::device_context->DrawIndexed(numIndices, 0, 0);
//}


Particles::Particles(const char *filename, int max)
{
	obj = std::make_unique<Skin_Mesh>(filename, "DATA/Shader/test01_vs.cso", "DATA/Shader/test01_ps.cso");
	p = std::make_unique<particleData[]>(max);
	ZeroMemory(p.get(), sizeof(particleData)*max);
	this->max = max;
}

void Particles::update()
{
	float alpha;

	for (int i = 0; i < max; i++)
	{
		if (!p[i].isAlive) continue;

		p[i].vec.x += p[i].power.x;
		p[i].vec.y += p[i].power.y;
		p[i].vec.z += p[i].power.z;

		p[i].pos.x += p[i].vec.x;
		p[i].pos.y += p[i].vec.y;
		p[i].pos.z += p[i].vec.z;

		alpha = (float)p[i].timer / p[i].maxTimer;
		p[i].color.x = alpha;
		p[i].color.y = alpha;
		p[i].color.z = alpha;
		p[i].color.w = alpha;

		p[i].timer--;
		if (p[i].timer <= 0) p[i].isAlive = false;
	}
}

void Particles::render(XMMATRIX& view, XMMATRIX& projection)
{
	static blender blender(framework::device.Get());

	// 加算合成
	framework::device_context->OMSetBlendState(blender.states[blender::BS_ALPHA].Get(), nullptr, 0xFFFFFFFF);
	XMFLOAT4X4 view_2;
	XMStoreFloat4x4(&view_2, view);

	XMFLOAT4X4 projection_2;
	XMStoreFloat4x4(&projection_2, projection);

	light_direction = XMFLOAT4(0, 0, 1, 0);
	material_color = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);

	for (int i = 0; i < max; i++)
	{
		if (!p[i].isAlive) continue;
		obj->SetPos(p[i].pos);
		obj->SetScalse(p[i].scale);
		obj->Update();

		obj->Render(view, projection, light_direction, material_color, false);
	}

}

void Particles::set(int type, Vector3 pos, Vector3 vec, Vector3 power, Vector3 scale, int timer)
{
	for (int i = 0; i < max; i++)
	{
		if (p[i].isAlive) continue;
		p[i].isAlive = true;
		p[i].type = type;
		p[i].pos = pos;
		p[i].vec = vec;
		p[i].power = power;
		p[i].scale = scale;
		p[i].timer = p[i].maxTimer = timer;
		break;
	}
}

void Particles::test(Vector3 p)
{
	for (int i = 0; i < max; i++)
	{
		Vector3 pos, vec, power;
		pos.x = p.x + (rand() % 1000 - 500) * 0.001f;
		pos.y = p.y + (rand() % 1000 - 500) * 0.001f;
		pos.z = p.z + (rand() % 1000 - 500) * 0.001f;

		vec.x = (rand() % 1000 - 500) * 0.0001f;
		vec.y = (rand() % 1000 - 500) * 0.0001f + 0.1f;
		vec.z = (rand() % 1000 - 500) * 0.0001f;

		power.x = 0.0f;
		power.y = -0.001f;
		power.z = 0.0f;

		set(rand() % 12, pos, vec, power, Vector3(0.1f, 0.1f, 0.1f), 20);
	}
}

void Particles::coin(Vector3 p)
{
	for (int i = 0; i < max; i++)
	{
		Vector3 pos, vec, power;
		pos.x = p.x + (rand() % 1000 - 500) * 0.001f;
		pos.y = p.y + (rand() % 1000 - 500) * 0.001f;
		pos.z = p.z + (rand() % 1000 - 500) * 0.001f;

		vec.x = (rand() % 1000 - 500) * 0.0001f;
		vec.y = (rand() % 1000 - 500) * 0.0001f + 0.1f;
		vec.z = (rand() % 1000 - 500) * 0.0001f;

		power.x = 0.0f;
		power.y = -0.001f;
		power.z = 0.0f;

		set(rand() % 12, pos, vec, power, Vector3(0.1f, 0.1f, 0.1f), 60);
	}
}
