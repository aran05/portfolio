#include "Score.h"
#include "Constant.h"
#include <fstream>
static char FileName[] = "DATA/result.txt";	// ハイスコアを格納するファイル名

Score::Score() 
{
	for(int i= ZERO_CLEAR;i<NUM_2;i++)
	{
		SAFE_DELETE(number[i]);
	}
	
}

Score::~Score()
{

}


void Score::Initialize()
{
	number[NUM_0] = new D2Render();
	number[NUM_0]->setText(L"PixelMplus10",SIZE_93);

	number[NUM_1] = new D2Render();
	number[NUM_1]->setText(L"PixelMplus10",SIZE_92);
	runkScore = ZERO_CLEAR;
}

void Score::Release()
{

}

//TODO SCORE　レンダーをまとめる必要あり
void Score::RenderScore(int x, int y, int w, int h)
{
	std::wstring one = L"1位:";
	one += std::to_wstring(saveData[NUM_0]);

	if (newnum == saveData[NUM_0])
	{
		number[NUM_0]->TextRender(ZERO_CLEAR, RH, W,H, one,yellow);
		number[NUM_1]->TextRender(ZERO_CLEAR, RH, W,H, one);
		
	}
	else
	{
		number[NUM_0]->TextRender(ZERO_CLEAR, RH, W, H, one, yellow);
		number[NUM_1]->TextRender(ZERO_CLEAR, RH, W, H, one);
	}

}

void Score::RenderScore2(int x, int y, int w, int h)
{


	std::wstring two = L"2位:";
	two += std::to_wstring(saveData[NUM_1]);
	if (newnum == saveData[NUM_1])
	{
		number[NUM_0]->TextRender(ZERO_CLEAR, RH2, W, H, two, yellow);
		number[NUM_1]->TextRender(ZERO_CLEAR, RH2, W, H, two);

	}
	else
	{
		number[NUM_0]->TextRender(ZERO_CLEAR, RH2, W, H, two, yellow);
		number[NUM_1]->TextRender(ZERO_CLEAR, RH2, W, H, two);
	}
}


void Score::RenderScore3(int x, int y, int w, int h)
{

	std::wstring three = L"3位:";
	three += std::to_wstring(saveData[NUM_2]);
	if (newnum == saveData[NUM_2])
	{
		number[NUM_0]->TextRender(ZERO_CLEAR, RH3, W, H, three, yellow);
		number[NUM_1]->TextRender(ZERO_CLEAR, RH3, W, H, three);

	}
	else
	{
		number[NUM_0]->TextRender(ZERO_CLEAR, RH3, W, H, three);
		number[NUM_1]->TextRender(ZERO_CLEAR, RH3, W, H, three);
	}
}





void Score::setScore(int n) {// 加算
	runkScore += n;
}

void Score::setScore2(int n) {// 代入
	runkScore = n;
}


void Score::sort()
{
	int i, j, temp;

	for (i = 0; i < SORT; i++) {
		for (j = SORT; j > i; j--) {
			if (saveData[j - NUM_1] < saveData[j]) {  /* 前の要素の方が大きかったら */
				temp = saveData[j];        /* 交換する */
				saveData2[j] = temp;
				saveData[j] = saveData[j - NUM_1];
				saveData[j - NUM_1] = temp;

			}
		}
	}

}

//-----------------------------------------------------------------------------
// 関数名　：　HiScore_Load()　
// 機能概要：　ハイスコアを読み込む
//-----------------------------------------------------------------------------
int Score::HiScore_Load(void)
{
	FILE *fp;
	//int HiScore;

	if ((fp = fopen(FileName, "r")) == NULL)
		for (int i = 0; i < 3; i++)
		{
			saveData[i] = 0;
		}
	else
	{
		fscanf(fp, "%d", &saveData[0]);

		fscanf(fp, "%d\n", &saveData[1]);

		fscanf(fp, "%d\n\n", &saveData[2]);

		fscanf(fp, "%d\n\n\n", &saveData[3]);

		fscanf(fp, "%d\n\n\n", &saveData[4]);

		sort();

	

		char mes[80];

		if ((fp = fopen(FileName, "w")) == NULL)
		{
			wsprintf(mes, "データファイルのオープンに失敗しました。保存を中止しました。\n");
			OutputDebugString(mes);
			//return;
		}
		for (int i = 0; i < 4; i++)
		{
			fprintf(fp, "%d\n", saveData[i]);
		}
		fclose(fp);
	}

	return saveData[0], saveData[1], saveData[2], saveData[3];

}

//-----------------------------------------------------------------------------
// 関数名　：　HiScore_Save()　
// 機能概要：　ハイスコアを保存する
//-----------------------------------------------------------------------------
void Score::HiScore_Save(int NewScore)
{
	FILE *fp;
	char mes[80];

	if ((fp = fopen(FileName, "a")) == NULL)
	{
		wsprintf(mes, "データファイルのオープンに失敗しました。保存を中止しました。\n");
		OutputDebugString(mes);
		return;
	}

	if (saveData[3]<NewScore)
	{
		fprintf(fp, "%d", NewScore);
	}

	fclose(fp);

}


void Score::release()
{
	for (int i = ZERO_CLEAR; i<NUM_2; i++)
	{
		SAFE_DELETE(number[i]);
	}

}

void Score::swap(int * a, int * b)
{
	{
		int t;

		t = *a;
		*a = *b;
		*b = t;
	}
}

