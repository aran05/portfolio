#pragma once

// UNIT.16
#include <d3d11.h>
#include <wrl.h>
#include <directxmath.h>
using namespace DirectX;
#include <vector>
#include "Math\math.h"
#define MAX_BONE_INFLUENCES 4

#define MAX_BONES 128
class Shader;
class Texture;
class skinned_mesh
{
public:
	Texture* texture;

	enum CullMode
	{
		NONE,
		FRONT,
		BACK,

	};

	int cull_num;
	struct vertex
	{
		DirectX::XMFLOAT3 position;
		DirectX::XMFLOAT3 normal;
		DirectX::XMFLOAT2 texcoord;
		FLOAT bone_weights[MAX_BONE_INFLUENCES] = { 1, 0, 0, 0 };
		INT bone_indices[MAX_BONE_INFLUENCES] = {};
	};
	struct cbuffer
	{
		DirectX::XMFLOAT4X4 world_view_projection;
		DirectX::XMFLOAT4X4 world;
		DirectX::XMFLOAT4 material_color;
		DirectX::XMFLOAT4 light_direction;
		DirectX::XMFLOAT4X4 bone_transforms[MAX_BONES] = { { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 } };

	};

	struct material
	{
		DirectX::XMFLOAT4 color = { 0.8f, 0.8f, 0.8f, 1.0f }; // w channel is used as shininess by only specular.
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> shader_resource_view;
	};
	
	struct subset
	{
		u_int index_start = 0;	// start number of index buffer
		u_int index_count = 0;	// number of vertices (indices)
		material diffuse;
	};
	
	struct bone
	{
		DirectX::XMFLOAT4X4 transform;
	};
	
	typedef std::vector<bone> skeletal;
	struct skeletal_animation : public std::vector<skeletal>
	{
		float sampling_time = 1 / 24.0f;
		float animation_tick = 0.0f;
	};

	
	struct mesh
	{
		Microsoft::WRL::ComPtr<ID3D11Buffer> vertex_buffer;
		Microsoft::WRL::ComPtr<ID3D11Buffer> index_buffer;
		std::vector<subset> subsets;
		DirectX::XMFLOAT4X4 global_transform = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };
		std::vector<skeletal_animation> skeletal_animations;
		void create_buffers(vertex *vertices, int num_vertices, u_int *indices, int num_indices);
	};
	std::vector<mesh> meshes;


private:
	Microsoft::WRL::ComPtr<ID3D11Buffer> constant_buffer;

	Microsoft::WRL::ComPtr<ID3D11VertexShader> vertex_shader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader> pixel_shader;
	Microsoft::WRL::ComPtr<ID3D11GeometryShader> geometry_shader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout> input_layout;

	Microsoft::WRL::ComPtr<ID3D11RasterizerState> rasterizer_states[2];
	Microsoft::WRL::ComPtr<ID3D11DepthStencilState> depth_stencil_state;

	Microsoft::WRL::ComPtr<ID3D11SamplerState> sampler_state;

private:
	struct Face {
		XMFLOAT3 pos[3];
		int material;
	};
	std::vector<Face> faces;

public:

	//FBXの初期設定
	void CreateFBX(const char *fbx_filename);

	//各種ステートをまとめたもの
	void CreateState();
	
	skinned_mesh(const char *fbx_filename, const char * vs_filename, const char * ps_filename, const char * geo_filename);
	skinned_mesh(const char *fbx_filename, const char * vs_filename, const char * ps_filename);
	skinned_mesh(const char *fbx_filename);

	virtual ~skinned_mesh();

	void render(const DirectX::XMFLOAT4X4 &world_view_projection, const DirectX::XMFLOAT4X4 &world, const DirectX::XMFLOAT4 &light_direction, const DirectX::XMFLOAT4 &material_color, const DirectX::XMFLOAT4 Edge_type,float elapsed_time/*UNIT.23*/, size_t animation = 0, int blender_type=1);
	void render(const DirectX::XMFLOAT4X4 &world_view_projection, const DirectX::XMFLOAT4X4 &world, const DirectX::XMFLOAT4 &light_direction, const DirectX::XMFLOAT4 &material_color,float elapsed_time/*UNIT.23*/, size_t animation = 0, int blender_type = 1);


	void render(Shader* shader,Texture* tex,const DirectX::XMFLOAT4X4 &world_view_projection, const DirectX::XMFLOAT4X4 &world, const DirectX::XMFLOAT4 &light_direction, const DirectX::XMFLOAT4 &material_color, float speed=0.5f, size_t animation = 0, int blender_type = 1);
	void render(Shader* shader, const DirectX::XMFLOAT4X4 &world_view_projection, const DirectX::XMFLOAT4X4 &world, const DirectX::XMFLOAT4 &light_direction, const DirectX::XMFLOAT4 &material_color, float speed = 0.5f, size_t animation = 0, int blender_type = 1);



	void SetCullMode(int i)
	{
		cull_num = i;
	};

	int raypick(Vector3* out, Vector3* pos, Vector3* vec, float* dist);

public:
	DirectX::XMFLOAT4X4 coordinate_conversion = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1 };
public:

};
