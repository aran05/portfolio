#include "Scene.h"
#include "FrameWork.h"
#include <process.h>

void sceneManager::update()
{
	scene->Update();
}

void sceneManager::render()
{
	static blender blender(framework::device.Get());

	framework::device_context->OMSetBlendState(
		blender.states[blender::BS_ALPHA].Get(),
		nullptr,
		0xFFFFFFFF);
	scene->Render();
}

void sceneManager::changeScene(Scene* newScene)
{
	scene.reset(newScene);
	scene->Initialize();
}
