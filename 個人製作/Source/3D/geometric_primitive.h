#pragma once

// UNIT.10
#include <d3d11.h>
#include <wrl.h>
#include <directxmath.h>

using namespace DirectX;

class geometric_primitive
{
public:
	struct vertex
	{
		XMFLOAT3 position;
		XMFLOAT3 normal;
	};
	struct cbuffer
	{
		XMFLOAT4X4 world_view_projection;
		XMFLOAT4X4 world_inverse_transpose;
		XMFLOAT4 material_color;
		XMFLOAT4 light_direction;
	};

private:
	Microsoft::WRL::ComPtr<ID3D11Buffer> vertex_buffer;
	Microsoft::WRL::ComPtr<ID3D11Buffer> index_buffer;
	Microsoft::WRL::ComPtr<ID3D11Buffer> constant_buffer;

	Microsoft::WRL::ComPtr<ID3D11VertexShader> vertex_shader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader> pixel_shader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout> input_layout;

	Microsoft::WRL::ComPtr<ID3D11RasterizerState> rasterizer_states[2];
	Microsoft::WRL::ComPtr<ID3D11DepthStencilState> depth_stencil_state;


public:
	geometric_primitive();
	virtual ~geometric_primitive() {}

	void render(const DirectX::XMFLOAT4X4 &world_view_projection, const DirectX::XMFLOAT4X4 &world_inverse_transpose, const DirectX::XMFLOAT4 &light_direction, const DirectX::XMFLOAT4 &material_color, bool wireframe = false);

protected:
	void create_buffers(vertex *vertices, int num_vertices, u_int *indices, int num_indices);
};

class geometric_cube : public geometric_primitive
{
public:
	geometric_cube();
};

class geometric_cylinder : public geometric_primitive
{
public:
	geometric_cylinder(u_int slices);
};

class geometric_sphere : public geometric_primitive
{
public:
	geometric_sphere(u_int slices, u_int stacks);
};

class geometric_capsule : public geometric_primitive
{
public:
	geometric_capsule();
};

