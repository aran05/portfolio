#pragma once
#include <DirectXMath.h>
using namespace DirectX;
#include "Texture.h"
#include "Shader.h"
#include "Math\math.h"
//--------------------------------------------------------------------
//	PlainMeshクラス
//--------------------------------------------------------------------

class PlainMesh
{
private:
	ID3D11Buffer* VertexBuffer;
	int iNumVertices;	// 頂点数　

	ID3D11Buffer* IndexBuffer;
	int iNumIndices;	// 頂点数　

	//テクスチャ利用
	Texture* texture;

	ID3D11Buffer* ConstantBuffer;
	struct ConstantBufferForPerMesh
	{
		DirectX::XMMATRIX world;
		DirectX::XMMATRIX matWVP;
	};
	// 情報
	Vector3 pos = { 0.0f,0.0f,0.0f };
	Vector3 scale = { 1.0f, 1.0f, 1.0f };
	Vector3 angle = { 0.0f, 0.0f ,0.0f };
	XMFLOAT4X4 WorldMatrix;

public:
	PlainMesh(const wchar_t* filename,int row, int col);

	virtual ~PlainMesh();
	void Update();
	void Render(Shader* shader, const XMMATRIX& view, const XMMATRIX& projection, enum D3D_PRIMITIVE_TOPOLOGY = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	void SetPos(Vector3 p) { pos = p; }
	void SetScale(Vector3 s) { scale = s; }
	void SetAngle(Vector3 a) { angle = a; }

};