#include "FrameWork.h"
#include "SphereMesh.h"
#include <vector>


//初期化
SphereMesh::SphereMesh(const wchar_t* filename, int slices, int stacks)
{

	ZeroMemory(this, NULL);
	{
		//頂点数
		Mesh.iNumVertices = (slices + 1)*(stacks + 1);

		// 頂点定義
		VERTEX* vertices = new VERTEX[Mesh.iNumVertices];
		for (int y = 0; y < stacks + 1; y++) {
			for (int x = 0; x < slices + 1; x++) {
				int index = y * (slices + 1) + x;
				float h = 0.5f * cosf(y*XM_PI / stacks);
				float w = 0.5f * sinf(y*XM_PI / stacks);
				float rad_slices = x * XM_PI * 2.0f / slices;

				vertices[index].Pos.x = w*sinf(rad_slices);
				vertices[index].Pos.y = h;
				vertices[index].Pos.z = w*cosf(rad_slices);

				vertices[index].Normal.x = vertices[index].Pos.x * 2.0f;
				vertices[index].Normal.y = vertices[index].Pos.y * 2.0f;
				vertices[index].Normal.z = vertices[index].Pos.z * 2.0f;


				vertices[index].Tex.x = 1.0f - (float)x / slices;
				vertices[index].Tex.y = (float)y / stacks - 1.0f;
				vertices[index].Color = XMFLOAT4(1, 1, 1, 1);
			}
		}
		{
			// 頂点バッファ確保
			D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));
			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.ByteWidth = sizeof(VERTEX)*Mesh.iNumVertices;
			bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			bd.CPUAccessFlags = 0;
			bd.MiscFlags = 0;

			D3D11_SUBRESOURCE_DATA InitData;
			InitData.pSysMem = vertices;//変更
			framework::device.Get()->CreateBuffer(&bd, &InitData, &Mesh.VertexBuffer);
		}


		// インデックス数
		Mesh.iNumIndices = stacks * slices * 2 * 3;
		// インデックス設定
		u_int *indices = new u_int[Mesh.iNumIndices];
		for (int y = 0; y < stacks; y++) {
			for (int x = 0; x < slices; x++) {
				int face = (y*slices + x);
				int vertices_index = y*(slices + 1) + x;
				indices[face * 6] = vertices_index + 1;
				indices[face * 6 + 1] = vertices_index;
				indices[face * 6 + 2] = vertices_index + (slices + 1);

				indices[face * 6 + 3] = vertices_index + 1;
				indices[face * 6 + 4] = vertices_index + (slices + 1);
				indices[face * 6 + 5] = vertices_index + (slices + 1) + 1;

			}
		}
		{
			D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));
			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.ByteWidth = sizeof(u_int)*Mesh.iNumIndices;
			bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
			bd.CPUAccessFlags = 0;
			bd.MiscFlags = 0;

			D3D11_SUBRESOURCE_DATA InitData;
			InitData.pSysMem = indices;
			framework::device.Get()->CreateBuffer(&bd, &InitData, &Mesh.IndexBuffer);

		}
	}


	//テクスチャロード
	texture = new Texture();
	texture->Load(filename);

	// 定数バッファ生成(行列)
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.ByteWidth = sizeof(ConstantBufferForPerMesh);
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;

	HRESULT hr = framework::device.Get()->CreateBuffer(&bd, NULL, constant_buffer.GetAddressOf());
	if (FAILED(hr))
	{
		assert(false && "ID3D11Device::CreateBuffer() Failed.");
		return;
	}

	D3D11_RASTERIZER_DESC rasterizer_desc = {};
	rasterizer_desc.FillMode = D3D11_FILL_SOLID; //D3D11_FILL_WIREFRAME, D3D11_FILL_SOLID

	
	rasterizer_desc.CullMode = D3D11_CULL_BACK;
	

	rasterizer_desc.FrontCounterClockwise = FALSE;
	rasterizer_desc.DepthBias = 0;
	rasterizer_desc.DepthBiasClamp = 0;
	rasterizer_desc.SlopeScaledDepthBias = 0;
	rasterizer_desc.DepthClipEnable = TRUE;
	rasterizer_desc.ScissorEnable = FALSE;
	rasterizer_desc.MultisampleEnable = FALSE;
	rasterizer_desc.AntialiasedLineEnable = FALSE;
	hr = framework::device.Get()->CreateRasterizerState(&rasterizer_desc, rasterizer_states.GetAddressOf());
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	WorldMatrix = XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
}
SphereMesh::~SphereMesh()
{
	constant_buffer.Reset();
	if(texture)
	{
		delete texture;
		texture = NULL;
	}
}

void SphereMesh::Update()
{
	//ワールド変換行列作成
	
	XMMATRIX mW = XMMatrixIdentity();
	//拡大
	XMMATRIX s = XMMatrixScaling(scale.x, scale.y, scale.z);
	//回転
	XMMATRIX a = XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);
	//移動
	XMMATRIX p = XMMatrixTranslation(pos.x, pos.y, pos.z);
	// 行列の合成
	mW = s*a*p;
	XMStoreFloat4x4(&WorldMatrix, mW);

}

void SphereMesh::Render(Shader* shader, const XMMATRIX& view, const XMMATRIX& projection, D3D_PRIMITIVE_TOPOLOGY topology)
{

	shader->Activate();


	ConstantBufferForPerMesh cb;
	cb.world = XMLoadFloat4x4(&WorldMatrix);
	cb.matWVP = cb.world*view*projection;
	
	framework::device_context.Get()->UpdateSubresource(constant_buffer.Get(), 0, NULL, &cb, 0, 0);
	framework::device_context.Get()->VSSetConstantBuffers(0, 1, constant_buffer.GetAddressOf());
	framework::device_context.Get()->GSSetConstantBuffers(0, 1, constant_buffer.GetAddressOf());


	//変更　プリミティブ・トポロジーをセット
	framework::device_context.Get()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	////デプスステンシルステートをセット
	//DxSystem::DeviceContext->OMSetDepthStencilState(DepthStencilState, 1);

	//バーテックスバッファーをセット
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	framework::device_context.Get()->IASetVertexBuffers(0, 1, &Mesh.VertexBuffer, &stride, &offset);
	//インデックスバッファ
	framework::device_context.Get()->IASetIndexBuffer(Mesh.IndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	//テクスチャ設定
	if (texture)
	{
		texture->Set(0);
	}
	framework::device_context.Get()->OMSetBlendState(framework::blend_state[0].Get(), nullptr, 0xFFFFFFF);


	//レンダリング(インデックス付き)
	framework::device_context.Get()->DrawIndexed(Mesh.iNumIndices, 0, 0);

}


