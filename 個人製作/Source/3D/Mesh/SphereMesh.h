#pragma once
//--------------------------------------------------------------------
//	SphereMeshクラス
//--------------------------------------------------------------------

class SphereMesh
{
private:
	struct MESH
	{
		ID3D11Buffer* VertexBuffer;
		int iNumVertices;	// 頂点数　
		ID3D11Buffer* IndexBuffer;
		int iNumIndices;	// インデックス数　
	};

	MESH Mesh;
	//テクスチャ利用
	Texture* texture;

	Microsoft::WRL::ComPtr<ID3D11Buffer> constant_buffer;

	Microsoft::WRL::ComPtr<ID3D11RasterizerState> rasterizer_states;
	Microsoft::WRL::ComPtr<ID3D11VertexShader> vertex_shader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader> pixel_shader;
	Microsoft::WRL::ComPtr<ID3D11GeometryShader> geometry_shader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout> input_layout;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilState> depth_stencil_state;

	Microsoft::WRL::ComPtr<ID3D11SamplerState> sampler_state;
	
	struct ConstantBufferForPerMesh
	{
		DirectX::XMMATRIX world;
		DirectX::XMMATRIX matWVP;
	};

	// 情報
	Vector3 pos = { 0.0f,0.0f,0.0f };
	Vector3 scale = { 1.0f, 1.0f, 1.0f };
	Vector3 angle = { 0.0f, 0.0f, 0.0f };
	XMFLOAT4X4 WorldMatrix;

public:
	SphereMesh(const wchar_t* filename, int slices, int stacks);

	virtual ~SphereMesh();
	void Update();
	void Render(Shader* shader,const XMMATRIX& view, const XMMATRIX& projection, enum D3D_PRIMITIVE_TOPOLOGY = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	void SetPos(Vector3 p) { pos = p; }
	void SetScale(Vector3 s) { scale = s; }
	void SetAngle(Vector3 a) { angle = a; }
};