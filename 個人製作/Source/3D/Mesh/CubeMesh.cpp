#include "FrameWork.h"
#include "CubeMesh.h"


//初期化
CubeMesh::CubeMesh(const wchar_t* filename)
{

	//頂点を定義
	VERTEX vertices[24] =
	{
		XMFLOAT3(0.5f,	0.5f,  -0.5f),	XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(1,0),XMFLOAT4(1,1,1,1),
		XMFLOAT3(-0.5f,  0.5f, -0.5f),	XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(0,0),XMFLOAT4(1,1,1,1),
		XMFLOAT3(0.5f,  -0.5f, -0.5f),	XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(1,1),XMFLOAT4(1,1,1,1),
		XMFLOAT3(-0.5f, -0.5f, -0.5f),	XMFLOAT3(0.0f, 0.0f, -1.0f), XMFLOAT2(0,1),XMFLOAT4(1,1,1,1),

		XMFLOAT3(-0.5f,	0.5f, 0.5f),	XMFLOAT3(0.0f, 0.0f, 1.0f),  XMFLOAT2(1,0),XMFLOAT4(1,1,1,1),
		XMFLOAT3(0.5f,  0.5f, 0.5f),	XMFLOAT3(0.0f, 0.0f, 1.0f),  XMFLOAT2(0,0),XMFLOAT4(1,1,1,1),
		XMFLOAT3(-0.5f,-0.5f, 0.5f),	XMFLOAT3(0.0f, 0.0f, 1.0f),  XMFLOAT2(1,1),XMFLOAT4(1,1,1,1),
		XMFLOAT3(0.5f, -0.5f, 0.5f),	XMFLOAT3(0.0f, 0.0f, 1.0f),  XMFLOAT2(0,1),XMFLOAT4(1,1,1,1),

		XMFLOAT3(0.5f,	0.5f, 0.5f),	XMFLOAT3(1.0f, 0.0f, 0.0f),  XMFLOAT2(1,0),XMFLOAT4(1,1,1,1),
		XMFLOAT3(0.5f,  0.5f, -0.5f),	XMFLOAT3(1.0f, 0.0f, 0.0f),  XMFLOAT2(0,0),XMFLOAT4(1,1,1,1),
		XMFLOAT3(0.5f, -0.5f, 0.5f),	XMFLOAT3(1.0f, 0.0f, 0.0f),  XMFLOAT2(1,1),XMFLOAT4(1,1,1,1),
		XMFLOAT3(0.5f, -0.5f, -0.5f),	XMFLOAT3(1.0f, 0.0f, 0.0f),  XMFLOAT2(0,1),XMFLOAT4(1,1,1,1),

		XMFLOAT3(-0.5f,	0.5f, -0.5f),	XMFLOAT3(-1.0f, 0.0f, 0.0f), XMFLOAT2(1,0),XMFLOAT4(1,1,1,1),
		XMFLOAT3(-0.5f,  0.5f, 0.5f),	XMFLOAT3(-1.0f, 0.0f, 0.0f), XMFLOAT2(0,0),XMFLOAT4(1,1,1,1),
		XMFLOAT3(-0.5f, -0.5f, -0.5f),	XMFLOAT3(-1.0f, 0.0f, 0.0f), XMFLOAT2(1,1),XMFLOAT4(1,1,1,1),
		XMFLOAT3(-0.5f, -0.5f, 0.5f),	XMFLOAT3(-1.0f, 0.0f, 0.0f), XMFLOAT2(0,1),XMFLOAT4(1,1,1,1),

		XMFLOAT3(0.5f,	 0.5f,  0.5f),	XMFLOAT3(0.0f, 1.0f, 0.0f),  XMFLOAT2(1,0),XMFLOAT4(1,1,1,1),
		XMFLOAT3(-0.5f,  0.5f,  0.5f),	XMFLOAT3(0.0f, 1.0f, 0.0f),  XMFLOAT2(0,0),XMFLOAT4(1,1,1,1),
		XMFLOAT3(0.5f,  0.5f, -0.5f),	XMFLOAT3(0.0f, 1.0f, 0.0f),  XMFLOAT2(1,1),XMFLOAT4(1,1,1,1),
		XMFLOAT3(-0.5f,  0.5f, -0.5f),	XMFLOAT3(0.0f, 1.0f, 0.0f),  XMFLOAT2(0,1),XMFLOAT4(1,1,1,1),

		XMFLOAT3(0.5f,	 -0.5f, -0.5f),	XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT2(1,0),XMFLOAT4(1,1,1,1),
		XMFLOAT3(-0.5f,  -0.5f, -0.5f),	XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT2(0,0),XMFLOAT4(1,1,1,1),
		XMFLOAT3(0.5f,  -0.5f, 0.5f),	XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT2(1,1),XMFLOAT4(1,1,1,1),
		XMFLOAT3(-0.5f,  -0.5f, 0.5f),	XMFLOAT3(0.0f, -1.0f, 0.0f), XMFLOAT2(0,1),XMFLOAT4(1,1,1,1),

	};

	//上の頂点でバーテックスバッファ作成
	Mesh.iNumVertices = sizeof(vertices) / sizeof(vertices[0]);
	{
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(VERTEX)*Mesh.iNumVertices;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA InitData;
		InitData.pSysMem = vertices;
		framework::device.Get()->CreateBuffer(&bd, &InitData,
			&Mesh.VertexBuffer);
	}


	//インデックスを定義
	unsigned int indices[36];
	for (int face = 0; face < 6; face++) {
		indices[face * 6] = face * 4;
		indices[face * 6 + 1] = face * 4 + 2;
		indices[face * 6 + 2] = face * 4 + 1;
		indices[face * 6 + 3] = face * 4 + 1;
		indices[face * 6 + 4] = face * 4 + 2;
		indices[face * 6 + 5] = face * 4 + 3;
	}


	//上の頂点でインデックスバッファ作成
	Mesh.iNumIndices = sizeof(indices) / sizeof(indices[0]);
	{
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(VERTEX)*Mesh.iNumIndices;
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA InitData;
		InitData.pSysMem = indices;
		framework::device.Get()->CreateBuffer(&bd, &InitData,
			&Mesh.IndexBuffer);
	}


	//テクスチャロード
	if (filename) {
		texture = new Texture();
		texture->Load(filename);
	}


	// 定数バッファ生成(行列)
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.ByteWidth = sizeof(ConstantBufferForPerMesh);
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;

	HRESULT hr = framework::device.Get()->CreateBuffer(&bd, NULL, &ConstantBuffer);
	if (FAILED(hr))
	{
		assert(false && "ID3D11Device::CreateBuffer() Failed.");
		return;
	}

	matWorld = XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);	
}

CubeMesh::~CubeMesh()
{
	if(texture)
	{
		delete texture;
		texture = NULL;
	}

	SAFE_RELEASE(ConstantBuffer);
}







void CubeMesh::Update()
{
	//ワールド変換行列作成
	//XMMATRIX mW = XMMatrixIdentity();
	matWorld = XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
	//拡大
	XMMATRIX s = XMMatrixScaling(scales.x, scales.y, scales.z);
	//回転
	XMMATRIX a = XMMatrixRotationRollPitchYaw(angles.x, angles.y, angles.z);
	//移動
	XMMATRIX p = XMMatrixTranslation(position.x, position.y, position.z);
	// 行列の合成
	XMMATRIX mW = s*a*p;

	XMStoreFloat4x4(&matWorld, mW);

}

void CubeMesh::Render(Shader* shader, const XMMATRIX& view, const XMMATRIX& projection)
{

	shader->Activate();

	ConstantBufferForPerMesh cb;
	cb.world = XMLoadFloat4x4(&matWorld);
	cb.matWVP = cb.world*view*projection;
	framework::device_context.Get()->UpdateSubresource(ConstantBuffer, 0, NULL, &cb, 0, 0);
	framework::device_context.Get()->VSSetConstantBuffers(0, 1, &ConstantBuffer);
	framework::device_context.Get()->GSSetConstantBuffers(0, 1, &ConstantBuffer);


	//変更　プリミティブ・トポロジーをセット
	framework::device_context.Get()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//バーテックスバッファーをセット
	UINT stride = sizeof(VERTEX);
	UINT offset = 0;
	framework::device_context.Get()->IASetVertexBuffers(0, 1, &Mesh.VertexBuffer, &stride, &offset);
	//インデックスバッファ
	framework::device_context.Get()->IASetIndexBuffer(Mesh.IndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	//テクスチャ設定
	if (texture) texture->Set(0);
	//レンダリング(インデックス付き)
	framework::device_context.Get()->DrawIndexed(Mesh.iNumIndices, 0, 0);

}




