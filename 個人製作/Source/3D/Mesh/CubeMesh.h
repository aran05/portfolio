#pragma once
#include <DirectXMath.h>
using namespace DirectX;
#include "Texture.h"
#include "Shader.h"
//--------------------------------------------------------------------
//	CubeMeshクラス
//--------------------------------------------------------------------
class CubeMesh
{
private:
protected:

	struct MESH
	{
		ID3D11Buffer* VertexBuffer;
		int iNumVertices;	// 頂点数　
		ID3D11Buffer* IndexBuffer;
		int iNumIndices;	// インデックス数　
	};

	MESH Mesh;
	//テクスチャ利用
	Texture* texture;

	ID3D11Buffer* ConstantBuffer;
	struct ConstantBufferForPerMesh
	{
		DirectX::XMMATRIX world;
		DirectX::XMMATRIX matWVP;
	};


	// 情報
	
	Vector3 position= Vector3(0,0,0);
	Vector3 scales = Vector3(1, 1,1);
	Vector3 angles = Vector3(0, 0, 0);
	XMFLOAT4X4 matWorld;
	Microsoft::WRL::ComPtr<ID3D11RasterizerState> rasterizer_states;


public:
	CubeMesh(const wchar_t* filename);

	~CubeMesh();


	void Update();
	void Render(Shader* shader, const XMMATRIX& view, const XMMATRIX& projection);
	void SetPos(Vector3 pos) { position = pos; }
	void SetScale(Vector3 scale) { scales = scale; }
	void SetAngle(Vector3 angle) { angles = angle; }
	
	Vector3 GetPos() { return position; }
	Vector3 GetScale() { return scales; }
	Vector3 GetAngle() { return angles; }


};

