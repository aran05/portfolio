// UNIT.16
#include "shader.h"
#include "texture.h"
#include "misc.h"
#include "skinned_mesh.h"

#include <fbxsdk.h>
#include "FrameWork.h"
using namespace fbxsdk;

#include <vector>
#include <functional>


void fbxamatrix_to_xmfloat4x4(const FbxAMatrix &fbxamatrix, DirectX::XMFLOAT4X4 &xmfloat4x4)
{
	for (int row = 0; row < 4; row++)
	{
		for (int column = 0; column < 4; column++)
		{
			xmfloat4x4.m[row][column] = static_cast<float>(fbxamatrix[row][column]);
		}
	}
}

struct bone_influence
{
	int index;	// index of bone
	float weight; // weight of bone
};
typedef std::vector<bone_influence> bone_influences_per_control_point;

void fetch_bone_influences(const FbxMesh *fbx_mesh, std::vector<bone_influences_per_control_point> &influences)
{
	const int number_of_control_points = fbx_mesh->GetControlPointsCount();
	influences.resize(number_of_control_points);


	const int number_of_deformers = fbx_mesh->GetDeformerCount(FbxDeformer::eSkin);
	for (int index_of_deformer = 0; index_of_deformer < number_of_deformers; ++index_of_deformer)
	{
		FbxSkin *skin = static_cast<FbxSkin *>(fbx_mesh->GetDeformer(index_of_deformer, FbxDeformer::eSkin));

		
		const int number_of_clusters = skin->GetClusterCount();
		for (int index_of_cluster = 0; index_of_cluster < number_of_clusters; ++index_of_cluster)
		{
			FbxCluster* cluster = skin->GetCluster(index_of_cluster);

			const int	number_of_control_point_indices = cluster->GetControlPointIndicesCount();
			const int *array_of_control_point_indices = cluster->GetControlPointIndices();
			const double *array_of_control_point_weights = cluster->GetControlPointWeights();

			for (int i = 0; i < number_of_control_point_indices; ++i)
			{
				bone_influences_per_control_point &influences_per_control_point = influences.at(array_of_control_point_indices[i]);
				bone_influence influence;
				influence.index = index_of_cluster;
				influence.weight = static_cast<float>(array_of_control_point_weights[i]);
				influences_per_control_point.push_back(influence);
			}
		}
	}
}

void fetch_bone_matrices(FbxMesh *fbx_mesh, std::vector<skinned_mesh::bone> &skeletal, FbxTime time)
{
	//スキンの数を取得
	int number_of_deformers = fbx_mesh->GetDeformerCount(FbxDeformer::eSkin);
	for (int index = 0; index < number_of_deformers; ++index)
	{
		//index番目のスキンを取得
		FbxSkin *skin = static_cast<FbxSkin *>(fbx_mesh->GetDeformer(index, FbxDeformer::eSkin));


		FbxSkin::EType skinning_type = skin->GetSkinningType();
		_ASSERT_EXPR(skinning_type == FbxSkin::eLinear || skinning_type == FbxSkin::eRigid, L"only support FbxSkin::eLinear or FbxSkin::eRigid");

		FbxCluster::ELinkMode link_mode = skin->GetCluster(0)->GetLinkMode();
		_ASSERT_EXPR(link_mode != FbxCluster::eAdditive, L"only support FbxCluster::eNormalize or FbxCluster::eTotalOne");

		const int number_of_clusters = skin->GetClusterCount();
		skeletal.resize(number_of_clusters);
		
		for (int index_of_cluster = 0; index_of_cluster < number_of_clusters; ++index_of_cluster)
		{

			skinned_mesh::bone &bone = skeletal.at(index_of_cluster);

			FbxCluster *cluster = skin->GetCluster(index_of_cluster);

			FbxAMatrix reference_global_init_position;
			cluster->GetTransformMatrix(reference_global_init_position);

			
			FbxAMatrix cluster_global_init_position;
			cluster->GetTransformLinkMatrix(cluster_global_init_position);

			FbxAMatrix cluster_global_current_position;
			cluster_global_current_position = cluster->GetLink()->EvaluateGlobalTransform(time);

			FbxAMatrix reference_global_current_position;
			reference_global_current_position = fbx_mesh->GetNode()->EvaluateGlobalTransform(time);

			FbxAMatrix transform = reference_global_current_position.Inverse() * cluster_global_current_position * cluster_global_init_position.Inverse() * reference_global_init_position;

			fbxamatrix_to_xmfloat4x4(transform, bone.transform);
		}
	}
}


void fetch_animations(FbxMesh *fbx_mesh, std::vector<skinned_mesh::skeletal_animation> &skeletal_animations, u_int sampling_rate = 0)
{

	// Get the list of all the animation stack.
	FbxArray<FbxString *> array_of_animation_stack_names;
	fbx_mesh->GetScene()->FillAnimStackNameArray(array_of_animation_stack_names);

	
	int number_of_animations = array_of_animation_stack_names.Size();
	
	
	skeletal_animations.resize(number_of_animations);
	
	for (int index_of_animation = 0; index_of_animation < number_of_animations; index_of_animation++)
	{
		
		
		skinned_mesh::skeletal_animation &skeletal_animation = skeletal_animations.at(index_of_animation);

	
		FbxTime::EMode time_mode = fbx_mesh->GetScene()->GetGlobalSettings().GetTimeMode();
		FbxTime frame_time;


		frame_time.SetTime(0, 0, 0, 1, 0, time_mode);


		sampling_rate = sampling_rate > 0 ? sampling_rate : static_cast<u_int>(frame_time.GetFrameRate(time_mode));
		float sampling_time = 1.0f / sampling_rate;
		skeletal_animation.sampling_time = sampling_time;
		skeletal_animation.animation_tick = 0.0f;


		FbxString *animation_stack_name = array_of_animation_stack_names.GetAt(index_of_animation); // UNIT.24
		FbxAnimStack * current_animation_stack = fbx_mesh->GetScene()->FindMember<FbxAnimStack>(animation_stack_name->Buffer());
		fbx_mesh->GetScene()->SetCurrentAnimationStack(current_animation_stack);



		FbxTakeInfo *take_info = fbx_mesh->GetScene()->GetTakeInfo(animation_stack_name->Buffer());
		_ASSERT_EXPR(take_info, L"must need FbxTakeInfo");
		FbxTime start_time = take_info->mLocalTimeSpan.GetStart();
		FbxTime end_time = take_info->mLocalTimeSpan.GetStop();


		FbxTime smapling_step;
		smapling_step.SetTime(0, 0, 1, 0, 0, time_mode);
		smapling_step = static_cast<FbxLongLong>(smapling_step.Get() * sampling_time);
		for (FbxTime current_time = start_time; current_time < end_time; current_time += smapling_step)
		{
			skinned_mesh::skeletal skeletal;
			fetch_bone_matrices(fbx_mesh, skeletal, current_time);
			skeletal_animation.push_back(skeletal);
		}
	}
	for (int i = 0; i < number_of_animations; i++)
	{
		delete array_of_animation_stack_names[i];
	}
}


void skinned_mesh::CreateFBX(const char * fbx_filename)
{
	FbxManager* manager = FbxManager::Create();


	manager->SetIOSettings(FbxIOSettings::Create(manager, IOSROOT));


	FbxImporter* importer = FbxImporter::Create(manager, "");


	bool import_status = false;
	import_status = importer->Initialize(fbx_filename, -1, manager->GetIOSettings());
	_ASSERT_EXPR_A(import_status, importer->GetStatus().GetErrorString());


	FbxScene* scene = FbxScene::Create(manager, "");


	import_status = importer->Import(scene);
	_ASSERT_EXPR_A(import_status, importer->GetStatus().GetErrorString());

	fbxsdk::FbxGeometryConverter geometry_converter(manager);
	geometry_converter.Triangulate(scene, /*replace*/true);

	std::vector<FbxNode*> fetched_meshes;
	std::function<void(FbxNode*)> traverse = [&](FbxNode* node)
	{
		if (node)
		{
			FbxNodeAttribute *fbx_node_attribute = node->GetNodeAttribute();
			if (fbx_node_attribute)
			{
				switch (fbx_node_attribute->GetAttributeType())
				{
				case FbxNodeAttribute::eMesh:
					fetched_meshes.push_back(node);
					break;
				}
			}
			for (int i = 0; i < node->GetChildCount(); i++)
			{
				traverse(node->GetChild(i));
			}
		}
	};
	traverse(scene->GetRootNode());

	meshes.resize(fetched_meshes.size());

	for (size_t i = 0; i < fetched_meshes.size(); i++)
	{

		FbxMesh *fbx_mesh = fetched_meshes.at(i)->GetMesh();
		mesh &mesh = meshes.at(i);


		FbxAMatrix global_transform = fbx_mesh->GetNode()->EvaluateGlobalTransform(0);
		fbxamatrix_to_xmfloat4x4(global_transform, mesh.global_transform);


		const int number_of_materials = fbx_mesh->GetNode()->GetMaterialCount();

		mesh.subsets.resize(number_of_materials > 0 ? number_of_materials : 1);
		for (int index_of_material = 0; index_of_material < number_of_materials; ++index_of_material)
		{

			subset &subset = mesh.subsets.at(index_of_material);

			const FbxSurfaceMaterial *surface_material = fbx_mesh->GetNode()->GetMaterial(index_of_material);

			const FbxProperty property = surface_material->FindProperty(FbxSurfaceMaterial::sDiffuse);
			const FbxProperty factor = surface_material->FindProperty(FbxSurfaceMaterial::sDiffuseFactor);
			if (property.IsValid() && factor.IsValid())
			{
				FbxDouble3 color = property.Get<FbxDouble3>();
				double f = factor.Get<FbxDouble>();
				subset.diffuse.color.x = static_cast<float>(color[0] * f);
				subset.diffuse.color.y = static_cast<float>(color[1] * f);
				subset.diffuse.color.z = static_cast<float>(color[2] * f);
				subset.diffuse.color.w = 1.0f;
			}
			if (property.IsValid())
			{
				const int number_of_textures = property.GetSrcObjectCount<FbxFileTexture>();
				if (number_of_textures)
				{
					const FbxFileTexture* file_texture = property.GetSrcObject<FbxFileTexture>();
					if (file_texture)
					{
						const char *filename = file_texture->GetRelativeFileName();

						wchar_t fbx_unicode[256];
						MultiByteToWideChar(CP_ACP, 0, fbx_filename, strlen(fbx_filename) + 1, fbx_unicode, 1024);
						wchar_t texture_unicode[256];
						MultiByteToWideChar(CP_ACP, 0, file_texture->GetFileName(), strlen(file_texture->GetFileName()) + 1, texture_unicode, 1024);
						combine_resource_path(texture_unicode, fbx_unicode, texture_unicode);


						D3D11_TEXTURE2D_DESC texture2d_desc;
						load_texture_from_file(texture_unicode, subset.diffuse.shader_resource_view.GetAddressOf(), &texture2d_desc);
					}
				}
			}
		}

		for (subset &subset : mesh.subsets)
		{
			if (!subset.diffuse.shader_resource_view)
			{
				make_dummy_texture(framework::device.Get(), subset.diffuse.shader_resource_view.GetAddressOf());
			}
		}


		if (number_of_materials > 0)
		{

			const int number_of_polygons = fbx_mesh->GetPolygonCount();
			for (int index_of_polygon = 0; index_of_polygon < number_of_polygons; ++index_of_polygon)
			{
				const u_int material_index = fbx_mesh->GetElementMaterial()->GetIndexArray().GetAt(index_of_polygon);

				mesh.subsets.at(material_index).index_count += 3;

			}

			int offset = 0;

			for (subset &subset : mesh.subsets)
			{
				subset.index_start = offset;
				offset += subset.index_count;
				subset.index_count = 0;
			}
		}


		std::vector<vertex> vertices;
		std::vector<u_int> indices;
		u_int vertex_count = 0;
		Face f;

		FbxStringList uv_names;
		fbx_mesh->GetUVSetNames(uv_names);

		const FbxVector4 *array_of_control_points = fbx_mesh->GetControlPoints();
		const int number_of_polygons = fbx_mesh->GetPolygonCount();


		indices.resize(number_of_polygons * 3);


		std::vector<bone_influences_per_control_point> bone_influences;
		fetch_bone_influences(fbx_mesh, bone_influences);


		bool all_by_control_point = true;
		bool has_normal = fbx_mesh->GetElementNormalCount() > 0;
		bool has_uv = fbx_mesh->GetElementUVCount() > 0;
		if (has_normal && fbx_mesh->GetElementNormal(0)->GetMappingMode() != FbxGeometryElement::EMappingMode::eByControlPoint)
		{
			all_by_control_point = false;
		}
		if (has_uv && fbx_mesh->GetElementUV(0)->GetMappingMode() != FbxGeometryElement::EMappingMode::eByControlPoint)
		{
			all_by_control_point = false;
		}
		if (all_by_control_point)
		{
			const FbxGeometryElementNormal *geometry_element_normal = fbx_mesh->GetElementNormal(0);
			const FbxGeometryElementUV *geometry_element_uv = fbx_mesh->GetElementUV(0);
			const int number_of_control_points = fbx_mesh->GetControlPointsCount();
			for (int index_of_control_point = 0; index_of_control_point < number_of_control_points; ++index_of_control_point)
			{
				vertex vertex;
				vertex.position.x = static_cast<float>(array_of_control_points[index_of_control_point][0]);
				vertex.position.y = static_cast<float>(array_of_control_points[index_of_control_point][1]);
				vertex.position.z = static_cast<float>(array_of_control_points[index_of_control_point][2]);

				bone_influences_per_control_point influences_per_control_point = bone_influences.at(index_of_control_point);
				for (size_t index_of_influence = 0; index_of_influence < influences_per_control_point.size(); index_of_influence++)
				{
					if (index_of_influence < MAX_BONE_INFLUENCES)
					{
						vertex.bone_weights[index_of_influence] = influences_per_control_point.at(index_of_influence).weight;
						vertex.bone_indices[index_of_influence] = influences_per_control_point.at(index_of_influence).index;
					}
					else
					{
						for (int i = 0; i < MAX_BONE_INFLUENCES; i++)
						{
							if (vertex.bone_weights[i] < influences_per_control_point.at(index_of_influence).weight)
							{
								//vertex.bone_weights[i] = influences_per_control_point.at(index_of_influence).weight;
								vertex.bone_weights[i] += influences_per_control_point.at(index_of_influence).weight;
								vertex.bone_indices[i] = influences_per_control_point.at(index_of_influence).index;
								break;
							}
						}
					}
				}

				if (has_normal)
				{
					int i = index_of_control_point;
					if (geometry_element_normal->GetReferenceMode() == FbxLayerElement::eIndexToDirect)
					{
						i = geometry_element_normal->GetIndexArray().GetAt(index_of_control_point);
					}
					vertex.normal.x = static_cast<float>(geometry_element_normal->GetDirectArray().GetAt(i)[0]);
					vertex.normal.y = static_cast<float>(geometry_element_normal->GetDirectArray().GetAt(i)[1]);
					vertex.normal.z = static_cast<float>(geometry_element_normal->GetDirectArray().GetAt(i)[2]);
				}

				if (has_uv)
				{
					int i = index_of_control_point;
					if (geometry_element_uv->GetReferenceMode() == FbxLayerElement::eIndexToDirect)
					{
						i = geometry_element_uv->GetIndexArray().GetAt(index_of_control_point);
					}
					vertex.texcoord.x = static_cast<float>(geometry_element_uv->GetDirectArray().GetAt(i)[0]);
					vertex.texcoord.y = 1.0f - static_cast<float>(geometry_element_uv->GetDirectArray().GetAt(i)[1]);
				}
				vertices.push_back(vertex);
			}
		}


		for (int index_of_polygon = 0; index_of_polygon < number_of_polygons; index_of_polygon++)
		{

			int index_of_material = 0;
			if (number_of_materials > 0)
			{
				index_of_material = fbx_mesh->GetElementMaterial()->GetIndexArray().GetAt(index_of_polygon);
			}


			subset &subset = mesh.subsets.at(index_of_material);
			const int index_offset = subset.index_start + subset.index_count;

			f.material = index_of_material;
			for (int index_of_vertex = 0; index_of_vertex < 3; index_of_vertex++)
			{
				const int index_of_control_point = fbx_mesh->GetPolygonVertex(index_of_polygon, index_of_vertex);


				if (all_by_control_point)
				{
					indices.at(index_offset + index_of_vertex) = static_cast<unsigned int>(index_of_control_point);
				}
				else
				{
					vertex vertex;
					vertex.position.x = static_cast<float>(array_of_control_points[index_of_control_point][0]);
					vertex.position.y = static_cast<float>(array_of_control_points[index_of_control_point][1]);
					vertex.position.z = static_cast<float>(array_of_control_points[index_of_control_point][2]);


					f.pos[index_of_vertex].x = vertex.position.x;
					f.pos[index_of_vertex].y = vertex.position.y;
					f.pos[index_of_vertex].z = vertex.position.z;


					bone_influences_per_control_point influences_per_control_point = bone_influences.at(index_of_control_point);
					for (size_t index_of_influence = 0; index_of_influence < influences_per_control_point.size(); index_of_influence++)
					{
						if (index_of_influence < MAX_BONE_INFLUENCES)
						{
							vertex.bone_weights[index_of_influence] = influences_per_control_point.at(index_of_influence).weight;
							vertex.bone_indices[index_of_influence] = influences_per_control_point.at(index_of_influence).index;
						}
					
						else
						{
							for (int i = 0; i < MAX_BONE_INFLUENCES; i++)
							{
								if (vertex.bone_weights[i] < influences_per_control_point.at(index_of_influence).weight)
								{
									//vertex.bone_weights[i] = influences_per_control_point.at(index_of_influence).weight;
									vertex.bone_weights[i] += influences_per_control_point.at(index_of_influence).weight;
									vertex.bone_indices[i] = influences_per_control_point.at(index_of_influence).index;
									break;
								}
							}
						}
					}

				
					if (fbx_mesh->GetElementNormalCount() > 0)
					{
						FbxVector4 normal;
						fbx_mesh->GetPolygonVertexNormal(index_of_polygon, index_of_vertex, normal);
						vertex.normal.x = static_cast<float>(normal[0]);
						vertex.normal.y = static_cast<float>(normal[1]);
						vertex.normal.z = static_cast<float>(normal[2]);
					}

					
					if (fbx_mesh->GetElementUVCount() > 0)
					{
						FbxVector2 uv;
						bool unmapped_uv;
						fbx_mesh->GetPolygonVertexUV(index_of_polygon, index_of_vertex, uv_names[0], uv, unmapped_uv);
						vertex.texcoord.x = static_cast<float>(uv[0]);
						vertex.texcoord.y = 1.0f - static_cast<float>(uv[1]);
					}

					vertices.push_back(vertex);

		
					indices.at(index_offset + index_of_vertex) = static_cast<u_int>(vertex_count);
				}
				vertex_count += 1;
			}
			subset.index_count += 3;
			// 面データ保存
			faces.push_back(f);
		}

		fetch_animations(fbx_mesh, mesh.skeletal_animations);

		mesh.create_buffers(vertices.data(), vertices.size(), indices.data(), indices.size());
	}
	manager->Destroy();
}

void skinned_mesh::CreateState()
{
	
	HRESULT hr = S_OK;
	//コンスタントバッファの作成
	D3D11_BUFFER_DESC buffer_desc = {};
	buffer_desc.ByteWidth = sizeof(cbuffer);
	buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	buffer_desc.CPUAccessFlags = 0;
	buffer_desc.MiscFlags = 0;
	buffer_desc.StructureByteStride = 0;
	hr = framework::device.Get()->CreateBuffer(&buffer_desc, nullptr, constant_buffer.GetAddressOf());
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));

	//サンプラーステートの作成
	D3D11_SAMPLER_DESC sampler_desc = {};
	sampler_desc.Filter = D3D11_FILTER_ANISOTROPIC;
	sampler_desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampler_desc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampler_desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampler_desc.MipLODBias = 0;
	sampler_desc.MaxAnisotropy = 16;
	sampler_desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	memcpy(sampler_desc.BorderColor, &DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f), sizeof(DirectX::XMFLOAT4));
	sampler_desc.MinLOD = 0;
	sampler_desc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = framework::device->CreateSamplerState(&sampler_desc, sampler_state.GetAddressOf());
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	
}

skinned_mesh::skinned_mesh(const char *fbx_filename, const char * vs_filename, const char * ps_filename, const char * geo_filename)
{
	
	CreateFBX(fbx_filename);
	

	CreateState();

	HRESULT hr = S_OK;

	D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		// UNIT.17
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		// UNIT.20
		{ "WEIGHTS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONES", 0, DXGI_FORMAT_R32G32B32A32_SINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	create_vs_from_cso(framework::device.Get(), vs_filename, vertex_shader.GetAddressOf(), input_layout.GetAddressOf(), input_element_desc, ARRAYSIZE(input_element_desc));
	create_ps_from_cso(framework::device.Get(), ps_filename, pixel_shader.GetAddressOf());
	create_geo_from_cso(framework::device.Get(), geo_filename, geometry_shader.GetAddressOf());

}

skinned_mesh::skinned_mesh(const char * fbx_filename, const char * vs_filename, const char * ps_filename)
{

	
	CreateFBX(fbx_filename);
	CreateState();

	D3D11_INPUT_ELEMENT_DESC input_element_desc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		// UNIT.17
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		// UNIT.20
		{ "WEIGHTS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONES", 0, DXGI_FORMAT_R32G32B32A32_SINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	create_vs_from_cso(framework::device.Get(), vs_filename, vertex_shader.GetAddressOf(), input_layout.GetAddressOf(), input_element_desc, ARRAYSIZE(input_element_desc));
	create_ps_from_cso(framework::device.Get(), ps_filename, pixel_shader.GetAddressOf());

}

skinned_mesh::skinned_mesh(const char * fbx_filename)
{
	CreateFBX(fbx_filename);

	CreateState();
}

skinned_mesh::~skinned_mesh()
{
	
}

void skinned_mesh::mesh::create_buffers(vertex *vertices, int num_vertices, u_int *indices, int num_indices)
{
	HRESULT hr;
	{
		D3D11_BUFFER_DESC buffer_desc = {};
		D3D11_SUBRESOURCE_DATA subresource_data = {};

		buffer_desc.ByteWidth = sizeof(vertex)*num_vertices;
		//buffer_desc.Usage = D3D11_USAGE_DEFAULT;
		buffer_desc.Usage = D3D11_USAGE_IMMUTABLE;
		buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		buffer_desc.CPUAccessFlags = 0;
		buffer_desc.MiscFlags = 0;
		buffer_desc.StructureByteStride = 0;
		subresource_data.pSysMem = vertices;
		subresource_data.SysMemPitch = 0; //Not use for vertex buffers.mm 
		subresource_data.SysMemSlicePitch = 0; //Not use for vertex buffers.

		hr = framework::device->CreateBuffer(&buffer_desc, &subresource_data, vertex_buffer.ReleaseAndGetAddressOf());
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	}
	{
		D3D11_BUFFER_DESC buffer_desc = {};
		D3D11_SUBRESOURCE_DATA subresource_data = {};

		buffer_desc.ByteWidth = sizeof(u_int)*num_indices;
		//buffer_desc.Usage = D3D11_USAGE_DEFAULT;
		buffer_desc.Usage = D3D11_USAGE_IMMUTABLE;
		buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		buffer_desc.CPUAccessFlags = 0;
		buffer_desc.MiscFlags = 0;
		buffer_desc.StructureByteStride = 0;
		subresource_data.pSysMem = indices;
		subresource_data.SysMemPitch = 0; //Not use for index buffers.
		subresource_data.SysMemSlicePitch = 0; //Not use for index buffers.
		hr = framework::device->CreateBuffer(&buffer_desc, &subresource_data, index_buffer.ReleaseAndGetAddressOf());
		_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	}
}

void skinned_mesh::render(const DirectX::XMFLOAT4X4 &world_view_projection, const  DirectX::XMFLOAT4X4 &world, const DirectX::XMFLOAT4 &light_direction, const DirectX::XMFLOAT4 &material_color, const DirectX::XMFLOAT4 Edge_type, float elapsed_time/*UNIT.23*/, size_t animation/*UNIT.24*/, int blender_type)
{

	for (mesh &mesh : meshes)
	{
		u_int stride = sizeof(vertex);
		u_int offset = 0;

		framework::device_context->IASetVertexBuffers(0, 1, mesh.vertex_buffer.GetAddressOf(), &stride, &offset);
		framework::device_context->IASetIndexBuffer(mesh.index_buffer.Get(), DXGI_FORMAT_R32_UINT, 0);
		framework::device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		framework::device_context->IASetInputLayout(input_layout.Get());

		framework::device_context->VSSetShader(vertex_shader.Get(), nullptr, 0);
		framework::device_context->PSSetShader(pixel_shader.Get(), nullptr, 0);
		framework::device_context->GSSetShader(geometry_shader.Get(), nullptr, 0);

		framework::device_context->OMSetDepthStencilState(depth_stencil_state.Get(), 1);
		//framework::device_context->RSSetState(rasterizer_states[0].Get());

		cbuffer data;
		// UNIT.19
		DirectX::XMStoreFloat4x4(&data.world_view_projection, DirectX::XMLoadFloat4x4(&mesh.global_transform) * DirectX::XMLoadFloat4x4(&coordinate_conversion) * DirectX::XMLoadFloat4x4(&world_view_projection));
		DirectX::XMStoreFloat4x4(&data.world, DirectX::XMLoadFloat4x4(&mesh.global_transform) * DirectX::XMLoadFloat4x4(&coordinate_conversion) * DirectX::XMLoadFloat4x4(&world));

		
		
		data.light_direction = light_direction;
		//data.EyePos = { View::camera->GetPos().x,View::camera->GetPos().y,View::camera->GetPos().z,0 };
		//data.Edge = Edge_type;
		

		//アニメーション処理
		size_t number_of_animations = mesh.skeletal_animations.size();
		if (mesh.skeletal_animations.size() > 0)
		{
			// UNIT.24
			_ASSERT_EXPR(number_of_animations > animation, L"'animation' is invalid number.");
			skeletal_animation &skeletal_animation = mesh.skeletal_animations.at(animation);


			// UNIT.24
			size_t frame = static_cast<size_t>(skeletal_animation.animation_tick / skeletal_animation.sampling_time);
			if (frame > skeletal_animation.size() - 1)
			{
				frame = 0;
				skeletal_animation.animation_tick = 0;
			}

			std::vector<bone> &skeletal = skeletal_animation.at(frame);
			size_t number_of_bones = skeletal.size();
			_ASSERT_EXPR(number_of_bones < MAX_BONES, L"'the number_of_bones' exceeds MAX_BONES.");
			for (size_t i = 0; i < number_of_bones; i++)
			{
				DirectX::XMStoreFloat4x4(&data.bone_transforms[i], DirectX::XMLoadFloat4x4(&skeletal.at(i).transform));
			}
			skeletal_animation.animation_tick += elapsed_time;
		}

		for (subset &subset : mesh.subsets)
		{
			data.material_color.x = subset.diffuse.color.x * material_color.x;
			data.material_color.y = subset.diffuse.color.y * material_color.y;
			data.material_color.z = subset.diffuse.color.z * material_color.z;
			data.material_color.w = material_color.w;
		


			framework::device_context->UpdateSubresource(constant_buffer.Get(), 0, 0, &data, 0, 0);
			framework::device_context->VSSetConstantBuffers(0, 1, constant_buffer.GetAddressOf());

			framework::device_context->PSSetShaderResources(0, 1, subset.diffuse.shader_resource_view.GetAddressOf());
			framework::device_context->PSSetSamplers(0, 1, sampler_state.GetAddressOf());
			framework::device_context->DrawIndexed(subset.index_count, subset.index_start, 0);
			
		}
	}
	
	

}

void skinned_mesh::render(const DirectX::XMFLOAT4X4 & world_view_projection, const DirectX::XMFLOAT4X4 & world, const DirectX::XMFLOAT4 & light_direction, const DirectX::XMFLOAT4 & material_color, float elapsed_time, size_t animation, int blender_type)
{

	for (mesh &mesh : meshes)
	{
		u_int stride = sizeof(vertex);
		u_int offset = 0;

		framework::device_context->IASetVertexBuffers(0, 1, mesh.vertex_buffer.GetAddressOf(), &stride, &offset);
		framework::device_context->IASetIndexBuffer(mesh.index_buffer.Get(), DXGI_FORMAT_R32_UINT, 0);
		framework::device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		framework::device_context->IASetInputLayout(input_layout.Get());

		framework::device_context->VSSetShader(vertex_shader.Get(), nullptr, 0);
		framework::device_context->PSSetShader(pixel_shader.Get(), nullptr, 0);
		framework::device_context->GSSetShader(geometry_shader.Get(), nullptr, 0);

		framework::device_context->OMSetDepthStencilState(depth_stencil_state.Get(), 1);
		cbuffer data;
		// UNIT.19
		DirectX::XMStoreFloat4x4(&data.world_view_projection, DirectX::XMLoadFloat4x4(&mesh.global_transform) * DirectX::XMLoadFloat4x4(&coordinate_conversion) * DirectX::XMLoadFloat4x4(&world_view_projection));
		DirectX::XMStoreFloat4x4(&data.world, DirectX::XMLoadFloat4x4(&mesh.global_transform) * DirectX::XMLoadFloat4x4(&coordinate_conversion) * DirectX::XMLoadFloat4x4(&world));



		data.light_direction = light_direction;


		//アニメーション処理
		size_t number_of_animations = mesh.skeletal_animations.size();
		if (mesh.skeletal_animations.size() > 0)
		{
			// UNIT.24
			_ASSERT_EXPR(number_of_animations > animation, L"'animation' is invalid number.");
			skeletal_animation &skeletal_animation = mesh.skeletal_animations.at(animation);


			// UNIT.24
			size_t frame = static_cast<size_t>(skeletal_animation.animation_tick / skeletal_animation.sampling_time);
			if (frame > skeletal_animation.size() - 1)
			{
				frame = 0;
				skeletal_animation.animation_tick = 0;
			}

			std::vector<bone> &skeletal = skeletal_animation.at(frame);
			size_t number_of_bones = skeletal.size();
			_ASSERT_EXPR(number_of_bones < MAX_BONES, L"'the number_of_bones' exceeds MAX_BONES.");
			for (size_t i = 0; i < number_of_bones; i++)
			{
				DirectX::XMStoreFloat4x4(&data.bone_transforms[i], DirectX::XMLoadFloat4x4(&skeletal.at(i).transform));
			}
			skeletal_animation.animation_tick += elapsed_time;
		}

		for (subset &subset : mesh.subsets)
		{
			data.material_color.x = subset.diffuse.color.x * material_color.x;
			data.material_color.y = subset.diffuse.color.y * material_color.y;
			data.material_color.z = subset.diffuse.color.z * material_color.z;
			data.material_color.w = material_color.w;
			framework::device_context->UpdateSubresource(constant_buffer.Get(), 0, 0, &data, 0, 0);
			framework::device_context->VSSetConstantBuffers(0, 1, constant_buffer.GetAddressOf());

			framework::device_context->PSSetShaderResources(0, 1, subset.diffuse.shader_resource_view.GetAddressOf());
			framework::device_context->PSSetSamplers(0, 1, sampler_state.GetAddressOf());
			//framework::device_context.Get()->OMSetBlendState(framework::blend->states[blender_type].Get(), nullptr, 0xFFFFFFF);
			framework::device_context->DrawIndexed(subset.index_count, subset.index_start, 0);

		}
	}
}

void skinned_mesh::render(Shader * shader, Texture * tex, const DirectX::XMFLOAT4X4 & world_view_projection, const DirectX::XMFLOAT4X4 & world, const DirectX::XMFLOAT4 & light_direction, const DirectX::XMFLOAT4 & material_color, float speed, size_t animation, int blender_type)
{
	shader->Activate();

	for (mesh &mesh : meshes)
	{
		u_int stride = sizeof(vertex);
		u_int offset = 0;

		framework::device_context->IASetVertexBuffers(0, 1, mesh.vertex_buffer.GetAddressOf(), &stride, &offset);
		framework::device_context->IASetIndexBuffer(mesh.index_buffer.Get(), DXGI_FORMAT_R32_UINT, 0);
		framework::device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		framework::device_context->IASetInputLayout(shader->Get_InputLayout());

		framework::device_context->VSSetShader(shader->Get_VS(), nullptr, 0);
		framework::device_context->PSSetShader(shader->Get_PS(), nullptr, 0);
		framework::device_context->GSSetShader(shader->Get_GS(), nullptr, 0);

		framework::device_context->OMSetDepthStencilState(depth_stencil_state.Get(), 1);
		framework::device_context->RSSetState(framework::GetRasterizerState(blender_type).Get());

		cbuffer data;
		// UNIT.19
		DirectX::XMStoreFloat4x4(&data.world_view_projection, DirectX::XMLoadFloat4x4(&mesh.global_transform) * DirectX::XMLoadFloat4x4(&coordinate_conversion) * DirectX::XMLoadFloat4x4(&world_view_projection));
		DirectX::XMStoreFloat4x4(&data.world, DirectX::XMLoadFloat4x4(&mesh.global_transform) * DirectX::XMLoadFloat4x4(&coordinate_conversion) * DirectX::XMLoadFloat4x4(&world));



		data.light_direction = light_direction;


		//アニメーション処理
		size_t number_of_animations = mesh.skeletal_animations.size();
		if (mesh.skeletal_animations.size() > 0)
		{
			// UNIT.24
			_ASSERT_EXPR(number_of_animations > animation, L"'animation' is invalid number.");
			skeletal_animation &skeletal_animation = mesh.skeletal_animations.at(animation);


			// UNIT.24
			size_t frame = static_cast<size_t>(skeletal_animation.animation_tick / skeletal_animation.sampling_time);
			if (frame > skeletal_animation.size() - 1)
			{
				frame = 0;
				skeletal_animation.animation_tick = 0;
			}

			std::vector<bone> &skeletal = skeletal_animation.at(frame);
			size_t number_of_bones = skeletal.size();
			_ASSERT_EXPR(number_of_bones < MAX_BONES, L"'the number_of_bones' exceeds MAX_BONES.");
			for (size_t i = 0; i < number_of_bones; i++)
			{
				DirectX::XMStoreFloat4x4(&data.bone_transforms[i], DirectX::XMLoadFloat4x4(&skeletal.at(i).transform));
			}
			skeletal_animation.animation_tick += (framework::elapsed_time*speed);
		}

		for (subset &subset : mesh.subsets)
		{
			data.material_color.x = subset.diffuse.color.x * material_color.x;
			data.material_color.y = subset.diffuse.color.y * material_color.y;
			data.material_color.z = subset.diffuse.color.z * material_color.z;
			data.material_color.w = material_color.w;

			framework::device_context->UpdateSubresource(constant_buffer.Get(), 0, 0, &data, 0, 0);
			framework::device_context->VSSetConstantBuffers(0, 1, constant_buffer.GetAddressOf());

			framework::device_context->PSSetShaderResources(0, 1, subset.diffuse.shader_resource_view.GetAddressOf());
			framework::device_context->PSSetSamplers(0, 1, sampler_state.GetAddressOf());
			framework::device_context->DrawIndexed(subset.index_count, subset.index_start, 0);

		}
		if (tex) tex->Set();
	}
}

void skinned_mesh::render(Shader * shader,const DirectX::XMFLOAT4X4 & world_view_projection, const DirectX::XMFLOAT4X4 & world, const DirectX::XMFLOAT4 & light_direction, const DirectX::XMFLOAT4 & material_color, float speed, size_t animation, int blender_type)
{
	shader->Activate();

	for (mesh &mesh : meshes)
	{
		u_int stride = sizeof(vertex);
		u_int offset = 0;

		framework::device_context->IASetVertexBuffers(0, 1, mesh.vertex_buffer.GetAddressOf(), &stride, &offset);
		framework::device_context->IASetIndexBuffer(mesh.index_buffer.Get(), DXGI_FORMAT_R32_UINT, 0);
		framework::device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		framework::device_context->IASetInputLayout(shader->Get_InputLayout());

		framework::device_context->VSSetShader(shader->Get_VS(), nullptr, 0);
		framework::device_context->PSSetShader(shader->Get_PS(), nullptr, 0);
		framework::device_context->GSSetShader(shader->Get_GS(), nullptr, 0);

		framework::device_context->OMSetDepthStencilState(depth_stencil_state.Get(), 1);
		framework::device_context->RSSetState(framework::GetRasterizerState(blender_type).Get());

		cbuffer data;
		// UNIT.19
		DirectX::XMStoreFloat4x4(&data.world_view_projection, DirectX::XMLoadFloat4x4(&mesh.global_transform) * DirectX::XMLoadFloat4x4(&coordinate_conversion) * DirectX::XMLoadFloat4x4(&world_view_projection));
		DirectX::XMStoreFloat4x4(&data.world, DirectX::XMLoadFloat4x4(&mesh.global_transform) * DirectX::XMLoadFloat4x4(&coordinate_conversion) * DirectX::XMLoadFloat4x4(&world));



		data.light_direction = light_direction;


		//アニメーション処理
		size_t number_of_animations = mesh.skeletal_animations.size();
		if (mesh.skeletal_animations.size() > 0)
		{
			// UNIT.24
			_ASSERT_EXPR(number_of_animations > animation, L"'animation' is invalid number.");
			skeletal_animation &skeletal_animation = mesh.skeletal_animations.at(animation);


			// UNIT.24
			size_t frame = static_cast<size_t>(skeletal_animation.animation_tick / skeletal_animation.sampling_time);
			if (frame > skeletal_animation.size() - 1)
			{
				frame = 0;
				skeletal_animation.animation_tick = 0;
			}

			std::vector<bone> &skeletal = skeletal_animation.at(frame);
			size_t number_of_bones = skeletal.size();
			_ASSERT_EXPR(number_of_bones < MAX_BONES, L"'the number_of_bones' exceeds MAX_BONES.");
			for (size_t i = 0; i < number_of_bones; i++)
			{
				DirectX::XMStoreFloat4x4(&data.bone_transforms[i], DirectX::XMLoadFloat4x4(&skeletal.at(i).transform));
			}
			skeletal_animation.animation_tick += (framework::elapsed_time*speed);
		}

		for (subset &subset : mesh.subsets)
		{
			data.material_color.x = subset.diffuse.color.x * material_color.x;
			data.material_color.y = subset.diffuse.color.y * material_color.y;
			data.material_color.z = subset.diffuse.color.z * material_color.z;
			data.material_color.w = material_color.w;

			framework::device_context->UpdateSubresource(constant_buffer.Get(), 0, 0, &data, 0, 0);
			framework::device_context->VSSetConstantBuffers(0, 1, constant_buffer.GetAddressOf());

			framework::device_context->PSSetShaderResources(0, 1, subset.diffuse.shader_resource_view.GetAddressOf());
			framework::device_context->PSSetSamplers(0, 1, sampler_state.GetAddressOf());
			framework::device_context->DrawIndexed(subset.index_count, subset.index_start, 0);

		}
		//if (tex) tex->Set();
	}
}

int skinned_mesh::raypick(Vector3* out, Vector3* pos, Vector3* vec, float* dist)
{

	int ret = -1;

	XMVECTOR p = XMLoadFloat3((XMFLOAT3*)pos);
	XMVECTOR vv = XMLoadFloat3((XMFLOAT3*)vec);
	XMVECTOR v1, v2, v3;
	XMVECTOR n;
	XMVECTOR l1, l2, l3;
	XMVECTOR temp;
	XMVECTOR cp;
	XMVECTOR p1, p2, p3;
	float neart = *dist;

	for (const auto& it : faces)
	{
		//　面頂点取得
		v1 = XMLoadFloat3(&it.pos[0]);
		v2 = XMLoadFloat3(&it.pos[1]);
		v3 = XMLoadFloat3(&it.pos[2]);
		//  ３辺算出
		l1 = v2 - v1;
		l2 = v3 - v2;
		l3 = v1 - v3;

		//	外積による法線算出		
		n = XMVector3Cross(l1, l2);

		//	内積の結果がプラスならば裏向き
		XMVECTOR vdot = XMVector3Dot(vv, n);
		float fdot;
		XMStoreFloat(&fdot, vdot);
		if (fdot >= 0) continue;

		//	交点算出
		p1 = v1 - p;
		XMVECTOR vt = XMVector3Dot(n, p1) / fdot;
		float ft;
		XMStoreFloat(&ft, vt);
		if (ft < .0f || ft > neart) continue;
		cp = vv*ft + p;

		//  内点判定
		p1 = v1 - cp;
		temp = XMVector3Cross(p1, l1);
		XMVECTOR work = XMVector3Dot(temp, n);
		float fwork;
		XMStoreFloat(&fwork, work);
		if (fwork < 0.0f) continue;

		p2 = v2 - cp;
		temp = XMVector3Cross(p2, l2);
		work = XMVector3Dot(temp, n);
		XMStoreFloat(&fwork, work);
		if (fwork < 0.0f) continue;


		p3 = v3 - cp;
		temp = XMVector3Cross(p3, l3);
		work = XMVector3Dot(temp, n);
		XMStoreFloat(&fwork, work);
		if (fwork < 0.0f) continue;

		//情報保存
		XMStoreFloat3((XMFLOAT3*)out, cp);
		XMStoreFloat3((XMFLOAT3*)vec, n);
		ret = it.material;
		neart = ft;
	}
	//最も近いヒット位置
	*dist = neart;
	return ret;
}



