#include "Coin.h"
#include "Constant.h"


Coin::Coin()
{
}

enum 
{
	ANGLE_ANIME,
	ERASING_ANIME,
};
Coin::~Coin()
{
	//coin.reset(NULL);
	SAFE_DELETE(coin);
	SAFE_DELETE(shader);
}

void Coin::Initialize(Vector3 pos)
{
	//モデルの初期化
	coin = new Skin_Mesh("DATA/coin/coin.fbx");
	eff2 = std::make_unique<Particles>("DATA/eff.fbx", 20);
	p = pos;
	coin->SetPos(p);
	coin->SetScalse(Vector3(COIN_SCALE, COIN_SCALE, COIN_SCALE));
	coin->SetAngle(Vector3(ZERO_CLEARF, COIN_SCALE, ZERO_CLEARF));
	coin->Update();
	coin->setMotion(ANGLE_ANIME);
	shader = new Shader();
	erase = false;
#if SHADER_CHANGE
	shader->Create("DATA/Shader/test01_vs.cso", "DATA/Shader/test01_ps.cso");
#else
	shader->Create("DATA/Shader/Toon_vs.cso", "DATA/Shader/Toon_ps.cso", "DATA/Shader/Toon_gs.cso");
#endif 
}


void Coin::Update(Player & player, Vector3 & local, Vector3 & localangle, int & coin_count)
{
	
	static int timer;
	if(player.Get_Pos().z<p.z- DELETE_X&&coin!=NULL)
	{
		SAFE_DELETE(coin);
		//Reset(c);
	}

	
	eff2->update();
	if (coin != NULL)
	{

		static int add = 1;
		//正面当たり判定
		int cret = coin->checkFrontward(player.obj->GetPos(), local, player.Get_Angle(), localangle);
		//左当たり判定
		int cl = coin->checkLeft(player.obj->GetPos(), local, player.Get_Angle(), localangle);
		//右当たり判定
		int cr = coin->checkRight(player.obj->GetPos(), local, player.Get_Angle(), localangle);

		//左側にあった場合
		if (cl >= 0&& coin != NULL&&erase == false)
		{
			A_PlaySound(SE::COIN, false);

			coin_count += add;
			erase = true;
			timer = TIME25;
			coin->setMotion(ERASING_ANIME);
			player.Set_Move_Flag(false);
		}
		//右側に当たった場合
		else if (cr >= 0 && coin != NULL&&erase == false)
		{
			A_PlaySound(SE::COIN, false);

			coin_count += add;
			erase = true;
			timer = TIME25;
			coin->setMotion(ERASING_ANIME);
			player.Set_Move_Flag(false);
		}
		//正面に当たった場合
		else if (cret >= 0 && coin != NULL&&erase == false)
		{
			A_PlaySound(SE::COIN, false);
			coin_count +=add;
			timer = TIME25;
			erase = true;
			coin->setMotion(ERASING_ANIME);
			player.Set_Move_Flag(false);
		}

	}

	if (erase == true)
	{
		eff2->coin(p + Vector3(ZERO_CLEARF, COIN_ANIME_POS_Y, -COIN_ANIME_POS_Z));
		--timer;
		if(timer<ZERO_CLEAR)
		{
		
			SAFE_DELETE(coin);
			timer = ZERO_CLEAR;
			erase = false;
		}
	}

}

void Coin::Render(View * view)
{
	
	if (coin != NULL)
	{
#if SHADER_CHANGE
		coin->SetPos(p);
		coin->SetScalse(Vector3(COIN_SCALE, COIN_SCALE, COIN_SCALE));
		coin->Update();
		coin->Render(shader, view->GetView(), view->GetProjection(), light_direction, material_color, framework::RS_FRONT, 1.5f);
#else
		coin->SetPos(p);
		coin->SetScalse(Vector3(COIN_LINE_SCALE, COIN_LINE_SCALE, COIN_LINE_SCALE));
		coin->Update();
		coin->Render(shader, view->GetView(), view->GetProjection(), light_direction, black_color, framework::RS_SOLID,1.5f);

		coin->SetPos(p);
		coin->SetScalse(Vector3(COIN_SCALE, COIN_SCALE, COIN_SCALE));
		coin->Update();
		coin->Render(shader, view->GetView(), view->GetProjection(), light_direction, material_color, framework::RS_FRONT,1.5f);

#endif 
	}
	
	eff2->render(view->GetView(), view->GetProjection());
}


