#pragma once
#include "OBJ_Mesh.h"
#include "A_System.h"
#include "Player.h"
#include <sstream>
#include <memory>
#include "loadtext\loadtext.h"

#include"Math\math.h"


class Skin_Mesh;
class Coin
{
	Shader* shader;
	bool erase;
	Vector3 p;
public:
	Skin_Mesh* coin;
	Coin();
	~Coin();
	std::unique_ptr<Particles> eff2;
	void Initialize(Vector3 p);
	void Update(Player& p, Vector3& local, Vector3&localangle, int& coin_count);//1:プレイヤー位置,2:ローカル座標,3ローカルアングル,4コイン枚数
	void Render(View* v);
	
};

