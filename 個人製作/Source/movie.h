#pragma once
#include "Scene.h"
#include "Sprite.h"
#include "FrameWork.h"

#include <list>
#include <memory>

class IGraphBuilder;
class IMediaControl;
class IMediaEvent;
class IVideoWindow;
class IMediaPosition;

class movie
{
	HWND     hNotifyWnd;        // DirectShowライブラリからのメッセージ送信先ウィンドウ
	UINT     nGraphNotifyMsg;   // 通知メッセージ

	IGraphBuilder *pGraphBuilder;
	IMediaControl *pMediaControl;
	IMediaEvent *pMediaEvent;
	IVideoWindow *pVideoWindow;
	IMediaPosition*  pMediaPosition;
	HRESULT hr;

	long eventCode;
	bool flag;
public:
	void init(const wchar_t *file_name);
	void update();
	movie();
	~movie();
};

