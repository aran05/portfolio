#pragma once
#include <Windows.h>



// デバック時のみ停止
#ifdef _DEBUG
#define ASSERT( str )  \
	{	\
		static bool execution = true; \
		if( execution ) \
		{ \
			int ret = MyAssert( str ); \
			if( ret == 5 ) \
			{ \
				execution = false; \
			} \
			else if( ret == IDABORT ) \
			{ \
				*LPLONG(0xcdcdcdcd) =0; \
			} \
		} \
	}

#else
#define ASSERT( str ) {}
#endif

// HRがエラーだったら止まる
#ifdef _DEBUG
#define ASSERT_HR( str, hr )  \
	if( FAILED(hr) ) \
	{	\
		static bool execution = true; \
		if( execution ) \
		{ \
			int ret = MyAssertHR( str, hr ); \
			if( ret == 5 ) \
			{ \
				execution = false; \
			} \
			else if( ret == IDABORT ) \
			{ \
				*LPLONG(0xcdcdcdcd) =0; \
			} \
		} \
	}

#else
#define ASSERT_HR( str, hr ) { hr=hr;}
#endif

// 強制停止
#define ASSERT2( str )  \
	{	\
		static bool execution = true; \
		if( execution ) \
		{ \
			int ret = MyAssert( str ); \
			if( ret == 5 ) \
			{ \
				execution = false; \
			} \
			else if( ret == IDABORT ) \
			{\
				*((char *) 0xcdcdcdcd) = 0; \
			}\
		} \
	}
		
bool DebugPrintf( const char *str, ...); 

// 使わない
int MyAssert( char* str );
int MyAssertHR( char* str, HRESULT hr );