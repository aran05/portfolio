#pragma once
#include <DirectXMath.h>
using namespace DirectX;

struct POINTLIGHT
{
	XMFLOAT4 index_range_type;//光の届く範囲と有効か無効か
	XMFLOAT4 pos;
	XMFLOAT4 color;
};

class Light
{
private:

	
public:
	static const int POINTMAX = 32;
	static XMFLOAT4 LightDir;
	static XMFLOAT4 DirLightColor;
	static XMFLOAT4 EyePos;
	static XMFLOAT4 Ambient;
	static POINTLIGHT PointLight[POINTMAX];
	static XMFLOAT4 Edge;
	static XMFLOAT4X4 ShadowViewProjection;
	Light();
	~Light();
	static void SetDirLight(Vector3 dir, Vector3 color);
	static void SetAmbient(Vector3 amb);
	static void SetPointLight(int index, Vector3 pos, Vector3 color, float range);
	ID3D11Buffer* ConstantBuffer;
	

};



