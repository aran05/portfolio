#include "A_System.h"
#include "Math\math.h"
#include "Light.h"
#include "FrameWork.h"
XMFLOAT4 Light::LightDir;
XMFLOAT4 Light::DirLightColor;
XMFLOAT4 Light::Ambient;
XMFLOAT4 Light::EyePos;
//POINTLIGHT Light::PointLight[Light::POINTMAX] = {};
XMFLOAT4 Light::Edge;
XMFLOAT4X4 Light::ShadowViewProjection;

Light::Light()
{
	
}


Light::~Light()
{
}


void Light::SetDirLight(Vector3 dir, Vector3 color)
{
	LightDir = { dir.x,dir.y,dir.z,0 };
	DirLightColor = { color.x,color.y,color.z,0 };
	
}

void Light::SetAmbient(Vector3  amb)
{
	Ambient = { amb.x,amb.y,amb.z,0 };

}

void Light::SetPointLight(int index, Vector3  pos, Vector3  color, float range)
{
	if (index < 0) return;
	if (index >= POINTMAX) return;
	//PointLight[index].index_range_type = XMFLOAT4(index, range, 1.0f, 0.0f);
	//PointLight[index].pos = XMFLOAT4(pos.x, pos.y, pos.z, 0.0f);
	//PointLight[index].color = XMFLOAT4(color.x, color.y, color.z, 0.0f);

}

//bool light::createconstantbuffer()
//{
//	 定数バッファ生成
//	d3d11_buffer_desc bd;
//	zeromemory(&bd, sizeof(d3d11_buffer_desc));
//	bd.bytewidth = sizeof(constantbufferforperframe);
//	bd.usage = d3d11_usage_default;
//	bd.bindflags = d3d11_bind_constant_buffer;
//	bd.cpuaccessflags = 0;
//
//	hresult hr = framework::device.get()->createbuffer(&bd, null, &constantbuffer);
//	if (failed(hr))
//	{
//		assert(false && "id3d11device::createbuffer() failed.");
//		return false;
//	}
//	return true;
//
//}
//bool light::createconstantbuffer(id3d11buffer ** ppcb, u_int size)
//{
//	 定数バッファ生成
//	d3d11_buffer_desc bd;
//	zeromemory(&bd, sizeof(d3d11_buffer_desc));
//	bd.bytewidth = size;
//	bd.usage = d3d11_usage_default;
//	bd.bindflags = d3d11_bind_constant_buffer;
//	bd.cpuaccessflags = 0;
//
//	hresult hr = framework::device.get()->createbuffer(&bd, null, ppcb);
//	if (failed(hr))
//	{
//		assert(false && "id3d11device::createbuffer() failed.");
//		return false;
//	}
//	return true;
//}
//void Light::Light_Uadate()
//{
//
//	ID3D11RenderTargetView* backbuffer = framework::render_target_view.Get();
//	framework::device_context.Get()->OMSetRenderTargets(1, &backbuffer, framework::depth_stencil_view.Get());
//	framework::device_context.Get()->OMSetDepthStencilState(framework::GetDephtStencilState(framework::DS_FALSE).Get(), 1);
//
//}


