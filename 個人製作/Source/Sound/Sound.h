#pragma once

//*****************************************************************************
//
//		オーディオ関連
//
//*****************************************************************************
#include	<windows.h>
#include	<dsound.h>
#include "..\ogg\vorbisfile.h"
#include "Math\math.h"

#define _CRT_SECURE_NO_WARNINGS
#define	_CRT_SECURE_NO_DEPRECATE

#include "Define.h"


#define	STR_NORMAL	0
#define	STR_FADEIN	1
#define	STR_FADEOUT	2

#define	TYPE_WAV	0
#define	TYPE_OGG	1

// A_Sound.cppに処理を実装 //

class SoundBuffer
{
private:
	LPBYTE LoadWAV(LPSTR fname, LPDWORD size, LPWAVEFORMATEX wfx);
	LPDIRECTSOUNDBUFFER		lpBuf;
	LPDIRECTSOUND3DBUFFER	lpBuf3D;

public:
	SoundBuffer(LPDIRECTSOUND lpDS, char* filename, bool b3D);
	~SoundBuffer();

	LPDIRECTSOUNDBUFFER GetBuf() { return lpBuf; }
	void Play(BOOL loop);
	void Stop();

	void	SetVolume(int volume);
	BOOL	isPlay();

	void	SetPos(Vector3* pos);
};

class A_StreamSound
{
private:
	static const int STRSECOND = 1;
	int		rate;
	LPDIRECTSOUNDBUFFER	lpStream;		// ストリーム用二次バッファ
	DSBPOSITIONNOTIFY	pn[3];

	HANDLE	hStrThread;
	u32	dwThreadId;
	u32	dwThrdParam;

	FILE*	hStrFile;
	OggVorbis_File	vf;
	u32	StrSize;
	u32	StrPos;
	u32	LoopPtr;

	u8		mode;
	s32		param;
	s32		volume;

	void Initialize(LPDIRECTSOUND lpDS, int rate);

	BOOL	OGGRead(LPBYTE dst, unsigned long size);

public:
	BYTE	type;
	HANDLE	hEvent[3];
	LPDIRECTSOUNDNOTIFY	lpStrNotify;


	A_StreamSound(LPDIRECTSOUND lpDS, LPSTR filename, BYTE mode, int param);
	~A_StreamSound();

	BOOL	SetBlockOGG(int block);
	BOOL	SetBlockWAV(int block);

	BOOL	SetWAV(LPDIRECTSOUND lpDS, char* filename);
	BOOL	SetOGG(LPDIRECTSOUND lpDS, char* filename);

	void	Stop();
	void	SetVolume(int volume);
	int		GetVolume() { return volume; }

	void	SetMode(BYTE mode, int param);
	u8		GetMode() { return mode; }
	int		GetParam() { return param; }

};

typedef A_StreamSound DSSTREAM, *LPDSSTREAM;

class A_Sound
{
private:
	static const int WavNum = 128;

	HWND	hWndWAV;

	LPDIRECTSOUND8			lpDS;		// DirectSoundオブジェクト
	LPDIRECTSOUNDBUFFER	lpPrimary;		// 一次バッファ

	LPDIRECTSOUND3DLISTENER lp3DListener;

	SoundBuffer* buffer[WavNum];

public:
	A_Sound();
	~A_Sound();

	void Set(int no, char* filename, bool b3D = false);
	void SetPos(int no, Vector3* pos) { buffer[no]->SetPos(pos); }
	void Play(int no, BOOL loop = FALSE);
	void Stop(int no);

	void SetListener(Vector3* pos, Vector3* target);


	void	SetVolume(int no, int volume);
	BOOL	isPlay(int no);

	A_StreamSound* PlayStream(char* filename);
	A_StreamSound* PlayStream(char* filename, BYTE mode, int param);

};


//	サウンドシステム初期化・終了
BOOL	A_InitAudio();
void	A_ReleaseAudio();
//	サウンドセット・再生・停止	
BOOL	A_SetWAV(s32 no, LPSTR fname, bool b3D = false);
void	A_PlaySound(s32 no, BOOL loop);
void	A_StopSound(s32 no);
//	情報設定・取得
BOOL	A_GetSoundStatus(s32 no);
void	A_SetSoundVolume(s32 no, s32 volume);

//	再生・停止・解放	
LPDSSTREAM	A_PlayStreamSound(LPSTR filename);
LPDSSTREAM	A_PlayStreamSoundEx(LPSTR filename, u8 mode, s32 param);
BOOL		A_StopStreamSound(LPDSSTREAM lpStream);
BOOL		A_ReleaseStreamSound(LPDSSTREAM lpStream);

//	情報設定
void	A_SetStreamSoundVolume(LPDSSTREAM lpStream, s32 volume);
void	A_SetStreamMode(LPDSSTREAM lpStream, u8 mode, s32 param);