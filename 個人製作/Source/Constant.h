#pragma once
#include "d2d1.h"
#include<cassert>
#include<DirectXMath.h>
#include "Math\math.h"

//シェイダーの切り替え
#define SHADER_CHANGE  0

//定数 一覧
/**************スコア加算**********************/
constexpr int COIN_ADD = 10;
constexpr int SCORE_ADD = 10;
/********************************************/


/***************ステージ**********************/
constexpr int STAGE_MAX = 8;//ステージ:最大値
constexpr int STAGE_MIN = 0;//ステージ:最小値
constexpr int STAGE_X = 3;	//ステージ:X
constexpr float STAGE_SCALE= 1.0f;//ステージ:スケール
constexpr float STAGE_LINE_SCALE = 1.001f;//ステージ：ラインスケール
/********************************************/



/***************コイン**********************/
constexpr int COIN_MAX = 50;//コイン:最大値
constexpr int COIN_MIN = 0;//コイン:最小値
constexpr float COIN_SCALE = 3.0f;//コイン:スケール
constexpr float COIN_LINE_SCALE = 3.2f;//コイン：ラインスケール
constexpr float COIN_ANGLE_Y = 3.0f;//コイン:アングルY
constexpr float DELETE_X = 10;//補正値
constexpr float COIN_ANIME_POS_Y =  8;//コイン:アニメY
constexpr float COIN_ANIME_POS_Z = 5;//コイン:アニメZ
/*******************************************/


/***************CSVデータ用****************/
constexpr int DATA_MAX = 8;
constexpr int FONT_SIZE = 32;
constexpr int DATA = 4;
/******************************************/


/****************プレイヤー*****************/
const Vector3 START_POS = Vector3(0, 0, 100);
const Vector3 START_MOVE = Vector3(1.0f, 0, 0.1f);
const Vector3 START_ANGLE = Vector3(0, 3, 0);


//ジャンプ力
constexpr float JUMP_POW = 2.0f;

//Xの制限距離
constexpr float MAX_X = 8.0f;
constexpr float MIN_X = -8.0f;

//Yの制限距離
constexpr float MAX_Y= 15.0f;


//移動量
constexpr float  MOVE_X = 0.2f;
constexpr float  MOVE_Y = 0.8f;
constexpr float  MOVE_Z = 0.5f;


//回転
constexpr float MAX_ANGLE_Y = 3.0f;
constexpr float ANGLE_SPEED = 2.0f;


constexpr float PLYAER_SCALE = 0.04f;//スケール

constexpr float PLYAER_LINE_SCALE = 0.041f;//ラインスケール
constexpr float PLAYER_ANIME_SPEED = 0.5f;//アニメーションスピード
/**********************************************/


/****************カメラ***********************/
const Vector3 VIEW_POS = Vector3(0.0f, 12.0f, 120.0f);
const Vector3 VIEW_UPOS = Vector3(0.0f, 1.0f, 0.0f);//上方向ベクトル
const Vector3 VIEW_TPOS = Vector3(0.0f, 1.0f, 0.0f);//ターゲットポジション
constexpr float C_Z20=20.0f;
constexpr float C_Y15 = 15.0f;
constexpr float C_Y10 = 10.0f;
constexpr float C_NEAR = 0.1f;
constexpr float C_FAR  = 1000.0f;

/*********************************************/
/****************スコア***********************/

constexpr int RH  = 120;//1位:縦幅
constexpr int RH2 = 230;//2位:縦幅
constexpr int RH3 = 330;//3位:縦幅
constexpr int SORT = 6 - 1;
constexpr int NUM_0 = 0;//0
constexpr int NUM_1 = 1;//1
constexpr int NUM_2 = 2;//2
/**********************************************/

/****************文字サイズ&位置*************/
constexpr int W = 1024;//横幅
constexpr int H = 512;//縦幅
constexpr int SIZE_100 =100;
constexpr int SIZE_96 = 96;
constexpr int SIZE_93 = 93;
constexpr int SIZE_92 = 92;
constexpr int SIZE_90 = 90;

//sceneclear
constexpr int SC_POS_X = 400;
constexpr int SC_POS_Y = 500;
constexpr int GS_POS_X = 64;
constexpr int GS_POS_Y = 128;
constexpr int GS_SIZE  = 32;
////////////////////////////

//sceneTitle
constexpr int ST_POS_X2 = 800;
constexpr int ST_POS_X = 120;
constexpr int ST_POS_Y = 100;

/***************************************/



/***************************カラーバリエーション****************************************/
const D2D1::ColorF white = D2D1::ColorF::White;//白
const D2D1::ColorF yellow = D2D1::ColorF::Yellow;//黄色
const D2D1::ColorF blue = D2D1::ColorF::Blue;//青

const DirectX::XMFLOAT4 light_direction = DirectX::XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
const DirectX::XMFLOAT4 material_color = DirectX::XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
const DirectX::XMFLOAT4 black_color = DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);//黒

const DirectX::XMFLOAT4 RED = DirectX::XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);//赤
const DirectX::XMFLOAT4 RED2 = DirectX::XMFLOAT4(0.8f, 0.4f, 0.4f, 1.0f);//赤
const DirectX::XMFLOAT4 BLUE = DirectX::XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);//青
const DirectX::XMFLOAT4 GREEN = DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);//黄色
const DirectX::XMFLOAT4 GREEN2 = DirectX::XMFLOAT4(0.4f, 0.8f, 0.4f, 1.0f);
const DirectX::XMFLOAT4 LIGHT_BLUE = DirectX::XMFLOAT4(0.1f, 0.5f, 0.8f, 1.0f);//水色


const Vector3 AMBIENT_COLOR = Vector3(0.8f, 0.8f, 0.8f);//アンビエントカラー
const Vector3 LIGHT_COLOR = Vector3(0.8f, 0.8f, 0.8f);//ライトカラー


/****************************************************************************************/

/**************タイマー****************************/
constexpr int SECOND  = 60;//1秒
constexpr int SECOND2 = SECOND*2;//2秒
constexpr int SECOND3 = SECOND * 3;//2秒

constexpr int TIME25 = 25;
/***************************************************/


//ゼロの値を入れる
constexpr float ZERO_CLEARF = 0.0f;
constexpr int ZERO_CLEAR = 0;
const Vector3 ZERO_CLEARV = Vector3(0.0f, 0.0f, 0.0f);




/*パーティクル*/
constexpr int PARTICLE60 = 60; //描画するマックス値



/*************SE**************/
constexpr int BGM_VOLUME = 100;
enum SE
{
	JUMP,
	COIN,


};
/************SE****************/